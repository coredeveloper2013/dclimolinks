Timepicker for Twitter Bootstrap 2.x
------------------------------------

A simple timepicker component for Twitter Bootstrap.

Documentation
=============

Read the <a href="//jdewit.github.com/bootstrap-timepicker">documentation</a>.
