<?php
include_once 'session_auth.php';

$_page = 'elc';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Extra luggage Cost settings</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>

    <!--dynamic table-->
    <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet"/>
    <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css"/>

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/limo_car.css" rel="stylesheet">
    <link href="css/bootstrap-multiselect.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" media="screen" href="css/timepicker/bootstrap-datetimepicker.min.css"/>
    <script src="js/lib/jquery.js"></script>
    <script src="pageJs/validation.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="JQ_timepicker/jquery_addon_css.css">
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
        ul.vehicle-list {
            padding: 0;
            margin: 0;
            list-style: none;
            margin-top: 15px
        }

        ul.vehicle-list li a {
            display: block;
            border-bottom: 1px dotted #CFCFCF;
            padding: 10px;
            text-decoration: none
        }

        ul.vehicle-list li:first-child a {
            border-top: 1px dotted #CFCFCF
        }

        ul.vehicle-list li a:hover {
            background-color: #DBF9FF
        }

        ul.vehicle-list li span {
            display: block
        }

        ul.vehicle-list li span.vehicle-name {
            font-weight: 600;
            color: #8F8F8F
        }

        ul.vehicle-list li span.vehicle-tier-count {
            color: #8C8C8C
        }

        .top-links a {
            font-size: 12px
        }

        .top-links {
            text-align: right
        }

        div.rate-group-selector {
            display: inline-block
        }

        div.vehicle-list .panel-default > .panel-heading {
            background-color: transparent;
            padding: 15px
        }

        div.vehicle-list .panel-default {
            border-color: transparent
        }

        div.vehicle-list .panel {
            border: none;
            box-shadow: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            -o-box-shadow-none: none
        }

        #vehicle-list h4.panel-title {
            font-style: normal;
            font-size: 14px
        }

        div.vehicle-list .panel-group .panel + .panel {
            margin-top: 0px
        }

        .input-xs {
            height: 22px;
            padding: 5px 5px;
            font-size: 12px;
            line-height: 1.5;
            border-radius: 3px
        }

        div.vehicle-list .table thead th {
            padding-bottom: 5px !important;
            text-transform: none;
            color: #898989;
            font-weight: normal
        }

        div.vehicle-list .table > tbody > tr > td, div.vehicle-list .table thead {
            border-top: none
        }

        .dropdown-menu {
            /* max-height: 138px !important;*/

            z-index: 9999;
        }

        div.vehicle-list .table-condensed > tbody > tr > td {
            padding: 2px
        }

        .table-action {
            text-align: right;
            font-size: 14px
        }

        .editable-text {
            width: 30%
        }

        .set { /* position:relative; *//* width:100%; *//* height:auto; *//* background-color:#f5f5f5 */
        }

        .set > a {
            display: block;
            padding: 20px 15px;
            text-decoration: none;
            color: #555;
            font-weight: 600;
            border-bottom: 1px solid #ddd;
            -webkit-transition: all 0.2s linear;
            -moz-transition: all 0.2s linear;
            transition: all 0.2s linear
        }

        .set > a:hover {
            background-color: cornsilk
        }

        .set > a i {
            position: relative;
            float: right;
            margin-top: 4px;
            color: #666
        }

        .set > a.active {
            background-color: #1fb5ad;
            color: #fff
        }

        .set > a.active i {
            color: #fff
        }

        .acc-content {
            position: relative;
            width: 100%;
            height: auto;
            background-color: #fff;
        }

        .form-control {
            color: black;
        }

        .serviceType_css {
            margin-top: 8%;
        }

        .serviceRate_css {
            margin-top: 3%;
        }

        #add_point_rate_to option {
            max-height: 10px !important;
        }

        select[multiple] {
            height: 35px;
        }

        .form-control1 {
            color: black;
        }

        .loadingGIF {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width: 100%;
            padding-left: 30%;
            padding-top: 10%;
        }

        .alignCENTER {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }

        .functionTITLE {
            font-size: 16px;
            font-weight: 800;
        }

        .set_rate th {
            padding: 1px 29px 1px 1px !important;
        }

        .add_rate_check {
            margin-left: 24% !important;
        }

        #vehicl_rate_name td {
            margin-bottom: 7% !important;
        }

        .per_pax_holiday {
            display: none;
        }


    </style>
</head>

<body>
<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-title alignCENTER"> Extra luggage cost setup </h4>
                </div>
            </div>
            <div id="vue_app" class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-info" style="display:table;width: 100%;">
                        <!-- form start -->
                            <div class="col-md-6">
                                <h4 style="text-align: center; color: #0a0a0a; margin-bottom: 40px;">Free of cost combination</h4>
                                <div class="row">

                                    <div class="col-xs-3" style="text-align:center;"><b>Large<br></b></div>
                                    <div class="col-xs-3" style="text-align:center;"><b>Medium</b></div>
                                    <div class="col-xs-3" style="text-align:center;"><b>Small</b></div>
                                    <div class="col-xs-3" style="text-align:center;">&nbsp;</div>

                                </div>

                                <div class="row" v-for="(item , index) in combinations" style="margin-bottom: 8px">

                                    <div class="col-xs-3" >
                                        <input type="number" min="0" v-model="item.large" class="form-control">
                                    </div>
                                    <div class="col-xs-3" >
                                        <input type="number" min="0" v-model="item.medium" class="form-control">
                                    </div>
                                    <div class="col-xs-3" >
                                        <input type="number" min="0" v-model="item.small" class="form-control">
                                    </div>
                                    <div class="col-xs-3" @click.prevent="remove(index)" style="width:8%;cursor:pointer; margin-left:10px" id="sma_state_check">
                                        <img src="images/remove.png" alt="" width="50%" class="chkmrk-icon" style="margin-top: 5px">
                                    </div>


                                </div>


                                <div class="col-xs-12 " style="margin-top:30px;">
                                    <button type="button" class="btn btn-primary" @click.prevent="add" style="width:20%"> Add Combination</button>
                                </div>
                            </div>
                        <div class="col-md-6">
                            <h4 style="text-align: center; color: #0a0a0a; margin-bottom: 40px;">Surcharge Setup</h4>
                            <div class="row">
                                <div class="col-md-4" style="font-weight: bold">
                                    Luggage Size
                                </div>
                                <div class="col-md-6" style="font-weight: bold">
                                    Surcharge per additional item
                                </div>
                                <br>
                                <br>
                                <div class="col-md-4">
                                    Large
                                </div>
                                <div class="col-md-6"><span>$</span><input class="form-control" min="0"  style="max-width: 85px; display: inline" type="number" v-model="extra_price.large"></div>
                                <div class="col-md-4">
                                    Medium
                                </div>
                                <div class="col-md-6"><span>$</span><input class="form-control" min="0"  style="max-width: 85px; display: inline" type="number" v-model="extra_price.medium"></div>
                                <div class="col-md-4">
                                    Small
                                </div>
                                <div class="col-md-6"><span>$</span><input class="form-control" min="0"  style="max-width: 85px; display: inline" type="number" v-model="extra_price.small"></div>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <h4 style="text-align: center; color: #0a0a0a; margin-bottom: 40px;">Disclaimer Message</h4>
                            <div class="row">
                                <textarea v-model="disclaimer" class="form-control" rows="8" ></textarea>


                            </div>
                        </div>




                            <br/>
                            <br/>
                            <div class="col-xs-12 " style="text-align:center; margin-top:30px;">
                                <button type="button" id="save_rate" class="btn btn-primary" @click.prevent="save_combination" style="width:10%"> Save</button>
                            </div>

                    </div>
                </div>
            </div>
            <!-- ############################## additional rate####################################################-->

            <!--##################finish###########################-->

            <!-- page end-->
        </section>
    </section>

    <!--main content end--> <!-- Right Side Bar Goes Here if Required
<div class="right-sidebar">
<div class="right-stat-bar"> </div>
</div> --></section>

<!-- Third view popup start here -->
<!-- popup code start here -->


<!--Third view popup End here  -->

<!-- Star add rate in vehicle -->


<!-- Loading Indicator Ends-->
<script src="pageJs/dashboard.js"></script>
<script src="pageJs/logout.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="js/moment.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="assets/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="pageJs/searchbox.js"></script>
<script src="pageJs/holidays.js"></script>
<!--<script src="assets/flot-chart/jquery.flot.js"></script>
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="assets/flot-chart/jquery.flot.resize.js"></script>
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script> -->

<!--dynamic table-->
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>

<script>
    var app = new Vue({
        el: '#vue_app',
        data: {
            combinations : [],
            extra_price  : {
                large : 0,
                medium: 0,
                small : 0
            },
            disclaimer : ''
        },
        methods : {
            add(){
                this.combinations.push({
                    large : 0,
                    medium : 0,
                    small : 0
                })
            },
            remove(index){
                this.combinations.splice(index , 1);
            },
            save_combination(){
                console.log(JSON.stringify(this.combinations));

                var url =  location.href.replace('extra_luggage_cost.php' , 'phpfile/extra_luggage_server.php')

                $.ajax({
                    type: 'post',
                    url: url,
                    data: {
                        action : 'saveInfo',
                        combinations: JSON.stringify(this.combinations)
                    },
                    success: function (response) {
                        //console.log(response);
                    }
                });

                $.ajax({
                    type: 'post',
                    url: url,
                    data: {
                        action : 'saveDisclaimer',
                        disclaimer: this.disclaimer
                    },
                    success: function (response) {
                        //console.log(response);
                    }
                });

                $.ajax({
                    type: 'post',
                    url: url,
                    data: {
                        action : 'savePrice',
                        extra_price: JSON.stringify(this.extra_price)
                    },
                    success: function (response) {
                        //console.log(response);
                        location.reload();
                    }
                });
            },
            getInfo(){
                var url =  location.href.replace('extra_luggage_cost.php' , 'phpfile/extra_luggage_server.php')
                var SELF = this;
                $.ajax({
                    type: 'post',
                    url: url,
                    data: {
                        action : 'getInfo'
                    },
                    success: function (response) {

                        var result = JSON.parse(response);
                        result.data.forEach(function (item) {
                            if(item.type === 'combinations'){
                                SELF.combinations = JSON.parse(item.content);
                            }
                            if(item.type === 'extra_price'){
                                SELF.extra_price = JSON.parse(item.content);
                            }

                            if(item.type === 'disclaimer'){
                                SELF.disclaimer = item.content;
                            }
                        })
                    }
                });
            }
        },

        created(){
            this.getInfo();
        }
    })
</script>

<!--dynamic table initialization -->
<script src="js/dynamic_table/dynamic_table_init.js"></script>

<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="JQ_timepicker/jquery_addon_js.js"></script>
<script src="JQ_timepicker/slideaccess.js"></script>
<script>
    $("table.sieve").sieve();
    $(function () {


        window.localStorage.removeItem("rateSetupValue");


        $("#calender").datepicker();
        $("#calender_end").datepicker();
        $('#start_time, #end_time').timepicker({timeFormat: "hh:mm tt"});


        $('#add_vehicle_rate').on("click", function () {

            $('#add_rate_modal').show();

        });


        $('#close_rate_postal_popup').on("click", function () {


            $('#add_rate_modal').hide();

        });

        $('#close_rate_vehicle_rate_popup').on("click", function () {


            $('#view_rate_vehicle_modal').hide();

        })


    });


    setTimeout(function () {

            //getState(cityAndState);
            $("#apply_vehicle_type").multiselect('destroy');
            $('#apply_vehicle_type').multiselect({
                maxHeight: 200,
                buttonWidth: '155px',
                includeSelectAllOption: true
            });

            $("#apply_sma").multiselect('destroy');
            $('#apply_sma').multiselect({
                maxHeight: 200,
                buttonWidth: '155px',
                includeSelectAllOption: true
            });
        }
        , 900);


</script>

<!--<script src="pageJs/dashboard.js"></script>
<script src="pageJs/getairports.js"></script>
<script src="pageJs/getState.js"></script>
<script src="pageJs/getSeaport.js"></script>
<script src="pageJs/getStatePopup.js"></script>
<script src="pageJs/getVehicle.js"></script>
<script src="pageJs/getService.js"></script>
<script src="pageJs/peakhour.js"></script>
<script src="pageJs/extrachild.js"></script> -->

</body>
</html>
