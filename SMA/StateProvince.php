<?php
include_once 'session_auth.php';

$_page = 'states';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | State/Province Setup</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet">
    <style>
        .close_popup {
            width: 16px;
            margin-left: 97%;
            margin-top: -10%;
            cursor: pointer;
            height: 16px;
        }

        .selectBox {
            border: 1px solid #e2e2e4;
            box-shadow: none;
            color: #848181;
            width: 193px;
            height: 37px;
            border-radius: 3px;
            margin-bottom: 18px;
        }

        .btn-default {
            background-color: #1FB5AD;
            border-color: #1FB5AD;
            color: #fff;
        }

        .btn-default:hover {
            background-color: #2CA7A0;
            border-color: #2CA7A0;
            color: #fff;
        }

        .loadingGIF {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width: 100%;
            padding-left: 30%;
            padding-top: 10%;
        }

        .alignCENTER {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
    </style>
</head>

<body>
<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <div class="col-sm-12">
                            <header class="alignCENTER page-header ">
                                <h4>State/Province Setup</h4>
                            </header>
                        </div>
                        <div class="col-sm-6" style="margin:4px 0px 2px 0px;">
                            <div class="top-links"><a class="btn btn-default popup_edit_city  " seq="SED"
                                                      style="float:right;padding: 1%;"> <i class="fa fa-plus"></i> Add
                                State/Province</a></div>
                        </div>
                        <div id="pad-wrapper">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 class="page-title alignCENTER">State/Province List</h4>
                                    <table class="table table-condensed ng-scope sieve">
                                        <thead>
                                        <tr>
                                            <th>State/Province ID</th>
                                            <th><span class="line"></span> State/Province Name</th>
                                            <th><span class="line"></span>State/Province Code</th>
                                            <th><span class="line">Action</span></th>
                                        </tr>
                                        </thead>
                                        <tbody class="sortable-table ui-sortable" id="StatesDetails">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <!-- page end-->
        </section>
    </section>
    <!--main content end--></section>
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display:none;"
     id="view_rate_vehicle_modal">
    <div style="position:relative; background:black; opacity:0.5; top:0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:-17%; width:136%; height:100%; margin-left:auto; margin-right:auto;">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%;display:table-row">
                <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; "
                         id="asseriesUpdateOverlay">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div style="width:80%">
                                <!--  code of popup start-->
                                <div class="container">
                                    <div id="modal" class="popupContainer"
                                         style="display:block;  top: 23px; position: absolute; width: 26%; border-radius: 4px; box-shadow: 0px 10px 20px rgba(53, 51, 51, 0.84); left: 39%; top: 30px; background: #FFF;">
                                        <header class="popupHeader" style="  background: #F4F4F2; position: relative;padding: 10px 20px 0px 20px; border-bottom: 1px solid #DDD;font-weight: bold;font-family: 'Source Sans Pro', sans-serif;font-size: 16px;color: #666;text-transform: capitalize; text-align: center;
 height:9%; margin-top:2px;"><span class="header_title">Add State/Province</span> <img src="images/remove.png"
                                                                                       alt="delete"
                                                                                       id="close_rate_vehicle_rate_popup"
                                                                                       class="close_popup"> <span
                                                id="errmsg_add_airport_rate" style="color:red;"></span></header>
                                        <section class="popupBody">
                                            <div class="form-group serviceType_css">
                                                <div class="col-xs-12 show_postal" style=" font-size: 17px;">
                                                    <div style="height:auto;">
                                                        <form id="add_edit_country">
                                                            <div class="form-group">
                                                                <div class="col-sm-6"
                                                                     style="font-weight: bold;font-size: 14px;text-align: center;">
                                                                    Country Name
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <select class="selectBox" id="coutnry_state">
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-6"
                                                                     style="font-weight: bold;font-size: 14px;text-align: center;">
                                                                    State/Province Name
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control"
                                                                           id="country_name"
                                                                           style="margin-bottom: 15px;" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-6"
                                                                     style="font-weight: bold;font-size: 14px;text-align: center;">
                                                                    State/Province Code
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control"
                                                                           style="margin-bottom: 15px;"
                                                                           id="country_abbr" required>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <button type="submit" class="btn btn-primary" seq="default"
                                                                    style="margin: 20px 5px 5px 10px;">Submit
                                                            </button>
                                                            <br>
                                                            <br>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Core js-->
<script src="js/lib/jquery.js"></script>
<script src="pageJs/dashboard.js"></script>
<script src="pageJs/logout.js"></script>
<script src="js/lib/jquery.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="assets/sparkline/jquery.sparkline.js"></script>
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script src="pageJs/searchbox.js"></script>
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<script src="pageJs/getState.js"></script>
<script>
    $("table.sieve").sieve();
    $('.close_popup').on("click", function () {

        $('#view_rate_vehicle_modal').hide();


    })


    $('.submit_popup').on("click", function () {
        $('#signup_form_overlay').show();
    })
    $('#popup_close_vrc,#cancel_trans').on("click", function () {
        $('#signup_form_overlay').hide();
    })

    $('.popup_edit_city').on("click", function () {

        $('#add_edit_country').find('button').attr("seq", "default");
        $('#add_edit_country').find('button').html("Submit");

        $('#country_name').val("");
        $('#country_abbr').val("");

        $('#view_rate_vehicle_modal').show();


    });


</script>
</body>
</html>
