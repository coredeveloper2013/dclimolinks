/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: get state page
    Author: Mylimoproject
---------------------------------------------*/

/*set the service url using php path*/
var _SERVICEPATH="phpfile/sma_client.php";
/*call the required function on page load*/
GetCountries();
GetCountriesInPage();
/*---------------------------------------------
    Function Name: GetCountriesInPage()
    Input Parameter:user_id
    return:json data
---------------------------------------------*/		
function GetCountriesInPage()
{
	var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
	if(typeof(getLocalStoragevalue)=="string")
	{
	   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
    }
	$.ajax({
		url: _SERVICEPATH,
		type: 'POST',
		data: "action=getSmaCountry&user_id="+getLocalStoragevalue[0].id,
		success: function(response) {
		    var responseHTML='';
		    var responseOption='';
		    var responseObj=response;
			if(typeof(response)=="string")
			{
				responseObj=JSON.parse(response);					
			}
				
			$.each(responseObj.data,function(index,dataUse){
				responseOption+='<option value="'+dataUse.id+'">'+dataUse.country_name+'</option>';
			});

			$('#coutnry_state').html(responseOption);
		}
	});

}

/*---------------------------------------------
    Function Name: GetCountries()
    Input Parameter:user_id
    return:json data
---------------------------------------------*/	
function GetCountries()
{
	var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
	if(typeof(getLocalStoragevalueUserInformation)=="string")
	{
	   	getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
		
	}
	$('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
	
	var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
	if(typeof(getLocalStoragevalue)=="string")
	{
	   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		
	}
			
	$.ajax({
		url: _SERVICEPATH,
		type: 'POST',
		data: "action=getSmaStateAll&user_id="+getLocalStoragevalue[0].id,
		success: function(response) {
			console.log(response);
			var responseHTML='';
			var responseStateFromHTML='';
			var responseStateToHTML='';
			var responseAirportToHTML='';
			var responseSeaportToHTML='';
			var responseObj=response;
			if(typeof(response)=="string")
			{
				responseObj=JSON.parse(response);					
			}
			responseStateFromHTML+='<option>Select State</option>';
			responseStateToHTML+='<option>Select State</option>';
			responseAirportToHTML+='<option>Select State</option>';
			responseSeaportToHTML+='<option>Select State</option>';
               
			if(responseObj.code==1007){             
				$.each(responseObj.data,function(index,dataUse){
					
					responseHTML+='<tr ><td>'+(parseInt(index)+1)+'</td> <td>'+dataUse.state_name+'</td><td>'+dataUse.state_abbr+'</td><td><div><a class="btn btn-xs editCountry" seq="edit_'+dataUse.id+'"> Edit</a> <a class="btn btn-xs deleteCountry" seq="'+dataUse.id+'" >Delete</a> </div></td></tr>';
					responseStateFromHTML+='<option value="'+dataUse.state_abbr+'">'+dataUse.state_name+'</option>';
					responseStateToHTML+='<option value="'+dataUse.state_abbr+'">'+dataUse.state_name+'</option>';
					responseAirportToHTML+='<option value="'+dataUse.state_abbr+'">'+dataUse.state_name+'</option>';
					responseSeaportToHTML+='<option value="'+dataUse.state_abbr+'">'+dataUse.state_name+'</option>';
                  
			    });
                 
            }else{
                responseHTML+='<tr><td colspan="4">Data not Found.</td></tr>';
            }
                    	
			$('#StatesDetails').html(responseHTML);
			$('#get_state_point_from').html(responseStateFromHTML)
			$('#get_state_point_to').html(responseStateToHTML)
			$('#add_airport_rate_state').html(responseAirportToHTML)
		    $('#add_seaport_state_to').html(responseSeaportToHTML)

	        $('.editCountry').on("click",function(){
				var getSeq=$(this).attr("seq");
			    $('#add_edit_country').find('button').attr("seq",getSeq);
				newCountryFunction.getSmaCountrySpecific(getSeq);
						
			});

			$('.deleteCountry').on("click",function(){
				$('#refresh_overlay').css("display","block");	
				var getSeq=$(this).attr("seq");
				newCountryFunction.deleteSmaCountrySpecific(getSeq);

     		});
			
		}
	});
}

/*Added by sunil for getting the list of state */
/*---------------------------------------------
    Function Name: GetCountries()
    Input Parameter:user_id
    return:json data
---------------------------------------------*/	
function GetCountries()
{
	var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
	if(typeof(getLocalStoragevalueUserInformation)=="string")
	{
	   	getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
	}
	$('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
	var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
	if(typeof(getLocalStoragevalue)=="string")
	{
	   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
	}
			
	$.ajax({
		url: _SERVICEPATH,
		type: 'POST',
		data: "action=SmaStateAll&user_id="+getLocalStoragevalue[0].id,
		success: function(response) {
			var responseHTML='';
			var responseStateFromHTML='';
			var responseStateToHTML='';
			var responseAirportToHTML='';
			var responseSeaportToHTML='';
			var responseObj=response;
			if(typeof(response)=="string")
			{
				responseObj=JSON.parse(response);					
			}
			responseStateFromHTML+='<option>Select State</option>';
			responseStateToHTML+='<option>Select State</option>';
			responseAirportToHTML+='<option>Select State</option>';
			responseSeaportToHTML+='<option>Select State</option>';
                
			if(responseObj.code==1007){             
				$.each(responseObj.data,function(index,dataUse){
					
					responseHTML+='<tr ><td>'+(parseInt(index)+1)+'</td> <td>'+dataUse.state_name+'</td><td>'+dataUse.state_abbr+'</td><td><div><a class="btn btn-xs editCountry" seq="edit_'+dataUse.id+'"> Edit</a> <a class="btn btn-xs deleteCountry" seq="'+dataUse.id+'" >Delete</a> </div></td></tr>';
					responseStateFromHTML+='<option value="'+dataUse.state_abbr+'">'+dataUse.state_name+'</option>';
					responseStateToHTML+='<option value="'+dataUse.state_abbr+'">'+dataUse.state_name+'</option>';
					responseAirportToHTML+='<option value="'+dataUse.state_abbr+'">'+dataUse.state_name+'</option>';
					responseSeaportToHTML+='<option value="'+dataUse.state_abbr+'">'+dataUse.state_name+'</option>';
                   
			    });
                 
            }else{

                responseHTML+='<tr><td colspan="4">Data not Found.</td></tr>';
            }
			$('#StatesDetails').html(responseHTML);
			$('#get_state_point_from').html(responseStateFromHTML)
			$('#get_state_point_to').html(responseStateToHTML)
			$('#add_airport_rate_state').html(responseAirportToHTML)
			$('#add_seaport_state_to').html(responseSeaportToHTML)

			$('.editCountry').on("click",function(){
				var getSeq=$(this).attr("seq");
				$('#add_edit_country').find('button').attr("seq",getSeq);
				newCountryFunction.getSmaCountrySpecific(getSeq);
			});

			$('.deleteCountry').on("click",function(){
				$('#refresh_overlay').css("display","block");	
				var getSeq=$(this).attr("seq");
				newCountryFunction.deleteSmaCountrySpecific(getSeq);
			})
			
		}
	});

}

/*create a class for the country function*/
var newCountryFunction= {
	/*set the web service path using php file*/
	 _SERVICEPATHSecond:"phpfile/sma_client.php",
/*---------------------------------------------
    Function Name: deleteSmaCountrySpecific()
    Input Parameter:countryid
    return:json data
---------------------------------------------*/	
	deleteSmaCountrySpecific:function(countryid)
	{
	 	var getCountryId={
	 		"country_id":countryid,
	 		"action":"deleteSmaStateSpecific"
	 	};
	 	$.ajax({
			url: this._SERVICEPATHSecond,
			type: 'POST',
			data:getCountryId,
			dataType:'json',
			success: function(response) {
				$('#refresh_overlay').css("display","none");	
			    GetCountries();
			}
	    });
    },

   /*---------------------------------------------
    Function Name: setCountryName()
    Input Parameter:formDataone
    return:json data
   ---------------------------------------------*/	
	setCountryName:function(formDataone)
	{
		var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
		if(typeof(getLocalStoragevalue)=="string")
		{
		   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
			
		}
		formDataone.append("user_id",getLocalStoragevalue[0].id);
		formDataone.append("action","setSmaState");
		$.ajax({
			url: this._SERVICEPATHSecond,
			type: 'POST',
			processData: false,
	  		contentType: false,
			data:formDataone,
			success: function(response) {		
				$('#view_rate_vehicle_modal').hide();
				GetCountries();
			}
		});
	},

    /*---------------------------------------------
        Function Name: setCountryName()
        Input Parameter:formDataone
        return:json data
    ---------------------------------------------*/	
	updateCountryName:function(newFormData)
	{
	 	newFormData.append("action","updateStateName");
	 	$.ajax({
			url: this._SERVICEPATHSecond,
			type: 'POST',
			processData: false,
	  		contentType: false,
			data:newFormData,
			success: function(response) {	
			    GetCountries();
			    $('#view_rate_vehicle_modal').hide();
		    }
		});

	 },

    /*---------------------------------------------
        Function Name: getSmaCountrySpecific()
        Input Parameter:getCountryID
        return:json data
    ---------------------------------------------*/	
	 getSmaCountrySpecific:function(getCountryID){
	 	var getCountryIdJson={"country_id":getCountryID,
	 							"action":"getSmaStateSpecific"};
	    var getResult=JSON.stringify(getCountryIdJson);
	    $.ajax({
		    url: this._SERVICEPATHSecond,
		    type: 'POST',
		    data:getCountryIdJson,
		    dataType:'json',
		    success: function(response) {	
                $('#refresh_overlay').css("display","none");	
			    $('#add_edit_country').find('button').html("Update");
			    $('#country_name').val(response.data[0]['sma_state']);
			    $('#country_abbr').val(response.data[0]['state_abbr']);
			    $('#coutnry_state').val(response.data[0]['sma_country_id']);
			    $('#view_rate_vehicle_modal').show();

		    }
	    });
	},

}

/*---------------------------------------------
    Function Name: getSmaCountrySpecific()
    Input Parameter:getCountryID
    return:json data
---------------------------------------------*/	
$('.editCountry').on("click",function(){
	$('#refresh_overlay').css("display","block");	
	var getSeq=$(this).attr("seq");
	$('#add_edit_country').find('button').attr("seq",getSeq);
	newCountryFunction.getSmaCountrySpecific(getSeq);
});

/*---------------------------------------------
    Function Name: click on edit countr
    Input Parameter:getCountryID
    return:json data
---------------------------------------------*/	
 $('#add_edit_country').submit(function(event){
    event.preventDefault(); 
    var getFormSubmitSeq=$(this).find('button').attr("seq");
    var geacountryAbr=$('#country_abbr').val();
    var geacountryName=$('#country_name').val();
    var getCountriesName=$('#coutnry_state').val();
	var newFormData=new FormData();
    	newFormData.append("country_abbr",geacountryAbr);
    	newFormData.append("country_name",geacountryName);
    	newFormData.append("coutnry_state",getCountriesName);
    if(getFormSubmitSeq=='default')
    {
    	newCountryFunction.setCountryName(newFormData)
    }
    else
    {
        var getId=getFormSubmitSeq.split("_");
    	newFormData.append("country_name_id",getId[1]);
    	newCountryFunction.updateCountryName(newFormData)
    }

 });

