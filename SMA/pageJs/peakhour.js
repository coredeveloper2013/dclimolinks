
var _SERVICEPATH="phpfile/service.php";
getPeakHourVehicle();

function getPeakHourVehicle()
{
	
		var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
	if(typeof(getLocalStoragevalue)=="string")
	{
	   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		
	}
	
	
	$.ajax({
		url: _SERVICEPATH,
		type: 'POST',
		data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
		success: function(response) {
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);					
				}
				
				for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++) {
    responseHTML+='<div class="panel-heading"> <h4 class="panel-title"> <a class="accordion-toggle ng-binding" ng-click="isOpen = !isOpen" accordion-transclude="heading"> <div class="row ng-scope show_detail_peakhours"  seq="'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'" > <div class="col-sm-6"> <span class="vehicle-name text-black ng-binding"> <i class="fa fa-truck"></i> '+responseObj.VehicleTypes.VehicleType[i].VehTypeTitle+'</span> </div> <div class="col-sm-5"> <span class="ng-binding">1 distance rate tiers</span> </div><i class="pull-right glyphicon text-muted glyphicon-chevron-right"></i> </div> </a> </h4> </div><div > <accordion-heading class="ng-scope"></accordion-heading> <div class="peak-rate-main-div" id="peak-rate-main-div_'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'" style="border-top:1px solid #000;"> <fieldset class="my-fieldset"> <legend class="my-legend"> <p >Peak Rate Shedule</p> </legend> <p>Percent of increase of rate per mile/kilometer during peak times:<input type="number" name="quantity" min="0" step="0.01" max="10" value="5"> &nbsp;&nbsp;%</p> <table border="0" cellpadding="3" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber14"><tr> <th>Select</th> <th>Day</th> <th style="text-align:center;">PickUp Time</th> <th></th> <th text-align:center;>Drop Time</th> </tr> <tbody><tr><td style="width:10px;"><input type="checkbox"  class="selectBoxClass" id="Mon_'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'" name="vehPeakDays" value="MON" > </td><td width="5%">Monday</td><td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td><td width="6%" align="center">-</td><td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--> </td></tr><tr><td width="4%"><input type="checkbox"  class="selectBoxClass" id="Tue_'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'" name="vehPeakDays" value="TUE" ></td><td width="13%">Tuesday</td><td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td> <td width="6%" align="center">-</td> <td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td></tr><tr><td width="4%"><input type="checkbox" name="vehPeakDays" class="selectBoxClass" id="Wed_'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'" value="WED" checked=""></td><td width="13%">Wednesday</td><td width="11%"> <div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td><td width="6%" align="center">-</td><td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td></tr><tr><td width="4%"><input type="checkbox"  class="selectBoxClass" id="Thur_'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'" name="vehPeakDays" value="THU" ></td><td width="13%">Thursday</td><td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td><td width="6%" align="center">-</td><td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td> </tr> <tr> <td width="4%"> <input type="checkbox"  class="selectBoxClass" id="Fri_'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'" name="vehPeakDays" value="FRI" checked=""></td> <td width="13%">Friday</td> <td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td><td width="6%" align="center">-</td><td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td></tr><tr><td width="4%"><input type="checkbox"  class="selectBoxClass" id="Sat_'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'" name="vehPeakDays" value="SAT"></td><td width="13%">Saturday</td><td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--> </td><td width="6%" align="center">-</td><td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td> </tr> <tr><td width="4%"><input type="checkbox" class="selectBoxClass" id="Sun_'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'" name="vehPeakDays" value="SUN"></td><td width="13%">Sunday</td><td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td><td width="6%" align="center">-</td><td width="11%"><div class="input-group bootstrap-timepicker"> <input type="text" class="form-control timepicker-default"> <span class="input-group-btn"> <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button> </span> </div><!--timer--></td></tr></tbody></table> </fieldset> <div class="row"> <div class="col-lg-12 col-md-12"> <a href="#" style="text-align:center; border:1px solid #ccc;" class="btn my-btn" id="update_vehicle_'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'">Update Vehicle Type Rates</a> </div> </div> </div><!--peak-rate-main-div--> </div>';
					
					
				  }
				
					$('#peakhour_dynamic').html(responseHTML);
					
					getPeakRateHoulyClick();
				     commanFunction();
					  checkboxOnChange();
				
			}
	 });
}


function getPeakRateHoulyClick()
{
	
	/*###################################show_detail_peakhours###################################################*/
	$(".show_detail_peakhours").off();
		$(".show_detail_peakhours").on("click", function(){
    var sequence=$(this).attr("seq");
   if($(this).hasClass('active')){
                $(this).removeClass("active");
               $('#peak-rate-main-div_'+sequence).slideUp("slow");
           $(".change_icon i").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
            }
   
   
   else
   {
   
     $(this).addClass("active");
	 $('.peak-rate-main-div').slideUp("slow");
      $('#peak-rate-main-div_'+sequence).slideDown("slow");
    $(".change_icon i").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
    
   }
    });
	}
	/*##############################when merege remove this function##########################################*/
function commanFunction()
{
	
	/*###################################### code to hide and show data in add rate in rate setup page#####################################*/
	     $(".set > a").off();
	      $(".set > a").on("click", function(){
			  
            if($(this).hasClass('active')){
                $(this).removeClass("active");
                $(this).siblings('.acc-content').slideUp("slow");
				$('.show_data').hide();
				$('.show_data_airport').hide();
				$('.show_data_seaport').hide();
				$('.show_data_point').hide();
                $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
            }else{
                $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
                $(this).find("i").removeClass("fa-plus").addClass("fa-minus");
                $(".set > a").removeClass("active");
                $(this).addClass("active");
                $('.acc-content').slideUp("slow");
                $(this).siblings('.acc-content').slideDown("slow");
            }

        });
	
}

function checkboxOnChange(sequence)
{

$('.selectBoxClass').on("change",function(){
	
	alert(this.id);
	if($(this.id).is(':checked'))
  {  
   alert(12)  // checked
	
  }
else
{
	
    alert(13)
}
	});	
}