/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: System Shut Down
  Author: Mylimoproject
---------------------------------------------*/

/*set the web serice php path*/
var _Serverpath="phpfile/client.php";

/*call the getsystemShutdownInfo function on page load */
 getsystemShutdownInfo();

/*---------------------------------------------
    Function Name: call on sytem shutdown submit form
    Input Parameter:All the form value
    return:json data
 ---------------------------------------------*/
	$('#system_shutdown_form').on("submit",function(event){
		$('#refresh_overlay').css("display","block");
	        event.preventDefault();
	    var start_date=$('#start_date').val();
        var end_date=$('#end_date').val();
	    var start_time=$('#start_time').val();
	    var end_time=$('#end_time').val();
	    var newstart_date=new Date(start_date);
	    var newend_date=new Date(end_date);
	    if(newstart_date>newend_date)
		{
	        alert("Start Date is greater than End Date");
	        $('#refresh_overlay').css("display","none");
			return false;
	    }

	    var new_start_time= new Date(start_time); 
	    var new_end_time= new Date(end_time);
	    var new_start_time = new Date("November 13, 2013 " + start_time);
	        new_start_time = new_start_time.getTime();
	    var new_end_time = new Date("November 13, 2013 " + end_time);
	        new_end_time = new_end_time.getTime();
	          
	    if(new_start_time>new_end_time){
               
	        alert("Start Time is greater than End Time");
	        $('#refresh_overlay').css("display","none");
			return false;
	    }
	        
	    var fd = new FormData($('#system_shutdown_form')[0]);
			fd.append("action","systemshutdowninfo");
		var getUserId=window.localStorage.getItem('companyInfo');

	    if(typeof(getUserId)=="string")
	    {
		    getUserId=JSON.parse(getUserId);
	    }
          
	    fd.append("user_id",getUserId[0].id);

	    $.ajax({
		    url:_Serverpath,
		    type: 'POST',
		    processData:false,
			contentType:false,
			data: fd
		}).done(function(result){

		   	result=JSON.parse(result);
			if(result.code==1002)
			{
				$('#refresh_overlay').css("display","none");
				var btnValue= localStorage.getItem('btnValie');
				if(btnValue=='Save'){
			    	alert('Added Successfully');
		        }else{
		            alert('Updated Successfully');
		        }  

			   	location.reload();
			}

        });
    
	});    
/*---------------------------------------------
    Function Name: call on sytem shutdown submit form
    Input Parameter:All the form value
    return:json data
 ---------------------------------------------*/
	function getsystemShutdownInfo(){

	    var getUserId=window.localStorage.getItem('companyInfo');
		$('#refresh_overlay').css("display","block");
	    if(typeof(getUserId)=="string")
	    {
		    getUserId=JSON.parse(getUserId);
	    }
	    var user_id=getUserId[0].id;
	    $.ajax({
			url: _Serverpath,
			type: 'POST',
			data: "action=getsysteminfo&user_id="+user_id,
			success: function(response) {

				var responseObj=response;
			    var responseHTML='';
			    if(typeof(response)=="string")
			    {
				    responseObj=JSON.parse(response);
				}	

			    if(responseObj.code==1007)
		        {
		              
		            var start_date=responseObj.data[0].start_date;
		            var start_date=start_date.split('-');
		            var start_date=start_date[1]+"/"+start_date[2]+"/"+start_date[0];
	           	    var end_date=responseObj.data[0].end_date;
		            var end_date=end_date.split('-');
		            var end_date=end_date[1]+"/"+end_date[2]+"/"+end_date[0];
	                var start_time=responseObj.data[0].start_time.split(':');
	                        
	                if(start_time[0]>12){
	                    var start_time1=start_time[0]-12;
		                    start_time1='0'+start_time1+":"+start_time[1]+" "+"pm"
	                }else{
	                     	
	                    start_time1=start_time[0]+":"+start_time[1]+" "+"am"
	                }

	                var end_time=responseObj.data[0].end_time.split(':');
	                if(end_time[0]>12){
		                 var end_time1=end_time[0]-12;
		                 end_time1='0'+end_time1+":"+end_time[1]+" "+"pm"
	                }else{
	                     end_time1=end_time[0]+":"+end_time[1]+" "+"am"
	                }		

			        $("#save_btn").html('Update');
			        localStorage.setItem('btnValie','Update');	
		    	    $('#start_date').val(start_date);
		    	    $('#end_date').val(end_date);
		    	    $('#start_time').val(start_time1);
		    	    $('#end_time').val(end_time1);
		    	    $('#system_message').val(responseObj.data[0].message);
		    	    $('#refresh_overlay').css("display","none");

		        }else{

		       	    $("#save_btn").html('Save');
		       	    localStorage.setItem('btnValie','Save');
		       	    $('#refresh_overlay').css("display","none");
		        }

			},
			error:function(){
				alert("Some Error");
			}
		});
	}

   /*---------------------------------------------
    Function Name: click on remove button
    Input Parameter:user_id,
    return:json data
   ---------------------------------------------*/
	$('#remove_btn').on('click',function(event){
	    $('#refresh_overlay').css("display","block");
	    event.preventDefault();
	    var getUserId=window.localStorage.getItem('companyInfo');

	    if(typeof(getUserId)=="string")
	    {
		    getUserId=JSON.parse(getUserId);
	    }
	    var user_id=getUserId[0].id;
	
	    $.ajax({
			url: _Serverpath,
			type: 'POST',
			data: "action=deletesysteminfo&user_id="+user_id,
			success: function(response){
				var responseObj=response;
			    var responseHTML='';
				if(typeof(response)=="string")
				{
				    responseObj=JSON.parse(response);
				}	
			    if(responseObj.code==1010)
		        {
				    $('#refresh_overlay').css("display","none");	
		    	    location.reload();
		   	    }

			},
			error:function(){
				alert("Some Error");
			}
	    });

    });






		

