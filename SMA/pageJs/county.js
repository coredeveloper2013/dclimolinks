
var _SERVICEPATH="phpfile/sma_client.php";
GetCountries();
//GetCountriesInPage();
GetCountryList();

function GetCountryList()

{
	var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
	if(typeof(getLocalStoragevalue)=="string")
	{
	   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		
	}
	
	
	$.ajax({
		url: _SERVICEPATH,
		type: 'POST',
		data: "action=getSmaCountry&user_id="+getLocalStoragevalue[0].id,
		success: function(response) {


				var responseHTML='';
				var responseOption="<option value=''>Select Country</option>";
				var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);					
				}
				

				$.each(responseObj.data,function(index,dataUse){
					
					

					responseOption+='<option value="'+dataUse.id+'">'+dataUse.country_name+'</option>';



				});

			
				
				$('#country_list_name').html(responseOption);
                
                $('#country_list_name').on('change',function(){
                  
                    var getCountries_id=$('#country_list_name :selected').val();

                    //alert(getCountries_id);
                    GetCountriesInPage(getCountries_id);

                })

			}
		});


}


function GetCountriesInPage(selected_country_id)

{

	
	var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
	if(typeof(getLocalStoragevalue)=="string")
	{
	   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		
	}
	
	
	$.ajax({
		url: _SERVICEPATH,
		type: 'POST',
		data: "action=getSmaStateAll&country_id="+selected_country_id+"&user_id="+getLocalStoragevalue[0].id,
		success: function(response) {


				var responseHTML='';
				var responseOption="<option value=''>Select State</option>";
				var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);					
				}
				

				$.each(responseObj.data,function(index,dataUse){
					
					

					responseOption+='<option value="'+dataUse.id+'">'+dataUse.state_name+'</option>';



				});

			
				
				$('#coutnry_state').html(responseOption);



			}
		});


}




function GetCountries()
{
	
	
		var getLocalStoragevalueUserInformation=window.localStorage.getItem("companyInfo");
		
		if(typeof(getLocalStoragevalueUserInformation)=="string")
	{
	   	getLocalStoragevalueUserInformation=JSON.parse(getLocalStoragevalueUserInformation);
		
	}

		
		
		$('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
	
	
	
		var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
	if(typeof(getLocalStoragevalue)=="string")
	{
	   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		
	}
			
				$.ajax({
		url: _SERVICEPATH,
		type: 'POST',
		data: "action=getSmaCountyAll&user_id="+getLocalStoragevalue[0].id,
		success: function(response) {
				
				
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);					
				}

				var responseHTML='';
               
				$.each(responseObj.data,function(index,dataUse){
					 if(dataUse.id!=null){
					responseHTML+='<tr ><td>'+(parseInt(index)+1)+'</td><td>'+dataUse.sma_state+'</td><td>'+dataUse.county_name+'</td><td><div><a class="btn btn-xs editCountry" seq="edit_'+dataUse.id+'"> Edit</a> <a class="btn btn-xs deleteCountry" seq="'+dataUse.id+'" >Delete</a> </div></td></tr>';
				     }else{
               responseHTML+='<tr><td colspan="4">Data not Found.</td></tr>';


			}

					

				});
			
                

			
				$('#StatesDetails').html(responseHTML);
			




						$('.editCountry').on("click",function(){

							$('#refresh_overlay').css("display","block");

						var getSeq=$(this).attr("seq");
						$('#add_edit_country').find('button').attr("seq",getSeq);
						newCountryFunction.getSmaCountrySpecific(getSeq);
						
                          	//$('#refresh_overlay').css("display","none");

					});



						$('.deleteCountry').on("click",function(){
							$('#refresh_overlay').css("display","block");

							var getSeq=$(this).attr("seq");
							newCountryFunction.deleteSmaCountrySpecific(getSeq);

                             


						})
			
			}
	 });

}


var newCountryFunction= {
	 _SERVICEPATHSecond:"phpfile/sma_client.php",
	 deleteSmaCountrySpecific:function(countryid)
	 {

	 	var getCountryId={
	 		"country_id":countryid,
	 		"action":"deleteSmaCountySpecific"

	 	};



	 		$.ajax({
		url: this._SERVICEPATHSecond,
		type: 'POST',
		data:getCountryId,
		dataType:'json',
		success: function(response) {	
		
           $('#refresh_overlay').css("display","none");
		
			GetCountries();

		}

		
	});





	 },
	 setCountryName:function(formDataone)
	 {


		var getLocalStoragevalue=window.localStorage.getItem("companyInfo");
	if(typeof(getLocalStoragevalue)=="string")
	{
	   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		
	}

	formDataone.append("user_id",getLocalStoragevalue[0].id);
	formDataone.append("action","setSmaCounty");

		$.ajax({
		url: this._SERVICEPATHSecond,
		type: 'POST',
		processData: false,
  		contentType: false,
		data:formDataone,
		success: function(response) {		
			$('#view_rate_vehicle_modal').hide();
			GetCountries();


	
		}
	});





			




	 },

	 updateCountryName:function(newFormData)
	 {
	 	newFormData.append("action","updateCountyName");
	 	$.ajax({
		url: this._SERVICEPATHSecond,
		type: 'POST',
		processData: false,
  		contentType: false,
		data:newFormData,
		success: function(response) {	

			GetCountries();
			$('#view_rate_vehicle_modal').hide();


		}
		});

	 },


	 getSmaCountrySpecific:function(getCountryID){
	 	var getCountryIdJson={"country_id":getCountryID,
	 							"action":"getSmaCountySpecific"};
	 							var getResult=JSON.stringify(getCountryIdJson);

	 		$.ajax({
		url: this._SERVICEPATHSecond,
		type: 'POST',
		data:getCountryIdJson,
		dataType:'json',
		success: function(response) {	
               $('#refresh_overlay').css("display","none");
			
			 $('#add_edit_country').find('button').html("Update");
			$('#country_list_name').val(response.data[0]['country_id'])
			$('#country_name').val(response.data[0]['county_name']);
			//$('#country_abbr').val(response.data[0]['state_abbr']);
			$('#coutnry_state').val(response.data[0]['state_id']);
			$('#country_abbr').val(response.data[0]['state_abbr']);
			$('#view_rate_vehicle_modal').show();



		}

		
	});




	 },




	}








$('.editCountry').on("click",function(){
						var getSeq=$(this).attr("seq");

						$('#add_edit_country').find('button').attr("seq",getSeq);

						newCountryFunction.getSmaCountrySpecific(getSeq);
						


	});



 $('#add_edit_country').submit(function(event){
    	event.preventDefault(); 
    	var getFormSubmitSeq=$(this).find('button').attr("seq");
    	
    	var geacountryName=$('#country_name').val();
    	var getCountriesName=$('#coutnry_state').val();

      var getuserId=window.localStorage.getItem("companyInfo");


	if(typeof(getuserId)=="string")
	{
	   	getuserIdValue=JSON.parse(getuserId);
		
	}

	 	var newFormData=new FormData();

    	
    	newFormData.append("country_name",geacountryName);
    	newFormData.append("coutnry_state",getCountriesName);
    	//newFormData.append("user_id",getuserIdValue[0].id);


    	if(getFormSubmitSeq=='default')
    	{

    		newCountryFunction.setCountryName(newFormData)

    	}
    	else
    	{

    		var getId=getFormSubmitSeq.split("_");
    		newFormData.append("country_name_id",getId[1]);

    		newCountryFunction.updateCountryName(newFormData)

    	}


    	
    	




  });

