
/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Airport
  Author: Mylimoproject
---------------------------------------------*/
/*declare a class for the page*/

var getPointToPointClass={
    
    /*set php url for the webservice call*/ 
	_Serverpath:"phpfile/rate_point_client.php",
	_SERVICEPATHCAR:"phpfile/service.php",
   



	/**********************************************
    Method:             getServiceTypes()
    Return:Rate         List Of service type Name
*************************************************/
/*function getServiceTypes*/    
 getServiceTypes:function(){
    var userInfo=window.localStorage.getItem("companyInfo");
    var userInfoObj=userInfo;
    if(typeof(userInfo)=="string"){
        userInfoObj=JSON.parse(userInfo);
    }

    var serviceTypeData = [];

    $.ajax({
        url : "phpfile/service_type.php",
        type : 'post',
        data : 'action=GetServiceTypes&user_id='+userInfoObj[0].id,
        dataType : 'json',
        success : function(data){
            if(data.ResponseText == 'OK'){
                var ResponseHtml='';
                $.each(data.ServiceTypes.ServiceType, function( index, result){
                    ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
                });
                               
                $('#service_type').html(ResponseHtml);
            }
            
            $("#service_type").multiselect('destroy');
            $('#service_type').multiselect({
                maxHeight: 200,
                buttonWidth: '178px',
                includeSelectAllOption: true
            });
        }
    });
},



    /*---------------------------------------------
       Function Name: peakHourRate()
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/
		peakHourRate:function(){
			var getCompanyInfo=localStorage.getItem("companyInfo");
			if(typeof(getCompanyInfo)=="string")
			{
				getCompanyInfo=JSON.parse(getCompanyInfo);
			}
			var getJson={"action":"getCommanpeakHourRate","user_id":getCompanyInfo[0].id};
			$.ajax({
			    url: getPointToPointClass._Serverpath,
			    type: 'POST',
			    dataType:'json',
			    data: getJson,
		    	success: function(response) {
			      	var responseHTML='<option value="">Select</option>';
				    if(response.code==1007)
				    {
						$.each(response.data,function(index,result){
							responseHTML+='<option value='+result.id+'>'+result.peak_hour_name+'</option>';	
						});
				    }
				    else
				   {
						responseHTML+='<option value="">Data Not Found</option>';	
				   }
				   $('#TollPeakHrsDatabase').html(responseHTML);

			    }

		    });

		},

	/*---------------------------------------------
       Function Name: deletePointToPointVehicleRate()
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/	  
	deletePointToPointVehicleRate:function(JsonData)
	{
		$.ajax({
		   url: getPointToPointClass._Serverpath,
		   type: 'POST',
		   dataType:'json',
		   data: JsonData,
		   success: function(response) {
			    getPointToPointClass.getPointToPointRetSetUp();

		    }
	    });
	},
    
    /*---------------------------------------------
       Function Name: getCarFromLimoAnyWhere()
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/
	getCarFromLimoAnyWhere:function(){
      
      var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
	   if(typeof(getLocalStoragevalue)=="string")
	   {
	   	  getLocalStoragevalue=JSON.parse(getLocalStoragevalue);

	   }

       var getuserId=window.localStorage.getItem("companyInfo");
	    if(typeof(getuserId)=="string")
		{
		   getuserId=JSON.parse(getuserId);
		}

		var getUserInformationJSon={
			"action":"GetVehicleTypes",
			"limoApiID":getLocalStoragevalue.limo_any_where_api_id,
			"limoApiKey":getLocalStoragevalue.limo_any_where_api_key,
			"user_id":getuserId[0].id
		};

		$.ajax({
			url: getPointToPointClass._SERVICEPATHCAR,
			type: 'POST',
			dataType:'json',
			data: getUserInformationJSon,
			success: function(response) {
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}
				for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++)
				{
				 	if(i==0)
				 	{
				 		responseHTML +='<tr><td><input type="checkbox" class="add_rate_check" id="checked_by_id_'+i+'"  name="vehicle" value="'+i+'"></td><td class="rate_vehicle_code_box_'+i+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</td> <td><input type="number" style="width:67%" min="1" class=" checkvalidation toleAmountCheck'+i+'" disabled ></td><td><input type="checkbox" class="tollAmount'+i+' checkAllInputBox" seq="'+i+'"  ></td></tr>';
				 	}
				 	else
				 	{
				 		responseHTML +='<tr><td><input type="checkbox" class="add_rate_check" id="checked_by_id_'+i+'"  name="vehicle" value="'+i+'"></td><td class="rate_vehicle_code_box_'+i+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</td> <td><input type="number" style="width:67%" min="1" class=" checkvalidation toleAmountCheck'+i+'" disabled></td></tr>';	
				 	}

				}
				$('#vehicl_rate_name').html(responseHTML);
				$('.checkAllInputBox').on("change",function(){
			    var getSeq=$(this).attr("seq");
			    var getSmallValue=0;
			    if( $(this).is(':checked') )
					{
						while($('.checkvalidation ').hasClass('toleAmountCheck'+getSeq))
						{
							if($('.toleAmountCheck'+getSeq).attr('disabled')==undefined)
							{
						
								if($('.toleAmountCheck'+getSeq).val()!='')	
								{
									getSmallValue=$('.toleAmountCheck'+getSeq).val();

									var i=0;
									while($('.checkvalidation ').hasClass('toleAmountCheck'+i))
									{	
									
										if($('.toleAmountCheck'+i).attr('disabled')==undefined)
									    {
						
											$('.toleAmountCheck'+i).val(getSmallValue);
										}
									    i++;	

									}
									break;
								}
							}
							
							getSeq++;

						}

					}
					else
					{
						var elsegetSeq=0;
						while($('.checkvalidation ').hasClass('toleAmountCheck'+elsegetSeq))
						{
								$('.toleAmountCheck'+elsegetSeq).val('');
								elsegetSeq++;
						}

					}
			
				});


				$('.checkvalidation').on("keyup",function(){

					var getValue=$(this).val();
					if(getValue<1 && getValue!='')
					{
						$(this).val(1);
					}

				});

				$('.add_rate_check').on("change",function()
				{
					var getseq=$(this).attr("value");
					if($(this).is(":checked"))
					{	
						$('.rate_input_box_'+getseq).removeAttr("disabled");
						$('.toleAmountCheck'+getseq).removeAttr("disabled");
					}
					else
					{
						$('.rate_input_box_'+getseq).val("");
						$('.toleAmountCheck'+getseq).val("");
						$('.rate_input_box_'+getseq).attr("disabled","true");	
						$('.toleAmountCheck'+getseq).attr("disabled","true");
				    }

				});
			
			}

		});

	},

    /*---------------------------------------------
       Function Name: savePointToPointVehicleRate()
       Input Parameter:seq
       return:json data
    ---------------------------------------------*/
	savePointToPointVehicleRate:function(seq)
	{
		$.ajax({
			url: getPointToPointClass._Serverpath,
			type: 'POST',
			data:seq,
			dataType:'json',
			success: function(response) {
				if(response.code==1007)
				{
				    var responseHTML='';
					$.each(response.data,function(index,result){
						responseHTML+='<tr><td>'+result.vehicle_id+'</td><td>'+result.amount+'</td><td>'+result.toll_amt+'</td></tr>';

					});
				}
				else
				{
				    responseHTML="<tr><td calspan=5>Data Not Found</td</tr>";
				}
				$('#view_vehicl_rate_name').html(responseHTML);
				$('#view_rate_vehicle_modal').show();
				$("#refresh_overlay").css('display','none');
	
			}
		});
	},
	/*---------------------------------------------
       Function Name: savePointToPointRate()
       Input Parameter:jsonData
       return:json data
    ---------------------------------------------*/
	savePointToPointRate:function(jsonData)
	{

    	$.ajax({
		    url: getPointToPointClass._Serverpath,
		    type: 'POST',
		    data:jsonData,
		    dataType:'json',
		    success: function(response) {
			    getPointToPointClass.getPointToPointRetSetUp();
			    localStorage.removeItem('zoneCity');
				localStorage.removeItem('zoneCounty');
				localStorage.removeItem('zoneZip');
			    localStorage.removeItem('rateSetupValuePoint');
			    location.reload();
		        $('#tollZone-A-smaSetup').val('');
			    $('#tollZone-B-smaSetup').val('');
			    $('#getIncrementValue').val('');
			    $('#TollPeakHrsDatabase').val('');
			    $('#getpointName').val('');
			    // getPointToPointClass.getCarFromLimoAnyWhere();
		    }	
	    });
	},

	/*---------------------------------------------
       Function Name: showPointToPointVehicleRate()
       Input Parameter:getJson
       return:json data
    ---------------------------------------------*/
	showPointToPointVehicleRate:function(getJson)
	{
	    var getUserId=window.localStorage.getItem('companyInfo');
		$.ajax({
		    url: getPointToPointClass._Serverpath,
		    type: 'POST',
		    data: getJson,
		    success: function(response) {
			    if(typeof(response)=="string")
			    {
				    response=JSON.parse(response);
			    }
				$('#tollZone-A-smaSetup').val(response.data[0]['sma_from_id']);
				$('#tollZone-B-smaSetup').val(response.data[0]['sma_to_id']);
				$('#TollPeakHrsDatabase').val(response.data[0]['peak_hour_db']);
				$('#SaveTollZone').val(response.data[0]['name']);


				if(response.data[0]['disclaimer_box']==1)
				  {

				  	 $("#disclaimerBox").prop('checked', true);
				  	 $("#tollAmountDsclmr").prop('disabled',false);


				  	 $('#tollAmountDsclmr').val(response.data[0]['disclaimer_message']);
				  }	
				  else
				  {
				   
				    	$("#disclaimerBox").prop('checked', false);
				    	$("#tollAmountDsclmr").prop('disabled', true);
				  	 $('#tollAmountDsclmr').val('');

				    
				  }





				
				$('#SaveTollZone').val(response.data[0]['name']);
				$('#SaveTollZone').val(response.data[0]['name']);
				var showZoneService=[];
				$.each(response.data[0]['zone_service'],function(index,result){
					showZoneService.push(result.service_type);
				})
				$("#service_type").val(showZoneService);
				$("#service_type").multiselect('refresh');
				$('#tollZone-A-smaSetup').change();
				$('#tollZone-B-smaSetup').change();

				



				$("input[type='radio'][name='reversed-toll-setup'][value='"+response.data[0]['is_reverse']+"']").prop("checked",true);
				window.localStorage.setItem("zoneCity",JSON.stringify(response.data[0]['zone_city']));
				window.localStorage.setItem("zoneZip",JSON.stringify(response.data[0]['zoneZipResult']));
				window.localStorage.setItem("zoneCounty",JSON.stringify(response.data[0]['zone_county']));






				
				$('#back_tollZone_location').css({"display":"inline-block"});
				responseHTML='';
				var getArray=[];
				var i=0;
				while($('.rate_vehicle_code_box_'+i).html()!=undefined)
				{	
					getArray.push({"number":i,"vehicle_code":$('.rate_vehicle_code_box_'+i).html()});
					i++;
					$( '#checked_by_id_'+i ).prop( "checked", false );
					$('.rate_input_box_'+i).attr("disabled","true").val('');	
					$('.tollAmount'+i).attr("disabled","true").val('');	
					$('.toleAmountCheck'+i).attr("disabled","true").val('');	
				}
				var getInc=[];
				var getJson1=[];
				$.each(response.data[0]['zone_rate'],function(index,result){
					for(var j=0; j<getArray.length; j++)
					{
						if(getArray[j]['vehicle_code']==result.vehicle_name)
						{
							getInc[j]=getArray[j]['number'];	
						    $( '#checked_by_id_'+getArray[j]['number'] ).prop( "checked", true );
							$('.rate_input_box_'+getArray[j]['number']).val(result.rate).removeAttr("disabled");
							$('.toleAmountCheck'+getArray[j]['number']).val(result.rate).removeAttr("disabled");
							$('.tollAmount'+getArray[j]['number']).removeAttr("disabled");
						    getJson1.push({"vehicle_rate":result.rate,"vehicle_code":result.vehicle_name,"tollRate":result.rate});
						
				    	}

					}	

				});

				setTimeout(function(){
					$('#toll_zone_a_city_check').click();
				$('#toll_zone_b_city_check').click();

				},5000)

				window.localStorage.setItem("rateSetupValuePoint",JSON.stringify(getJson1));
				$("#refresh_overlay").css('display','none');

		    }

	    });
	},

    /*---------------------------------------------
       Function Name: click on add button of passenger rate
       Input Parameter:user_id
       return:json data
     ---------------------------------------------*/
 	getPointToPointRetSetUp:function()
	{
		$("#refresh_overlay").css('display','block');
		var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string")
		{
			getUserId=JSON.parse(getUserId);
		}

		$.ajax({
		    url: getPointToPointClass._Serverpath,
		    type: 'POST',
		    data: "action=getZonetoZoneToll&user_id="+getUserId[0].id,
		    success: function(response) {
			    var responseObj='';
    			if(typeof(response)=="string")
				{
					responseObj=JSON.parse(response);
				}
				var responseHTML='';
				if(responseObj.code==1007){
					$.each(responseObj.data,function(index,result){
						console.log("result....",result);
						var zone_fromcityHTML='';
						var zone_tocityHTML='';
						$.each(result.zone_city,function(inde,zone_city){
							if(zone_city.type=="from"){
							zone_fromcityHTML+='<option value="' + zone_city.city_name + '">' + zone_city.city_name + '</option>';


							}
							else
							{
							zone_tocityHTML+='<option value="' + zone_city.city_name + '">' + zone_city.city_name + '</option>';

							}
						})


						var zone_fromzipHTML='';
						var zone_tozipHTML='';
						$.each(result.zone_zip,function(inde,zone_zip){
							if(zone_zip.type=="from"){
							zone_fromzipHTML+='<option value="' + zone_zip.postal_code + '">' + zone_zip.postal_code + '</option>';


							}
							else
							{
							zone_tozipHTML+='<option value="' + zone_zip.postal_code + '">' + zone_zip.postal_code + '</option>';

							}
						})


						// $.each(result.zone_city,function(inde,zone_city){
						// 	zone_cityHTML+='<option value="' + zone_city.id + '">' + zone_city.city_id + '</option>';
						// })



						responseHTML+='<tr><td>'+result.name+'</td><td>'+result.smafrom+'</td><td><select>'+zone_fromcityHTML+'</select></td><td><select>'+zone_fromzipHTML+'</select></td><td>'+result.smato+'</td><td><select>'+zone_tocityHTML+'</select></td><td><select>'+zone_tozipHTML+'</select></td><td><button class="btn btn-primary editRateSetup" seq="'+result.id+'" >Edit</button>&nbsp;&nbsp;<button class="btn btn-primary deleteRateSetup" seq="'+result.id+'">Delete</button></td></tr>';
					});
                 }else{
                    responseHTML='<tr><td colspan="1">Data not Found.</td></tr>';
                 }
				$('#sma_City_list').html(responseHTML);
				$("#refresh_overlay").css('display','none');
				/*click on edit rate button*/
				$('.editRateSetup').on("click",function(){
					$("#refresh_overlay").css('display','block');
					var getseq=$(this).attr("seq");
					var getJson={"getSeq":getseq,"action":"showZoneToZoneTollRate"};
					$('#add_tollZone_location').html("Update");
					$('#add_tollZone_rate').html("Update Rate");		
					$('#add_tollZone_location').attr("seq",getseq);
					getPointToPointClass.showPointToPointVehicleRate(getJson);
				});
                /*click on delete rate button*/
				$('.deleteRateSetup').on("click",function(){
					var r = confirm("Are you sure you want to delete this item?");
					if(r==true)
					{
						var getseq=$(this).attr("seq");
						var getJson={"getSeq":getseq,"action":"showZoneToZoneTollRateDelete"};
							getPointToPointClass.deletePointToPointVehicleRate(getJson);
					}

			    });

                /*click on view rate button*/
			    $('.viewRate').on("click",function(){
				    $("#refresh_overlay").css('display','block');
					var getSeq=$(this).attr("seq");
					var getJson={"getSeq":getSeq,"action":"savePointToPointVehicleRate"};
					getPointToPointClass.savePointToPointVehicleRate(getJson);
						
				});

		    }
	    });
	},

    /*---------------------------------------------
       Function Name: updatePointToPointRate();
       Input Parameter:user_id
       return:json data
    ---------------------------------------------*/
	updatePointToPointRate:function(getJsonData)
	{
		$.ajax({
		   url: getPointToPointClass._Serverpath,
		   type: 'POST',
		   data: getJsonData,
		   success: function(response) {
		   		location.reload();
				$('#tollZone-A-smaSetup').val('');
				$('#tollZone-B-smaSetup').val('');
				$('#getIncrementValue').val('');
				$('#TollPeakHrsDatabase').val('');
				$('#getpointName').val('');
				$('#add_tollZone_location').html("save");
				$('#back_tollZone_location').css("display","none");
				$('#add_vehicle_rate').html('<i class="glyphicon glyphicon-plus"></i>&nbsp; Add Rates');
			    getPointToPointClass.getCarFromLimoAnyWhere();
			    getPointToPointClass.getPointToPointRetSetUp();
		    }

	    }); 
	},

	/*---------------------------------------------
     Function Name: updatePointToPointRate();
     Input Parameter:user_id
     return:json data
    ---------------------------------------------*/
	getPointToPointZone:function()
	{
		var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string")
		{
			getUserId=JSON.parse(getUserId);
		}
		var availableTags = [];
  		$.ajax({
		    url: getPointToPointClass._Serverpath,
		    type: 'POST',
		    data: "action=getzoneTozoneSma&user_id="+getUserId[0].id,
		    success: function(response) {
				var responseOBJ='';
			    if(typeof(response)=="string")
			    {
				   responseOBJ=JSON.parse(response);
			    }
                var getHtml='<option>Select Zone </option>';
			    if(responseOBJ.code==1007)
			    {
				    $.each(responseOBJ.data,function(index,result){

				    	availableTags.push(result.save_as);
							getHtml+='<option value="'+result.id+'">'+result.sma_name+'</option>';
						
			
					});
					$('#tollZone-B-smaSetup').html(getHtml);
					$('#tollZone-A-smaSetup').html(getHtml);
				
				
			    }
		   	}
	    });
	}
}

   /*---------------------------------------------
     Function Name: click on rate set button;
     Input Parameter:all form value
     return:json data
    ---------------------------------------------*/
	$('#TollzoneAddForm').on("submit",function(eve){

		eve.preventDefault();

		var selected = $("input[type='radio'][name='reversed-toll-setup']:checked").val();

		

		var getLocalStorgavalue=window.localStorage.getItem('rateSetupValuePoint');
		if(typeof(getLocalStorgavalue)=="string")
		{	
			if($('#add_tollZone_location').html()!='Update')
			{
				var getInfo=window.localStorage.getItem("rateSetupValuePoint");
					getInfo=JSON.parse(getInfo);
				var tollZone_A_smaSetup=$('#tollZone-A-smaSetup').val();
				var tollZone_B_smaSetup=$('#tollZone-B-smaSetup').val();
				var zoneMatrixName=$('#SaveTollZone').val();
				// var peakHrsDatabase=$('#TollPeakHrsDatabase').val();
				var peakHrsDatabase=0;


				/**/
				var service_type = $('#service_type option:selected');
			if(service_type.length!=0){
				var serviceTypeObject = [];
				$(service_type).each(function(index, selectedState){
					serviceTypeObject.push($(this).val());
				});
			}

			var zip_a = $('#tollZone-A-zipcodeSetup option:selected');
			var postelCodeObject = [];
			if(zip_a.length!=0){
				
				$(zip_a).each(function(index, selectedState){
					postelCodeObject.push($(this).val());
				});
			}


			var zip_b = $('#tollZone-B-zipcodeSetup option:selected');
			var postelbCodeObject = [];
			if(zip_b.length!=0){
				
				$(zip_b).each(function(index, selectedState){
					postelbCodeObject.push($(this).val());
				});
			}
				/**/


				/**/
				var tollZoneACity = $('#tollZone-A-citySetup option:selected');
			if(tollZoneACity.length!=0){
				var tollZoneACityObject = [];
				$(tollZoneACity).each(function(index, selectedState){
					tollZoneACityObject.push($(this).val());
				});

			}

			var tollCountyACity = $('#tollZone-A-countySetup option:selected');
			if(tollCountyACity.length!=0){
				var tollZoneACountyObject = [];
				$(tollCountyACity).each(function(index, selectedState){
					tollZoneACountyObject.push($(this).val());
				});

			}

			var tollCountyBCity = $('#tollZone-B-countySetup option:selected');
			if(tollCountyBCity.length!=0){
				var tollZoneBCountyObject = [];
				$(tollCountyBCity).each(function(index, selectedState){
					tollZoneBCountyObject.push($(this).val());
				});

			}
				/**/
				/**/
				var tollZoneBCity = $('#tollZone-B-citySetup option:selected');
			if(tollZoneBCity.length!=0){
				var tollZoneBCityObject = [];
				$(tollZoneBCity).each(function(index, selectedState){
					tollZoneBCityObject.push($(this).val());
				});

			}
				/**/	
				var disclaimerbox='0';
				var disclaimerMessage='';

				if($("#disclaimerBox").is(":checked"))
				{


					disclaimerbox='1';
					disclaimerMessage=$('#tollAmountDsclmr').val();
				}
				


				
		        var getUserId=window.localStorage.getItem('companyInfo');
		            getUserId=JSON.parse(getUserId);
				var  getJson={
						"tollZone_A_smaSetup":tollZone_A_smaSetup,
						"tollZone_B_smaSetup":tollZone_B_smaSetup,
						"tollZoneACity":tollZoneACityObject,
						"tollZoneBCity":tollZoneBCityObject,
						"serviceType":serviceTypeObject,
						"peakHrsDatabase":peakHrsDatabase,
						"zoneMatrixName":zoneMatrixName,
						"vehicle_rate":getInfo,
						"user_id":getUserId[0].id,
						"isRevese":selected,
						"disclaimerbox":disclaimerbox,
						"disclaimerMessage":disclaimerMessage,
						"tollZoneACountyObject":tollZoneACountyObject,
						"tollZoneBCountyObject":tollZoneBCountyObject,
						"postelCodeObject":postelCodeObject,
						"postelbCodeObject":postelbCodeObject,
						"action":"saveZoneToZoneRateMatrix"
					};
				getPointToPointClass.savePointToPointRate(getJson);
			}
			else
			{



				var getInfo=window.localStorage.getItem("rateSetupValuePoint");
					getInfo=JSON.parse(getInfo);
				var tollZone_A_smaSetup=$('#tollZone-A-smaSetup').val();
				var tollZone_B_smaSetup=$('#tollZone-B-smaSetup').val();
				var zoneMatrixName=$('#SaveTollZone').val();
				// var peakHrsDatabase=$('#TollPeakHrsDatabase').val();
				var peakHrsDatabase=0


				/**/
				var service_type = $('#service_type option:selected');
			if(service_type.length!=0){
				var serviceTypeObject = [];
				$(service_type).each(function(index, selectedState){
					serviceTypeObject.push($(this).val());
				});
			}
				/**/


				/**/
				var tollZoneACity = $('#tollZone-A-citySetup option:selected');
			if(tollZoneACity.length!=0){
				var tollZoneACityObject = [];
				$(tollZoneACity).each(function(index, selectedState){
					tollZoneACityObject.push($(this).val());
				});

			}



			var zip_a = $('#tollZone-A-zipcodeSetup option:selected');
			var postelCodeObject = [];
			if(zip_a.length!=0){
				
				$(zip_a).each(function(index, selectedState){
					postelCodeObject.push($(this).val());
				});
			}


			var zip_b = $('#tollZone-B-zipcodeSetup option:selected');
			var postelbCodeObject = [];
			if(zip_b.length!=0){
				
				$(zip_b).each(function(index, selectedState){
					postelbCodeObject.push($(this).val());
				});
			}

				/**/
				/**/
				var tollZoneBCity = $('#tollZone-B-citySetup option:selected');
			if(tollZoneBCity.length!=0){
				var tollZoneBCityObject = [];
				$(tollZoneBCity).each(function(index, selectedState){
					tollZoneBCityObject.push($(this).val());
				});

			}
				/**/



					var tollCountyACity = $('#tollZone-A-countySetup option:selected');
			if(tollCountyACity.length!=0){
				var tollZoneACountyObject = [];
				$(tollCountyACity).each(function(index, selectedState){
					tollZoneACountyObject.push($(this).val());
				});

			}

			var tollCountyBCity = $('#tollZone-B-countySetup option:selected');
			if(tollCountyBCity.length!=0){
				var tollZoneBCountyObject = [];
				$(tollCountyBCity).each(function(index, selectedState){
					tollZoneBCountyObject.push($(this).val());
				});

			}

			var disclaimerbox='0';
				var disclaimerMessage='';

				if($("#disclaimerBox").is(":checked"))
				{


					disclaimerbox='1';
					disclaimerMessage=$('#tollAmountDsclmr').val();
				}

				
		        var getUserId=window.localStorage.getItem('companyInfo');
		            getUserId=JSON.parse(getUserId);
		            var getSeq=$('#add_tollZone_location').attr("seq");
				var  getJson={
						"tollZone_A_smaSetup":tollZone_A_smaSetup,
						"tollZone_B_smaSetup":tollZone_B_smaSetup,
						"tollZoneBCountyObject":tollZoneBCountyObject,
						"tollZoneACountyObject":tollZoneACountyObject,
						"tollZoneACity":tollZoneACityObject,
						"tollZoneBCity":tollZoneBCityObject,
						"serviceType":serviceTypeObject,
						"peakHrsDatabase":peakHrsDatabase,
						"zoneMatrixName":zoneMatrixName,
						"disclaimerbox":disclaimerbox,
						"disclaimerMessage":disclaimerMessage,
						"vehicle_rate":getInfo,
						"user_id":getUserId[0].id,
						"getSeq":getSeq,
						"isRevese":selected,
						"postelbCodeObject":postelbCodeObject,
						"postelCodeObject":postelCodeObject,
						"action":"updateZoneToZoneRateMatrix"
					};
				getPointToPointClass.updatePointToPointRate(getJson);
			}
		}
		else
		{
			alert("Set Rate")
		}

	});

    /*---------------------------------------------
     Function Name: click on rate set button;
     Input Parameter:all form value
     return:json data
    ---------------------------------------------*/
	$('#selected_vehicle_rat_btn').on("click",function(){
		var getVecleValue=[];
		var i=0;
		var checkFullArrayFlag='enable';
		$('.add_rate_check').each(function(index,selectedValue){
			var getSeq=$(this).attr("value");
			if($(this).is(":checked"))
			{	
				if($('.rate_input_box_'+getSeq).val()!="")
				{						
					getVecleValue.push({ "vehicle_code":$('.rate_vehicle_code_box_'+getSeq).html(),
									     "vehicle_rate":$('.toleAmountCheck'+getSeq).val(),
									   });
				
						i++;
				}
				else
				{
					checkFullArrayFlag="desable";
					alert("Enter Rate For All Selected Vehicle");
				}
				
			}

		});
   
        /*---------------------------------------------
          Function Name: click on rate set button;
          Input Parameter:all form value
          return:json data
        ---------------------------------------------*/ 
		if(getVecleValue.length>0 && checkFullArrayFlag=="enable")
		  {
			  window.localStorage.setItem("rateSetupValuePoint",JSON.stringify(getVecleValue));
			  $('#add_zone_rate_modal').hide();
		  }

	    });



	localStorage.removeItem('zoneCity');
	localStorage.removeItem('zoneCounty');
	localStorage.removeItem('zoneZip');


        /*call the function on load of the page*/
        getPointToPointClass.getServiceTypes();
        getPointToPointClass.getPointToPointZone();
        getPointToPointClass.getCarFromLimoAnyWhere();
        getPointToPointClass.getPointToPointRetSetUp();
        getPointToPointClass.peakHourRate();





