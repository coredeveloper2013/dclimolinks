/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: connect to limo
  Author: Mylimoproject
---------------------------------------------*/
/*php web server link using php file*/
var _SERVICEPATHServer = "phpfile/client.php";
var _SERVICEPATH = "phpfile/testApiKey.php";

/*call the connectwithlimoallclick function on page load */
connectwithlimoallclick();


/*---------------------------------------------
   Function Name: getUserProfileInfo
   Input Parameter: rowId,categoryId
   return:json data
 ---------------------------------------------*/


function connectwithlimoallclick() {

    $('#refresh_overlay').css("display", "block");
    var getLocalStoragevalueUserInformation = window.localStorage.getItem("companyInfo");
    if (typeof(getLocalStoragevalueUserInformation) == "string") {
        getLocalStoragevalueUserInformation = JSON.parse(getLocalStoragevalueUserInformation);
    }
    $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
    var getApiIdInformation = window.localStorage.getItem("apiInformation");

    if (getApiIdInformation != null) {

        if (typeof(getApiIdInformation) == "string") {
            getApiIdInformation = JSON.parse(getApiIdInformation);

        }

        // $('#limo_api_key').val(getApiIdInformation.limo_any_where_api_key);
        // $('#limo_api_id').val(getApiIdInformation.limo_any_where_api_id);

        $('#limo_api_id').val(getApiIdInformation.client_id);
        $('#limo_api_key').val(getApiIdInformation.client_secret);
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1] + "/" + getUrl.pathname.split('/')[2] + "/" + getUrl.pathname.split('/')[3];
        setTimeout(function () {
            $('#limo_api_url').html(baseUrl + "/checkLimoanywhere.html?id=" + getApiIdInformation.url_name);
        }, 1000);
    }
    $('#test_api_btn').on("click", function () {
        $('#refresh_overlay').css("display", "block");
        var limoApiKey = $('#limo_api_key').val();
        var limoApiID = $('#limo_api_id').val();
        $.ajax({
            url: _SERVICEPATH,
            type: 'POST',
            // data: "action=GetVehicleTypes&limoApiKey="+limoApiKey+"&limoApiID="+limoApiID,
            data: {client_secret: limoApiKey, client_id: limoApiID, check: 1},
            success: function (response) {
                var res = JSON.parse(response);
                console.log(res);
                if (res.error !== undefined) {
                    alert(res.error_description);
                } else {
                    alert("Client ID and Client secret is correct");
                    $('#refresh_overlay').css("display", "none");
                }
                /*var responseObj=response;
                if(typeof(response)=="string")
                {
                    responseObj=JSON.parse(response);
                }
                if(responseObj.ResponseCode=='0')
                {
                  alert("api key and api Id is correct");
                  $('#refresh_overlay').css("display","none");
                }
                else
                {
                  alert(responseObj.ResponseText);
                }*/
            }

        });
        $('#refresh_overlay').css("display", "none");
    });

    $('#submit_api_btn').on("click", function () {

        var client_secret = $('#limo_api_key').val();
        var client_id = $('#limo_api_id').val();
        var param = getApiIdInformation;
        param['action'] = 'setRestApiKey';
        param['client_id'] = client_id;
        param['client_secret'] = client_secret;
        updateApiKey(param);
        /*$('#refresh_overlay').css("display", "block");
        var limoApiKey = $('#limo_api_key').val();
        var limoApiID = $('#limo_api_id').val();

        $.ajax({
            url: _SERVICEPATH,
            type: 'POST',
            data: "action=GetVehicleTypes&limoApiKey=" + limoApiKey + "&limoApiID=" + limoApiID,
            success: function (response) {
                var responseObj = response;
                if (typeof(response) == "string") {
                    responseObj = JSON.parse(response);
                }
                if (responseObj.ResponseText != "Invalid ApiId or ApiKey") {
                    setApiKey(limoApiKey, limoApiID);
                }
                else {
                    alert('Invalid ApiId or ApiKey');
                }

            }

        });
        $('#refresh_overlay').css("display", "none");*/
    });
    $('#refresh_overlay').css("display", "none");
}

/*---------------------------------------------
   Function Name: setApiKey
   Input Parameter: limoApiKey,limoApiID
   return:json data
 ---------------------------------------------*/

function updateApiKey(param) {
    $('#refresh_overlay').css("display", "block");
    $.ajax({
        url: _SERVICEPATHServer,
        type: 'POST',
        data: param,
        success: function (response) {
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            if (responseObj.code == 1015) {
                alert("successfully key registered");
                getServerValue = JSON.stringify(responseObj.data.data[0]);
                window.localStorage.setItem("apiInformation", getServerValue);
                location.reload();
            }
            $('#refresh_overlay').css("display", "none");
        }

    });
    $('#refresh_overlay').css("display", "none");
}
function setApiKey(limoApiKey, limoApiID) {
    $('#refresh_overlay').css("display", "block");
    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;
    if (typeof(userInfo) == "string") {
        userInfoObj = JSON.parse(userInfo);

    }
    $.ajax({
        url: _SERVICEPATHServer,
        type: 'POST',
        data: "action=setApiKey&limoApiKey=" + limoApiKey + "&limoApiID=" + limoApiID + "&user_id=" + userInfoObj[0].id,
        data: "action=setApiKey&limoApiKey=" + limoApiKey + "&limoApiID=" + limoApiID + "&user_id=" + userInfoObj[0].id,
        success: function (response) {
            var responseObj = response;
            if (typeof(response) == "string") {
                responseObj = JSON.parse(response);
            }
            if (responseObj.code == 1015) {
                alert("successfully key registered");
                getServerValue = JSON.stringify(responseObj.data.data[0]);
                window.localStorage.setItem("apiInformation", getServerValue);
                location.reload();
            }
            $('#refresh_overlay').css("display", "none");
        }

    });
    $('#refresh_overlay').css("display", "none");
}

