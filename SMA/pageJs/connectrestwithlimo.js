/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: connect to limo
  Author: Mylimoproject
---------------------------------------------*/
/*php web server link using php file*/
var _SERVICEPATHServer = "phpfile/client.php";
var _SERVICEPATH = "phpfile/testApiKey.php";
var _RESTSERVICEPATH = "https://sandbox-api.mylimobiz.com/oauth2/token";

/*call the connectwithlimoallclick function on page load */
connectwithlimoallclick();


/*---------------------------------------------
   Function Name: getUserProfileInfo
   Input Parameter: rowId,categoryId
   return:json data
 ---------------------------------------------*/

function connectwithlimoallclick() {

    $('#refresh_overlay').css("display", "block");
    var getLocalStoragevalueUserInformation = window.localStorage.getItem("companyInfo");
    if (typeof(getLocalStoragevalueUserInformation) == "string") {
        getLocalStoragevalueUserInformation = JSON.parse(getLocalStoragevalueUserInformation);
    }
    $('#userName').html(getLocalStoragevalueUserInformation[0].full_name);
    var getApiIdInformation = window.localStorage.getItem("apiInformation");

    if (getApiIdInformation != null) {

        if (typeof(getApiIdInformation) == "string") {
            getApiIdInformation = JSON.parse(getApiIdInformation);

        }

        $('#client_id').val(getApiIdInformation.client_id);
        $('#client_secret').val(getApiIdInformation.client_secret);
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1] + "/" + getUrl.pathname.split('/')[2] + "/" + getUrl.pathname.split('/')[3];
        setTimeout(function() {
            $('#limo_api_url').html(baseUrl + "/checkLimoanywhere.html?id=" + getApiIdInformation.url_name);
        }, 1000);
    }
    

    $('#submit_api_btn').on("click", function() {
                $('#refresh_overlay').css("display", "block");
                var client_id = $('#client_id').val();
                var client_secret = $('#client_secret').val();




                var getTokenData = {
                    grant_type: "client_credentials",
                    client_id: client_id,
                    client_secret: client_secret
                };
                getTokenData = JSON.stringify(getTokenData);

                $.ajax({
                            url: _RESTSERVICEPATH,
                            type: 'POST',
                            headers: {
						        "Content-Type":"application/json"
						    },

                            data: getTokenData,
                            success: function(response) {

                                    console.log("response...", response);
                                   
                                    var userInfo = window.localStorage.getItem("companyInfo");
                                    var userInfoObj = userInfo;
                                    if (typeof(userInfo) == "string") {
                                        userInfoObj = JSON.parse(userInfo);

                                    }

                                    $.ajax({
                                                url: _SERVICEPATHServer,
                                                type: 'POST',
                                                data: "action=setRestApiKey&client_id=" + client_id + "&client_secret=" + client_secret + "&access_token=" + response.access_token + "&token_type=" + response.token_type + "&expires_in=" + response.expires_in + "&user_id=" + userInfoObj[0].id,
                                                success: function(response) {
                                                        var responseObj = response;
                                                        if (typeof(response) == "string") {
                                                            responseObj = JSON.parse(response);
                                                        }
                                                        if (responseObj.code == 1015) {                          alert("successfully key registered");
                            getServerValue = JSON.stringify(responseObj.data.data[0]);
                            window.localStorage.setItem("apiInformation", getServerValue);
                            location.reload();
                        }
                        $('#refresh_overlay').css("display", "none");

                    }

                		});








            },
            error:function(err){
            	console.log("error...",err)


            }
        });



        $('#refresh_overlay').css("display", "none");
    });;
    $('#refresh_overlay').css("display", "none");
}

/*---------------------------------------------
	   Function Name: setApiKey
	   Input Parameter: limoApiKey,limoApiID
	   return:json data
	 ---------------------------------------------*/