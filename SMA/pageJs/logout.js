
/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Logout
  Author: Mylimoproject
---------------------------------------------*/

/*---------------------------------------------
    Function Name: logout function 
    Input Parameter: 
    return:redirect the page
 ---------------------------------------------*/
$('#logout').on('click', function(e){
	e.preventDefault();
    var _SERVICEPATHServer = "phpfile/client.php";
    $.ajax({
        url: _SERVICEPATHServer,
        type: 'POST',
        data: {action:'logOut'},
        success: function (response) {
            window.location.href="login.php";
        }

    });
 });