<?php
include_once 'session_auth.php';

$_page = 'prm';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="ThemeBucket">

    <link rel="shortcut icon" href="images/favicon.png">

    <title>Per Passenger/Shuttle rate matrix setup</title>
    <script src="js/lib/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.21/vue.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>


    <!--Core CSS -->

    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-reset.css" rel="stylesheet">

    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>


    <!--dynamic table-->

    <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet"/>

    <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet"/>
    <link href="assets/bootstrap-datepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"/>


    <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css"/>


    <!-- Custom styles for this template -->

    <link href="css/style.css" rel="stylesheet">

    <link href="css/limo_car.css" rel="stylesheet">

    <link href="css/bootstrap-multiselect.css" rel="stylesheet">

    <link href="css/style-responsive.css" rel="stylesheet"/>


    <script src="pageJs/validation.js"></script>

    <!-- Just for debugging purposes. Don't actually copy this line! -->

    <!--[if lt IE 9]>
    <script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->

    <style>

        ul.vehicle-list {

            padding: 0;

            margin: 0;

            list-style: none;

            margin-top: 15px

        }

        ul.vehicle-list li a {

            display: block;

            border-bottom: 1px dotted #CFCFCF;

            padding: 10px;

            text-decoration: none

        }

        ul.vehicle-list li:first-child a {

            border-top: 1px dotted #CFCFCF

        }

        ul.vehicle-list li a:hover {

            background-color: #DBF9FF

        }

        ul.vehicle-list li span {

            display: block

        }

        ul.vehicle-list li span.vehicle-name {

            font-weight: 600;

            color: #8F8F8F

        }

        ul.vehicle-list li span.vehicle-tier-count {

            color: #8C8C8C

        }

        .top-links a {

            font-size: 12px

        }

        .top-links {

            text-align: right

        }

        div.rate-group-selector {

            display: inline-block

        }

        div.vehicle-list .panel-default > .panel-heading {

            background-color: transparent;

            padding: 15px

        }

        div.vehicle-list .panel-default {

            border-color: transparent

        }

        div.vehicle-list .panel {

            border: none;

            box-shadow: none;

            -webkit-box-shadow: none;

            -moz-box-shadow: none;

            -o-box-shadow-none: none

        }

        #vehicle-list h4.panel-title {

            font-style: normal;

            font-size: 14px

        }

        div.vehicle-list .panel-group .panel + .panel {

            margin-top: 0px

        }

        .input-xs {

            height: 22px;

            padding: 5px 5px;

            font-size: 12px;

            line-height: 1.5;

            border-radius: 3px

        }

        div.vehicle-list .table thead th {

            padding-bottom: 5px !important;

            text-transform: none;

            color: #898989;

            font-weight: normal

        }

        div.vehicle-list .table > tbody > tr > td, div.vehicle-list .table thead {

            border-top: none

        }

        div.vehicle-list .table-condensed > tbody > tr > td {

            padding: 2px

        }

        .table-action {

            text-align: right;

            font-size: 14px

        }

        .editable-text {

            width: 30%

        }

        .set { /* position:relative; *//* width:100%; *//* height:auto; *//* background-color:#f5f5f5 */

        }

        .set > a {

            display: block;

            padding: 20px 15px;

            text-decoration: none;

            color: #555;

            font-weight: 600;

            border-bottom: 1px solid #ddd;

            -webkit-transition: all 0.2s linear;

            -moz-transition: all 0.2s linear;

            transition: all 0.2s linear

        }

        .set > a:hover {

            background-color: cornsilk

        }

        .set > a i {

            position: relative;

            float: right;

            margin-top: 4px;

            color: #666

        }

        .set > a.active {

            background-color: #1fb5ad;

            color: #fff

        }

        .set > a.active i {

            color: #fff

        }

        .acc-content {

            position: relative;

            width: 100%;

            height: auto;

            background-color: #fff;

        }

        .form-control {

            color: black;

        }

        .serviceType_css {

            margin-top: 8%;

        }

        .serviceRate_css {

            margin-top: 3%;

        }

        #add_point_rate_to option {

            max-height: 10px !important;

        }

        select[multiple] {

            height: 35px;

        }

        .loadingGIF {

            margin-left: auto;

            margin-right: auto;

            text-align: center;

            width: 100%;

            padding-left: 30%;

            padding-top: 10%;

        }

        .alignCENTER {

            margin-left: auto;

            margin-right: auto;

            text-align: center;

        }

    </style>
    <style>
        .pickUpDates{
            width: 100%;
            display: inline-block;
            background: #dedef7;
            padding: 20px 0;
            margin-top: 10px;
        }
        .bootstrap-timepicker-hour,
        .bootstrap-timepicker-meridian,
        .bootstrap-timepicker-minute{
            border: none;
            box-shadow: none;
        }
    </style>

</head>


<body>

<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->

    <section id="main-content">

        <section class="wrapper">

            <!-- page start-->

            <div class="row">

                <div class="col-sm-12">

                    <h4 class="page-title"> Per Passenger/Shuttle Rate SetUp </h4>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <!-- general form elements -->

                    <div class="box box-info" style="height:520px;  overflow-y:scroll;     overflow-x: hidden;">

                        <!-- form start -->

                        <form onsubmit="return false;">

                            <div class="box-body row">

                                <div class="col-xs-12 col-sm-12 col-md-12" style="text-align:center">

                                    <h4><strong>Per Passenger/Shuttle Rate Matrix</strong></h4>

                                    <hr>

                                </div>

                                <!-- col-xs-12-->

                            </div>

                            <!-- row-->

                            <div class=" row">

                                <div class="col-sm-12 text-center">
                                    <b>Copy From Existing Rate Matrix? &nbsp;&nbsp;&nbsp;</b>
                                    <input type="radio" value="yes" id="copy_existing" name="copy_existing">
                                    YES&nbsp;&nbsp;
                                    <input type="radio" value="no" id="copy_existing" checked name="copy_existing">
                                    NO
                                    <br>
                                </div>

                                <div class="col-sm-12 text-center">
                                    <div class="row">
                                        <div class="col-sm-4 col-sm-offset-4" style="display: none;" id="select_matrix_div">
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select class="form-control" id="select_matrix">
                                                        <option>--Select Matrix--</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <button type="button" id="copy_rate_matrix" class="btn btn-primary btn-block">Copy</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="col-xs-2 col-sm-2 col-md-2" style="text-align:right;padding-right:70px; margin-top:20px;"><b>Action</b></div>-->
                                </div>
                                <div class="col-sm-12">
                                    <hr>
                                </div>

                            </div>

                            <!-- row-->


                            <div class=" row">

                                <div class="col-xs-2 col-sm-2 col-md-2"
                                     style="text-align:right;width:19%; padding-right:0px;">

                                    <h5> Per&nbsp;Pax Fare:</h5>

                                </div>

                                <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:left;width:11%">

                                    <input type="text" class="form-control" id="min_base_rate" placeholder="$"/>

                                </div>

                                <div class="col-xs-2 col-sm-2 col-md-2"
                                     style="text-align:right;width:19%; padding-right:0px;">

                                    <h5>Applies To&nbsp;First:</h5>

                                </div>

                                <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:left;width:16%">

                                    <input type="text" class="form-control" id="apply_to_passenger_0"
                                           placeholder="passenger(s)" style="padding-left:8px; padding-right:6px"></div>

                                <div class="col-xs-6" style="text-align:right; width:34%;">

                                    <button type="button" id="edit_passenger_rate" class="btn btn-primary"
                                            style="visibility:hidden">Edit
                                    </button>

                                    <button type="button" id="add_passenger_rate" class="btn btn-primary"
                                            style="margin-right:2%"><i class="fa fa-plus"></i> Add Rate
                                    </button>

                                </div>

                                <div class="col-sm-12">
                                    <br>
                                </div>
                            </div>

                            <div id="additionl_passenger_rates"></div>

                            <!-- Remaining Miles Row-->

                            <div class="row">

                                <div class="col-xs-5 col-sm-5 col-md-5"
                                     style="text-align:right;width:25%; padding-right:0px;">

                                    <h5> Remaining Passengers </h5>

                                </div>


                                <div class="col-xs-2 col-sm-2 col-md-2"
                                     style="text-align:right;width:24%; padding-right:0px;">

                                    <h5>Per&nbsp;Pax Fare:</h5>

                                </div>

                                <div class="col-xs-1 col-sm-1 col-md-1" style="text-align:left;width:14%">

                                    <input type="text" class="form-control" id="remaining_rate" placeholder="$.$$"
                                           style="padding-left:8px; padding-right:6px">

                                </div>

                                <div class="col-xs-4 col-sm-4 col-md-4"
                                     style="text-align:right;width:40%; padding-right:0px;"></div>


                                <br>

                                <br>

                                <br>

                            </div>
                            <hr>


                            <!-- Remaining Miles Row Ends-->

                            <div class="row">

                                <div class="col-xs-3 col-sm-3 col-md-3"
                                     style="text-align:right;width:19%; padding-right:0px;">

                                    <h5> Select pickup&nbsp;zone:</h5>

                                </div>

                                <div class="col-xs-2 col-sm-2 col-md-2" style="text-align:left;width:19%">

                                    <select id="pick_zone_name" class="form-control">

                                    </select>
                                    <br>
                                    <textarea style="resize: vertical" name="pick_zone_disclaimer" id="pick_zone_disclaimer" rows="5" class="form-control" placeholder="Pickup Instructions"></textarea>

                                </div>

                                <div class="col-xs-2 col-sm-2 col-md-2"
                                     style="text-align:right;width:19%; padding-right:0px;">

                                    <h5> Select Drop-off&nbsp;Zone:</h5>

                                </div>

                                <div class="col-xs-2 col-sm-2 col-md-2" style="text-align:left;width:19%">

                                    <select id="drop_zone_name" class="form-control">

                                    </select>
                                    <br>
                                    <textarea style="resize: vertical" name="drop_zone_disclaimer" id="drop_zone_disclaimer" rows="5" class="form-control" placeholder="Drop-Off Instructions"></textarea>

                                </div>

                            </div>
                            <div class="pickUpDates" id="pickUpDatesApp">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="col-sm-12" id="ptpicker">
                                                <table class="table">
                                                    <tr>
                                                        <th>Pick Up Day</th>
                                                        <th>Pick Up Time</th>
                                                        <th></th>
                                                    </tr>
                                                    <tr v-for="(day,index) in days">
                                                        <td><strong>{{day.title}}</strong></td>
                                                        <td>
                                                            <div v-if="day.times.length > 0">
                                                                <span class="btn btn-xs btn-default" v-for="time in day.times">{{time.tm}} ({{time.max_pass}})</span>
                                                            </div>
                                                            <em v-if="day.times.length == 0">No pickup time selected yet</em>
                                                        </td>
                                                        <td><a class="btn btn-sm btn-default" @click="updateSetupModal(index)">Pickup Time Setup</a></td>
                                                    </tr>
                                                </table>
                                            </div>
<!--                                            <div class="col-sm-12 text-left">-->
<!--                                                <div class="col-sm-12 text-left">-->
<!--                                                    <a class="btn btn-sm btn-primary" onclick="createNewRowTD()"><i class="fa fa-plus fa-fw"></i></a>-->
<!--                                                </div>-->
<!--                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="pickUpTimeSetupModal">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content" v-if="curDay != null">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">{{curDay.title}} Pick up time setup</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <input type="radio" v-model="newDateType" value="new" > New
                                                    <input type="radio" v-model="newDateType" value="existing" > Select from existing
                                                </p>
                                                <div class="col-md-12" v-if="newDateType !== 'new'">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <select v-model="selectedExistingDate" class="form-control">
                                                                        <option  value="">
                                                                            Select Day
                                                                        </option>
                                                                        <option v-for="item in existingPickupTime" >
                                                                            {{ item }}
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <button type="button" class="btn btn-primary btn-block" @click.prevent="AddExistingDate">Copy</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-4">Pickup Time</div>
                                                        <div class="col-md-4">Max Passenger</div>
                                                        <div class="col-md-4"><a @click="addNewTime" class="btn btn-sm btn-primary">Add New</a></div>
                                                    </div>

                                                </div>

                                                <div id="draggable_new" class="col-md-12"  style="margin-top: 10px">
                                                    <div class="row sortable-item" :saved="time.tm + '$$' + time.max_pass" style="margin-bottom: 5px; margin-top: 5px;" :key="index+'11'" v-for="(time,index) in curDay.times">
                                                        <div class="col-md-4">
                                                            <input type="text" @change="triggerSort" v-model="time.tm" class="form-control ppTimePicker" placeholder="Select Time" :data-type="index">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="number" @change="triggerSort" v-model="time.max_pass" min="0" class="form-control " placeholder="Max Passenger">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <a class="btn btn-sm btn-danger" @click="removeThisSchedule(index)"><i class="fa fa-fw fa-remove"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
<!--                                                <table class="table">-->
<!--                                                    <tr>-->
<!--                                                        <th>Pickup Time</th>-->
<!--                                                        <th>Max Passenger</th>-->
<!--                                                        <th class="text-right"><a @click="addNewTime" class="btn btn-sm btn-primary">Add New</a></th>-->
<!--                                                    </tr>-->
<!--                                                    <tr :key="time.max_pass + time.tm" v-for="(time,index) in curDay.times">-->
<!--                                                        <td><input type="text" v-model="time.tm" class="form-control ppTimePicker" placeholder="Select Time" :data-type="index"></td>-->
<!--                                                        <td><input type="number" v-model="time.max_pass" class="form-control ppPassPicker" placeholder="Max Passenger" @keyup="getPassValue($event,index)" @keyup="getPassValue($event,index)"></td>-->
<!--                                                        <td class="text-right"><a class="btn btn-sm btn-danger" @click="removeThisSchedule(index)"><i class="fa fa-fw fa-remove"></i></a></td>-->
<!--                                                    </tr>-->
<!--                                                </table>-->
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" @click="saveCurrentData" data-dismiss="modal">Done</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pickUpDates" id="pickUpDatesAppEdit" style="display: none;">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="col-sm-12" id="ptpicker">
                                                <table class="table">
                                                    <tr>
                                                        <th>Pick Up Day</th>
                                                        <th>Pick Up Time</th>
                                                        <th></th>
                                                    </tr>
                                                    <tr :key="index + 'vbg'" v-for="(day,index) in days">
                                                        <td><strong>{{day.title}}</strong></td>
                                                        <td>
                                                            <div v-if="day.times.length > 0">
                                                                <span class="btn btn-xs btn-default" v-for="time in day.times">{{time.tm}} ({{time.max_pass}})</span>
                                                            </div>
                                                            <em v-if="day.times.length == 0">No pickup time selected yet</em>
                                                        </td>
                                                        <td><a class="btn btn-sm btn-default" @click="updateSetupModal(index)">Pickup Time Setup</a></td>
                                                    </tr>
                                                </table>
                                            </div>
<!--                                            <div class="col-sm-12 text-left">-->
<!--                                                <div class="col-sm-12 text-left">-->
<!--                                                    <a class="btn btn-sm btn-primary" onclick="createNewRowTD()"><i class="fa fa-plus fa-fw"></i></a>-->
<!--                                                </div>-->
<!--                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="pickUpTimeSetupEditModal">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content" v-if="curDay != null">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">{{curDay.title}} Pick up time setup</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <input type="radio" v-model="newDateType" value="new" > New
                                                    <input type="radio" v-model="newDateType" value="existing" > Select from existing
                                                </p>
                                                <div class="col-md-12" v-if="newDateType !== 'new'">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <select v-model="selectedExistingDate" class="form-control">
                                                                        <option  value="">
                                                                            Select Day
                                                                        </option>
                                                                        <option v-for="item in existingPickupTime" >
                                                                            {{ item }}
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <button type="button" class="btn btn-primary btn-block" @click.prevent="AddExistingDate">Copy</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-4">Pickup Time</div>
                                                        <div class="col-md-4">Max Passenger</div>
                                                        <div class="col-md-4"><a @click="addNewTime" class="btn btn-sm btn-primary">Add New</a></div>
                                                    </div>

                                                </div>
                                                <div id="draggable" class="col-md-12"  style="margin-top: 10px">
                                                    <div class="row sortable-item" :saved="time.tm + '$$' + time.max_pass" style="margin-bottom: 5px; margin-top: 5px;" :key="index + 'yu'" v-for="(time,index) in curDay.times">
                                                        <div class="col-md-4">
                                                            <input type="text" @change="triggerSort" v-model="time.tm"  class="form-control ppTimePicker" placeholder="Select Time" :data-type="index">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="number" @change="triggerSort" v-model="time.max_pass" class="form-control" placeholder="Max Passenger">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <a class="btn btn-sm btn-danger" @click="removeThisSchedule(index)"><i class="fa fa-fw fa-remove"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
<!--                                                <table id="draggable" class="table">-->
<!--                                                    <tr>-->
<!--                                                        <th>Pickup Time</th>-->
<!--                                                        <th>Max Passenger</th>-->
<!--                                                        <th class="text-right"><a @click="addNewTime" class="btn btn-sm btn-primary">Add New</a></th>-->
<!--                                                    </tr>-->
<!--                                                    <tr :key="time.max_pass + time.tm" v-for="(time,index) in curDay.times">-->
<!--                                                        <td><input type="text" v-model="time.tm"  class="form-control ppTimePicker" placeholder="Select Time" :data-type="index"></td>-->
<!--                                                        <td><input type="number" v-model="time.max_pass" class="form-control ppPassPicker" placeholder="Max Passenger" @keyup="getPassValue($event,index)" @keyup="getPassValue($event,index)"></td>-->
<!--                                                        <td class="text-right"><a class="btn btn-sm btn-danger" @click="removeThisSchedule(index)"><i class="fa fa-fw fa-remove"></i></a></td>-->
<!--                                                    </tr>-->
<!--                                                </table>-->

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" @click="saveCurrentData">Done</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-xs-4 col-sm-4 col-md-4"
                                     style="text-align:justify;width:19%; text-align:right;"><br>

                                    <h5>Peak&nbsp;Hour fare&nbsp;increase (Per&nbsp;Pax)</h5>

                                </div>

                                <div class="col-xs-2 col-sm-2 col-md-2" style="padding-left:0px; width:13%"><br>

                                    <input type="text" style="width: 55%;display: inline-block;" id="percent_increase"
                                           class="form-control" min=0 onkeyup="checknegative()"/>

                                    <select id="currencyType"
                                            style="height: 34px;display: inline-block;    border: 1px solid #e2e2e4;"
                                            name="currencyType" class="currencyType">

                                        <option value="%">%</option>

                                        <option value="$">$</option>

                                    </select>

                                </div>

                                <div class="col-xs-2 col-sm-2 col-md-2 isInputBoxFill"
                                     style="visibility:hidden; text-align:right;"><br>

                                    <h5>Select Peak Hrs DB:</h5></div>

                                <div class="col-xs-4 col-sm-4 col-md-4 isInputBoxFill"
                                     style="visibility:hidden; text-align:left; padding-left:5px;"><br><select
                                            id="pickHrsDatabase" style="" class="form-control" required>

                                    </select>

                                    <br>

                                </div>


                                <!-- <div class="col-xs-1">



                          </div> -->

                                <br>

                                <br>

                                <br>

                            </div>

                            <!-- row -->

                            <!-- Apply to vehicle type-->

                            <div class="row">

                                <div class="col-xs-3 col-sm-3 col-md-3"
                                     style="text-align:right;width:19%; padding-right:0px;">

                                    <h5>Applies&nbsp;to Vehicle&nbsp;Type: </h5>

                                </div>

                                <div class="col-xs-1 col-sm-3 col-md-1" style="text-align:left;width:17%">

                                    <select id="apply_vehicle_type" multiple="multiple">

                                        <option>--Select--</option>

                                    </select>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3"
                                     style="text-align:right;width:19%; padding-right:0px;">

                                    <h5>Associate with&nbsp;SMA:</h5>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3" style="text-align:left;">

                                    <select id="apply_sma" multiple="multiple">

                                        <option>Select SMA</option>

                                    </select>

                                </div>

                                <br>

                                <br>

                            </div>

                            <!-- Associate with SMA -->

                            <br>

                            <div class="row" id="luggage_setup" style="margin-top:15px">

                                <div class="col-xs-3 col-sm-3 col-md-3"
                                     style="text-align:right;width:19%; padding-right:0px;">

                                    <h5>Luggage settings</h5>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3" style="text-align:left;width:30%;">

                                    <button type="button" @click.prevent="fetchData" class="btn btn-primary luggage-trigger">manage</button>

                                </div>

                                    <div class="modal fade" id="luggage_settings_modal">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Extra luggage setup</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <p>
                                                        <input type="radio" v-model="active" value="1"  > active
                                                        <input type="radio" v-model="active" value="0"  > deactive
                                                    </p>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h4 style="text-align: center; color: #0a0a0a; margin-bottom: 40px;">Surcharge Setup</h4>
                                                            <div class="row">
                                                                <div class="col-md-4" style="font-weight: bold">
                                                                    Luggage Size
                                                                </div>
                                                                <div class="col-md-6" style="font-weight: bold">
                                                                    Maximum allowed
                                                                </div>
                                                                <br>
                                                                <br>
                                                                <div class="col-md-4">
                                                                    Large
                                                                </div>
                                                                <div class="col-md-6"><input class="form-control" min="0"  style="max-width: 85px; display: inline" type="number" v-model="max_allowed.large"></div>
                                                                <div class="col-md-4">
                                                                    Medium
                                                                </div>
                                                                <div class="col-md-6"><input class="form-control" min="0"  style="max-width: 85px; display: inline" type="number" v-model="max_allowed.medium"></div>
                                                                <div class="col-md-4">
                                                                    Small
                                                                </div>
                                                                <div class="col-md-6"><input class="form-control" min="0"  style="max-width: 85px; display: inline" type="number" v-model="max_allowed.small"></div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <h4 style="text-align: center; color: #0a0a0a; margin-bottom: 40px;">Surcharge Setup</h4>
                                                            <div class="row">
                                                                <div class="col-md-4" style="font-weight: bold">
                                                                    Luggage Size
                                                                </div>
                                                                <div class="col-md-6" style="font-weight: bold">
                                                                    Surcharge per additional item
                                                                </div>
                                                                <br>
                                                                <br>
                                                                <div class="col-md-4">
                                                                    Large
                                                                </div>
                                                                <div class="col-md-6"><span>$</span><input class="form-control" min="0"  style="max-width: 85px; display: inline" type="number" v-model="extra_price.large"></div>
                                                                <div class="col-md-4">
                                                                    Medium
                                                                </div>
                                                                <div class="col-md-6"><span>$</span><input class="form-control" min="0"  style="max-width: 85px; display: inline" type="number" v-model="extra_price.medium"></div>
                                                                <div class="col-md-4">
                                                                    Small
                                                                </div>
                                                                <div class="col-md-6"><span>$</span><input class="form-control" min="0"  style="max-width: 85px; display: inline" type="number" v-model="extra_price.small"></div>

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <h4 style="text-align: center; color: #0a0a0a; margin-bottom: 40px;">Disclaimer Message</h4>
                                                            <div class="row">
                                                                <textarea v-model="disclaimer" class="form-control" rows="8" ></textarea>


                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal" @click="saveTemp">Done</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                            </div>


                            <br>



                            <div class="row" style="margin-top:15px">

                                <div class="col-xs-3 col-sm-3 col-md-3"
                                     style="text-align:right;width:19%; padding-right:0px;">

                                    <h5>Save Rate&nbsp;Matrix&nbsp;as:</h5>

                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3" style="text-align:left;width:30%;">

                                    <input type="text" class="form-control" id="passenger_matrix_name">

                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6 " style="text-align:center; width:35%;">

                                    <button type="button" id="save_perpass_rate" class="btn btn-primary"> Save Rate
                                        Matrix
                                    </button>

                                    <button type="button" id="back_button" class="btn btn-primary"
                                            style="display:none;"> Back
                                    </button>

                                </div>

                            </div>

                            <!-- row save matrix button-->


                            <!-- /.box-body -->


                        </form>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div data-ng-controller="SharedData" class="m-t-md ng-scope">

                        <!--#########################################point to point ####################################################-->

                        <div>

                            <div class="row">

                                <div class="col-xs-12 col-sm-12 col-md-12"></div>

                            </div>

                            <div class="row mb-sm ng-scope">

                                <div class="col-xs-12 col-sm-12 col-md-12"></div>

                            </div>

                            <div style="overflow:scroll;min-height:500px;">

                                <table class="table table-condensed ng-scope sieve

">

                                    <thead>

                                    <tr>

                                        <th style="text-align:center">Per Pax Matix Name</th>

                                        <th style="text-align:center">Min.Fare($)</th>

                                        <th style="">Apply To Vehicle</th>

                                        <th style="">Associate with SMA</th>

                                        <th style="text-align:center">Action</th>

                                        <th class="table-action"><span class="table-action"></span></th>

                                    </tr>

                                    </thead>

                                    <tbody id="rate_matrix_list">

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- ############################## additional rate####################################################-->


            <!--##################finish###########################-->


            <!-- page end-->

        </section>

    </section>


    <!--main content end--> <!-- Right Side Bar Goes Here if Required

<div class="right-sidebar">

<div class="right-stat-bar"> </div>

</div> --></section>


<!--#######################code to add popup on sma list##########################################################-->

<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display:none;"
     id="postal_code_modal">

    <div style="position:relative; background:black; opacity:0.5; top: 0px; left:0px; width:100%; height:100%"></div>

    <div style="position:absolute; top:0px; left:-17%; width:136%; height:100%; margin-left:auto; margin-right:auto;">

        <div style="width:100%;display:table; height:100%;">

            <div style="width:100%;display:table-row">

                <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">

                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; ">

                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;">

                            <!--<div style="width:100%">

                                          <img class="" style="width:50%; height:150px;" src="img/One-shop_Logo_Revised.png"></div>-->

                            <br>

                            <br>

                            <br>

                            <br>

                            <br>

                            <br>

                            <div style="width:80%">

                                <!--  code of popup start-->

                                <div class="container">

                                    <div id="modal" class="popupContainer" style="display:block;  top: 23px; position: absolute;

  width: 21%;



  left: 39%;

  top: 30px;

  background: #FFF;">

                                        <header class="popupHeader" style="  background: #F4F4F2;

  position: relative;

  padding: 10px 20px;

  border-bottom: 1px solid #DDD;

  font-weight: bold;

  font-family: 'Source Sans Pro', sans-serif;

  font-size: 14px;

  color: #666;

  font-size: 16px;

  text-transform: uppercase;

  text-align: left;

  height:9%"><span class="header_title">Postal Codes </span> <img src="images/remove.png" alt="" id="close_postal_popup"
                                                                  class="close_popup"> <span
                                                    id="errmsg_add_airport_rate" style="color:red;"></span></header>

                                        <section class="popupBody">

                                            <div class="form-group serviceType_css">

                                                <div class="col-xs-9 show_postal"
                                                     style="margin-bottom: 8%; font-size: 17px;"></div>

                                            </div>

                                        </section>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<!--end of add location popup-->

<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;"
     id="add_sma">

    <div style="position:relative; background:black; opacity:0.5; top: 0px; left:0px; width:100%; height:100%"></div>

    <div style="position:absolute; top:0px; left:-17%; width:136%; height:100%; margin-left:auto; margin-right:auto;">

        <div style="width:100%;display:table; height:100%;">

            <div style="width:100%;display:table-row">

                <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">

                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px;">

                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;">

                            <!--<div style="width:100%">

                                          <img class="" style="width:50%; height:150px;" src="img/One-shop_Logo_Revised.png"></div>-->

                            <br>

                            <br>

                            <br>

                            <br>

                            <br>

                            <br>

                            <div style="width:80%">

                                <!--  code of popup start-->


                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<!-- Placed js at the end of the document so the pages load faster -->


<!--Core js-->


<!--Loading indicator-->

<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;"
     id="refresh_overlay">

    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"></div>

    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto">

        <div style="width:100%;display:table; height:100%;">

            <div style="width:100%; display:table-row">

                <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto">

                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px">

                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto"><br>

                            <br>

                            <br>

                            <br>

                            <br>

                            <br>

                            <br>

                            <br>

                            <br>

                            <br>

                            <br>

                            <div class="loadingGIF"><img style="height:70px;width:70px;" src="images/loading.gif"
                                                         alt="Page loading indicator"></div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<script src="pageJs/dashboard.js"></script>

<script src="pageJs/logout.js"></script>

<script src="bs3/js/bootstrap.min.js"></script>
<script src="pageJs/peak_hour.js"></script>
<script src="assets/bootstrap-datepicker/js/bootstrap-timepicker.js"></script>


<script src="js/bootstrap-multiselect.js"></script>

<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>

<script src="js/scrollTo/jquery.scrollTo.min.js"></script>

<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>

<script src="js/nicescroll/jquery.nicescroll.js"></script>

<!--Easy Pie Chart-->

<script src="assets/easypiechart/jquery.easypiechart.js"></script>

<!--Sparkline Chart-->

<script src="assets/sparkline/jquery.sparkline.js"></script>

<!--jQuery Flot Chart-->

<script src="pageJs/searchbox.js"></script>

<script src="pageJs/per_passenger_pricing.js?js=<?php echo uniqid(); ?>"></script>

<script src="assets/flot-chart/jquery.flot.js"></script>

<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script>

<script src="assets/flot-chart/jquery.flot.resize.js"></script>

<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script>


<!--dynamic table-->

<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>


<!--dynamic table initialization -->

<script src="js/dynamic_table/dynamic_table_init.js"></script>


<!--common script init for all pages-->

<script src="js/scripts.js"></script>

<script>

    $("table.sieve").sieve();

    $(document).ready(function () {

        $("#percent_increase").keydown(function (e) {

            // Allow: backspace, delete, tab, escape, enter and .

            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||

                // Allow: Ctrl+A, Command+A

                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||

                // Allow: home, end, left, right, down, up

                (e.keyCode >= 35 && e.keyCode <= 40)) {

                // let it happen, don't do anything

                return;

            }

            // Ensure that it is a number and stop the keypress

            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {

                e.preventDefault();

            }

        });

    });

    $('#percent_increase').keyup(function () {


        if ($('#percent_increase').val() == '') {

            $('.isInputBoxFill').css("visibility", "hidden");


        }

        else {


            $('.isInputBoxFill').css("visibility", "visible");

        }


    })


    setTimeout(function () {



            //getState(cityAndState);

            $("#apply_vehicle_type").multiselect('destroy');

            $('#apply_vehicle_type').multiselect({

                maxHeight: 200,

                buttonWidth: '155px',

                includeSelectAllOption: true

            });

            $("#apply_sma").multiselect('destroy');

            $('#apply_sma').multiselect({

                maxHeight: 200,

                buttonWidth: '155px',

                includeSelectAllOption: true

            });

        }

        , 900);

</script>


<!--<script src="pageJs/dashboard.js"></script>

<script src="pageJs/getairports.js"></script>

<script src="pageJs/getState.js"></script>

<script src="pageJs/getSeaport.js"></script>

<script src="pageJs/getStatePopup.js"></script>

<script src="pageJs/getVehicle.js"></script>

<script src="pageJs/getService.js"></script>

<script src="pageJs/peakhour.js"></script>

<script src="pageJs/extrachild.js"></script> -->


<!-- Loading Indicator Ends-->

<script>
    $(function(){
        $('.timepicker').timepicker({});

    })
</script>

</body>

</html>

