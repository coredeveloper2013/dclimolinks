<?php
include_once 'session_auth.php';

$_page = 'cos';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="images/favicon.png">
<title>My Limo Project | Reservation cut-off time setup</title>

<!--Core CSS -->
<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-reset.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

<!--dynamic table-->
<link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />

<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<link href="css/limo_car.css" rel="stylesheet">
<link href="css/bootstrap-multiselect.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" media="screen" href="css/timepicker/bootstrap-datetimepicker.min.css" />
<script src="js/lib/jquery.js"></script>
<script src="pageJs/validation.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="JQ_timepicker/jquery_addon_css.css">
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<style>
.loadingGIF {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	width: 100%;
	padding-left: 30%;
	padding-top: 10%;
}
.alignCENTER {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}
</style>
</head>

<body>
<section id="container" >

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
  <section id="main-content">
    <section class="wrapper"> 
      <!-- page start-->
      <div class="row">
        <div class="col-md-12 alignCENTER">
          <h3 class="page-title "> Cut-off time setup </h3>
        </div>
      </div>
      <div class="row" >
        <div class="col-md-12" > 
          <!-- general form elements -->
          <div class="box box-info" style="height:300px;"> 
            <!-- form start -->
            <form id="cutoffSettingForm" >
              <div class=" row">
                <div class="col-sm-1" style="text-align:center;"></div>
                <div class="col-xs-2" style="text-align:center;"><b> </b> </div>
                <div class="col-xs-12" style="text-align:center;">
                  <label>
                    <input type="checkbox" class="networkCutoffTime" value="">
                    Network/eFarm-in cut-off Window</label>
                </div>
              </div>
              <div class=" row"> 
                
                <!--  <div class="col-sm-1" style="text-align:center;"></div> -->
                <div class="col-xs-2" style="text-align:center;"><b> </b> </div>
                <div class="col-xs-2" style="text-align:center;width:19%"><b>Service Type</b>
                  <select name="service type" class="service_type" id="service_type" multiple="multiple" required >
                    <option value="">--Select Service--</option>
                  </select>
                  
                  <!-- <input type="text" class="form-control " id="amount" placeholder="$"> --></div>
                <div class="col-xs-2" style="text-align:center;"><b>Set Cut-off time</b> 
                  <!--  <input type="text" class="form-control time-picker" id="cutoff_time"> -->
                  
                  <select class="form-control" style="float:left;" id="hhTime" required><option value="">HH</option>
                  </select>
                </div>
                <div class="col-xs-2" style="text-align:center;"><br>
                  <!--  <input type="text" class="form-control time-picker" id="cutoff_time"> -->
                  
                  <select class="form-control" id="mmTime" required>
                    <option value="">MM</option>
                    <option value="0">00</option>
                    <option value="15">15</option>
                    <option value="30">30</option>
                    <option value="45">45</option>
                  </select>
                </div>
                <div class="col-xs-2" style="text-align:center;"><b>Save As</b>
                  <input type="text" class="form-control" id="save_as" required>
                </div>
              </div>
              <br/>
              <br/>
              <!-- Remaining Miles Row-->
              <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-2"><b> </b> </div>
                <div class="col-xs-3" style="text-align:center;"><b>Cutt-off time disclaimer message</b>
                  <textarea class="form-control" rows="3" id="cutoff_comment"></textarea>
                </div>
                <div class="col-xs-3" style="margin-top:3%;">
                  <input type="submit" class="btn btn-primary" id="save_rate" value="submit">
                  <!-- <button type="submit"   style="width:10%;" >  Save  </button> -->
                  <button type="button" id="back_button" class="btn btn-primary" style="display:none;"> Back </button>
                </div>
                <br>
                <br>
              </div>
              <!-- Apply to vehicle type-->
              <div class="row"> <br>
                <br>
                <br>
              </div>
              <!-- Associate with SMA --> 
              
              <br>
              <div class="row"> </div>
              <!-- row save matrix button--> 
              
              <!-- /.box-body -->
              
            </form>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div data-ng-controller="SharedData" class="m-t-md ng-scope"> 
            <!--#########################################point to point ####################################################-->
            <div>
              <div class="row">
                <div class="col-xs-12"></div>
              </div>
              <div class="row mb-sm ng-scope">
                <div class="col-sm-12"></div>
              </div>
              <div style="overflow:scroll;min-height:500px;">
                <table class="table table-condensed ng-scope sieve">
                  <thead >
                    <tr >
                      <th style="text-align:center">#</th>
                      <th style="text-align:center">Name</th>
                      <th style="text-align:center">Service Type</th>
                      <th style="text-align:center">Cutoff time</th>
                      <th style="text-align: center;padding-right: 2%;">Network/Frame-in cut-off </th>
                      <!-- <th style="text-align: center;padding-right: 2%;">Associate with SMA</th> -->
                      <th style="text-align:center">Action</th>
                      <th class="table-action"><span class="table-action"></span></th>
                    </tr>
                  </thead>
                  <tbody id="cutoffMatrixList">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <!-- page end--> 
    </section>
  </section>
</section>

<!--#######################code to add popup on sma list##########################################################-->

<!--Core js--> 

<!--Loading indicator-->

<!--Loading indicaator-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;" id="refresh_overlay">
  <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%;display:table-row">
        <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; " >
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;"> 
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>                            <div class="loadingGIF"> <img style="height:70px;width:70px;" src="images/loading.gif" alt="Page loading indicator"> </div>

</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Loading Indicator Ends--> 
<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/logout.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script src="js/bootstrap-multiselect.js"></script> 
<script type="text/javascript" src="js/moment.js"></script> 
<script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 
<!--Easy Pie Chart--> 
<script src="assets/easypiechart/jquery.easypiechart.js"></script> 
<!--Sparkline Chart--> 
<script src="assets/sparkline/jquery.sparkline.js"></script> 
<!--jQuery Flot Chart--> 
<script src="pageJs/cutoffTimeSetting.js"></script> 
<!--<script src="assets/flot-chart/jquery.flot.js"></script> 
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script> 
<script src="assets/flot-chart/jquery.flot.resize.js"></script> 
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script> --> 

<!--dynamic table--> 
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script> 
<script src="pageJs/searchbox.js"></script> 
<!--dynamic table initialization --> 
<script src="js/dynamic_table/dynamic_table_init.js"></script> 

<!--common script init for all pages--> 
<script src="js/scripts.js"></script> 
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> 
<script src="JQ_timepicker/jquery_addon_js.js"></script> 
<script src="JQ_timepicker/slideaccess.js"></script> 
<script>
$("table.sieve").sieve();
$(function() {

var selectOption='<option value="">HH</option>';
var selectOptionMM='<option value="">MM</option>';

$('#hhTime').on("change",function(){



if($('#hhTime').val()==24)
{

  $('#mmTime').prop("disabled",true).val('');


}
else
{

  $('#mmTime').prop("disabled",false);

}


});




for(var i=0; i<=24; i++)
{


  selectOption+="<option>"+i+"</option>";
}


/*for(var i=1; i<60; i++)
{


  selectOptionMM+="<option>"+i+"</option>";
}*/

  $('#hhTime').html(selectOption);
  //$('#mmTime').html(selectOptionMM);
    
	// $('#cutoff_time').timepicker({timeFormat: "hh:mm tt",use24hours: true});
	
  });
					setTimeout(function(){

				//getState(cityAndState);
				$("#apply_vehicle_type").multiselect('destroy');
					$('#apply_vehicle_type').multiselect({
					maxHeight: 200,
					buttonWidth: '155px',
					includeSelectAllOption: true
					});
				$("#value").multiselect('destroy');
					$('#value').multiselect({
					maxHeight: 200,
					buttonWidth: '155px',
					includeSelectAllOption: true
					});
				$("#apply_sma").multiselect('destroy');
					$('#apply_sma').multiselect({
					maxHeight: 200,
					buttonWidth: '155px',
					includeSelectAllOption: true
					});					
				}
				,900);
</script> 

<!--<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/getairports.js"></script> 
<script src="pageJs/getState.js"></script> 
<script src="pageJs/getSeaport.js"></script> 
<script src="pageJs/getStatePopup.js"></script> 
<script src="pageJs/getVehicle.js"></script> 
<script src="pageJs/getService.js"></script> 
<script src="pageJs/peakhour.js"></script> 
<script src="pageJs/extrachild.js"></script> --> 

<!-- Loading Indicator Ends-->
</body>
</html>
