<?php
include_once 'session_auth.php';

$_page = 'sosp';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Social OAuth Setup</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="assets/jvector-map/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="css/clndr.css" rel="stylesheet">
    <!--clock css-->
    <link href="assets/css3clock/css/style.css" rel="stylesheet">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="assets/morris-chart/morris.css">
    <link rel="stylesheet" href="build/css/intlTelInput.css">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
        .form-control {
            border: 1px solid #e2e2e4;
            box-shadow: none;
            color: #847F7F;
        }

        #setting_mode .notActive {
            color: #074759;
            background-color: #fff;
        }

        #setting_mode .active {
            color: #fff;
            background-color: #074759 !important;
        }

        #map_mode .notActive {
            color: #074759;
            background-color: #fff;
        }

        #map_mode .active {
            color: #fff;
            background-color: #074759 !important;
        }
    </style>
</head>
<body>

<!--header start-->
<?php include_once './global/header.php'; ?>
<!--header end-->

<!--sidebar start-->
<?php include_once './global/sideNav.php'; ?>
<!-- sidebar menu end-->

<section id="main-content">
    <section class="wrapper">
        <div class="panel-body">
            <form onsubmit="UpdateSocialInfo(event)" class="form-horizontal bucket-form" method="get">
                <br>
                <div class="form-group">
                    <div class="col-sm-12">
                        <h3 class="text-center">Social OAuth Setup</h3>
                        <br>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">Facebook APP ID</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="fb_id">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Facebook Secret key</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="fb_secret">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">Google APP ID</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="google_id">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Google Secret key</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="google_secret">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Google Map API</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="google_map">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">Linked In APP ID</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="linked_id">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Linked In Secret key</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="linked_secret">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                </div>
                
            </form>
        </div>
    </section>
</section>

<!--Loading indicator-->
<div style="position: fixed; top: 0px; z-index: 10000; text-align: center; width: 100%; height: 100%; display: none;"
     id="refresh_overlay">
    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%; display:table-row">
                <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto"><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="loadingGIF"><img style="height:70px;width:70px;" src="images/loading.gif"
                                                         alt="Page loading indicator"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
<script src="js/lib/jquery.js"></script>
<script src="pageJs/dashboard.js"></script>
<script src="pageJs/logout.js"></script>
<!-- <script src="assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>  -->
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>
<!--[if lte IE 8]>
<script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="assets/skycons/skycons.js"></script>
<script src="assets/jquery.scrollTo/jquery.scrollTo.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="assets/calendar/clndr.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="assets/calendar/moment-2.2.1.js"></script>
<script src="js/calendar/evnt.calendar.init.js"></script>
<script src="assets/jvector-map/jquery-jvectormap-1.2.2.min.js"></script>
<script src="assets/jvector-map/jquery-jvectormap-us-lcc-en.js"></script>
<script src="assets/gauge/gauge.js"></script>
<!--clock init-->
<script src="assets/css3clock/js/script.js"></script>
<!--Easy Pie Chart-->
<script src="assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="assets/sparkline/jquery.sparkline.js"></script>
<!--Morris Chart-->
<script src="assets/morris-chart/morris.js"></script>
<script src="assets/morris-chart/raphael-min.js"></script>
<!--jQuery Flot Chart-->
<script src="assets/flot-chart/jquery.flot.js"></script>
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="assets/flot-chart/jquery.flot.resize.js"></script>
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="assets/flot-chart/jquery.flot.animator.min.js"></script>
<script src="assets/flot-chart/jquery.flot.growraf.js"></script>
<script src="js/custom-select/jquery.customSelect.min.js"></script>
<!--common script init for all pages-->
<script src="build/js/intlTelInput.js"></script>
<script src="js/scripts.js"></script>
<script src="pageJs/UserProfile.js"></script>

<script>
var _SERVICEPATH = "phpfile/client.php";

function getSocialInfo(){
    $('#refresh_overlay').show();
    $.ajax({
        url: _SERVICEPATH,
        type: 'POST',
        data: {action : 'getSocialInfo'},
        success: function (response) {
            $('#refresh_overlay').hide();
            var res = JSON.parse(response);
            console.log(res);
            $('#fb_id').val(res.fb_id);
            $('#fb_secret').val(res.fb_secret);
            $('#google_id').val(res.google_id);
            $('#google_secret').val(res.google_secret);
            $('#google_map').val(res.google_map);
            $('#linked_id').val(res.linked_id);
            $('#linked_secret').val(res.linked_secret);
        }

    });
}
function UpdateSocialInfo(e){
    e.preventDefault();
    $.ajax({
        url: _SERVICEPATH,
        type: 'POST',
        data: {
            action : 'saveSocialInfo',
            fb_id : $('#fb_id').val(),
            fb_secret : $('#fb_secret').val(),
            google_id : $('#google_id').val(),
            google_secret : $('#google_secret').val(),
            google_map : $('#google_map').val(),
            linked_id : $('#linked_id').val(),
            linked_secret : $('#linked_secret').val()
        },
        success: function (response) {
            $('#refresh_overlay').hide();
            alert('Information has been updated successfully');
        }

    });
}

$(function(){
    getSocialInfo();
});

    $('#setting_mode a').on('click', function () {
        var setting_sel = $(this).data('title');
        var setting_tog = $(this).data('toggle');

        $('#' + setting_tog).prop('value', setting_sel);

        $('a[data-toggle="' + setting_tog + '"]').not('[data-title="' + setting_sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + setting_tog + '"][data-title="' + setting_sel + '"]').removeClass('notActive').addClass('active');
    });
    $('#map_mode a').on('click', function () {
        var setting_sel = $(this).data('title');
        var setting_tog = $(this).data('toggle');

        $('#' + setting_tog).prop('value', setting_sel);

        $('a[data-toggle="' + setting_tog + '"]').not('[data-title="' + setting_sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + setting_tog + '"][data-title="' + setting_sel + '"]').removeClass('notActive').addClass('active');
    });
</script>
</body>
</html>