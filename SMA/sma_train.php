<?php
include_once 'session_auth.php';

$_page = 'tDB';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="images/favicon.png">
<title>My Limo Project | Seaport Zone Database Setup</title>
<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-reset.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<style>
#sma_train_list td {
	text-align: center;
}
ul.vehicle-list {
	padding: 0;
	margin: 0;
	list-style: none;
	margin-top: 15px
}
ul.vehicle-list li a {
	display: block;
	border-bottom: 1px dotted #CFCFCF;
	padding: 10px;
	text-decoration: none
}
ul.vehicle-list li:first-child a {
	border-top: 1px dotted #CFCFCF
}
ul.vehicle-list li a:hover {
	background-color: #DBF9FF
}
ul.vehicle-list li span {
	display: block
}
ul.vehicle-list li span.vehicle-name {
	font-weight: 600;
	color: #8F8F8F
}
ul.vehicle-list li span.vehicle-tier-count {
	color: #8C8C8C
}
.top-links a {
	font-size: 12px
}
.top-links {
	text-align: right
}
div.rate-group-selector {
	display: inline-block
}
div.vehicle-list .panel-default>.panel-heading {
	background-color: transparent;
	padding: 15px
}
div.vehicle-list .panel-default {
	border-color: transparent
}
div.vehicle-list .panel {
	border: none;
	box-shadow: none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	-o-box-shadow-none: none
}
#vehicle-list h4.panel-title {
	font-style: normal;
	font-size: 14px
}
div.vehicle-list .panel-group .panel+.panel {
	margin-top: 0px
}
.input-xs {
	height: 22px;
	padding: 5px 5px;
	font-size: 12px;
	line-height: 1.5;
	border-radius: 3px
}
div.vehicle-list .table thead th {
	padding-bottom: 5px !important;
	text-transform: none;
	color: #898989;
	font-weight: normal
}
div.vehicle-list .table>tbody>tr>td, div.vehicle-list .table thead {
	border-top: none
}
div.vehicle-list .table-condensed>tbody>tr>td {
	padding: 2px
}
.table-action {
	text-align: right;
	font-size: 14px
}
.editable-text {
	width: 30%
}
.set {/* position:relative; *//* width:100%; *//* height:auto; *//* background-color:#f5f5f5 */
}
.set>a {
	display: block;
	padding: 20px 15px;
	text-decoration: none;
	color: #555;
	font-weight: 600;
	border-bottom: 1px solid #ddd;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	transition: all 0.2s linear
}
.set>a:hover {
	background-color: cornsilk
}
.set>a i {
	position: relative;
	float: right;
	margin-top: 4px;
	color: #666
}
.set>a.active {
	background-color: #1fb5ad;
	color: #fff
}
.set>a.active i {
	color: #fff
}
.acc-content {
	position: relative;
	width: 100%;
	height: auto;
	background-color: #fff;
}
.form-control {
	color: black;
}
.serviceType_css {
	margin-top: 8%;
}
.serviceRate_css {
	margin-top: 3%;
}
#add_point_rate_to option {
	max-height: 10px !important;
}
select[multiple] {
	height: 35px;
}
.loadingGIF {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	width: 100%;
	padding-left: 30%;
	padding-top: 10%;
}
.alignCENTER {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}
</style>
</head>

<body>
<section id="container" >

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
  <section id="main-content">
    <section class="wrapper"> 
      <!-- page start-->
      <div class="row">
        <div class="col-md-12">
          <h4 class="page-title col-md-12 alignCENTER"> CREATE TRAIN STATION ZONE DATABASE </h4>
        </div>
        <!-- upload file comment --> 
        <!--  <div class="col-sm-3" style=" margin-top: 12px;display:none">Upload Location DataBase</div>
                  <div class="col-sm-3">
                <input type="file" name="smaCsvFile" class="btn btn-default add_color smaCsvFile" accept=".csv" value="browse" id="smaCsvFile" style="padding: 1%;display:none" seq="">
              </div> -->
        <div class="col-sm-3" style="display:none">
          <input type="submit" class="btn btn-default add_color uploadSmaCsv" value="Upload" name="submit" style="padding:1%;margin-left:11%;" onclick="uploadSmaCsv('+responseObj.data[i].id+')">
        </div>
      </div>
      <div class="row" >
        <div class="col-md-12" > 
          
          <!--  copy train all data into all field start here-->
          
          <div class=" row">
            <div class="col-xs-4" style="text-align:left"> <b>Copy From Existing Train Matrix? &nbsp;&nbsp;&nbsp;&nbsp;</b>
              <input type="radio" value="yes" id="train_copy_existing" name="train_copy_existing">
              YES&nbsp;&nbsp;
              <input type="radio" value="no" id="train_copy_existing" checked="" name="train_copy_existing">
              NO </div>
            <!-- col-xs-4-->
            
            <div class="col-xs-6" style="visibility: hidden;" id="train_db_select_div"><b style="margin:0% 20% 0% 2%;">Select Matrix</b>
              <select style="height:24px;border-radius:4px;width:36%;" id="select_matrix_db_train">
              </select>
              <button type="button" id="copy_rate_matrix_train" class="btn btn-primary" style="float:right" >Copy</button>
            </div>
            <br>
            <br>
          </div>
          
          <!-- copy tain all data into all field end here --> 
          
          <!-- general form elements -->
          <div class="box box-info" style="overflow:hidden;"> 
            <!-- form start -->
            <form id="trainZoneForm" method="post" >
              <div class="box-body row cl-sm-12">
                <div class="col-xs-3" style="text-align:center" > <b> Select SMA</b> <br>
                  <select id="sma_in_db" class="form-control add_sma_country" required>
                    <option value="">-- SELECT SMA --</option>
                  </select>
                </div>
                <div class="col-xs-3 " style="text-align:center"> <b>Station Zip Code</b> <br>
                  <input type="text" id="train_zipcode"  style="height: 33px;" required>
                </div>
                <div class="col-xs-3 " style="text-align:center"> <b> Station Name</b> <br>
                  <input type="text" id="train_name" disabled style="height: 33px;" required>
                </div>
                <div class="col-xs-3 " style="text-align:center" > <b> Station Code</b> <br>
                  <input type="text" id="train_code" disabled style="height: 33px;" required >
                </div>

                <div class="clearfix" style="clear:both"></div>
                <div style="margin:20px 0px">
                  <div class="col-xs-6 "> <b> Optional Train Station Pickup Schg:</b>
                    <input type="text" id="train_schg" style="height: 33px; width:50%">
                  </div>
                  <div class="col-xs-6 "> <b> Q & R Disclaimer:</b>
                    <input type="text" id="qr_disclaimer" style="height: 33px; width:50%">
                  </div>
                </div>
              </div>
              
              <!--    <div class="row" style="display:none">
              <b class="col-xs-2">Service Market Area</b>
              	<div class="col-xs-3">
                
                <input type="text" id="add_sma_name" class="form-control" >
                </div>
                
                <div class ="col-xs-3"><b>Set Default </b>
                <input type="checkbox" id="set_default"/>
                <input type="hidden" id="hidden_sma_id"/>
                <input type="hidden" id="old_sma_name"/>
                </div>
              </div> --> 
              <br>
              <div class="box-footer" style="text-align: center; padding-top: 4%;">
                <button type="submit" id="add_train_location" class="btn btn-primary">Submit</button>
                <button type="button" id="back_train_location" class="btn btn-primary" >Back</button>
              </div>
              
              <!-- /.box-body -->
              
            </form>
          </div>
          <br>
          <br>
          <br>
        </div>
      </div>
      
      <!-- page end--> 
    </section>
  </section>
  
  <!--main content end--> <!-- Right Side Bar Goes Here if Required 
<div class="right-sidebar">
<div class="right-stat-bar"> </div>
</div> --></section>

<!--end of add sma popup--> 

<!-- Placed js at the end of the document so the pages load faster --> 

<!--Core js--> 

<!--Loading indicator-->
<!--Loading indicaator-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;" id="refresh_overlay">
  <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%;display:table-row">
        <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; " >
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;"> 
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>    
              <div class="loadingGIF"> <img style="height:70px;width:70px;" src="images/loading.gif" alt="Page loading indicator"> </div>

           </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Loading Indicator Ends--> 
<script src="js/lib/jquery.js"></script> 
<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/logout.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script src="js/bootstrap-multiselect.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 
<!--Easy Pie Chart--> 
<script src="assets/easypiechart/jquery.easypiechart.js"></script> 
<!--Sparkline Chart--> 
<script src="assets/sparkline/jquery.sparkline.js"></script> 
<!--jQuery Flot Chart--> 
<script src="pageJs/sma_train.js?js=<?php echo uniqid(); ?>"></script> 
<script src="assets/flot-chart/jquery.flot.js"></script> 
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script> 
<script src="assets/flot-chart/jquery.flot.resize.js"></script> 
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script> 

<!--dynamic table--> 
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script> 

<!--dynamic table initialization --> 
<script src="js/dynamic_table/dynamic_table_init.js"></script> 

<!--common script init for all pages--> 
<script src="js/scripts.js"></script> 
<script src="pageJs/searchbox.js"></script> 
<script type="text/javascript">
  // $("table.sieve").sieve();




      $("input[name='train_copy_existing']").click(function(){

        

    if($("input[name='train_copy_existing']:checked").val()=="yes")

    {

      

      $('#train_db_select_div').css("visibility","visible");

     

    }

  else{

      $('#train_db_select_div').css("visibility","hidden");

      

    

    }

  });






$('#back_train_location').on("click",function(){

  window.location.href="train_db.php";


});

// $("#train_zipcode,#train_name,#train_code").keyup(function(e){
//     name=$(this).val();
//       var re = /\,|\(|\)|\{|\}|\[|\]|\-|\+|\*|\%|\/|\=|\"|\'|\~|\!|\&|\||\<|\>|\?|\:|\;|\.| /;
//       if(re.test(name))
//       {
//            name=name.replace(/[^a-zA-Z 0-9]+/g,'');
//            $(this).val(name);
//       }
        
// })




</script> 
<!--<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/getairports.js"></script> 
<script src="pageJs/getState.js"></script> 
<script src="pageJs/getSeaport.js"></script> 
<script src="pageJs/getStatePopup.js"></script> 
<script src="pageJs/getVehicle.js"></script> 
<script src="pageJs/getService.js"></script> 
<script src="pageJs/peakhour.js"></script> 
<script src="pageJs/extrachild.js"></script> --> 

<!-- Loading Indicator Ends-->
</body>
</html>
