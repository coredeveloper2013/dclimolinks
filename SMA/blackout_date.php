<?php
include_once 'session_auth.php';

$_page = 'bes';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Back Out Date Setup</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">

    <!--dynamic table-->
    <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet"/>
    <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css"/>

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap-multiselect.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" media="screen" href="css/timepicker/bootstrap-datetimepicker.min.css"/>
    <link href="css/blackout_date.css" rel="stylesheet"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="JQ_timepicker/jquery_addon_css.css">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
        .loadingGIF {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width: 100%;
            padding-left: 30%;
            padding-top: 10%;
        }
        .form-group{
            width: 100%;
            display: table;
            margin-bottom: 5px;
        }
    </style>
</head>

<body>
<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-md-6">
                    <h4 class="page-title "> Blackout/Special Event Setup </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-info" style="height:400px;">
                        <!-- form start -->
                        <form onsubmit="return false;">
                            <div class="box-body row">
                                <div class="col-xs-12" style="text-align:center">
                                    <h4><strong><span class="page-title ">Set Up BlackOut/Special Event Date</span>
                                        </strong></h4>
                                    <br>
                                </div>
                                <!-- col-xs-12-->
                            </div>
                            <div class=" row">
                                <div class="col-md-6">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-3" style="margin-top: 5px">Blackout Date Name</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="value" placeholder="Blackout Date Name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-3" style="margin-top: 5px">Set Start Calender</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="JQdatepicker" placeholder="Set Start Calender">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-3" style="margin-top: 5px">Start Time</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="start_time" id="start_time" placeholder="Start Time">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-3" style="margin-top: 5px">Set End Calender</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="endCalenderSet" placeholder="Set End Calender">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-3" style="margin-top: 5px">End Time</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="end_time" id="end_time" placeholder="End Time">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-3" style="margin-top: 5px">Message</label>
                                            <div class="col-md-9">
                                                <textarea rows="3" class="form-control " id="blackout_msg" placeholder="Message"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-3" style="margin-top: 5px">Applies to Vehicle Type </label>
                                            <div class="col-md-9">
                                                <select id="apply_vehicle_type" multiple class="form-control ">
                                                    <option>--Select--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-3" style="margin-top: 5px">Associate with SMA</label>
                                            <div class="col-md-9">
                                                <select id="apply_sma" multiple class="form-control ">
                                                    <option>Select SMA</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-3" style="margin-top: 5px">Service Type</label>
                                            <div class="col-md-9">
                                                <select id="apply_service_type" multiple class="form-control ">
                                                    <option>Select Service Type</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Apply to vehicle type-->
                            <div class="row"><br>
                                <br>
                                <br>
                            </div>
                            <!-- Associate with SMA -->

                            <br>
                            <div class="row">
                                <div class="col-xs-12 " style="text-align:center">
                                    <button type="button" id="save_rate" class="btn btn-primary" style="width:10%;">
                                        Save
                                    </button>
                                    <button type="button" id="back_button" class="btn btn-primary"
                                            style="display:none;"> Back
                                    </button>
                                </div>
                            </div>
                            <!-- row save matrix button-->

                            <!-- /.box-body -->

                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div data-ng-controller="SharedData" class="m-t-md ng-scope">
                        <!--#########################################point to point ####################################################-->
                        <div>
                            <div class="row">
                                <div class="col-xs-12"></div>
                            </div>
                            <div class="row mb-sm ng-scope">
                                <div class="col-sm-12"></div>
                            </div>
                            <div style="overflow:scroll;min-height:500px;">
                                <table class="table table-condensed ng-scope sieve">
                                    <thead>
                                    <tr>
                                        <th style="text-align:center">Blackout Date Name</th>
                                        <th style="text-align:center">Start Date</th>
                                        <th style="text-align:center">Start Time</th>
                                        <th style="text-align:center">End Date</th>
                                        <th style="text-align:center">End Time</th>
                                        <th style="text-align: center;padding-right: 2%;">Apply To Vehicle</th>
                                        <th style="text-align: center;padding-right: 2%;">Associate with SMA</th>
                                        <th style="text-align: center;padding-right: 2%;">Service Type</th>
                                        <th style="text-align:center">Action</th>
                                        <th class="table-action"><span class="table-action"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody id="rate_matrix_list">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ############################## additional rate####################################################-->

            <!--##################finish###########################-->

            <!-- page end-->
        </section>
    </section>

    <!--main content end--> <!-- Right Side Bar Goes Here if Required
<div class="right-sidebar">
<div class="right-stat-bar"> </div>
</div> --></section>

<!--end of add sma popup-->

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->

<!--Loading indicator-->
<!--Loading indicaator-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;"
     id="refresh_overlay">
    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%;display:table-row">
                <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; ">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="loadingGIF"><img style="height:70px;width:70px;" src="images/loading.gif"
                                                         alt="Page loading indicator"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- Loading Indicator Ends-->
<script src="js/lib/jquery.js"></script>
<script src="pageJs/dashboard.js"></script>
<script src="pageJs/logout.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="js/moment.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>

<!--jQuery Flot Chart-->

<script src="pageJs/searchbox.js"></script>
<script src="pageJs/blackout_date.js?js=<?php echo uniqid() ?>"></script>

<!--dynamic table-->
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="JQ_timepicker/jquery_addon_js.js"></script>
<script src="JQ_timepicker/slideaccess.js"></script>
<script>
    $("table.sieve").sieve();
</script>
</body>
</html>
