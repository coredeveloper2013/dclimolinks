<?php
include_once 'session_auth.php';

$_page = 'smC';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="images/favicon.png">
<title>My Limo Project | City Zone Database Setup</title>

<!--Core CSS -->
<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-reset.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

<!--dynamic table-->
<link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />

<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<link href="css/limo_car.css" rel="stylesheet">
<link href="css/bootstrap-multiselect.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />
<script src="js/lib/jquery.js"></script>
<script src="pageJs/validation.js"></script>
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<style>
ul.vehicle-list {
	padding: 0;
	margin: 0;
	list-style: none;
	margin-top: 15px
}
ul.vehicle-list li a {
	display: block;
	border-bottom: 1px dotted #CFCFCF;
	padding: 10px;
	text-decoration: none
}
ul.vehicle-list li:first-child a {
	border-top: 1px dotted #CFCFCF
}
ul.vehicle-list li a:hover {
	background-color: #DBF9FF
}
ul.vehicle-list li span {
	display: block
}
ul.vehicle-list li span.vehicle-name {
	font-weight: 600;
	color: #8F8F8F
}
ul.vehicle-list li span.vehicle-tier-count {
	color: #8C8C8C
}
.top-links a {
	font-size: 12px
}
.top-links {
	text-align: right
}
div.rate-group-selector {
	display: inline-block
}
div.vehicle-list .panel-default>.panel-heading {
	background-color: transparent;
	padding: 15px
}
div.vehicle-list .panel-default {
	border-color: transparent
}
div.vehicle-list .panel {
	border: none;
	box-shadow: none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	-o-box-shadow-none: none
}
#vehicle-list h4.panel-title {
	font-style: normal;
	font-size: 14px
}
div.vehicle-list .panel-group .panel+.panel {
	margin-top: 0px
}
.input-xs {
	height: 22px;
	padding: 5px 5px;
	font-size: 12px;
	line-height: 1.5;
	border-radius: 3px
}
div.vehicle-list .table thead th {
	padding-bottom: 5px !important;
	text-transform: none;
	color: #898989;
	font-weight: normal
}
div.vehicle-list .table>tbody>tr>td, div.vehicle-list .table thead {
	border-top: none
}
div.vehicle-list .table-condensed>tbody>tr>td {
	padding: 2px
}
.open>.dropdown-menu {
	/* max-height: 138px !important;*/

	z-index: 9999;
}
.table-action {
	text-align: right;
	font-size: 14px
}
.editable-text {
	width: 30%
}
.set {/* position:relative; *//* width:100%; *//* height:auto; *//* background-color:#f5f5f5 */
}
.set>a {
	display: block;
	padding: 20px 15px;
	text-decoration: none;
	color: #555;
	font-weight: 600;
	border-bottom: 1px solid #ddd;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	transition: all 0.2s linear
}
#sma_loc_list td {
	text-align: center;
}
.set>a:hover {
	background-color: cornsilk
}
.set>a i {
	position: relative;
	float: right;
	margin-top: 4px;
	color: #666
}
.set>a.active {
	background-color: #1fb5ad;
	color: #fff
}
.set>a.active i {
	color: #fff
}
.acc-content {
	position: relative;
	width: 100%;
	height: auto;
	background-color: #fff;
}
.form-control {
	color: black;
}
.serviceType_css {
	margin-top: 8%;
}
.serviceRate_css {
	margin-top: 3%;
}
#add_point_rate_to option {
	max-height: 10px !important;
}
select[multiple] {
	height: 35px;
}
#sma_loc_list select {
	width: 55%;
}
.loadingGIF {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	width: 100%;
	padding-left: 30%;
	padding-top: 10%;
}
.alignCENTER {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}
.chkmrk-icon {
	width: 30px;
	height: 30px;
	margin-top: 3px;
}
 @media screen and (min-width: 1366px) {
.chkmrk-icon {
	margin-top: 0px;
}
}
</style>
</head>

<body>
<section id="container" >

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
  <section id="main-content">
    <section class="wrapper"> 
      <!-- page start-->
      <div class="row">
        <div class="col-md-12">
          <h4 class="page-title col-md-12 alignCENTER"> CREATE CITY ZONE DATABASE </h4>
        </div>
        <div class="col-sm-3" style=" margin-top: 12px;display:none;">Upload Location DataBase</div>
        <div class="col-sm-3" style="display:none">
          <input type="file" name="smaCsvFile" class="btn btn-default add_color smaCsvFile" accept=".csv" value="browse" id="smaCsvFile" style="padding: 1%;" seq="">
        </div>
        <div class="col-sm-3" style="display:none">
          <input type="submit" class="btn btn-default add_color uploadSmaCsv" value="Upload" name="submit" style="padding:1%;margin-left:11%;" onclick="uploadSmaCsv('+responseObj.data[i].id+')">
        </div>
      </div>
      <div class="row" >
        <div class="col-md-12" > 
          <!-- general form elements -->
          <div class="box box-info" style="height:200px; "> 
            <!-- form start -->
            <form id="cityAddForm" method="post" >
              <div class="box-body row col-sm-10">
                <div class="col-xs-3" style="text-align:center" > <b> Select&nbsp;SMA </b> <br>
                  <!-- <select id="zone_setup_country" name="sma_country" class="form-control add_sma_country" required> -->
                  
                  <select id="zone_setup_sma" name="zone_sma" class="form-control add_sma_country" required>
                  <option value="">-- SELECT SMA --</option>
                  </select>
                </div>
                <div class="col-xs-2" style="text-align:center"> <b>Select&nbsp;City</b> <br>
                  
                  <!--  <select id="zone_setup_state" name="sma_state" class="form-control " style="background-color:#b0b5b9;color:#fff;" multiple="multiple" required>
              </select> -->
                  
                  <select id="zone_setup_city" name="zone_city" class="form-control " style="background-color:#b0b5b9;color:#fff;" multiple="multiple" required>
                  </select>
                </div>
                <div class="col-xs-1" style="padding-top:20px; padding-right:30px;cursor:pointer; margin-left:6%" id="sma_state_check"> <img src="images/ok_checkmark_red_T.png" alt=""   class="chkmrk-icon" style="margin-left:1%"/> </div>
                <div class="col-xs-2 " style="text-align:center"><b> Zip&nbsp;Codes</b> <br>
                  <!-- <select id="zone_setup_zipcode" name="sma_county" class="form-control " style="background-color:#b0b5b9;color:#fff;"multiple="multiple" required >
              </select> -->
                  
                  <select id="zone_setup_zipcode" name="zone_zipcode" class="form-control " style="background-color:#b0b5b9;color:#fff;" multiple="multiple" required >
                  </select>
                </div>
                

              </div>
              <br>
              <br>
                       
              <div class="box-footer col-xs-12 col-sm-10 col-md-10 col-lg-10" style="margin-top:25px; text-align:center;">
<div class="col-xs-8 col-sm-6 col-md-6 col-lg-6" style="text-align:center" > <span style="font-weight:700; text-wrap:none; padding:0px;"> Save City Zone&nbsp;as:</span> <br>
                  <input type="text" name="saveZone" id="saveZone" style="height: 34px; width:90%" required>
                  
                </div>              
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3" style="text-align:left; margin-top: 18px;"><button type="submit" id="add_sma_location" class="btn btn-primary">Submit</button>
                <button type="button" id="back_sma_location" class="btn btn-primary" style="display:none;">Back</button></div>
                <br>
              </div>
              
              <!-- /.box-body -->
              
            </form>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div data-ng-controller="SharedData" class="m-t-md ng-scope"> 
            <!--#########################################point to point ####################################################-->
            <div id="sma_lists">
              <div class="row">
                <div class="col-xs-12"></div>
              </div>
              <div class="row mb-sm ng-scope">
                <div class="col-sm-12"></div>
              </div>
              <div style="overflow:scroll;min-height:500px;">
                <table class="table table-condensed ng-scope sieve">
                  <thead >
                    <tr >
                      <th>Associate with SMA</th>
                      <th>Zone Name</th>
                      <th>Zone City/Cities</th>
                      <th>Zone Zip Code</th>
                      <th>View</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="sma_City_list">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- ############################## additional rate####################################################--> 
      
      <!--##################finish###########################--> 
      
      <!-- page end--> 
    </section>
  </section>
  
  <!--main content end--> <!-- Right Side Bar Goes Here if Required 
<div class="right-sidebar">
<div class="right-stat-bar"> </div>
</div> --></section>

<!--end of add sma popup--> 

<!--Loading indicaator-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;" id="refresh_overlay">
  <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%;display:table-row">
        <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; " >
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;"> 
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>                 
              <div class="loadingGIF"> <img style="height:70px;width:70px;" src="images/loading.gif" alt="Page loading indicator"> </div>

           </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/logout.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script src="js/bootstrap-multiselect.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 
<script src="pageJs/sma_city.js?limo"></script>
<script src="pageJs/searchbox.js"></script> 
<script src="js/scripts.js"></script> 

<!-- Loading Indicator Ends--> 

<script type="text/javascript">

   $("table.sieve").sieve();
   $('#back_sma_location').on("click",function(){

    window.location.reload();


   });

</script>
</body>
</html>
