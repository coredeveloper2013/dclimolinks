<?php
include_once 'session_auth.php';

$_page = 'airDB';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="images/favicon.png">
<title>My Limo Project | Airport Zone Database Setup</title>
<!--Core CSS -->
<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-reset.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<!--dynamic table-->

<link href="css/style.css" rel="stylesheet">
<style>
#sma_loc_list td {
	text-align: center;
}
ul.vehicle-list {
	padding: 0;
	margin: 0;
	list-style: none;
	margin-top: 15px
}
ul.vehicle-list li a {
	display: block;
	border-bottom: 1px dotted #CFCFCF;
	padding: 10px;
	text-decoration: none
}
ul.vehicle-list li:first-child a {
	border-top: 1px dotted #CFCFCF
}
ul.vehicle-list li a:hover {
	background-color: #DBF9FF
}
ul.vehicle-list li span {
	display: block
}
ul.vehicle-list li span.vehicle-name {
	font-weight: 600;
	color: #8F8F8F
}
ul.vehicle-list li span.vehicle-tier-count {
	color: #8C8C8C
}
.top-links a {
	font-size: 12px
}
.top-links {
	text-align: right
}
div.rate-group-selector {
	display: inline-block
}
div.vehicle-list .panel-default>.panel-heading {
	background-color: transparent;
	padding: 15px
}
div.vehicle-list .panel-default {
	border-color: transparent
}
div.vehicle-list .panel {
	border: none;
	box-shadow: none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	-o-box-shadow-none: none
}
#vehicle-list h4.panel-title {
	font-style: normal;
	font-size: 14px
}
div.vehicle-list .panel-group .panel+.panel {
	margin-top: 0px
}
.input-xs {
	height: 22px;
	padding: 5px 5px;
	font-size: 12px;
	line-height: 1.5;
	border-radius: 3px
}
div.vehicle-list .table thead th {
	padding-bottom: 5px !important;
	text-transform: none;
	color: #898989;
	font-weight: normal
}
td, th {
	height: 50px;
	width: 13%;
}
div.vehicle-list .table>tbody>tr>td, div.vehicle-list .table thead {
	border-top: none
}
div.vehicle-list .table-condensed>tbody>tr>td {
	padding: 2px
}
.table-action {
	text-align: right;
	font-size: 14px
}
.editable-text {
	width: 30%
}
.set {/* position:relative; *//* width:100%; *//* height:auto; *//* background-color:#f5f5f5 */
}
.set>a {
	display: block;
	padding: 20px 15px;
	text-decoration: none;
	color: #555;
	font-weight: 600;
	border-bottom: 1px solid #ddd;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	transition: all 0.2s linear
}
.set>a:hover {
	background-color: cornsilk
}
.set>a i {
	position: relative;
	float: right;
	margin-top: 4px;
	color: #666
}
.set>a.active {
	background-color: #1fb5ad;
	color: #fff
}
.set>a.active i {
	color: #fff
}
.acc-content {
	position: relative;
	width: 100%;
	height: auto;
	background-color: #fff;
}
.form-control {
	color: black;
}
.serviceType_css {
	margin-top: 8%;
}
.serviceRate_css {
	margin-top: 3%;
}
#add_point_rate_to option {
	max-height: 10px !important;
}
select[multiple] {
	height: 35px;
}
.loadingGIF {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	width: 100%;
	padding-left: 30%;
	padding-top: 10%;
}
.alignCENTER {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}
.form-control1 {
	color: black;
}
</style>
</head>

<body>
<section id="container" >

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
  <section id="main-content">
    <section class="wrapper" style="overflow:auto"> 
      <!-- page start-->
      <div class="row">
        <div class="col-md-12">
          <h4 class="page-title col-md-12 alignCENTER" style="margin-bottom:30px"> CREATE AIRPORT ZONE DATABASE </h4>
        </div>
        <div class="col-sm-3" style=" margin-top: 12px;display:none">Upload Location DataBase</div>
        <div class="col-sm-3">
          <input type="file" name="smaCsvFile" class="btn btn-default add_color smaCsvFile" accept=".csv" value="browse" id="smaCsvFile" style="padding: 1%;display:none" seq="">
        </div>
        <div class="col-sm-3" style="display:none">
          <input type="submit" class="btn btn-default add_color uploadSmaCsv" value="Upload" name="submit" style="padding:1%;margin-left:11%;" onclick="uploadSmaCsv('+responseObj.data[i].id+')">
        </div>
      </div>
      <div class="row" >
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" > 
          <!-- general form elements -->
          
          <div class="box box-info" > 
            <!-- form start -->
            <form id="airportZoneForm" method="post" >
              <div class="box-body row col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom:25px">
                <div class="col-xs-3" style="text-align:center" > <b> Select SMA</b> <br>
                  <select id="add_sma_country_1" class="form-control add_sma_country" required >
                    <option value="">-- SELECT SMA --</option>
                  </select>
                </div>
                <div class="col-xs-3 " style="text-align:center"> <b>Airport Zip Code</b> <br>
                  <input type="text" id="airport_zipcode"  style="height: 33px;" required>
                </div>
                <div class="col-xs-3 " style="text-align:center"> <b> Airport Name</b> <br>
                  <input type="text" id="airport_name"  style="height: 33px;" required>
                </div>
                <div class="col-xs-3 " style="text-align:center" > <b> Airport Code</b> <br>
                  <input type="text" id="airport_code" style="height: 33px;" required maxlength="4">
                </div>
                <div class="col-xs-3" style="width:24%;text-align:center;display:none"> <b>Postal/Zip Code</b>
                  <textarea id="postal_code" class="form-control postal code" placeholder="Postal Code"></textarea>
                </div>
                <br>
                <br>
                <br>
              </div>
              <br>
              <div class="col-xs-5 col-sm-4 col-md-4 col-lg-4" style="text-align:left; margin-left: 0px;"> <b>Set as FBO (Fixed Base Operations)? &nbsp;&nbsp;&nbsp;</b>
                <input type="checkbox" value="yes" id="setFBO" name="FBO_setup">
                <input type="text"  id="FboDesclaimer" name="FboDesclaimer" style="visibility:hidden;height:33px;margin-left: 6%;">
              </div>
              <br>
              <br>
              
              <!--  copy airport all data into all field start here-->
              
              <div class="row" style="margin-bottom:-22px">
                <div class="col-xs-6 col-sm-5 col-md-5 col-lg-5" style="text-align:left; margin-bottom:12px;"> <b>Copy From Existing Airport Matrix? &nbsp;&nbsp;&nbsp;&nbsp;</b>
                  <input type="radio" value="yes" id="airport_copy_existing" name="airport_copy_existing">
                  &nbsp;YES&nbsp;&nbsp;
                  <input type="radio" value="no" id="airport_copy_existing" checked="" name="airport_copy_existing">
                  &nbsp;NO </div>
                <!-- col-xs-4-->
                
                <div class="col-xs-6 col-sm-5 col-md-5 col-lg-5" style="visibility: hidden;text-align:left" id="airport_db_select_div" ><b style="margin-right:20%;">Select Matrix</b>
                  <select style="height:24px;border-radius:4px;width:36%;" id="select_matrix_db">
                  </select>
                  <button type="button" id="copy_rate_matrix" class="btn btn-primary" style="float:right" >Copy</button>
                </div>
                <br>
                <br>
              </div>
              
              <!-- copy airport all data into all field end here --> 
              
              <br>
              <br>
              <div class="row" style="display:none"> <b class="col-xs-2">Service Market Area</b>
                <div class="col-xs-3">
                  <input type="text" id="add_sma_name" class="form-control" >
                </div>
                <div class ="col-xs-3"><b>Set Default </b>
                  <input type="checkbox" id="set_default"/>
                  <input type="hidden" id="hidden_sma_id"/>
                  <input type="hidden" id="old_sma_name"/>
                </div>
              </div>
              <div class="row">
                <div class="container">
                  <div class="col-xs-9 col-sm-8 col-md-8 col-lg-8 section-border" style="margin-bottom:40px">
                    <h4 style="text-align:center;">Airport Fees Setup</h4>
                    <hr style="width:90%; margin:auto">
                    <table  >
                      <tr>
                        <th>Select</th>
                        <th>Vehicle</th>
                        <th>Per hour parking fees</th>
                        <th>Domestic Flt schg</th>
                        <th>Int'l flt schg </th>
                        <th>FHV access fees </th>
                        <th>FBO schg </th>
                      </tr>
                      <tbody class="airportVehicleInfo">
                      </tbody>
                    </table>
                  </div>
                  <div class="col-xs-5 col-sm-4 col-md-4 col-lg-4" style="margin-top: 4.7%; padding-left:40px;">
                    <div class="form-group">
                      <label for="comment">Q&R parking fee disclaimer content: </label>
                      <textarea class="form-control" rows="3" style="width: 300px;height:67px;" id="comment"></textarea>
                    </div>
                    <label for="conciergeFee">Concierge Fee Setup?</label>
                    <input type="checkbox" name="vehicle" class="conciergeFeeCheckBox" value="conciergeFee">
                    <div class="form-group" >
                      <label for="comment">Optional Custom Greeter fee: </label>
                      <input type="text" class="form-control concergeFeesClass" style="width: 70px;" disabled id="greeterFee">
                    </div>
                    <div class="form-group" >
                      <label for="comment">Q&R optional greeter diclaimer content </label>
                      <textarea class="form-control concergeFeesClass" rows="3" disabled style="width: 300px;" id="greeterContent"></textarea>
                    </div>
                  </div>
                </div>
                <div class="container">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h4>Pickup Options<span class="form-group">
                      
                      </span></h4>
                    <div class="form-group">
                      <label for="comment">Inside Meet & Greet </label>
                      <input type="checkbox" style="height: inherit;width: 2.5%; margin-right: 4px;float: left;" class="form-control  insideMG">
                     <textarea rows="3" class="form-control" style="width: 320px;" id="insideMeetGreet" disabled></textarea>
                    </div>
                    <div class="form-group">
                      <label for="comment">Curbside </label>
                      <input type="checkbox" style="height: inherit;width: 2.5%; margin-right: 4px;float: left;" class="form-control curb">
                      <textarea rows="3" class="form-control" style="width: 320px;" id="curbside" disabled></textarea>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin-left:-15%">
                    <h4>Q&R Flight Options:</h4>
                    <div class="form-group">
                      <label for="comment">Domestic Flight </label>
                      <input type="checkbox" style="height: inherit;width: 2.5%; margin-right: 4px;float: left;" class="form-control dmFlight">
                      <textarea rows="3" class="form-control" style="width: 320px;" id="domFlight" disabled></textarea>
                    </div>
                    <div class="form-group">
                      <label for="comment"> Int'l Flight </label>
                      <input type="checkbox" style="height: inherit;width: 2.5%; margin-right: 4px;float: left;" class="form-control iFlight">
                      <textarea rows="3" class="form-control" style="width: 320px;" id="intFlight" disabled></textarea>
                    </div>
                    <div class="form-group" style="display:none">
                      <label for="comment"> Optional FBO Pickup Schg fee</label>
                      <input type="text" class="form-control" style="width: 62px;display: inline-block;" id="schgFee1">
                      <input type="text" class="form-control" style="width: 62px;display: inline-block;" id="schgFee2">
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" id="add_sma_location" style="margin-left: 42%;" class="btn btn-primary">Submit</button>
                <button type="button" id="back_sma_location" class="btn btn-primary" > Back </button>
              </div>
              
              <!-- /.box-body -->
              
            </form>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div data-ng-controller="SharedData" class="m-t-md ng-scope"> 
            <!--#########################################point to point ####################################################-->
            <div id="sma_lists">
              <div class="row">
                <div class="col-xs-12"></div>
              </div>
              <div class="row mb-sm ng-scope">
                <div class="col-sm-12"></div>
              </div>
              <div > 
                
                <!-- temprory table hiddent -->
                <table class="table table-condensed ng-scope sieve" style="display:none;" >
                  <thead >
                    <tr >
                      <th style="text-align:center">Associate with SMA</th>
                      <th style="text-align:center">Airport Name</th>
                      <th style="text-align:center">Airport Code</th>
                      <th style="text-align:center">Airport Zip/Postal Code</th>
                      <th style="text-align:center">view</th>
                      <th style="text-align:center">Action</th>
                    </tr>
                  </thead>
                  <tbody id="sma_airport_list">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- ############################## additional rate####################################################--> 
      
      <!--##################finish###########################--> 
      
      <!-- page end--> 
    </section>
  </section>
  
  <!--main content end--> <!-- Right Side Bar Goes Here if Required 
<div class="right-sidebar">
<div class="right-stat-bar"> </div>
</div> --></section>

<!--end of add sma popup--> 

<!--Loading indicaator-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;" id="refresh_overlay">
  <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%;display:table-row">
        <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; " >
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;"> <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <div class="loadingGIF"> <img style="height:70px;width:70px;" src="images/loading.gif" alt="Page loading indicator"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="js/lib/jquery.js"></script> 
<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/logout.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 
<script src="js/scripts.js"></script> 
<script src="pageJs/sma_airport.js?js=<?php echo uniqid(); ?>"></script> 
<script src="pageJs/searchbox.js"></script> 
<script>
	
  // $("table.sieve").sieve();


$('.conciergeFeeCheckBox').on("change",function(){


if($(this).is(':checked'))
{

  $('.concergeFeesClass').removeAttr("disabled").val('');

}
else
{


  $('.concergeFeesClass').prop("disabled",true).val('');


}

})



  $('.insideMG').on("change",function(){

    if($(this).is(":checked"))
    { 
        $('#insideMeetGreet').prop("required","true").removeAttr("disabled").val('');
    }
    else
    {
        $('#insideMeetGreet').prop("disabled","true").removeAttr("required").val('');
    }

  }); 
  $('.dmFlight').on("change",function(){
    if($(this).is(":checked"))
    { 
        $('#domFlight').prop("required","true").removeAttr("disabled").val('');
    }
    else
    {
        $('#domFlight').prop("disabled","true").removeAttr("required").val('');
    }

  }); 
   $('.curb').on("change",function(){

    if($(this).is(":checked"))
    { 
        $('#curbside').prop("required","true").removeAttr("disabled").val('');
    }
    else
    {
        $('#curbside').prop("disabled","true").removeAttr("required").val('');
    }

  });
   $('.iFlight').on("change",function(){

    if($(this).is(":checked"))
    { 
        $('#intFlight').prop("required","true").removeAttr("disabled").val('');
    }
    else
    {
        $('#intFlight').prop("disabled","true").removeAttr("required").val('');
    }

  });


   $('#back_sma_location').on("click",function(){

    window.location.href="airport_db.php";


   });






   /*   select box code start here */



      $("input[name='airport_copy_existing']").click(function(){

        

    if($("input[name='airport_copy_existing']:checked").val()=="yes")

    {

      

      $('#airport_db_select_div').css("visibility","visible");

     

    }

  else{

      $('#airport_db_select_div').css("visibility","hidden");

      

    

    }

  });




$('#setFBO').on("change",function(){


  if($(this).is(":checked"))
  {
        $('#FboDesclaimer').css("visibility","visible");
  }
  else
  {
    $('#FboDesclaimer').css("visibility","hidden");
  }




    var isDeactivateAllRate=confirm("Are you sure deactive all vehicle fbo srchg rate!");
if(isDeactivateAllRate)
{

  $('.cmnclass').each(function(){
        var getSeq=$(this).attr('seq');
        $(this).removeAttr("checked");
        $('.phpfess'+getSeq).prop("disabled",true).removeAttr("required").val('');
        $('.domaticCLass'+getSeq).prop("disabled",true).removeAttr("required").val('');
        $('.intrfclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
        $('.fhvclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
        $('.fboclass'+getSeq).prop("disabled",true).removeAttr("required").val('');
      });


  $('.dmFlight').prop("checked",false);
  $('#domFlight').val('').prop("disabled",true);
  $('.iFlight').prop("checked",false);
  $('#intFlight').val('').prop("disabled",true);;



}
    






});
 
// $("#airport_name,#airport_code,#airport_zipcode").keyup(function(e){




//     name=$(this).val();
      


//       var re = /\,|\(|\)|\{|\}|\[|\]|\-|\+|\*|\%|\/|\=|\"|\'|\~|\!|\&|\||\<|\>|\?|\:|\;|\.| /;
     
//       if(re.test(name))
//       {
//            name=name.replace(/[^a-zA-Z 0-9]+/g,'');
//            $(this).val(name);
//       }


   
   
    


  
    
        
// })
   /*  select box code end here*/



</script> 

<!-- Loading Indicator Ends-->
</body>
</html>
