<?php
include_once 'session_auth.php';

$_page = 'cns';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Company Notifications Setup</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="assets/jvector-map/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="css/clndr.css" rel="stylesheet">
    <!--clock css-->
    <link href="assets/css3clock/css/style.css" rel="stylesheet">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="assets/morris-chart/morris.css">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <!-- FormValidation CSS file -->
    <link rel="stylesheet" href="validator/dist/css/formValidation.min.css">
    <link rel="stylesheet" href="build/css/intlTelInput.css">
    <!--  <link rel="stylesheet" href="build/css/demo.css"> -->
    <style>

    </style>
</head>
<body>

<!--header start-->
<?php include_once './global/header.php'; ?>
<!--header end-->

<!--sidebar start-->
<?php include_once './global/sideNav.php'; ?>
<!-- sidebar menu end-->


<section id="main-content">
    <section class="wrapper">
        <div class="panel-body">
            <form id="contactForm">
                <div class="notify-form">
                    <!-- Q&R Notify eMail Section Starts here -->
                    <div class="col-sm-12" style="text-align:center; padding-bottom:20px;">
                        <br/>
                        <h4><strong>Quote & Reserve Mail Notification Setup</strong></h4>
                    </div>
                    <div id="notify_emails">
                        <div id="notify_email_1">
                            <div class="col-sm-4"
                                 style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;">
                                <label>Q&R Notify Primary eMail:</label></div>
                            <div class="col-sm-7" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input
                                    type="email" class=" form-control" id="notify-email-1"
                                    placeholder="eMail address 1 for notifications"></div>
                        </div>

                    </div>
                    <div class="col-sm-4"
                         style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;">
                        <label>Add Q&amp;R Notify eMail:</label></div>
                    <div class="col-sm-8" style="text-align:left; margin-left:-5px; padding-bottom:10px;"><input
                            type="button" class="btn btn-primary" id="add-notifyEmail" value="+"></div>


                    <!-- Q&R Notify eMail Section Ends here -->
                    <div class="col-sm-12" style="text-align:center; padding-bottom:20px;">
                        <br/><h4><strong>Quote & Reserve SMS Notification Setup</strong></h4>
                    </div>
                    <!-- Q&R Notify SMS Section Starts here -->
                    <div id="notify_sms">
                        <div id="notify_sms_1">
                            <div class="col-sm-4"
                                 style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;">
                                <label>Q&R Notify Primary SMS:</label></div>
                            <div class="col-sm-7" style="text-align:left; margin-left:-5px; padding-bottom:10px;">
                                <input type="text" class=" form-control" id="notify-sms-1" name="phoneNumber" onkeyup="validPhoneNumber(this)">
                                <span>@</span>
                                <select name="service_provider" id="service_provider-1" class=" form-control">
                                    <option value="">WIRELESS PROVIDER</option>
                                    <option value="msg.acsalaska.com">Alaska Communications (ACS)</option>
                                    <option value="message.alltel.com">Alltel</option>
                                    <option value="paging.acswireless.com">Ameritech (ACSWireless)</option>
                                    <option value="txt.att.net">AT&amp;T Wireless</option>
                                    <option value="page.att.net">ATT Business Messaging (US)</option>
                                    <option value="txt.bellmobility.ca">Bell Mobility Canada</option>
                                    <option value="blsdcs.net">BellSouth Mobility</option>
                                    <option value="blueskyfrog.com">Blue Sky Frog</option>
                                    <option value="myboostmobile.com">Boost Mobile</option>
                                    <option value="sms-c1.bm">Cellone - Bermuda</option>
                                    <option value="csouth1.com">Cellular South</option>
                                    <option value="mobile.celloneusa.com">CellularOne (Dobson)</option>
                                    <option value="cwemail.com">Centennial Wireless</option>
                                    <option value="cingularme.com">Cingular</option>
                                    <option value="mobile.mycingular.com">Cingular</option>
                                    <option value="imcingular.com">Cingular IM Plus/Bellsouth IPS</option>
                                    <option value="sms.ctimovil.com.ar">Claro Argentina</option>
                                    <option value="mms.cricketwireless.net">Cricket</option>
                                    <option value="digitextbm.com">Digicel Bermuda</option>
                                    <option value="digitextjm.com">Digicel Jamaica</option>
                                    <option value="sms.edgewireless.com">Edge Wireless</option>
                                    <option value="fido.ca">Fido Canada</option>
                                    <option value="mobile.gci.net">General Communications Inc.</option>
                                    <option value="sms.goldentele.com">Golden Telecom</option>
                                    <option value="ideacellular.net">Idea Cellular</option>
                                    <option value="text.mtsmobility.com">Manitoba Telecom Systems</option>
                                    <option value="pcsms.com.au">Message Media (AU)</option>
                                    <option value="sms.messagebird.com">MessageBird</option>
                                    <option value="mymetropcs.com">Metro PCS</option>
                                    <option value="page.metrocall.com">Metrocall Pager</option>
                                    <option value="page.mobilfone.com">Mobilfone</option>
                                    <option value="sms.co.za">MTN (South Africa)</option>
                                    <option value="wirefree.informe.ca">NBTel</option>
                                    <option value="messaging.nextel.com">Nextel</option>
                                    <option value="npiwireless.com">NPI Wireless</option>
                                    <option value="pcs.ntelos.com">NTELOS</option>
                                    <option value="mmail.co.uk">O2 (UK)</option>
                                    <option value="optusmobile.com.au">Optus (Australia)</option>
                                    <option value="orange-gsm.ro">Orange (Romania)</option>
                                    <option value="orange.net">Orange (UK)</option>
                                    <option value="pagenet.net">Pagenet</option>
                                    <option value="pcs.rogers.com">PCS Rogers AT&amp;T Wireless</option>
                                    <option value="ptel.net">Powertel</option>
                                    <option value="sms.pscel.com">PSC Wireless</option>
                                    <option value="pager.qualcomm.com">Qualcomm</option>
                                    <option value="qwestmp.com">Qwest</option>
                                    <option value="smtext.com">Simple Mobile</option>
                                    <option value="skytel.com">Skytel - Alphanumeric</option>
                                    <option value="sms.smsglobal.com.au">SMS Global</option>
                                    <option value="tmomail.net" selected>Solavei</option>
                                    <option value="messaging.sprintpcs.com">Sprint PCS</option>
                                    <option value="tms.suncom.com">SunCom</option>
                                    <option value="mobile.surewest.com">SureWest Communications</option>
                                    <option value="movistar.net">Telefonica Movistar</option>
                                    <option value="onlinesms.telstra.com">Telstra</option>
                                    <option value="msg.telus.com">Telus Mobility</option>
                                    <option value="t-mobile.uk.net">T-Mobile (UK)</option>
                                    <option value="tmomail.net" selected>T-Mobile USA</option>
                                    <option value="email.uscc.net">US Cellular</option>
                                    <option value="vtext.com">Verizon Wireless</option>
                                    <option value="airtouchpaging.com">Verizon Wireless (formerly Airtouch)</option>
                                    <option value="myairmail.com">Verizon Wireless (myairmail.com)</option>
                                    <option value="vmobile.ca">Virgin (Canada)</option>
                                    <option value="vxtras.com">Virgin Mobile (UK)</option>
                                    <option value="vmobl.com">Virgin Mobile USA</option>
                                    <option value="voda.co.za">Vodacom (South Africa)</option>
                                    <option value="vodafone.net.au">Vodafone (AUS)</option>
                                    <option value="vodafone.net">Vodafone (UK)</option>
                                    <option value="airmessage.net">Weblink Wireless</option>
                                    <option value="wyndtell.com">WyndTell</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-4"
                             style="text-align:right; padding-top:10px; padding-right: 0px; padding-bottom:10px;">
                            <label>Add Q&amp;R Notify SMS Mobile #:</label></div>
                        <div class="col-sm-8" style="text-align:left; margin-left:-5px; padding-bottom:10px;">
                            <input type="button" class="btn btn-primary" id="add-notifySMS" value="+"></div>
                    </div>
                    <!-- Q&R Notify SMS Section Ends here -->
                    <div class="col-sm-12" style="text-align:center; padding:20px;">
                        <input type="button" value="Submit" class="btn btn-primary" id="updateNotifySetup"/></div>
                </div>
            </form>
        </div>
    </section>
</section>

<!--Loading indicator-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;"
     id="refresh_overlay;">
    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%;"></div>
    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%; display:table-row">
                <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px;">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;">
                            <div class="loadingGIF">
                                <img style="height:70px;width:70px;" src="images/loading.gif" alt="loading indicator">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- Loading Indicator Ends-->

<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
<script src="js/lib/jquery.js"></script>

<script src="pageJs/dashboard.js"></script>
<script src="pageJs/logout.js"></script>
<script src="assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>
<!--[if lte IE 8]>
<script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="assets/skycons/skycons.js"></script>
<script src="assets/jquery.scrollTo/jquery.scrollTo.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="assets/calendar/clndr.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="assets/calendar/moment-2.2.1.js"></script>
<!-- <script src="js/calendar/evnt.calendar.init.js"></script> -->
<script src="assets/jvector-map/jquery-jvectormap-1.2.2.min.js"></script>
<script src="assets/jvector-map/jquery-jvectormap-us-lcc-en.js"></script>
<script src="assets/gauge/gauge.js"></script>
<!--clock init-->
<script src="assets/css3clock/js/script.js"></script>
<!--Easy Pie Chart-->
<script src="assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="assets/sparkline/jquery.sparkline.js"></script>
<!--Morris Chart-->
<script src="assets/morris-chart/morris.js"></script>
<script src="assets/morris-chart/raphael-min.js"></script>
<!--jQuery Flot Chart-->
<script src="assets/flot-chart/jquery.flot.js"></script>
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="assets/flot-chart/jquery.flot.resize.js"></script>
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="assets/flot-chart/jquery.flot.animator.min.js"></script>
<script src="assets/flot-chart/jquery.flot.growraf.js"></script>
<script src="js/custom-select/jquery.customSelect.min.js"></script>
<!--common script init for all pages-->
<script src="js/scripts.js"></script>

<script src="pageJs/UserProfile.js"></script>
<!-- FormValidation plugin and the class supports validating Bootstrap form -->

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
<script src="validator/dist/js/formValidation.min.js"></script>
<script src="validator/dist/js/framework/bootstrap.min.js"></script>
<script src="build/js/intlTelInput.js"></script>
<script>
    intlTelInputValid = 1;
    function validPhoneNumber(trigger) {
        var trigger = $(trigger);
        trigger.closest('.col-sm-6').find('.phoneValidation').remove();
        var isValidNumber = trigger.intlTelInput("isValidNumber");
        if (!isValidNumber) {
            trigger.closest('.intl-tel-input').after('<div class="phoneValidation"><span style="font-size: 12px;color: #ff4268;">Invalid Number. Please insert valid one.</span></div>');
            intlTelInputValid = 0;
        } else {
            intlTelInputValid = 1;
            var countrySet = trigger.intlTelInput("getSelectedCountryData");
            trigger.intlTelInput("destroy");
            trigger.intlTelInput({
                utilsScript: 'build/js//utils.js',
                autoPlaceholder: true,
                initialCountry: countrySet.iso2,
                preferredCountries: ['us', 'fr', 'gb']
            });
        }

    }

</script>
<script src="pageJs/email_setup.js"></script>

<script type="text/javascript" src="js/jquery.mask.min.js"></script>



<script>
    getEmailNotifyData();
</script>
<!--script for this page-->
</body>
</html>
