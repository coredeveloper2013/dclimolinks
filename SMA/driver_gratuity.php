<?php
include_once 'session_auth.php';

$_page = 'dgs';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Driver Gratuity Setup</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">


    <!--dynamic table-->
    <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet"/>

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap-multiselect.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <link href="css/driver_graduaty.css" rel="stylesheet"/>
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
        .table-action {
            text-align: right;
            font-size: 14px
        }

        .loadingGIF {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width: 100%;
            padding-left: 30%;
            padding-top: 10%;
        }

        .alignCENTER {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
    </style>
</head>

<body>
<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-md-12 alignCENTER">
                    <h4 class="page-title "> Mandatory Driver Gratuity Setup </h4>

                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-info" style="height:300px;">
                        <!-- form start -->
                        <form onsubmit="return false;">
                            <div class="box-body row">
                                <div class="col-xs-12" style="text-align:center">

                                    <br>
                                </div><!-- col-xs-12-->
                            </div>

                            <div class="row col-xs-12 alignCENTER">
                                <div class="col-xs-1" style="text-align:left; width:1%"></div>

                                <div class="col-xs-2" style="text-align:center; width:18%; height: 30px;"><b>Applies to
                                    Service Type</b><br/>

                                    <select class="service_type" id="service_type" multiple="multiple"
                                            name="service_type" required>
                                        <option>--Select Service--</option>

                                    </select>

                                </div>
                                <div class="col-xs-2" style="text-align:center; width:18%; height: 30px;"><b> Gratuity
                                    for driver</b> <br/>
                                    <select id="value">
                                        <option value="">None selected</option>
                                        <option value="10">10%</option>
                                        <option value="15">15%</option>
                                        <option value="18">18%</option>
                                        <option value="20">20%</option>
                                        <option value="custom">Custom</option>
                                    </select>

                                </div>
                                <div class="col-xs-2" style="text-align:center; display:none;" id="enter_value_percent">
                                    <b>Enter Value(%)</b>

                                    <input type="text" class="form-control" id="value_percent" style="width:97%;"
                                           onkeyup="checknegative()">

                                </div>


                                <div class="col-xs-2" style="text-align:center; width:18%; height: 30px;"><b>Applies to
                                    Vehicle Type </b><br/>
                                    <select id="apply_vehicle_type" multiple="multiple">
                                        <option>--Select--</option>
                                    </select>
                                </div>

                                <div class="col-xs-2" style="text-align:center; width:18%; height: 30px;"><b>Associate
                                    with SMA</b> <br/>
                                    <select id="apply_sma" multiple="multiple">
                                        <option>Select SMA</option>
                                    </select>
                                </div>
                                <div class="col-xs-1" style="text-align:center; width:1%"></div>
                                <br>
                                <br>
                                <br>
                            </div>
                            <!-- Remaining Miles Row-->

                            <!-- Apply to vehicle type-->
                            <div class="row">

                                <br>
                            </div>
                            <!-- Associate with SMA -->

                            <br>
                            <div class="row">
                                <div class="col-xs-12 " style="text-align:center"><b></b>
                                    <button type="button" id="save_rate" class="btn btn-primary" style="width:10%;">
                                        Save
                                    </button>
                                    <button type="button" id="back_button" class="btn btn-primary"
                                            style="display:none;"> Back
                                    </button>


                                </div>
                            </div><!-- row save matrix button-->


                            <!-- /.box-body -->

                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div data-ng-controller="SharedData" class="m-t-md ng-scope">
                        <!--#########################################point to point ####################################################-->
                        <div>
                            <div class="row">
                                <div class="col-xs-12"></div>
                            </div>
                            <div class="row mb-sm ng-scope ">
                                <div class="col-sm-12"></div>
                            </div>
                            <div style="overflow:scroll;min-height:500px;">
                                <table class="table table-condensed ng-scope sieve">
                                    <thead>
                                    <tr>

                                        <th style="text-align:center">Gratuity for driver</th>

                                        <th style="text-align: center;padding-right: 6%;">Apply to Vehicle</th>
                                        <th style="text-align: center;padding-right: 6%;">Service Type</th>
                                        <th style="text-align: center;padding-right: 6%;">Associate with SMA</th>
                                        <th style="text-align:center">Action</th>
                                        <th class="table-action"><span class="table-action"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody id="rate_matrix_list">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- ############################## additional rate####################################################-->

            <!--##################finish###########################-->

            <!-- page end-->
        </section>
    </section>

    <!--main content end-->  <!-- Right Side Bar Goes Here if Required
<div class="right-sidebar">
<div class="right-stat-bar"> </div>
</div> --></section>


<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->
<!--Loading indicaator-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;"
     id="refresh_overlay">
    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%;display:table-row">
                <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; ">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="loadingGIF"><img style="height:70px;width:70px;" src="images/loading.gif"
                                                         alt="Page loading indicator"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/lib/jquery.js"></script>

<script src="pageJs/dashboard.js"></script>
<script src="pageJs/logout.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/bootstrap-multiselect.js"></script>
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>
<script src="pageJs/searchbox.js"></script>
<script src="pageJs/driver_gratuity.js"></script>

<!--dynamic table-->
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>


<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<script>

    $("table.sieve").sieve();

</script>
</body>
</html>
