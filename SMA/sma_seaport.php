<?php
include_once 'session_auth.php';

$_page = 'srr';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link rel="shortcut icon" href="images/favicon.png">
<title>My Limo Project | Seaport zone setup</title>

<!--Core CSS -->
<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-reset.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<style>
#sma_seaport_list td {
	text-align: center;
}
ul.vehicle-list {
	padding: 0;
	margin: 0;
	list-style: none;
	margin-top: 15px
}
ul.vehicle-list li a {
	display: block;
	border-bottom: 1px dotted #CFCFCF;
	padding: 10px;
	text-decoration: none
}
ul.vehicle-list li:first-child a {
	border-top: 1px dotted #CFCFCF
}
ul.vehicle-list li a:hover {
	background-color: #DBF9FF
}
ul.vehicle-list li span {
	display: block
}
ul.vehicle-list li span.vehicle-name {
	font-weight: 600;
	color: #8F8F8F
}
ul.vehicle-list li span.vehicle-tier-count {
	color: #8C8C8C
}
.top-links a {
	font-size: 12px
}
.top-links {
	text-align: right
}
div.rate-group-selector {
	display: inline-block
}
div.vehicle-list .panel-default>.panel-heading {
	background-color: transparent;
	padding: 15px
}
div.vehicle-list .panel-default {
	border-color: transparent
}
div.vehicle-list .panel {
	border: none;
	box-shadow: none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	-o-box-shadow-none: none
}
#vehicle-list h4.panel-title {
	font-style: normal;
	font-size: 14px
}
div.vehicle-list .panel-group .panel+.panel {
	margin-top: 0px
}
.input-xs {
	height: 22px;
	padding: 5px 5px;
	font-size: 12px;
	line-height: 1.5;
	border-radius: 3px
}
div.vehicle-list .table thead th {
	padding-bottom: 5px !important;
	text-transform: none;
	color: #898989;
	font-weight: normal
}
td, th {
	height: 50px;
	width: 18%;
}
div.vehicle-list .table>tbody>tr>td, div.vehicle-list .table thead {
	border-top: none
}
div.vehicle-list .table-condensed>tbody>tr>td {
	padding: 2px
}
.table-action {
	text-align: right;
	font-size: 14px
}
.editable-text {
	width: 30%
}
.set {/* position:relative; *//* width:100%; *//* height:auto; *//* background-color:#f5f5f5 */
}
.set>a {
	display: block;
	padding: 20px 15px;
	text-decoration: none;
	color: #555;
	font-weight: 600;
	border-bottom: 1px solid #ddd;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	transition: all 0.2s linear
}
.set>a:hover {
	background-color: cornsilk
}
.set>a i {
	position: relative;
	float: right;
	margin-top: 4px;
	color: #666
}
.set>a.active {
	background-color: #1fb5ad;
	color: #fff
}
.set>a.active i {
	color: #fff
}
.acc-content {
	position: relative;
	width: 100%;
	height: auto;
	background-color: #fff;
}
.form-control {
	color: black;
}
.serviceType_css {
	margin-top: 8%;
}
.serviceRate_css {
	margin-top: 3%;
}
#add_point_rate_to option {
	max-height: 10px !important;
}
select[multiple] {
	height: 35px;
}
	.loadingGIF {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	width: 100%;
	padding-left: 30%;
    padding-top: 10%;
}


.alignCENTER {
        margin-left: auto;
	margin-right: auto;
	text-align: center;
}
</style>
</head>

<body>
<section id="container" >

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
  <section id="main-content">
    <section class="wrapper" style="overflow:auto"> 
      <!-- page start-->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <h4 class="page-title col-md-12 alignCENTER" style="margin-bottom:30px"> CREATE SEAPORT ZONE DATABASE</h4>
        </div>
        
        <!--   <div class="col-sm-3" style=" margin-top: 12px;display:none">Upload Location DataBase</div>
                  <div class="col-sm-3">
                <input type="file" name="smaCsvFile" class="btn btn-default add_color smaCsvFile" accept=".csv" value="browse" id="smaCsvFile" style="padding: 1%;display:none" seq="">
              </div>
                  <div class="col-sm-3" style="display:none">
                <input type="submit" class="btn btn-default add_color uploadSmaCsv" value="Upload" name="submit" style="padding:1%;margin-left:11%;" onclick="uploadSmaCsv('+responseObj.data[i].id+')">
              </div> --> 
      </div>
      <div class="row" >
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" > 
          <!-- general form elements -->
          
          <div class="box box-info" > 
            <!-- form start -->
            <form id="seaportZoneForm" method="post" >
              <div class="box-body row col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-3" style="text-align:center" > <b> Select SMA</b> <br>
                  <select id="add_seaport_sma" class="form-control add_sma_country"  required>
                  <option value="">-- SELECT SMA --</option>
                  </select>
                </div>
                <div class="col-xs-3 " style="text-align:center"> <b>Seaport Zip Code</b> <br>
                  <input type="text" id="seaport_zipcode"  style="height: 33px;" required>
                </div>
                <div class="col-xs-3 " style="text-align:center"> <b>Seaport Name </b> <br>
                  <input type="text" id="seaport_name"  disabled style="height: 33px;" required>
                </div>
                <div class="col-xs-3 " style="text-align:center" > <b> Seaport Code</b> <br>
                  <input type="text" id="seaport_code" disabled style="height: 33px;" required maxlength="4">
                </div>
                <div class="col-xs-3" style="width:24%;text-align:center;display:none"> <b>Postal/Zip Code</b>
                  <textarea id="postal_code" class="form-control postal code" placeholder="Postal Code"></textarea>
                </div>
                <br>
                <br>
                <br>
              </div>
              
              <!-- copy seaport code start here --> 
              
              <br>
              <br>
              <div class=" row">
                <div class="col-xs-4 col-sm-5 col-md-5 col-lg-5" style="text-align:left; margin-top:25px"> <b>Copy From Existing Seaport Matrix? &nbsp;&nbsp;&nbsp;&nbsp;</b>
                  <input type="radio" value="yes" id="seaport_copy_existing" name="seaport_copy_existing">
                  YES&nbsp;&nbsp;
                  <input type="radio" value="no" id="seaport_copy_existing" checked="" name="seaport_copy_existing">
                  NO </div>
                <!-- col-xs-4-->
                
                <div class="col-xs-6 col-sm-5 col-md-5 col-lg-5" style="visibility: hidden;margin-top:25px" id="seaport_db_select_div"><b style="margin:0% 20% 0% 2%;">Select Matrix</b>
                  <select style="height:24px;border-radius:4px;width:36%;" id="select_seaport_matrix_db">
                  </select>
                  <button type="button" id="copy_rate_matrix_seaport" class="btn btn-primary" style="float:right" >Copy</button>
                </div>
                <br>
              </div>
              <!-- copy seaport code end here -->
              
              <div class="row">
                <div class="container">
                  <div class="col-xs-9 col-sm-8 col-md-8 col-lg-8 section-border" style="margin-bottom:40px">
                    <h4 style="text-align:center;">Seaport Fees Setup</h4><hr style="width:90%; margin:auto">
                    <table>
                      <tr>
                        <th>Select</th>
                        <th>Vehicle</th>
                        <th>Per hour parking fees</th>
                        <th>Seaport pickup schg</th>
                        <!-- <th>Int'l flt schg </th> -->
                        <th>FHV access fees </th>
                      </tr>
                      <tbody class="airportVehicleInfo">
                      </tbody>
                    </table>
                  </div>
                  <div class="col-xs-5 col-sm-4 col-md-4 col-lg-4" style="margin-top: 4.7%">
                    <div class="form-group">
                      <label for="comment">Q&R parking fee disclaimer content: </label>
                      <textarea class="form-control" rows="5" style="width: 300px;height:67px;" id="comment"></textarea>
                    </div>
<label for="conciergeFee">Concierge Fee Setup?</label>
                      <input type="checkbox" name="vehicle" class="conciergeFeeCheckBox" value="conciergeFee">
                            <div class="form-group" >                    <div class="form-group">
                      <label for="comment">Optional Custom Greeter fee amount: </label>
                      <input type="text" class="form-control fhv_fee" style="width: 70px;"  >
                    </div>
                    <div class="form-group">
                      <label for="comment">Q&R optional greeter diclaimer content </label>
                      <textarea rows="3" class="form-control fhv_diclaimer" style="width: 300px;" ></textarea>
                    </div>
                  </div>
                </div>
                <div class="container">
                
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h4>Pickup Options</h4>
                    <div class="form-group">
                      <label for="comment">Inside Meet & Greet </label>
                      <input type="checkbox" style="height: inherit;width: 2.5%; margin-right: 4px; float: left;" class="form-control inside_meet_greet_check">
                      <textarea rows="3" class="form-control inside_meet_greet" disabled style="width: 300px;" ></textarea>
                    </div>
                    <div class="form-group">
                      <label for="comment">Curbside </label>
                      <input type="checkbox" style="height: inherit;width: 2.5%; margin-right: 4px;float: left;" class="form-control curbside_check">
                      <textarea rows="3" class="form-control curbside_text" disabled style="width: 300px;" ></textarea>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin-top:40px; margin-left:-19%">
                    <div class="form-group">
                      <label for="comment">Q&R seaport pickup fee pricing dsclr content: </label>
                      <textarea rows="4" cols="50" class="seaportDeclairContent"></textarea>
                    </div>
                  </div>
                  
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" id="add_seaport_location" style="margin-left: 42%;" class="btn btn-primary">Submit</button>
                <button type="button" id="back_sma_location" class="btn btn-primary" > Back </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div data-ng-controller="SharedData" class="m-t-md ng-scope"> 
            <!--#########################################point to point ####################################################-->
            <div id="sma_lists">
              <div class="row">
                <div class="col-xs-12"></div>
              </div>
              <div class="row mb-sm ng-scope">
                <div class="col-sm-12"></div>
              </div>
              <div >
                <table class="table table-condensed ng-scope sieve" style="display:none;">
                  <thead >
                    <tr >
                      <th style="text-align:center">Associate with SMA</th>
                      <th style="text-align:center">Zone Seaport Zip Code</th>
                      <th style="text-align:center">Zone Seaport Name</th>
                      <th style="text-align:center">Zone Seaport Code</th>
                      <th style="text-align:center">View</th>
                      <th style="text-align:center">Action</th>
                    </tr>
                  </thead>
                  <tbody id="sma_seaport_list">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- ############################## additional rate####################################################--> 
      
      <!--##################finish###########################--> 
      
      <!-- page end--> 
    </section>
  </section>
</section>
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;" id="refresh_overlay">
  <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%;display:table-row">
        <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; " >
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;"> 
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>    <div class="loadingGIF"> <img style="height:70px;width:70px;" src="images/loading.gif" alt="Page loading indicator"> </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="js/lib/jquery.js"></script> 
<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/logout.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 
<script src="js/scripts.js"></script> 
<script src="pageJs/sma_seaport.js?js=<?php echo uniqid(); ?>"></script> 
<script src="pageJs/searchbox.js"></script> 
<script type="text/javascript">


   // $("table.sieve").sieve();


   $('#back_sma_location').on("click",function(){

    window.location.href="seaport_db.php";


   });


  $('.inside_meet_greet_check').on("change",function(){

    if($(this).is(":checked"))
    { 
        $('.inside_meet_greet').prop("required","true").removeAttr("disabled").val('');
    }
    else
    {
        $('.inside_meet_greet').prop("disabled","true").removeAttr("required").val('');
    }

  });

    $('.curbside_check').on("change",function(){

    if($(this).is(":checked"))
    { 
        $('.curbside_text').prop("required","true").removeAttr("disabled").val('');
    }
    else
    {
        $('.curbside_text').prop("disabled","true").removeAttr("required").val('');
    }

  });




     /*   select box code start here */



      $("input[name='seaport_copy_existing']").click(function(){

        
    if($("input[name='seaport_copy_existing']:checked").val()=="yes")

    {

      

      $('#seaport_db_select_div').css("visibility","visible");

     

    }

  else{

      $('#seaport_db_select_div').css("visibility","hidden");

      

    

    }

  });


 
//       $("#seaport_zipcode,#seaport_name,#seaport_code").keyup(function(e){
//     name=$(this).val();
//       var re = /\,|\(|\)|\{|\}|\[|\]|\-|\+|\*|\%|\/|\=|\"|\'|\~|\!|\&|\||\<|\>|\?|\:|\;|\.| /;
//       if(re.test(name))
//       {
//            name=name.replace(/[^a-zA-Z 0-9]+/g,'');
//            $(this).val(name);
//       }
        
// })
   /*  select box code end here*/



  </script>
</body>
</html>
