<?php
include_once 'session_auth.php';

if (IsLoggedIn()) {
    redirect('./');
}
if(!isset($_GET['reset'])){
    redirect('./');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Reset Password</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet"/>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">

    <form class="form-signin" onsubmit="resetPassword(event)">
        <h2 class="form-signin-heading">Reset Password</h2>
        <div class="login-wrap">
            <div class="user-login-info">
                <span id="loginInfoMsg" style="color:red;display:none">User Not Found</span>
                <span id="loginSuccessMsg" style="color:green;display:none">Login Successfully</span>
                <input type="password" class="form-control" id="user_password" placeholder="New Password">
                <input type="password" class="form-control" id="user_password_conf" placeholder="Confirm Password">
                <input type="hidden" class="form-control" id="reset" value="<?php echo $_GET['reset']; ?>">
            </div>
            <button class="btn btn-lg btn-login btn-block" type="submit">Reset Password</button>
        </div>
    </form>

</div>
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;"
     id="refresh_overlay">
    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%;display:table-row">
                <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; ">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="loadingGIF"><img style="height:70px;width:70px;margin-left: 500px;"
                                                         src="images/loading.gif" alt="Page loading indicator"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->
<script src="js/lib/jquery.js"></script>
<script src="pageJs/login.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>

</body>
</html>
