<?php
include_once 'session_auth.php';

$_page = 'sts';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link rel="shortcut icon" href="images/favicon.png">
  <title>My Limo Project | Service Types</title>

  <!--Core CSS -->
  <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-reset.css" rel="stylesheet">
  <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

  <!--dynamic table-->
  <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
  <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />
  <link href="css/limo_car.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  <style>
.form-control {
	color: black;
	border-color: rgb(169, 169, 169);
}
.close_popup {
	width: 16px;
	height: 16px;
	margin-left: 97%;
	margin-top: -13%;
	cursor: pointer;
}
.headerPopText {
	position: absolute;
	padding-left: 34%;
}
.popupHeader {
	background: #F4F4F2;
	position: relative;
	padding: 13px 20px 0px 20px;
	border-bottom: 1px solid #DDD;
	font-weight: bold;
	font-family: 'Source Sans Pro', sans-serif;
	font-size: 16px;
	color: #666;
	text-transform: capitalize;
	text-align: center;
	height: 9%;
	margin-top: 2px;
}
.btn-default {
	background-color: #1FB5AD;
	border-color: #1FB5AD;
	color: #fff;
}
.btn-default:hover {
	background-color: #24ADA5;
	border-color: #24ADA5;
	color: #fff;
}
.loadingGIF {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	width: 100%;
	padding-left: 30%;
	padding-top: 10%;
}
.alignCENTER {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}
</style>
  </head>

  <body>
<section id="container" >

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
    <section id="main-content">
    <section class="wrapper"> 
        <!-- page start-->
        
        <div class="row">
        <div class="col-sm-12">
            <section class="panel">
            <header class="panel-heading">
                <h4 class="page-title alignCENTER">Service Type</h4>
              </header>
            
            <!-- <div class="col-sm-12"> <div class="top-links"> <a class="btn btn-default popup_edit_city add_color " seq="SED" style="float:right;padding: 1%;">
          <i class="fa fa-plus"></i> Edit Service</a> </div> </div> 
                     -->
            <div class="row">
                <div class="col-sm-12">
                <table class="table sieve">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><span class="line"></span>Code</th>
                        <th><span class="line"></span>Service Type Name</th>
                        <th><span class="line"></span>Action</th>
                      </tr>
                  </thead>
                    <tbody class="sortable-table ui-sortable" id="service_type">
                  </tbody>
                  </table>
              </div>
              </div>
          </section>
          </div>
      </div>
        
        <!-- page end--> 
      </section>
  </section>
    <!--main content end--></section>
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;" id="update_service_popup">
    <div style="position:relative; background:black; opacity:0.5; top: 0px; left:0px; width:100%; height:100%"> </div>
    <div style="position:absolute; top:0px; left:-17%; width:136%; height:100%; margin-left:auto; margin-right:auto;">
    <div style="width:100%;display:table; height:100%;">
        <div style="width:100%;display:table-row">
        <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
            <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; " id="asseriesUpdateOverlay">
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;"> <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div style="width:80%"> 
                <!--  code of popup start-->
                <div class="container">
                    <div id="modal" class="popupContainer" style="display:block;  top: 23px; position: absolute;width: 22%; left: 39%;  top: 80px;  background: #FFF; border-radius: 4px; box-shadow: 0px 10px 20px rgba(53, 51, 51, 0.84);">
                    <header class="popupHeader"> <span class="header_title">Update Service Name</span> <img src="images/remove.png" alt="" id="popup_close_addrate" class="close_popup"> <span id="errmsg1" style="color:red; font-size:10px;"></span> <span id="errmsg2" style="color:red; font-size:10px;"></span> </header>
                    <section class="popupBody">
                        <div class="row" style="margin-top:6%;">
                        <div class="form-group">
                            <label class="col-sm-5 control-label" >Service Name</label>
                            <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Enter Service Name" id="serviceName" >
                          </div>
                          </div>
                        <br>
                      </div>
                        <div class="col-lg-12 col-md-12" style="margin:10px 5px 25px 5px; text-align:center;">
                        <button class="btn btn-primary" type="submit" id="update_service_name" style=  "width: 27%; text-align: center; margin-top: 6%;">Upadte</button>
                      </div>
                        <div > </div>
                      </section>
                  </div>
                  </div>
              </div>
              </div>
          </div>
          </div>
      </div>
      </div>
  </div>
  </div>
<!--Loading indicator-->
<div style="position: fixed; top: 0px; z-index: 10000; text-align: center; width: 100%; height: 100%; display: none;" id="refresh_overlay">
  <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%; display:table-row">
        <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px" >
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto"><br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <div class="loadingGIF"> <img style="height:70px;width:70px;" src="images/loading.gif" alt="Page loading indicator"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Loading Indicator Ends--> 
<!-- Placed js at the end of the document so the pages load faster --> 

<!--Core js--> 
<script src="js/lib/jquery.js"></script> 
<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/logout.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 
<script src="js/scripts.js"></script> 
<script src="pageJs/getservicelist.js"></script> 
<script src="pageJs/searchbox.js"></script> 
<script>
   $("table.sieve").sieve();



/* $('.popup_edit_city_state').on("click",function(){
		   $('#add_city_popup').show();
		   })
*/
  $('#popup_close_addrate').on("click",function(){

      $('#add_city_popup').hide();

      })
</script>
</body>
</html>
