<?php
include_once 'session_auth.php';

$_page = 'pgs';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="images/favicon.png">
<title>My Limo Project | Payment Gateway Integration</title>

<!--Core CSS -->
<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-reset.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

<!--dynamic table-->
<link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />

<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<link href="css/limo_car.css" rel="stylesheet">
<link href="css/bootstrap-multiselect.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />
<link href="css/sma.css" rel="stylesheet" />
<style>
.alignCENTER {
	text-align: center;
}
.more_setup, #optional_gatewaySetup {
/*opacity: .5;
	pointer-events: none;*/
}
.auth_pay_option {
/*display: none;*/
}
</style>
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
  <section id="main-content"> 
    <!-- wrapper section start-->
    <section class="wrapper">
      <div class="page-title col-xs-12 h2" style="margin:10px; text-align:center;" >Checkout Gateway Setup</div>
      <br/>
      <div class="box box-info col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:370px; margin-bottom:20px">
      <!-- Payment Gateway section starts here -->
      

      <!-- Optional Payment Gateway Setup  -->
      
      <div id="optional_gatewaySetup">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top:20px; padding-bottom:15px; ">
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 h5" style=" padding-bottom:10px;">&nbsp;&nbsp;&nbsp;&nbsp;<b>Default Payment Gateway </b><br/>
            <select name="paymentGatewayOptions" id="paymentGatewayOptions" >
              <option selected="selected" value="-1">-- Select Payment Gateway --</option>
              <option value="Stripe">Stripe (default)</option>
              <option value="payPal">PayPal</option>
              <option value="Square">Square</option>
              <option value="authorizeNET">Authorize.net</option>
              <option value="Other">Other</option>
            </select>
          </div>
          <div class="col-xs-1">&nbsp;&nbsp;</div>
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 h5" style="margin-top:10px; text-align:left">Gateway API ID/Key: <br/>
            <input name="gatewayIDkey" id="stripe_publishable_key" type="text" size="30%">
          </div>
          <div class="col-xs-1">&nbsp;&nbsp;</div>
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 h5" style="margin-top:10px; text-align:left">Access Token/Secret Key <br/>
            <input name="gatewaySecretToken" id="stripe_secret_key" type="text" size="30%">
          </div>
          <div id="checkout-option" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 h5" style=" padding-bottom:10px;">&nbsp;&nbsp;&nbsp;&nbsp;<b style="text-wrap:none">Checkout Option Setup</b><br/>
              <select name="checkout_option" id="checkout_option" >
                <option selected="selected" value="-1">-- Select checkout option --</option>
                <option value="store-for-later">Store Credit Card for later</option>
                <!-- if selected, saves credit card details for later use (stores in Limo Anywhere back-end) -->
                <option value="capture-deposit-and-store">Collect initial deposit</option>
                <!-- if selected, show dialog to enter deposit value in either percent or fixed amount -->
                <option value="pre-paid">Authorize and Capture</option>
                <!-- if selected, Capture full amount due -->
                <option value="authorize">Authorize</option>
                <!-- if selected, authorize full amount due -->
              </select>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 h5 auth_pay_option" style=" padding-top:12px; text-align: left;display: none;" >Authorize and Capture Deposit Amount:&nbsp;&nbsp;
              <input type="number" style="width:100px" class="setdepositValue" id="auth_capt_deposit_amt" name="auth_capt_deposit_amt">
              <select name="amount_type" id="amount_type" class="setblacoutValue setdepositValueincreaserateType" >
                <option value="pcntg">%</option>
                <option value="fxdAMT">$</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <!-- Payment Gateway section ends here --> 
      
      <br/>
      <!-- Apply & Association section starts here -->
      <div class="more_setup">
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 alignCENTER" style="margin-top:20px;">
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 style="text-align:center; width:30%; height: 30px; ""><b>Applies to Service Type</b><br/>
            <select class="service_type" id="service_type" multiple="multiple" name="service_type"  required>
              <option>--Select Service--</option>
            </select>
          </div>
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align:center; width:30%;height: 30px; "> <b>Applies to Vehicle Type </b><br/>
            <select id="apply_vehicle_type" multiple="multiple">
              <option>--Select--</option>
            </select>
          </div>
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align:center; width:30%; height: 30px;"> <b>Associate with SMA</b> <br/>
            <select id="apply_sma" multiple="multiple">
              <option>Select SMA</option>
            </select>
          </div>
        </div>
        <!-- Apply & Association section ends here --><br/>
        <!-- Save & Submit section -->
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 alignCENTER" style="margin-top:20px;">
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="margin-top:25px"> Save Gateway Payment Setup as:<br>
            <input name="payment-gateway_name" id="payment_gateway" type="text" size="35%" required>
           </div>
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="margin-top:25px">
            <label for="is_default" style="padding: 17px 0">
              <input name="is_default" id="is_default" type="checkbox">  Set as Default?
            </label>
           </div>
          <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="margin-top:35px; text-align:left">
            <input name="save-payment-gateway-db" id="gatewaySetup" class="btn btn-primary gatewaySetup"  value="Submit" type="submit">
            <input type="hidden" id="update_Setup_id" val="">
            <div  style="margin-top: 2%;margin-left: 49%;float: left;padding-right: 1%;display:none" id="backBtnBack">
              <input type="button" value="Back" class="btn btn-primary backBtnBack1">
            </div>
            <!-- Save & Submit section ENDS HERE --> 
          </div>
        </div>
      </div>
      <br>
      <br>
      <br>
      <br>
      <br>
      
      <!-- INFO BOX DIV ENDS HERE --> 
      
      <!-- Database view Starts here -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="margin-top:35px">
          <div data-ng-controller="SharedData" class="m-t-md ng-scope">
            <div>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
              </div>
              <div class="row mb-sm ng-scope">
                <div class="ccol-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
              </div>
              <div style="overflow:scroll;min-height:500px;">
                <table class="table table-condensed ng-scope sieve">
                  <thead >
                    <tr >
                      <th style="text-align:center">Checkout Option</th>
                      <th style="text-align:center">Publishable key</th>
                      >
                      <th style="text-align:center">Secret key</th>
                      <th style="text-align:center">Deposit Amount</th>
                      <th style="text-align:center"><span style="text-align: center;padding-right: 2%;">Amount type</span></th>
                      <th style="text-align: center;padding-right: 2%;">View Setup</th>
                      <th style="text-align: center;padding-right: 2%;"><span style="text-align:center">Action</span></th>
                      <th class="table-action"><span class="table-action"></span></th>
                    </tr>
                  </thead>
                  <tbody id="rate_matrix_list">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Database view ends here --> 
    </section>
    
    <!--main-content wrapper end--> 
  </section>
  <!--main-content end--> 
</section>
<!--container content end--> 

<!-- Placed js at the end of the document so the pages load faster --> 
<script src="js/lib/jquery.js"></script> 
<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/logout.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script src="js/bootstrap-multiselect.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 
<script src="pageJs/sma.js"></script> 
<script src="pageJs/paymentIntegration.js"></script> 
<!--dynamic table--> 
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 

<!--common script init for all pages--> 
<script src="js/scripts.js"></script> 
<script src="pageJs/searchbox.js"></script> 
<script type="text/javascript">


 $("table.sieve").sieve();
 

</script>
</body>
</html>