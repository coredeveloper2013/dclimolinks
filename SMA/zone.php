<?php
include_once 'session_auth.php';

$_page = 'zone';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="images/favicon.png">
<title>My Limo Project | Create Zone for Zone to Zone rate setup</title>

<!--Core CSS -->
<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-reset.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

<!--dynamic table-->
<link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />

<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />
<link href="css/limo_car.css" rel="stylesheet">


<style>
.form-control {
	color: black;
}
.loadingGIF {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	width: 100%;
	padding-left: 30%;
	padding-top: 10%;
}
.alignCENTER {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}
</style>
</head>

<body>
<section id="container" >

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->



    <!--sidebar end-->
  <!--main content start-->
  <section id="main-content">
    <section class="wrapper"> 
      <!-- page start-->
      
      <div class="row">
        <div class="col-sm-12">
          <section class="panel">
            <header class="panel-heading alignCENTER" style="text-transform:none">
              <h3>Create a Zone </h3>(for Zone to Zone rate setup)
            </header>
            <div class="col-sm-12">
              <div class="top-links"> <a class="btn btn-default popup_edit_city add_color " seq="SED" style="float:right;padding: 1%;"> <i class="fa fa-plus"></i> Add Zone</a> </div>
            </div>
            <div id="pad-wrapper">
              <div class="row">
                <div class="col-sm-12">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th> <span class="line"></span> Zone</th>
                        <th><span class="line"></span>Abbr</th>
                        <th><span class="line"></span>City</th>
                        <th><span class="line"></span>Zip/Postal code</th>
                        <th><span class="line"></span>Action</th>
                      </tr>
                    </thead>
                    <tbody class="sortable-table ui-sortable" id="cityState">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
      
      <!-- page end--> 
    </section>
  </section>
  <!--main content end--></section>
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;" id="add_city_popup">
  <div style="position:relative; background:black; opacity:0.5; top: 0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:-17%; width:136%; height:100%; margin-left:auto; margin-right:auto;">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%;display:table-row">
        <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; " id="asseriesUpdateOverlay">
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;"> <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <div style="width:80%"> 
                <!--  code of popup start-->
                <div class="container">
                  <div id="modal" class="popupContainer" style="display:block;  top: 23px; position: absolute;
  width: 21%;
  height: 60%;
  left: 39%;
  top: 80px;
  background: #FFF;">
                    <header class="popupHeader" style="  background: #F4F4F2;
  position: relative;
  padding: 10px 20px;
  border-bottom: 1px solid #DDD;
  font-weight: bold;
  font-family: 'Source Sans Pro', sans-serif;
  font-size: 14px;
  color: #666;
  font-size: 16px;
  text-transform: uppercase;
  text-align: left;
  height:19%"> <span class="header_title">Add Zone</span> <img src="images/remove.png" alt="delete" id="popup_close_addrate" style="   width: 6%;
  margin-left: 91%;
  margin-top: -20px;"> <span id="errmsg1" style="color:red; font-size:10px;"></span> <span id="errmsg2" style="color:red; font-size:10px;"></span> </header>
                    <section class="popupBody">
                      <div class="form-group">
                        <label class="col-sm-5 control-label" >Zone Name</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="cityName" >
                        </div>
                      </div>
                      <br>
                      <br>
                      <br>
                      <div class="form-group">
                        <label class="col-sm-5 control-label"  >Zone Abbr</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="cityAbbr">
                        </div>
                      </div>
                      <br>
                      <br>
                      <div class="form-group">
                        <label class="col-sm-5 control-label" >City</label>
                        <div class="col-sm-6" >
                          <input type="text" class="form-control" id="select_cities">
                        </div>
                        <br>
                        <br>
                        <br>
                       <div class="form-group">
                        <label class="col-sm-5 control-label" >City Code</label>
                        <div class="col-sm-6" >
                          <input type="text" class="form-control" id="city_code">
                        </div>
                       </div>
                        </div>
                        <br/>
                        <br/>
                        <div class="form-group">
                          <label class="col-sm-5 control-label"  >ZipCode</label>
                          <div class="col-sm-6">
                            <input type="text" class="form-control" id="zipcode">
                          </div>
                        </div>
                        <br>
                        <br>
                      
                      <div class="col-lg-offset-3 col-lg-6">
                        <button class="btn btn-primary" type="submit" id="save_add_city_data" style=  "width: 86%;
  margin-left: 50%;
  margin-top: 11%;">Save</button>
                      </div>
                      <div > </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Placed js at the end of the document so the pages load faster --> 

<!--Core js--> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="js/lib/jquery.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 
<!--Easy Pie Chart--> 
<script src="assets/easypiechart/jquery.easypiechart.js"></script> 
<!--Sparkline Chart--> 
<script src="assets/sparkline/jquery.sparkline.js"></script> 
<!--jQuery Flot Chart--> 
<script src="assets/flot-chart/jquery.flot.js"></script> 
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script> 
<script src="assets/flot-chart/jquery.flot.resize.js"></script> 
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script> 

<!--dynamic table--> 
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script> 
<!--common script init for all pages--> 
<script src="js/scripts.js"></script> 

<script src="pageJs/getZoneToZone.js"></script> 
<!--dynamic table initialization --> 
<script src="js/dynamic_table/dynamic_table_init.js"></script> 
<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/logout.js"></script> 
<script>
 $('.popup_edit_city_state').on("click",function(){
		   $('#add_city_popup').show();
		   })
</script>
</body>
</html>
