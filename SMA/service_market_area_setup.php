<?php
include_once 'session_auth.php';

$_page = 'ssm';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="images/favicon.png">
<title>My Limo Project | Service Market Area (SMA) Setup</title>

<!--Core CSS -->
<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-reset.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

<!--dynamic table-->
<link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />

<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<link href="css/limo_car.css" rel="stylesheet">
<link href="css/bootstrap-multiselect.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />
<link href="css/sma.css" rel="stylesheet" />

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<style>
.loadingGIF {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	width: 100%;
	padding-left: 30%;
	padding-top: 10%;
}
.alignCENTER {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}
.chkmrk-icon {
	width: 30px;
	height: 30px;
	margin-top: 3px;
	}
	@media screen and (min-width: 1366px) {
    .chkmrk-icon {
        margin-top: -8px;
    }
}
	
</style>
</head>

<body>
<section id="container" >

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
  <section id="main-content">
    <section class="wrapper"> 
      <!-- page start-->
      <div class="col-xs-12 alignCENTER page-title" style="margin-bottom:20px;">
        <h3>Service Market Area (SMA) Setup</h3>
      </div>
      <div class="col-xs-12" style="padding-bottom:30px;">
        <div class="col-xs-3" style=" text-align: right; "><b>Upload&nbsp;Location&nbsp;DataBase:</b> </div>
<div class="col-xs-7" style="width:30%; margin-left: 45px; margin-top: -10px;">
<input type="file" name="smaCsvFile" class="btn btn-default add_color smaCsvFile" accept=".csv" value="browse" id="smaCsvFile" style="padding: 1%; text-align: left" seq="">       </div>
<div class="col-xs-2" style="width:14%; margin-top: -5px; margin-left:120px;">
          <input type="submit" class="btn btn-default add_color uploadSmaCsv" value="Upload" name="submit" style="padding:4px;margin-left:5%;" onclick="uploadSmaCsv('+responseObj.data[i].id+')">
        </div>
      </div>
      <div class="row" >
        <div class="col-xs-12" > 
          <!-- general form elements -->
          <div class="box box-info" style="height:auto; overflow:visible;"> 
            <!-- form start -->
            <form onsubmit="return false;" >
              <div class="box-body row">
                <div class="col-xs-2" style="text-align:center" > <b> Country</b>
                  <select id="add_sma_country_1" class="form-control add_sma_country" >
                  </select>
                </div>
                <div class="col-xs-2 " style="text-align:center"> <b>State/Province</b>
                  <select id="add_sma_state_1" class="form-control " style="background-color:#b0b5b9;color:#fff;" multiple="multiple" >
                  </select>
                </div>
                <div class="col-xs-1" style="padding:2% 0;width:3%;cursor:pointer; margin-left:10px" id="sma_state_check"> <img src="images/ok_checkmark_red_T.png" alt="" class="chkmrk-icon"/>  </div>
                <div class="col-xs-2 " style="text-align:center; width: 14%"> <b>&nbsp;&nbsp;&nbsp;County&nbsp;&nbsp;&nbsp;&nbsp;</b>
                  <select id="add_sma_county_1" class="form-control " style="background-color:#b0b5b9;color:#fff;" multiple="multiple" >
                  </select>
                </div>
                <div class="col-xs-1" style="padding:2% 0;width:3%;cursor:pointer; margin-left:10px" id="sma_county_check"> <img src="images/checked-yellow.png" alt="" class="chkmrk-icon"> </div>
                <div class="col-xs-2 " style="text-align:center; width:14%" > <span style="font-weight:700">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;City&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                  <select id="add_sma_city_1" class="form-control" style="background-color:#b0b5b9;color:#fff;" multiple="multiple" >
                  </select>
                </div>
                <div class="col-xs-1" style="padding:2% 0;width:3%;cursor:pointer;margin-left:10px" id="sma_city_check"> <img src="images/checked-yellow.png" alt="" class="chkmrk-icon"> </div>
                <div class="col-xs-3" style="width:24%;text-align:center"> <b>Postal/Zip Code</b>
                  <textarea id="postal_code" class="form-control postal code" placeholder="Postal Code"></textarea>
                </div>
                <br>
                <br>
                <br>
              </div>
              <br>
              <br>
              <div class="row"> <span class=" col-xs-2" style="padding-top:5px; text-align:right; font-weight:600">Save&nbsp;SMA&nbsp;as:</span>
                <div class="col-xs-3" style="margin-left:0px; width:16.5%">
                  <input type="text" id="add_sma_name" class="form-control" >
                </div>
                <div class= "col-xs-3"><span style="padding-top:5px; padding-right:3px; font-weight:600">Set&nbsp;Default</span>
                  <input style="margin-top:10px;" type="checkbox" id="set_default"/>
                  <input type="hidden" id="hidden_sma_id"/>
                  <input type="hidden" id="old_sma_name"/>
                </div>
              </div>
              <br>
              <div class="box-footer">
                <button type="type" style="display: none" id="update_sma_location" class="btn btn-primary">Update</button>
                <button type="submit" id="add_sma_location" class="btn btn-primary">Submit</button>
                <button type="button" id="back_sma_location" class="btn btn-primary" style="display:none;">Back</button>
              </div>
              
              <!-- /.box-body -->
              
            </form>
          </div>
        </div>
      </div>
      <div style="display: table;width: 100%;background: #ffffff;margin: 20px 0;border: 2px solid #dddddd;">
        <div class="row">
          <div class="col-sm-12">
            <div style="padding: 20px;width: 100%;display: table;border-bottom: 2px solid #dddddd;">
              <div class="row">
                <div class="col-sm-2">
                  <div class="form-group">
                    <label>Page No</label>
                    <select class="form-control" id="cityPagination" onchange="getSmaLocListWithPage(this)">
                    </select>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <label>Per Page</label>
                    <select class="form-control" id="cityPerPage" onchange="getSmaLocListWithPerPage(this)">
                      <option value="1" selected>1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-3 col-sm-offset-5">
                  <div class="form-group">
                    <label>Search SMA</label>
                    <input type="text" class="form-control" placeholder="Search Cities" onkeyup="getStateWithSearch(this)">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div id="sma_lists">
              <table class="table table-condensed">
                <thead>
                <tr>
                  <th style="text-align:center">SMA</th>
                  <th style="text-align:center">Country</th>
                  <th style="text-align:center">State</th>
                  <th style="text-align:center">County</th>
                  <th style="text-align:center">City</th>
                  <th style="text-align:center">Postal Code</th>
                  <th style="text-align:center">Action</th>
                  <th class="table-action"><span class="table-action"></span></th>
                </tr>
                </thead>
                <tbody id="sma_loc_list"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
      <!-- page end--> 
    </section>
  </section>
</section>

<!--#######################code to add popup on sma list##########################################################-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display:none;" id="postal_code_modal">
  <div style="position:relative; background:black; opacity:0.5; top:0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:-17%; width:136%; height:100%; margin-left:auto; margin-right:auto;">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%;display:table-row">
        <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; " id="asseriesUpdateOverlay">
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;"> <br>
              <br>
              <br>
              <br>
              <br>
              <div style="width:80%"> 
                <!--  code of popup start-->
                <div class="container">
                  <div id="modal" class="popupContainer" style="display:block;  top: 23px; position: absolute;
  width: 21%;
  left: 39%;
  top: 30px;
  background: #FFF;">
                    <header class="popupHeader" style="  background: #F4F4F2;
  position: relative;
  padding: 10px 20px;
  border-bottom: 1px solid #DDD;
  font-weight: bold;
  font-family: 'Source Sans Pro', sans-serif;
  font-size: 14px;
  color: #666;
  font-size: 16px;
  text-transform: uppercase;
  text-align: left;
  height:9%"> <span class="header_title">Postal Codes </span> <img src="images/remove.png"  alt="" id="close_postal_popup" class="close_popup"> <span id="errmsg_add_airport_rate" style="color:red;"></span> </header>
                    <section class="popupBody">
                      <div class="form-group serviceType_css">
                        <div class="col-xs-9 show_postal"  style="margin-bottom: 8%; font-size: 17px;"> </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--end of add location popup--> 

<!--end of add sma popup--> 

<!--Loading indicator-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;" id="refresh_overlay">
  <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%; display:table-row">
        <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px" >
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto"><br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <div class="loadingGIF"> <img style="height:70px;width:70px;" src="images/loading.gif" alt="Page loading indicator"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="js/lib/jquery.js"></script>
<script src="pageJs/dashboard.js"></script>
<script src="pageJs/logout.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script src="js/bootstrap-multiselect.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 
<script src="pageJs/sma.js?js=<?php echo uniqid(); ?>"></script> 

<!--dynamic table--> 
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 

<!--common script init for all pages--> 
<script src="js/scripts.js"></script>
<!--<script src="pageJs/searchbox.js"></script> -->
<script type="text/javascript">


// $("table.sieve").sieve();
 

</script>
</body>
</html>
