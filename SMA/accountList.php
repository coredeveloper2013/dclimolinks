<?php
include_once 'session_auth.php';

$_page = 'acl';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Customer Account List</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>

    <!--dynamic table-->
    <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet"/>
    <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css"/>

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/limo_car.css" rel="stylesheet">
    <link href="css/bootstrap-multiselect.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <script src="js/lib/jquery.js"></script>
    <script src="pageJs/validation.js"></script>
    <link rel="stylesheet" href="JQ_timepicker/jquery_addon_css.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <style>
        .table-action {
            text-align: right;
            font-size: 14px
        }

        .loadingGIF {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width: 100%;
            padding-left: 30%;
            padding-top: 10%;
        }
    </style>
</head>

<body>
<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-md-11">
                    <h4 class="page-title" style="text-align:center;">User Account List</h4>
                </div>
                <br>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div data-ng-controller="SharedData" class="m-t-md ng-scope">
                        <!--######################################### Data Display ####################################################-->
                        <div>
                            <div class="row">
                                <div class="col-xs-12"></div>
                            </div>
                            <div class="row mb-sm ng-scope">
                                <div class="col-sm-12"></div>
                            </div>
                            <div style="overflow:scroll;min-height:500px;">
                                <table class="table table-condensed ng-scope sieve">
                                    <thead>
                                    <tr>
                                        <th style="text-align:center">Name</th>
                                        <th style="text-align:center">Email</th>
                                        <th style="text-align:center">Account No</th>
                                        <th style="text-align:center">Country(code)</th>
                                        <th style="text-align:center;">Account Type</th>
                                        <th style="text-align:center">Status</th>
                                        <th style="text-align:center">Action</th>
                                        <th class="table-action"><span class="table-action"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody id="user_account_list">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- page end-->
        </section>
    </section>

    <!--main content end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->

<!--Loading indicator-->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display: none;"
     id="refresh_overlay;">
    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%;"></div>
    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto;">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%; display:table-row">
                <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px;">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="loadingGIF">
                                <img style="height:70px;width:70px;" src="images/loading.gif" alt="loading indicator">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- Loading Indicator Ends-->
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/bootstrap-multiselect.js"></script>
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="assets/easypiechart/jquery.easypiechart.js"></script>

<!--Sparkline Chart-->
<script src="assets/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->

<script src="assets/flot-chart/jquery.flot.js"></script>
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="assets/flot-chart/jquery.flot.resize.js"></script>
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script>

<!--dynamic table-->

<script type="text/javascript" src="pageJs/add_account.js"></script>
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="JQ_timepicker/jquery_addon_js.js"></script>
<script src="JQ_timepicker/slideaccess.js"></script>
<script src="pageJs/searchbox.js"></script>
<!--dynamic table initialization -->
<script src="js/dynamic_table/dynamic_table_init.js"></script>

<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<script>
    $("table.sieve").sieve();
    //alert('ok1');

    /*$('#start_date, #endCalender').datepicker();

    $('#start_time, #end_time').timepicker({timeFormat: "hh:mm tt"});

     $('#adcs_schg_fee').on("click",function(){

       var getSeq=0;

       $('.commanRowId').each(function(index){

         getSeq=parseInt(index)+1;


       });


   */

    $("#office_phone").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything
        if (e.which > 31 && (e.which < 48 || e.which > 57)) {


            return false;

        }

    });
    $("#home_phone").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything
        if (e.which > 31 && (e.which < 48 || e.which > 57)) {


            return false;

        }

    });
    $("#cellular_phone").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything
        if (e.which > 31 && (e.which < 48 || e.which > 57)) {


            return false;

        }

    });

    $("#fax_number").keypress(function (e) {

        //if the letter is not digit then display error and don't type anything
        if (e.which > 31 && (e.which < 48 || e.which > 57)) {


            return false;

        }

    });


    $('#acount_type').multiselect({
        maxHeight: 200,
        buttonWidth: '155px',
        includeSelectAllOption: true
    });
    discountDbClass.getuserAccounList();
</script>
<script src="pageJs/dashboard.js"></script>

</body>
</html>