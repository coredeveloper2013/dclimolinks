<?php
include_once 'session_auth.php';

$_page = 'avt';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="images/favicon.png">
<title>My Limo Project | Add Vehicles</title>
<!--Core CSS -->
<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet">
<style type="text/css">
td {
	text-align: center;
}
.file-class {
	left: 935px;
	min-height: 33px;
	opacity: 0;
	position: absolute;
	text-align: right;
	top: 0;
	width: 12%;
}
.fileUpload {
	padding-bottom: 50px;
	width: 100%;
	padding-top: 20px;
}
.commanUpload >img {
	width: 85px;
	display: inline-block;
}
.close_popup {
	width: 16px;
	margin-left: 97%;
	margin-top: -2%;
	cursor: pointer;
	height: 16px;
}
.btn-default {
	color: #fff;
	background-color: #1FB5AD;
	border-color: #1FB5AD;
}
.btn-default:hover {
	color: #fff;
	background-color: #2FA9A2;
	border-color: #2FA9A2;
}
.btn-primary {
	color: #fff;
	background-color: #1FB5AD;
	border-color: #1FB5AD;
}
.btn-primary:hover {
	color: #fff;
	background-color: #2FA9A2;
	border-color: #2FA9A2;
}
.multiselect-container li {
	text-align: left;
}
div.box {
	margin-top: 1%;
	padding-top: 2%;
	border: 1px solid #C1C1C1;
	background-color: #E5E5FF;
}
</style>
</head>
<body>
<section id="container" >

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


  <!--main content start-->
  <section id="main-content">
    <section class="wrapper"> 
      <!-- page start-->
      
      <div class="row">
        <div class="col-sm-12">
          <section class="panel" style="height:570px;">
          <div class="panel-heading">
          <b style="text-align:left; padding-left:2%; font-size:16px;"> ADD VEHICLE</b>
          <div class="col-md-12" > 
            <!-- general form elements -->
            <div class="box box-info" style="height:504px;">
            <!-- form start -->
            <form id="addVehicleForm"  method="post"     enctype="multipart/form-data">
              <div class="col-xs-12 col-sm-6 col-md-6" style="margin-top:3%;">
                <div class="box-body"> 
                  <!-- vehiclecode starts-->
                  <div class="row" style="text-align:center; margin-bottom: 5%;">
                    <div class="col-xs-12 col-sm-4 col-md-4"> <b> Vehicle Code</b> </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                      <input type="text" required class="form-control" name="vehicle_code" id="vehicle_code">
                    </div>
                    <div class="col-xs-12 col-sm-2 col-md-2"></div>
                  </div>
                </div>
                <!-- vehicle code ends--> 
                <!-- vehicle type starts-->
                <div class="row" style="text-align:center; margin-bottom: 5%;">
                  <div class="col-xs-12 col-sm-4 col-md-4"> <b>Vehicle Type</b> </div>
                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <input type="text" required class="form-control" name="vehicle_type" id="vehicle_type">
                  </div>
                  <div class="col-xs-12 col-sm-2 col-md-2"></div>
                </div>
                <!-- vehicle type ends--> 
                
                <!-- Title/Description starts-->
                <div class="row" style="text-align:center; margin-bottom: 5%;">
                  <div class="col-xs-12 col-sm-4 col-md-4"> <b> Title/Description</b> </div>
                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <input type="text" required class="form-control" name="vehicle_title" id="vehicle_title">
                  </div>
                  <div class="col-xs-12 col-sm-2 col-md-2"></div>
                </div>
                <!-- Title/Description ends--> 
                <!--passenger capacity starts-->
                <div class="row" style="text-align:center; margin-bottom: 5%;">
                  <div class="col-xs-12 col-sm-4 col-md-4"> <b> Passenger Capacity</b> </div>
                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <input type="text" required class="form-control" id="vehicle_passenger_capacity" name="vehicle_passenger_capacity">
                  </div>
                  <div class="col-xs-12 col-sm-2 col-md-2"></div>
                </div>
                <!--passenger capacity ends--> 
                <!--luggage capacity starts-->
                <div class="row" style="text-align:center; margin-bottom: 5%;">
                  <div class="col-xs-12 col-sm-4 col-md-4"> <b> Luggage Capacity</b> </div>
                  <div class="col-xs-12 col-sm-6 col-md-6">
                    <input type="text" required class="form-control" id="vehicle_luggage_capacity" name="vehicle_luggage_capacity">
                  </div>
                  <div class="col-xs-12 col-sm-2 col-md-2"></div>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <input type="submit" value="Submit" class="btn btn-primary SaveBtn">
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <input type="button" value="Back" class="btn btn-primary Backbtn" style="display:none">
                    </div>
                  </div>
                </div>
              </div>
              <!--luggage capacity ends--> 
              <!--upload image starts-->
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="row" style="text-align:center; margin-bottom: 1%;">
                  <div class="col-xs-12 col-sm-12 col-md-12"> <b> Image Upload</b> </div>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="col-sm-6 col-md-6">
                      <input type="file" class="fileUpload newFile1" seq="1"  name="files2[]" multiple>
                      <input type="text" class="file_type_1" style="display:none" >
                    </div>
                    <div class="showImageBeforeUpload_1 commanUpload col-sm-4 col-md-4"> </div>
                    <div class="imageAction_1 commanShowImage col-sm-2 col-md-2"> </div>
                  </div>
                  <br>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="col-sm-6 col-md-6">
                      <input type="file" class="fileUpload newFile2" seq="2"   name="files2[]" multiple>
                      <input type="text" style="display:none" class="file_type_2">
                    </div>
                    <div class="showImageBeforeUpload_2 commanUpload col-sm-4 col-md-4" ></div>
                    <div class="imageAction_2 commanShowImage col-sm-2 col-md-2"> </div>
                  </div>
                  <br>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="col-sm-6 col-md-6">
                      <input type="file" class="fileUpload newFile3" seq="3"   name="files2[]" multiple>
                      <input type="text" style="display:none" class="file_type_3">
                    </div>
                    <div class="showImageBeforeUpload_3 commanUpload col-sm-4 col-md-4" ></div>
                    <div class="imageAction_3 commanShowImage col-sm-2 col-md-2"> </div>
                  </div>
                  <br>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="col-sm-6 col-md-6">
                      <input type="file" class="fileUpload newFile4" seq="4"  name="files2[]" multiple>
                      <input type="text" style="display:none" class="file_type_4">
                    </div>
                    <div class="showImageBeforeUpload_4 col-sm-4 col-md-4 commanUpload" ></div>
                    <div class="imageAction_4 commanShowImage col-sm-2 col-md-2"> </div>
                  </div>
                  <br>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="col-sm-6 col-md-6">
                      <input type="file" class="fileUpload newFile5" seq="5"  name="files2[]" multiple>
                      <input type="text" style="display:none" class="file_type_5">
                    </div>
                    <div class="showImageBeforeUpload_5 col-sm-4 col-md-4 commanUpload"></div>
                    <div class="imageAction_5 commanShowImage col-sm-2 col-md-2"> </div>
                  </div>
                  <br>
                </div>
              </div>
              <!--upload image ends--> 
              <br>
              <!--submit button starts-->
              <div class="row" style="text-align:center; margin-bottom: 1%;">
                <div class="col-xs-12 col-sm-2 col-md-2"> </div>
                <div class="col-xs-12 col-sm-7 col-md-7"></div>
              </div>
              <!--submit button ends-->
              
            </form>
        
        </div>
      </div>
      <div>
      </div>
      <div class="row">
        <table class="table table-condensed ng-scope sieve">
          <thead >
            <tr >
              <th style="text-align:center">#</th>
              <th style="text-align:center">Vehicle Code</th>
              <th style="text-align:center">Vehicle Type</th>
              <th style="text-align:center">Thumbnail</th>
              <th style="text-align:center">Passenger Capacity</th>
              <th style="text-align:center"> Luggage Capacity</th>
              <th style="text-align:center">view</th>
              <th style="text-align:center">Action</th>
            </tr>
          </thead>
          <tbody id="vehicleItem">
          </tbody>
        </table>
      </div>
      </div>
    </section>
    </div>
    </div>
    
    <!-- page end--> 
  </section>
</section>
<!--main content end-->
</section>

<!--Core js--> 
<!--Loading indicator-->
<div style="position: fixed; top: 0px; z-index: 10000; text-align: center; width: 100%; height: 100%; display: none;" id="refresh_overlay">
  <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"> </div>
  <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto">
    <div style="width:100%;display:table; height:100%;">
      <div style="width:100%; display:table-row">
        <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto">
          <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px" >
            <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto"><br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <div class="loadingGIF"> <img style="height:70px;width:70px;" src="images/loading.gif" alt="Page loading indicator"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Loading Indicator Ends--> 

<script src="js/lib/jquery.js"></script> 
<script src="pageJs/dashboard.js"></script> 
<script src="pageJs/logout.js"></script> 
<script src="bs3/js/bootstrap.min.js"></script> 
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.js"></script> 

<!--Easy Pie Chart--> 
<script src="assets/easypiechart/jquery.easypiechart.js"></script> 
<!--Sparkline Chart--> 

<!--dynamic table--> 
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script> 
<!--common script init for all pages--> 
<!-- code start here for search box--> 
<script src="pageJs/searchbox.js"></script> 
<!-- code end here for search box--> 

<script src="js/scripts.js"></script> 
<script src="pageJs/add_vehicle.js"></script> 
<script src="js/dynamic_table/dynamic_table_init.js"></script> 
<script src="js/bootstrap-multiselect.js"></script> 
<script type="text/javascript">
    $("table.sieve").sieve();
	
	
	

    $(".fileUpload").on('change', function () {
      var getSeq=$(this).attr("seq");
	  
	  
	  $('.fileUpload img').prop("img-responsive",true);
     //Get count of selected files
     var countFiles = $(this)[0].files.length;
 
     var imgPath = $(this)[0].value;

     var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
     var image_holder = $(".showImageBeforeUpload_"+getSeq);
     image_holder.empty();
 
     if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
         if (typeof (FileReader) != "undefined") {
             //loop for each file selected for uploaded.
             for (var i = 0; i < countFiles; i++) {
                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $("<img />", {
                         "src": e.target.result
                     }).appendTo(image_holder);
                 }
 
                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
                 $('.imageAction_'+getSeq).html('');
                 $('.imageAction_'+getSeq).html('<button class="btn btn-primary deleteImageClass" seq="'+getSeq+'">delete</button>');
                 
                 $('.file_type_'+getSeq).val(getSeq);


                 $('.deleteImageClass').on("click",function(){
                  var getseq=$(this).attr("seq");
                  $('.showImageBeforeUpload_'+getseq).html('');
                  $('.imageAction_'+getseq).html('');

                  $('input[seq='+getseq+']').val('');
                  $('.file_type_'+getseq).val('');

                 });
             }


             
 
         } else {
             alert("This browser does not support FileReader.");
         }
     } else {
         alert("Pls select only images");
         $('.file_type_'+getseq).val('');

     }
 });


    $('.Backbtn').on("click",function(){

      location.reload();

    });


</script>
</body>
</html>