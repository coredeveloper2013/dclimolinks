<?php
include_once 'session_auth.php';

$_page = 'seaDB';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Seaport Database Setup</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet">
    <style type="text/css">
        input[type=file] {
            left: 89%;
            min-height: 33px;
            opacity: 0;
            position: absolute;
            text-align: right;
            top: 0;
            width: 12%;
        }

        .multiselect-container li {
            text-align: left;
        }

        .multiselect-container {
            z-index: 289;
        }

        div.box {
            margin-top: 1%;
            padding-top: 2%;
            border: 1px solid #C1C1C1;
            background-color: #E5E5FF;
        }

        .close_popup {
            width: 16px;
            margin-left: 97%;
            margin-top: -2%;
            cursor: pointer;
            height: 16px;
        }

        .btn-default {
            color: #fff;
            background-color: #1FB5AD;
            border-color: #1FB5AD;
        }

        .btn-default:hover {
            color: #fff;
            background-color: #2FA9A2;
            border-color: #2FA9A2;
        }

        .btn-primary {
            color: #fff;
            background-color: #1FB5AD;
            border-color: #1FB5AD;
        }

        .btn-primary:hover {
            color: #fff;
            background-color: #2FA9A2;
            border-color: #2FA9A2;
        }

        .loadingGIF {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width: 100%;
            padding-left: 30%;
            padding-top: 10%;
        }

        .alignCENTER {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }

        .chkmrk-icon {
            width: 30px;
            height: 30px;
            margin-top: 3px;
        }

        @media screen and (min-width: 1366px) {
            .chkmrk-icon {
                margin-top: -8px;
            }
        }
    </style>
</head>
<body>
<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-xs-12">
                    <section class="panel">
                        <header class="panel-heading alignCENTER">
                            <h4>SEAPORT ZONE DATABASE SETUP</h4>
                            <div class="col-xs-12">
                                <div class="col-xs-6" style="width:95%"></div>
                                <div class="col-xs-2">
                                    <button class="btn btn-primary add_seaport" id="add_new_seaport">Add Seaport
                                    </button>
                                </div>
                                <div class="col-xs-2">
                                    <button class="btn btn-primary" style="margin-left:2%;" id="seaPortdbSetting">DB
                                        Setting
                                    </button>
                                </div>
                                <div class="col-xs-2" style="margin-left:30px;position: relative;">
                                    <form id="getformdata" style="float: right;">
                                        <div tabindex="500" class="btn btn-primary btn-file"><i
                                                class="glyphicon glyphicon-folder-open"></i> <span class="hidden-xs1">Import</span>
                                            <input id="importSeaport" name="input24" type="file" multiple class="" style="width: 100%;left: 0;">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </header>
                        <!-- Seaport Form Start here -->
                        <div>
                            <div class="col-xs-12">
                                <!-- general form elements -->
                                <div class="box box-info" style="height:130px;">
                                    <!-- form start -->
                                    <form id="seaportDbFormSubmit" style="z-index:333;">
                                        <div class="box-body">
                                            <div class="col-xs-2" style="text-align:center"><b> Country</b>
                                                <select id="seaport_db_country" class="form-control seaport_db_country">
                                                </select>
                                            </div>
                                            <div class="col-xs-2" style="text-align:center"><b> State/Province</b>
                                                <select id="seaport_db_state" class="form-control seaport_db_state">
                                                </select>
                                            </div>


                                            <div class="col-xs-2 " style="text-align:center; width:22.5%"><span
                                                    style="font-weight:700; white-space:nowrap;">City/Town</span><br/>
                                                <select id="seaport_db_city" class="form-control "
                                                        style="background-color:#b0b5b9;color:#fff;"
                                                        multiple="multiple">
                                                </select>
                                            </div>
                                            <div class="col-xs-1"
                                                 style="padding:2% 0;width:5%;cursor:pointer; margin-left:0px"
                                                 id="seaport_db_city_check"><img src="images/ok_checkmark_red_T.png"
                                                                                 alt="" class="chkmrk-icon"
                                                                                 style="margin-left:20px"/></div>
                                            <div class="col-xs-3 "
                                                 style="text-align:center; width: 16%; padding-left:20px; padding-right:10px;">
                                                <span style="font-weight:700; white-space: nowrap;">Seaport&nbsp;Name</span>
                                                <select id="seaport_db_seaport_code" class="form-control "
                                                        style="background-color:#b0b5b9;color:#fff;"
                                                        multiple="multiple">
                                                </select>
                                            </div>
                                            <div class="col-xs-1"></div>
                                            <div class="col-xs-1 " style="text-align:left; margin-top: 17px;">
                                                <button type="submit" class="btn btn-primary">Activate</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="pad-wrapper">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="page-title"></h4>
                                    <table class="table table-condensed ng-scope sieve ">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><span class="line">Seaport Code</span></th>
                                            <th><span class="line">Seaport Name</span></th>
                                            <th><span class="line">Seaport Zip Code</span></th>
                                            <th><span class="line">View</span></th>
                                            <th><span class="line">Action</span></th>
                                        </tr>
                                        </thead>
                                        <tbody id="seaport_db_table">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>

<!-- popup Code start here -->
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display:none;"
     id="add_seaport_popup">
    <div style="position:relative; background:black; opacity:0.5; top: 0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:-17%; width:136%; height:100%; margin-left:auto; margin-right:auto;">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%;display:table-row">
                <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; "
                         id="asseriesUpdateOverlay">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto;">
                            <!--<div style="width:100%">
                                          <img class="" style="width:50%; height:150px;" src="img/One-shop_Logo_Revised.png"></div>-->
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div style="width:80%">
                                <!--  code of popup start-->
                                <div class="container">
                                    <div id="modal" class="popupContainer"
                                         style="display:block;  top: 23px; position: absolute; width: 21%; left: 39%; top: 30px; background: #FFF; box-shadow: 0px 10px 20px rgba(53, 51, 51, 0.84); border-radius: 4px;">
                                        <header class="popupHeader" style="  background: #F4F4F2; position: relative; padding: 14px 20px;border-bottom: 1px solid #DDD;font-weight: bold;font-family: 'Source Sans Pro', sans-serif; font-size: 16px;color: #666;text-transform: capitalize; text-align: left;
            height:9%; margin-top:2px;"><span class="header_title" style="position:absolute; padding-left:30%">Add Seaport </span>
                                            <img src="images/remove.png" alt=""
                                                 id="close_rate_vehicle_rate_popup" class="close_popup"> <span
                                                    id="errmsg_add_airport_rate" style="color:red;"></span></header>
                                        <section class="popupBody">
                                            <div class="form-group serviceType_css">
                                                <div class="col-xs-12 show_postal" style=" font-size: 13px;">
                                                    <div style="height:auto;">
                                                        <form id="add_seaport_manual">
                                                            <div class="form-group">
                                                                <div class="col-sm-6" style="font-weight: bold;">
                                                                    Country Name
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <select class="form-control sma_country_list"
                                                                            id="country_name" name="country_name1"
                                                                            style="margin-bottom: 19px;" required>
                                                                        <option value="">Select Country</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-6" style="font-weight: bold;"> State
                                                                    Name
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <select class="form-control sma_state_list"
                                                                            id="state_name" name="state_name1"
                                                                            style="margin-bottom: 19px;" required>
                                                                        <option value="">Select State/Province</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-6" style="font-weight: bold;"> City
                                                                    Name
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <select class="form-control sma_city_list"
                                                                            style="margin-bottom: 19px;" id="city_name"
                                                                            name="city_name1" required>
                                                                        <option value="">Select City/Town</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-6" style="font-weight: bold;">
                                                                    Seaport Name
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control"
                                                                           id="seaport_name" name="seaport_name1"
                                                                           style="margin-bottom: 19px;" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-6" style="font-weight: bold;">
                                                                    Seaport Code
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control"
                                                                           id="seaport_code" name="seaport_code1"
                                                                           style="margin-bottom: 19px;" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-6" style="font-weight: bold;"> City
                                                                    Zip Code
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control"
                                                                           id="city_zip_code" name="city_zip_code1"
                                                                           style="margin-bottom: 19px;" required>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <div class="form-group">
                                                                <div class="col-sm-6">
                                                                    <button type="button" id="cancel_popup"
                                                                            class="btn btn-primary"
                                                                            style="margin:20px 5px 10px 5px;">Cancel
                                                                    </button>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <button type="submit" class="btn btn-primary"
                                                                            style="margin:20px 5px 10px 5px;">Submit
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <br>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- popup Code end here -->
<!--Core js-->

<script src="js/lib/jquery.js"></script>
<script src="pageJs/dashboard.js"></script>
<script src="pageJs/logout.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<script src="pageJs/searchbox.js"></script>
<script src="pageJs/seaport_db.js"></script>
<script src="pageJs/getcountry_state.js"></script>
<script src="js/bootstrap-multiselect.js"></script>
<script type="text/javascript">

    $("table.sieve").sieve();
    $('#seaport_db_city,#seaport_db_seaport_code').multiselect({
        maxHeight: 200,
        buttonWidth: '155px',
        includeSelectAllOption: true
    });


    $('#addnewFormSeaport').on("click", function () {

        window.localStorage.setItem("seaport_db_table_id", "string12");
        window.location.href = "sma_seaport.html";
    })


    /*  edit airport and country edit code stare here */
    $('#seaPortdbSetting').on("click", function () {

        window.location.href = "editSeaportDb.html";

    });
    /*  edit airport and country edit code End here */
    /*  add airport manually popup start here */
    $('.close_popup').on("click", function () {
        $('#add_seaport_popup').hide();
    });

    $('#cancel_popup').on('click', function () {

        $('#add_seaport_popup').hide();
    });

    $('.add_seaport').on("click", function () {


        $('#country_name').val("");
        $('#state_name').val("");
        $('#city_name').val("");
        $('#airport_name').val("");
        $('#airport_code').val("");
        $('#airport_zip_code').val("");
        $('#add_seaport_popup').show();

    });

    /*  add airport manually popup end here */
</script>
</body>
</html>
