<?php
include_once 'session_auth.php';

$_page = 'fds';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Fare Discounts Setup</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>

    <!--dynamic table-->
    <link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet"/>
    <link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css"/>

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/limo_car.css" rel="stylesheet">
    <link href="css/bootstrap-multiselect.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <script src="js/lib/jquery.js"></script>
    <script src="pageJs/validation.js"></script>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="JQ_timepicker/jquery_addon_css.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <style>
        select[multiple] {
            height: 35px;
        }

        .loadingGIF {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width: 100%;
            padding-left: 30%;
            padding-top: 10%;
        }

        .alignCENTER {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
    </style>
</head>

<body>
<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-md-6">
                    <h4 class="page-title ">Discount Form For Fare </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-info" style="height:650px;">
                        <!-- form start -->
                        <form id="discount_form">
                            <div class="box-body row">
                                <div class="col-xs-12" style="text-align:center">
                                    <h4><strong><span class="page-title ">Fare's Discount Coupon Setup</span> </strong>
                                    </h4>
                                    <br>
                                </div>
                                <!-- col-xs-12-->
                            </div>

                            <!-- row1-->
                            <br>
                            <div class="row" style="text-align:center;">
                                <div class="col-xs-1" style="text-align:left;"><b> Name</b></div>
                                <div class="col-xs-2" style="text-align:center;"><b></b>
                                    <input type="text" class="form-control" name="cupon_name" id="cuppon_name"
                                           placeholder="name" required>
                                </div>
                            </div>
                            <br>
                            <div class="row" style="text-align:center;">
                                <div class="col-xs-1" style="text-align:left;"><b>Code</b></div>
                                <div class="col-xs-2" style="text-align:center;"><b></b>
                                    <input type="text" class="form-control" name="cupon_code" id="cuppon_code"
                                           placeholder="code" required>
                                </div>
                            </div>
                            <br>
                            <div class=" row" style="text-align:center">
                                <div class="col-xs-1" style="text-align:left;"><b>Value</b></div>
                                <div class="col-xs-2" style="text-align:center;">
                                    <input type="text" class="form-control" name="cupon_value" id="cuppon_value"
                                           placeholder="value" required>
                                </div>
                                <div class="col-xs-1" style="text-align:left; width:10%">
                                    <select class="form-control typeRate" name="cupon_discount_type"
                                            id="cupon_discount_type" required>
                                        <option value="">--Select--</option>
                                        <option value="%">%</option>
                                        <option value="$">$</option>
                                    </select>
                                </div>
                            </div>
                            <!-- row2-->
                            <br>
                            <div class=" row" style="text-align:center">
                                <div class="col-xs-3" style="text-align:left;"><b><br>
                                    Travel Date Validation (service date) </b></div>
                                <div class="col-xs-2" style="text-align:center;"><b>Start Date</b>
                                    <input type="text" class="form-control datepicker" name="start_date" id="start_date"
                                           required>
                                </div>
                                <div class="col-xs-1" style="text-align:center;"><b><br>
                                    To</b></div>
                                <div class="col-xs-2" style="text-align:center;"><b>End Date</b>
                                    <input type="text" class="form-control datepicker" id="endCalender"
                                           name="endCalender" required>
                                </div>
                            </div>
                            <br>
                            <div class=" row" style="text-align:center">
                                <div class="col-xs-3" style="text-align:left;"><b><br>
                                    Online booking date range (booking date)</b></div>
                                <div class="col-xs-2" style="text-align:center;"><b>Start Date</b>
                                    <input type="text" class="form-control time-picker" id="start_time"
                                           name="start_time">
                                </div>
                                <div class="col-xs-1" style="text-align:center;"><b><br>
                                    To</b></div>
                                <div class="col-xs-2" style="text-align:center;"><b>End Date</b>
                                    <input type="text" class="form-control time-picker" id="end_time" name="end_time">
                                </div>
                            </div>
                            <!-- row3-->

                            <br>
                            <div class=" row" style="text-align:center">
                                <div class="col-sm-2" style="text-align:center;"><b>Applies on</b>
                                    <select class="rate_type" id="rate_type" name="rate_type">
                                        <option value="BSR"> Base Rate</option>
                                        <option value="GRT"> Grand Total</option>
                                    </select>
                                </div>
                                <div class="col-sm-3" style="text-align:center;"><b>Applies to Service Type</b>
                                    <select class="service_type" id="service_type" multiple="multiple"
                                            name="service_type" required>

                                        <!-- <option value="PTP"> Point-to-Point </option>
                                                <option value="AIRA"> From Airport </option>
                                                <option value="AIRD"> To Airport </option>
                                                <option value="MILG"> Mileage Based</option>
                                                <option value="SEAA"> From Seaport </option>
                                                <option value="SEAD"> To Seaport </option> -->
                                    </select>
                                </div>
                                <div class="col-xs-1"></div>
                                <div class="col-xs-3" style="text-align:left;"><b>Applies to Vehicle Type </b>
                                    <select id="apply_vehicle_type" name="apply_vehicle_type" multiple="multiple">
                                        <option value="">--Select--</option>
                                    </select>
                                </div>
                                <div class="col-xs-2" style="text-align:center;"><b>Associate with SMA</b>
                                    <select id="apply_sma" name="apply_sma" multiple="multiple">
                                        <option value="">Select SMA</option>
                                    </select>
                                </div>
                            </div>
                            <!-- row7-->
                            <br>
                            <div class=" row" style="text-align:center">
                                <div class="col-xs-3" style="text-align:center;">
                                    <input type="radio" class="autoApply" value="1" id="promo_code" name="promo_code">
                                    <b style="margin-left:5px">Auto Apply Promocode</b></div>
                                <div class="col-xs-2" style="text-align:center;">
                                    <input type="radio" class="promoCode" checked value="2" id="promo_code2"
                                           name="promo_code2">
                                    <!--renamed id/name to promo_code2" from "promo_code"-->
                                    <b style="margin-left:5px">Apply Promocode</b></div>
                                <div class="col-xs-1"></div>
                                <div class="col-xs-3" style="text-align:justify;">
                                    <input type="checkbox" name="is_combine_discount" id="is_combine_discount"
                                           class="combineDisc" value="1">
                                    <!-- Duplicate combineDisc class-->
                                    <b style="margin-left:5px">Combined Discount</b></div>
                            </div>

                            <!--  aplly procode code Start here -->

                            <div class="promocodeManageAll" style="display:none">

                                <!-- Apply Promocod to All  -->

                                <div class="allUserRadio col-xs-12">
                                    <input type="radio" class="applyAutoPromo" value="All" id="applyAutoPromo"
                                           name="applyAutoPromo">
                                    <b style="margin-left:5px">Apply Auto Promocode To All</b></div>
                                <div class="col-xs-4">
                                    <input type="radio" class="applyAutoPromoUser" value="AllUser"
                                           id="applyAutoPromoUser" name="applyAutoPromo">
                                    <b style="margin-left:5px">Apply Auto Promocode To User </b></div>
                                <div class="allUserMultiSelect col-xs-6" style="visibility:hidden;">
                                    <select multiple id="allUser">
                                        <option>select One</option>
                                        <option>select One</option>
                                        <option>select One</option>
                                        <option>select One</option>
                                        <option>select One</option>
                                    </select>
                                </div>

                                <!-- Apply Promocod End All  -->

                            </div>
                            <!--  aplly procode code end here -->

                            <br>
                            <div class=" row" style="text-align:center">
                                <div class="col-xs-4" style="text-align:left;"></div>
                                <br>
                                <br>
                            </div>
                            <!-- Remaining Miles Row-->

                            <!-- Apply to vehicle type-->
                            <div class="row"><br>
                            </div>
                            <!-- Associate with SMA -->

                            <div class="row">
                                <div class="col-xs-12 " style="text-align:center;"><b></b>
                                    <button type="submit" id="save_rate12" class="btn btn-primary"> Add</button>
                                    <button type="button" id="back_button" class="btn btn-primary"
                                            style="display:none;"> Back
                                    </button>
                                </div>
                            </div>
                            <!-- row save matrix button-->

                            <!-- /.box-body -->

                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div data-ng-controller="SharedData" class="m-t-md ng-scope">
                        <!--#########################################point to point ####################################################-->
                        <div>
                            <div class="row">
                                <div class="col-xs-12"></div>
                            </div>
                            <div class="row mb-sm ng-scope">
                                <div class="col-sm-12"></div>
                            </div>
                            <div style="overflow:scroll;min-height:500px;">
                                <table class="table table-condensed ng-scope sieve">
                                    <thead>
                                    <tr>
                                        <th style="text-align:center">Name</th>
                                        <th style="text-align:center">Code</th>
                                        <th style="text-align:center">Dis.value</th>
                                        <th style="text-align: center;padding-right: 6%;">Start Date</th>
                                        <th style="text-align: center;padding-right: 6%;">End Date</th>
                                        <th style="text-align: center;padding-right: 6%;">Applies to Vehicle Type</th>
                                        <th style="text-align: center;padding-right: 6%;">Associate with SMA</th>
                                        <th style="text-align:center">Action</th>
                                        <th class="table-action"><span class="table-action"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody id="discount_cuppon_list">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- page end-->
        </section>
    </section>
</section>
<!--main content end-->

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->
<!--Loading indicator-->
<div style="position: fixed; top: 0px; z-index: 10000; text-align: center; width: 100%; height: 100%; display: none;"
     id="refresh_overlay">
    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%; display:table-row">
                <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto"><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="loadingGIF"><img style="height:70px;width:70px;" src="images/loading.gif"
                                                         alt="Page loading indicator"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Loading Indicator Ends-->
<script src="bs3/js/bootstrap.min.js"></script>
<script src="js/bootstrap-multiselect.js"></script>
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="assets/easypiechart/jquery.easypiechart.js"></script>

<!--Sparkline Chart-->
<script src="assets/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->

<script src="assets/flot-chart/jquery.flot.js"></script>
<script src="assets/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="assets/flot-chart/jquery.flot.resize.js"></script>
<script src="assets/flot-chart/jquery.flot.pie.resize.js"></script>

<!--dynamic table-->

<script type="text/javascript" src="pageJs/discount.js"></script>
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="pageJs/searchbox.js"></script>
<script src="JQ_timepicker/jquery_addon_js.js"></script>
<script src="JQ_timepicker/slideaccess.js"></script>

<!--dynamic table initialization -->
<script src="js/dynamic_table/dynamic_table_init.js"></script>

<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<script>
    $(function () {
        $('#promo_code2').change(function () {
            if ($('#promo_code2').is(':checked')) {
                $('#promo_code').prop('checked', false);
                $('.promocodeManageAll').hide();
                $('#is_combine_discount').removeAttr('disabled');
            }
        });
        $('#promo_code').change(function () {
            if ($('#promo_code').is(':checked')) {
                $('#promo_code2').prop('checked', false);
                $('#is_combine_discount').prop('checked', false);
                $('#is_combine_discount').attr('disabled','disabled');
                $('.promocodeManageAll').show();
            }
        });
    });
    //alert('ok1');
    $("table.sieve").sieve();
    $('#start_date, #endCalender').datepicker({minDate: 0});

    $('#start_time, #end_time').datepicker({minDate: 0});

    $('#adcs_schg_fee').on("click", function () {

        var getSeq = 0;

        $('.commanRowId').each(function (index) {

            getSeq = parseInt(index) + 1;


        });


        $('#getHtml').append('<tr class="commanRowId removerow_' + getSeq + '"  seq=' + getSeq + '><td style="width: 26%;">Custom Mandatory fee Name</td><td><input type="text" class="feeName" required></td><td>Value</td><td><input type="text" required class="feeRate"> </td><td><select class="typeRate" required><option>%</option><option>$</option></select></td><td style="padding-bottom: 1%;"><button class="btn btn-primary removeRow" seq=' + getSeq + '>X</button></td></tr>');


        $('.removeRow').off();
        $('.removeRow').on("click", function () {

            var getseq = $(this).attr('seq')
            $('.removerow_' + getseq).remove();


        })


    });


    setTimeout(function () {

        $('#allUser').multiselect({
            maxHeight: 200,
            buttonWidth: '155px',
            includeSelectAllOption: true
        });

    }, 500)


    //setTimeout(function(){

    //getState(cityAndState);
    /*     $("#apply_vehicle_type").multiselect('destroy');
             $('#apply_vehicle_type').multiselect({
             maxHeight: 200,
             buttonWidth: '155px',
             includeSelectAllOption: true
             });
         $("#value").multiselect('destroy');
             $('#value').multiselect({
             maxHeight: 200,
             buttonWidth: '155px',
             includeSelectAllOption: true
             });
         $("#apply_sma").multiselect('destroy');
             $('#apply_sma').multiselect({
             maxHeight: 200,
             buttonWidth: '155px',
             includeSelectAllOption: true
             });
         }




         ,900);*/

    //$("#service_type").multiselect('destroy');
    /*   $('#service_type').multiselect({
       maxHeight: 200,
       buttonWidth: '155px',
       includeSelectAllOption: true
       });*/

    $('#service_type').multiselect({
        maxHeight: 200,
        buttonWidth: '155px',
        includeSelectAllOption: true
    });


    $('#rate_type').multiselect({
        maxHeight: 200,
        buttonWidth: '155px',
        includeSelectAllOption: true
    });


    $('.autoApply,.promoCode').on("change", function () {
        if (!$(".promoCode").is(":checked")) {
            $('.promocodeManageAll').css("display", "block");


        }
        else {

            $('.promocodeManageAll').css("display", "none");

        }

        $('.applyAutoPromoUser,.applyAutoPromo').on("change", function () {

            if ($('.applyAutoPromoUser').is(":checked")) {
                $('.allUserMultiSelect').css("visibility", "visible");


            }
            else {
                $('.allUserMultiSelect').css("visibility", "hidden");
            }


        });
    });

</script>
<script src="pageJs/dashboard.js"></script>
<!--<script src="pageJs/dashboard.js"></script> 
          <script src="pageJs/getairports.js"></script> 
          <script src="pageJs/getState.js"></script> 
          <script src="pageJs/getSeaport.js"></script> 
          <script src="pageJs/getStatePopup.js"></script> 
          <script src="pageJs/getVehicle.js"></script> 
          <script src="pageJs/getService.js"></script> 
          <script src="pageJs/peakhour.js"></script> 
          <script src="pageJs/extrachild.js"></script> -->

<!-- Loading Indicator Ends-->
</body>
</html>
