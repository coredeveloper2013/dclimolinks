
<aside>
    <div id="sidebar" class="nav-collapse">

        <!-- sidebar menu start-->

        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li><a class="<?php echo $_page == 'dashboard' ? 'active' : '' ?>" href="./"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> </a>
                </li>
                <li class="sub-menu"><a class="<?php echo $_page == in_array($_page, ['cpd','cns','cwl','pgs','sosp']) ? 'active' : '' ?>" href="javascript:void(0)"> <i class="fa fa-laptop"></i>Settings</a>
                    <ul class="sub">
                        <li class="<?php echo $_page == 'cpd' ? 'active' : '' ?>"><a href="./company_setup.php">Company Profile details</a></li>
                        <li class="<?php echo $_page == 'cns' ? 'active' : '' ?>"><a href="./company_notifications_setup.php">Company Notifications Setup</a></li>
                        <li class="<?php echo $_page == 'cwl' ? 'active' : '' ?>"><a href="./connectwithlimo.php">Connect with limoanywhere</a></li>
                        <li class="<?php echo $_page == 'pgs' ? 'active' : '' ?>"><a href="./payment-gateway-integration.php">Payment Gateway Setup</a></li>
                        <li class="<?php echo $_page == 'sosp' ? 'active' : '' ?>"><a href="./social_oauth_setup.php">Social OAuth Setup</a></li>
                    </ul>
                </li>
                <li class="sub-menu"><a class="<?php echo $_page == in_array($_page, ['cds','ss','cos']) ? 'active' : '' ?>" href="javascript:void(0);"> <i class="fa fa-cog"></i>Online Reservation
                        Settings</a>
                    <ul class="sub">
                        <li class="<?php echo $_page == 'cds' ? 'active' : '' ?>"><a href="./checkout_disclaimers.php">Checkout Disclaimer Setup</a></li>
                        <li class="<?php echo $_page == 'ss' ? 'active' : '' ?>"><a href="./system_shutdown.php">System shutdown</a></li>
                        <li class="<?php echo $_page == 'cos' ? 'active' : '' ?>"><a href="./cut_off_time_setting.php">cut-off setting</a></li>
                    </ul>
                </li>
                <li class="sub-menu"><a class="<?php echo $_page == in_array($_page, ['avt','vtl','ads','csds']) ? 'active' : '' ?>" href="javascript:void(0)"> <i class="fa fa-gears"></i>
                        <span>Vehicles/Airlines/Cruise Ships</span> </a>
                    <ul class="sub">
                        <li class="<?php echo $_page == 'avt' ? 'active' : '' ?>"><a href="./add_vehicle.php"><i class="fa fa-truck"></i>Add Vehicle Type</a></li>
                        <li class="<?php echo $_page == 'vtl' ? 'active' : '' ?>"><a href="./vehicle.php"><i class="fa fa-car"></i>Vehicle Type List</a></li>
                        <li class="<?php echo $_page == 'ads' ? 'active' : '' ?>"><a href="./airlines_database.php"><i class="fa fa-plane"></i>Airlines db setup</a></li>
                        <li class="<?php echo $_page == 'csds' ? 'active' : '' ?>"><a href="./cruiseShips_database.php"><i class="fa fa-ship"></i>Cruise Ships db setup</a>
                        </li>
                    </ul>
                </li>
                <li class="sub-menu"><a class="<?php echo $_page == 'sts' ? 'active' : '' ?>" href="./services.php"> <i class="fa fa-exchange"></i> <span>Service Type Setup</span>
                    </a></li>
                <li class="sub-menu"><a class="<?php echo $_page == in_array($_page, ['hrs','mrm','prm','ztzr','scs','ztztm']) ? 'active' : '' ?>" href="javascript:void(0);"> <i class="fa fa-th"></i> <span>Rate Management</span>
                    </a>
                    <ul class="sub">
                        <li class="<?php echo $_page == 'hrs' ? 'active' : '' ?>"><a href="./hourly_rate_setup.php">Hourly Rate Setup</a></li>
                        <li class="<?php echo $_page == 'mrm' ? 'active' : '' ?>"><a href="./mileage_based_pricing.php">Mileage Rate Matrix</a></li>
                        <li class="<?php echo $_page == 'prm' ? 'active' : '' ?>"><a href="./per_passenger_pricing.php">Passenger Rate Matrix</a></li>
                        <li class="<?php echo $_page == 'ztzr' ? 'active' : '' ?>"><a href="./point_to_point_rate_setup.php">Zone to Zone Rate</a></li>
                        <li class="<?php echo $_page == 'scs' ? 'active' : '' ?>"><a href="./stops_calculation.php">Stop Calculation Setup</a></li>
                        <li class="<?php echo $_page == 'ztztm' ? 'active' : '' ?>"><a href="./zone_to_zone_toll_setup.php">Zone to Zone Toll Matrix</a></li>
                    </ul>
                </li>
                <li class="sub-menu"><a class="<?php echo in_array($_page, ['srr','dgs','mfs','cpts','hss','bes','ocsss' , 'elc']) ? 'active' : '' ?>" href="javascript:void(0);"> <i class="fa fa-gears"></i>
                        <span>Miscellaneous Rates</span> </a>
                    <ul class="sub">
                        <li class="<?php echo $_page == 'srr' ? 'active' : '' ?>"><a href="./special_request.php">Special Request Rate</a></li>
                        <li class="<?php echo $_page == 'dgs' ? 'active' : '' ?>"><a href="./driver_gratuity.php">Driver Gratuity Setup</a></li>
                        <li class="<?php echo $_page == 'mfs' ? 'active' : '' ?>"><a href="./mandatory_fees.php">Mandatory Fees Setup</a></li>
                        <li class="<?php echo $_page == 'cpts' ? 'active' : '' ?>"><a href="./conditional_pickup_time_surcharge.php">Conditional Pickup Time Surcharge
                                Setup</a></li>
                        <li class="<?php echo $_page == 'hss' ? 'active' : '' ?>"><a href="./holiday_calendar_and_surcharge_setup.php">Holiday Surcharge Setup</a></li>
                        <li class="<?php echo $_page == 'bes' ? 'active' : '' ?>"><a href="./blackout_date.php">Blackout/Special Event Setup</a></li>
                        <li class="<?php echo $_page == 'ocsss' ? 'active' : '' ?>"><a href="./carseat_surcharge.php">Optional Car Seat Surcharge Setup</a></li>
<!--                        <li class="--><?php //echo $_page == 'elc' ? 'active' : '' ?><!--"><a href="./extra_luggage_cost.php">Extra Luggage settings</a></li>-->
                    </ul>
                </li>
                <li class="sub-menu"><a class="<?php echo $_page == 'ssm' ? 'active' : '' ?>" href="./service_market_area_setup.php"><i class="fa fa-th"></i><span>SMA Setup Menu</span>
                    </a></li>
                <li class="sub-menu"><a class="<?php echo $_page == 'phs' ? 'active' : '' ?>" href="./peak_hour.php"> <i class="fa fa-clock-o"></i>
                        <span>Peak Hour Setup</span> </a></li>
                <li class="sub-menu"><a class="<?php echo in_array($_page, ['fds','srpd']) ? 'active' : '' ?>" href="javascript:void(0);"> <i class="fa fa-minus-square-o"></i> <span>Discounts Setup Menu</span>
                    </a>
                    <ul class="sub">
                        <li class="<?php echo $_page == 'fds' ? 'active' : '' ?>"><a href="./discounts_setup.php">Fare Discount Setup</a></li>
                        <li class="<?php echo $_page == 'srpd' ? 'active' : '' ?>"><a href="./discount_special.php">Special Requests/Limo Pckg Discounts</a></li>
                    </ul>
                </li>
                <li class="sub-menu"><a class="<?php echo in_array($_page, ['countries','states','country','cities','zone']) ? 'active' : '' ?>" href="javascript:void(0);"> <i class="fa  fa-location-arrow"></i> <span>Location Setup Menu</span>
                    </a>
                    <ul class="sub">
                        <li class="<?php echo $_page == 'countries' ? 'active' : '' ?>"><a href="./countries_setup.php">Countries</a></li>
                        <li class="<?php echo $_page == 'states' ? 'active' : '' ?>"><a href="./StateProvince.php">State/Province</a></li>
                        <li class="<?php echo $_page == 'country' ? 'active' : '' ?>"><a href="./county_setup.php">County</a></li>
                        <li class="<?php echo $_page == 'cities' ? 'active' : '' ?>"><a href="./cities.php">Cities</a></li>
                        <li class="<?php echo $_page == 'zone' ? 'active' : '' ?>" style="display:none"><a href="./zone.php">Zone</a></li>
                    </ul>
                </li>
                <li class="sub-menu"><a class="<?php echo in_array($_page, ['smC','airDB','seaDB','tDB']) ? 'active' : '' ?>" href="javascript:void(0);"> <i class="fa fa-map-marker"></i> <span>Zone DataBase Menu</span>
                    </a>
                    <ul class="sub">
                        <li class="<?php echo $_page == 'smC' ? 'active' : '' ?>"><a href="./sma_city.php">City</a></li>
                        <li class="<?php echo $_page == 'airDB' ? 'active' : '' ?>"><a href="./airport_db.php">Airport</a></li>
                        <li class="<?php echo $_page == 'seaDB' ? 'active' : '' ?>"><a href="./seaport_db.php">Seaport</a></li>
                        <li class="<?php echo $_page == 'tDB' ? 'active' : '' ?>"><a href="./train_db.php">Train</a></li>
                    </ul>
                </li>
                <li class="sub-menu"><a class="<?php echo in_array($_page, ['acl','cns']) ? 'active' : '' ?>" href="javascript:void(0)"> <i class="fa fa-user"></i> <span>Customer Account Management</span>
                    </a>
                    <ul class="sub">
                        <li class="<?php echo $_page == 'cns' ? 'active' : '' ?>"><a href="./addaccount.php">Add/Edit Customer Account</a></li>
                        <li class="<?php echo $_page == 'acl' ? 'active' : '' ?>"><a href="./accountList.php">Customer Account List</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</aside>