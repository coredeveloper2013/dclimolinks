<?php
include_once 'session_auth.php';

$_page = 'airDB';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Airport Database Setup</title>
    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet">
    <style type="text/css">
        input[type=file] {
            left: 89%;
            min-height: 33px;
            opacity: 0;
            position: absolute;
            text-align: right;
            top: 0;
            width: 12%;
        }

        .close_popup {
            width: 16px;
            margin-left: 97%;
            margin-top: -2%;
            cursor: pointer;
            height: 16px;
        }

        .btn-default {
            color: #fff;
            background-color: #1FB5AD;
            border-color: #1FB5AD;
        }

        .btn-default:hover {
            color: #fff;
            background-color: #2FA9A2;
            border-color: #2FA9A2;
        }

        .btn-primary {
            color: #fff;
            background-color: #1FB5AD;
            border-color: #1FB5AD;
        }

        .btn-primary:hover {
            color: #fff;
            background-color: #2FA9A2;
            border-color: #2FA9A2;
        }

        .multiselect-container li {
            text-align: left;
        }

        div.box {
            margin-top: 1%;
            padding-top: 2%;
            border: 1px solid #C1C1C1;
            background-color: #E5E5FF;
        }

        .multiselect-container {
            z-index: 1 !important;
        }

        .loadingGIF {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width: 100%;
            padding-left: 30%;
            padding-top: 10%;
        }
    </style>
</head>
<body>
<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading"> AIRPORT ZONE DATABASE
                            <div class="col-sm-12">
                                <button class="btn btn-primary add_airport" id="add_new_airport"
                                        style="margin-left: 69%;">Add Airport
                                </button>
                                <button class="btn btn-primary" id="dbSetting" style="margin-left:1%">DB Setting
                                </button>
                                <form id="getformdata" style="float: right;">
                                    <div tabindex="500" class="btn btn-primary btn-file"><i
                                            class="glyphicon glyphicon-folder-open"></i> <span
                                            class="hidden-xs1">Import</span>
                                        <input id="input-24" name="input24" type="file" multiple class="">
                                    </div>
                                </form>
                            </div>
                        </header>

                        <!-- airport Code start here      -->

                        <div>
                            <div class="col-md-12">
                                <!-- general form elements -->
                                <div class="box box-info" style="height:107px;">
                                    <!-- form start -->
                                    <form id="airportDbFormSubmit">
                                        <div class="box-body">
                                            <div class="col-xs-2" style="text-align:center"><b> Country</b>
                                                <select id="airport_db_country" class="form-control airport_db_country">
                                                </select>
                                            </div>
                                            <div class="col-xs-2" style="text-align:center"><b> State/Province</b>
                                                <select id="airport_db_state" class="form-control airport_db_state">
                                                </select>
                                            </div>
                                            <div class="col-xs-2" style="text-align:center"><b>Category</b>
                                                <select id="category_db_state" disabled
                                                        class="form-control category_db_state">
                                                    <option value="">select</option>
                                                    <option value="main">Main</option>
                                                    <option value="fbo">Regional/FBO</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-2 " style="text-align:center"><b>City</b>
                                                <select id="airport_db_city" class="form-control "
                                                        style="background-color:#b0b5b9;color:#fff;"
                                                        multiple="multiple">
                                                </select>
                                            </div>
                                            <div class="col-xs-1" style="cursor:pointer; z-index:2; margin-top:15px;"
                                                 id="airport_db_city_check"><img src="images/ok_checkmark_red_T.png"
                                                                                 height="35" alt=""/></div>
                                            <div class="col-xs-2 " style="text-align:center"><b>Airport Name/Code</b>
                                                <select id="airport_db_airport_code" class="form-control "
                                                        style="background-color:#b0b5b9;color:#fff;"
                                                        multiple="multiple">
                                                </select>
                                            </div>
                                            <div class="col-xs-1 " style="text-align:center;padding-top:2%;">
                                                <button type="submit" class="btn btn-primary">Activate</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- airport code end here -->

                        <div id="pad-wrapper">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 class="page-title" style="text-align:center; padding-top: 20px;
      ">Airport Database List</h4>
                                    <table class="table table-condensed ng-scope sieve">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><span class="line">Airport Code</span></th>
                                            <th><span class="line">Airport Name</span></th>
                                            <th><span class="line">Airport Zip Code</span></th>
                                            <th><span class="line">View</span></th>
                                            <th><span class="line">Action</span></th>
                                        </tr>
                                        </thead>
                                        <tbody class="sortable-table ui-sortable" id="airport_db_table">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <!-- page end-->
        </section>
    </section>
    <!--main content end--></section>


<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display:none;"
     id="add_airport_popup">
    <div style="position:relative; background:black; opacity:0.5; top: 0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:-17%; width:136%; height:100%; margin-left:auto; margin-right:auto;">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%;display:table-row">
                <div style="width:100%;display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto;">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; "
                         id="asseriesUpdateOverlay">
                        <div style="display:table-cell; vertical-align:center;margin-left:auto; margin-right:auto;">
                            <!-- popup Code start here -->
                            <div class="container">
                                <div id="modal" class="popupContainer"
                                     style="display:block;  top: 23px; position: absolute; width: 21%; left: 39%; top: 30px; background: #FFF; box-shadow: 0px 10px 20px rgba(53, 51, 51, 0.84); border-radius: 4px;">
                                    <header class="popupHeader" style="  background: #F4F4F2; position: relative; padding: 14px 20px;border-bottom: 1px solid #DDD;font-weight: bold;font-family: 'Source Sans Pro', sans-serif; font-size: 16px;color: #666;text-transform: capitalize; text-align: left;
            height:9%; margin-top:2px;"><span class="header_title" style="position:absolute; padding-left:30%">Add Airport </span>
                                        <img src="images/remove.png"
                                             id="close_rate_vehicle_rate_popup" class="close_popup" alt=""> <span
                                                id="errmsg_add_airport_rate" style="color:red;"></span></header>
                                    <section class="popupBody">
                                        <div class="form-group serviceType_css">
                                            <div class="col-xs-12 show_postal" style=" font-size: 13px;">
                                                <div style="height:auto;">
                                                    <form id="add_airport_manual">
                                                        <div class="form-group">
                                                            <div class="col-sm-6" style="font-weight: bold;"> Country
                                                                Name
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <select class="form-control sma_country_list"
                                                                        id="country_name" name="country_name1"
                                                                        style="margin-bottom: 19px;" required>
                                                                    <option value=""> -- Select --</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-6" style="font-weight: bold;"> State
                                                                Name
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <select class="form-control sma_state_list"
                                                                        id="state_name" name="state_name1"
                                                                        style="margin-bottom: 19px;" required>
                                                                    <option value=""> -- Select --</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-6" style="font-weight: bold;"> City
                                                                Name
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <select class="form-control sma_city_list"
                                                                        style="margin-bottom: 19px;" id="city_name"
                                                                        name="city_name1" required>
                                                                    <option value=""> -- Select --</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-6" style="font-weight: bold;"> Airport
                                                                Name
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control"
                                                                       id="airport_name" name="airport_name1"
                                                                       style="margin-bottom: 19px;" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-6" style="font-weight: bold;"> Airport
                                                                Code
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control"
                                                                       id="airport_code" name="airport_code1"
                                                                       style="margin-bottom: 19px;" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-6" style="font-weight: bold;"> City Zip
                                                                Code
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control"
                                                                       id="city_zip_code" name="city_zip_code1"
                                                                       style="margin-bottom: 19px;" required>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <div class="form-group">
                                                            <div class="col-sm-6">
                                                                <button type="button" id="cancel_popup"
                                                                        class="btn btn-primary"
                                                                        style="margin:20px 5px 10px 5px;">Cancel
                                                                </button>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <button type="submit" class="btn btn-primary"
                                                                        style="margin:20px 5px 10px 5px;">Submit
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <br>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- popup Code end here -->
<!--Loading indicator-->
<div style="position: fixed; top: 0px; z-index: 10000; text-align: center; width: 100%; height: 100%; display: none;"
     id="refresh_overlay">
    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%; display:table-row">
                <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto"><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="loadingGIF"><img style="height:70px;width:70px;" src="images/loading.gif"
                                                         alt="Page loading indicator"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Loading Indicator Ends-->
<!--Core js-->

<script type="text/javascript" src="js/lib/jquery.js"></script>
<script type="text/javascript" src="pageJs/dashboard.js"></script>
<script type="text/javascript" src="pageJs/logout.js"></script>
<script type="text/javascript" src="bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script type="text/javascript" src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script type="text/javascript" src="js/nicescroll/jquery.nicescroll.js"></script>

<!--Easy Pie Chart-->
<script type="text/javascript" src="assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->

<!--dynamic table-->
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
<!--common script init for all pages-->
<!-- code start here for search box-->
<script type="text/javascript" src="pageJs/searchbox.js"></script>
<!-- code end here for search box-->

<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="pageJs/airport_db.js"></script>
<script type="text/javascript" src="pageJs/getcountry_state.js"></script>
<script type="text/javascript" src="js/dynamic_table/dynamic_table_init.js"></script>
<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
<script type="text/javascript">
    $("table.sieve").sieve();
    $('#airport_db_city,#airport_db_airport_code').multiselect({
        maxHeight: 200,
        buttonWidth: '155px',
        includeSelectAllOption: true
    });


    $('#addnewForm').on("click", function () {

        window.localStorage.setItem("airport_db_table_id", "string12");
        window.location.href = "sma_airport.php";
    });

    /*  edit airport and country edit code stare here */
    $('#dbSetting').on("click", function () {

        window.location.href = "editAirportDb.php";

    });
    /*  edit airport and country edit code End here */

    /*  add airport manually popup start here */
    $('.close_popup').on("click", function () {
        $('#add_airport_popup').hide();
    });

    $('#cancel_popup').on('click', function () {

        $('#add_airport_popup').hide();
    });

    $('.add_airport').on("click", function () {


        //$('#country_name').val("");
        $('#state_name').val("");
        $('#city_name').val("");
        $('#airport_name').val("");
        $('#airport_code').val("");
        $('#city_zip_code').val("");
        $('#add_airport_popup').show();

    });

    /*  add airport manually popup end here */

</script>
</body>
</html>
