<?php
include_once 'session_auth.php';

$_page = 'countries';
if (!IsLoggedIn()) {
    redirect('login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>My Limo Project | Add Country Setup</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet">
    <style type="text/css">
        .close_popup {
            width: 16px;
            margin-left: 97%;
            margin-top: -2%;
            cursor: pointer;
            height: 16px;
        }

        .btn-default {
            color: #fff;
            background-color: #1FB5AD;
            border-color: #1FB5AD;
        }

        .btn-default:hover {
            color: #fff;
            background-color: #2FA9A2;
            border-color: #2FA9A2;
        }

        .btn-primary {
            color: #fff;
            background-color: #1FB5AD;
            border-color: #1FB5AD;
        }

        .btn-primary:hover {
            color: #fff;
            background-color: #2FA9A2;
            border-color: #2FA9A2;
        }

        .alignCENTER {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
    </style>
</head>
<body>
<section id="container">

    <!--header start-->
    <?php include_once './global/header.php'; ?>
    <!--header end-->

    <!--sidebar start-->
    <?php include_once './global/sideNav.php'; ?>
    <!-- sidebar menu end-->


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-sm-12"><h3 class="page-title alignCENTER">Country Setup</h3>
                    <section class="panel">
                        <div class="col-sm-12">
                            <div class="top-links alignCENTER" style="padding-top: 15px;"><a
                                    class="btn btn-default popup_edit_city  " seq="SED"> <i class="fa fa-plus"></i> Add
                                a Country</a></div><!--seq=SED creates a W3C validation error -->
                        </div>
                        <div id="pad-wrapper">
                            <div class="row">
                                <div class="col-sm-12">

                                    <table class="table table-condensed ng-scope sieve">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Country Name</th>
                                            <th><span class="line"></span>Country Code</th>
                                            <th><span class="line">Action</span></th>
                                        </tr>
                                        </thead>
                                        <tbody class="sortable-table ui-sortable" id="CountryDetails">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <!-- page end-->
        </section>
    </section>
    <!--main content end--></section>
<div style="position: fixed; top: 0px; z-index: 10000; left: 0px; width: 100%; height: 100%; display:none;"
     id="view_rate_vehicle_modal">
    <div style="position:relative; background:black; opacity:0.5; top: 0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top: 0px; left:-17%; width:136%; height:100%" class="alignCENTER">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%;display:table-row">
                <div style="width:100%;display:table-cell; vertical-align:middle" class="alignCENTER">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px; "
                         id="asseriesUpdateOverlay">
                        <div style="display:table-cell; vertical-align:center;" class="alignCENTER"><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div style="width:80%">
                                <!--  code of popup start-->
                                <div class="container">
                                    <div id="modal" class="popupContainer"
                                         style="display:block;  top: 23px; position: absolute; width: 21%; left: 39%; top: 30px; background: #FFF; box-shadow: 0px 10px 20px rgba(53, 51, 51, 0.84); border-radius: 4px;">
                                        <header class="popupHeader" style="  background: #F4F4F2; position: relative; padding: 14px 20px;border-bottom: 1px solid #DDD;font-weight: bold;font-family: 'Source Sans Pro', sans-serif; font-size: 16px;color: #666;text-transform: capitalize; text-align: left;
height:9%; margin-top:2px;"><span class="header_title" style="position:absolute; padding-left:30%">Add Country </span>
                                            <img src="images/remove.png" alt=""
                                                 id="close_rate_vehicle_rate_popup" class="close_popup"> <span
                                                    id="errmsg_add_airport_rate" style="color:red;"></span></header>
                                        <section class="popupBody">
                                            <div class="form-group serviceType_css">
                                                <div class="col-xs-12 show_postal" style=" font-size: 13px;">
                                                    <div style="height:auto;">
                                                        <form id="add_edit_country">
                                                            <div class="form-group">
                                                                <div class="col-sm-6" style="font-weight: bold;">
                                                                    Country Name
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control"
                                                                           id="country_name"
                                                                           style="margin-bottom: 19px;" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-6" style="font-weight: bold;">
                                                                    Country Code
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" class="form-control"
                                                                           style="margin-bottom: 19px;"
                                                                           id="country_abbr" required>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <button type="submit" class="btn btn-primary"
                                                                    style="margin:20px 5px 10px 5px;">Submit
                                                            </button>
                                                            <br>
                                                            <br>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Core js-->
<!--Loading indicator-->
<div style="position: fixed; top: 0px; z-index: 10000; text-align: center; width: 100%; height: 100%; display: none;"
     id="refresh_overlay">
    <div style="position:relative; background:transparent; opacity:0; top:0px; left:0px; width:100%; height:100%"></div>
    <div style="position:absolute; top:0px; left:0px; width:100%; height:100%; margin-left:auto; margin-right:auto">
        <div style="width:100%;display:table; height:100%;">
            <div style="width:100%; display:table-row">
                <div style="width:100%; display:table-cell; vertical-align:middle; margin-left:auto; margin-right:auto">
                    <div style="width:90%; height:90%; background:transparent; display:table; border:0px solid black; border-radius:10px">
                        <div style="display:table-cell; vertical-align:center; margin-left:auto; margin-right:auto"><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="loadingGIF"><img style="height:70px;width:70px;" src="images/loading.gif"
                                                         alt="Page loading indicator"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Loading Indicator Ends-->
<script src="js/lib/jquery.js"></script>
<script src="pageJs/dashboard.js"></script>
<script src="pageJs/logout.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js"></script>
<script src="pageJs/searchbox.js"></script>
<!--Easy Pie Chart-->
<script src="assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->

<!--dynamic table-->
<script type="text/javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<script src="pageJs/getCountry.js"></script>
<!--dynamic table initialization -->
<script src="js/dynamic_table/dynamic_table_init.js"></script>
<script>
    $("table.sieve").sieve();
    $('.close_popup').on("click", function () {
        $('#view_rate_vehicle_modal').hide();
    });
    $('.submit_popup').on("click", function () {
        $('#signup_form_overlay').show();
    })
    $('#popup_close_vrc,#cancel_trans').on("click", function () {
        $('#signup_form_overlay').hide();
    });

    $('.popup_edit_city').on("click", function () {

        $('#add_edit_country').find('button').attr("seq", "default");
        $('#add_edit_country').find('button').html("Submit");
        $('#country_name').val("");
        $('#country_abbr').val("");
        $('#view_rate_vehicle_modal').show();

    });
</script>
</body>
</html>
