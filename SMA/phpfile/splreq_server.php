<?php 

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');



/***********************************************************
  * Method Name   : setMilesRate
  * Description       : Insert number of market_milege
  * @Param            : add_miles_cities vehicle_code  user_id and add_miles_rate  
  * @return            : json data
  ***********************************************************/
function setSpecialRequest()
{	
	 if(isset($_REQUEST['sma_id'])&&(isset($_REQUEST['vehicle_code'])  )&&(isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']))  && (isset($_REQUEST['amount']) && !empty($_REQUEST['amount']))&&(isset($_REQUEST['package_code']) && !empty($_REQUEST['package_code'])))
	   {
	      $userId=$_REQUEST['user_id'];
		  $minimum_base_rate=(isset($_REQUEST['minimum_base_rate']) && !empty($_REQUEST['minimum_base_rate']))?$_REQUEST['minimum_base_rate']:0;
		  
		   $VehicleCode=explode(',',$_REQUEST['vehicle_code']);
		   $service_typeObj=explode(',',$_REQUEST['service_typeObj']);
		   $addSma=explode(',',$_REQUEST['sma_id']);
		   $value=explode(',',$_REQUEST['value']);
		   $package_code=$_REQUEST['package_code'];
		   $amount=$_REQUEST['amount'];
		
		$query ="insert into special_request(package_code,user_id,amount,package_description) value('".$_REQUEST['package_code']."','".$userId."','".$_REQUEST['amount']."','".$_REQUEST['packageDescription']."')";
		$sr_id = operations($query);

	  for($i=0;$i<count($VehicleCode);$i++)
	  {
		  $Vehquery="insert into sr_vehicle(sr_id,vehicle_code,user_id) value('".$sr_id."','".$VehicleCode[$i]."','".$userId."')";	
		  $resource1 = operations($Vehquery);
	  }//VehicleCode

	  for($i=0;$i<count($service_typeObj);$i++)
	  {
		  $Vehquery="insert into sr_service(sr_id,service_type,user_id) value('".$sr_id."','".$service_typeObj[$i]."','".$userId."')";	
		  $resource1 = operations($Vehquery);
	  }//VehicleCode


		  for($j=0;$j<count($addSma);$j++)
	  	{
			$Smaquery="insert into sr_sma(sr_id,sma_id,user_id) value('".$sr_id."','".$addSma[$j]."','".$userId."')";	
		  $resource2 = operations($Smaquery);
		 }//addSma
			
	  for($k=0;$k<count($value);$k++)
			{
			$Valquery="insert into sr_value(sr_id,value) value('".$sr_id."','".$value[$k]."')";	
		  $resource3 = operations($Valquery);
		  }//addMiles

		   
		   $result=global_message(200,1008,$insertId);		   
	   }
	   else
	   {
		    $result=global_message(201,1003);
		   
		}	
	return $result;	
}

	/***********************************
	@MethodName			:getRateMatrixList
	@Description		:Get list of all Rate matrixes
	@param				:user_id
	@return			    :list of rate matrix	
	************************************/
function getRateMatrixList()
{
		if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
  {
	$query="Select * from special_request where user_id='".$_REQUEST['user_id']."' order by package_code asc";
	$resource= operations($query);

	
		
	$contents = array();
	$getArraySort=[];

	if(count($resource)>0 && gettype($resource)!="boolean")
	 {
	for($i=0; $i<count($resource); $i++)
		{
			$vehicle_code=''; 
			$sma_name='';
			$sma_id='';
			$value='';
			
			$Vehquery="Select vehicle_code from sr_vehicle where sr_id=".$resource[$i]['id'];
			$resource1= operations($Vehquery);
			for($j=0; $j<count($resource1); $j++)
				{
					$vehicle_code .=$resource1[$j]['vehicle_code'].',';
				}


				$service_type="Select service_type from sr_service where sr_id=".$resource[$i]['id'];
				$service_typeResult= operations($service_type);

					$service_typeString='';
			for($j=0; $j<count($service_typeResult); $j++)
				{
					$service_typeString .=$service_typeResult[$j]['service_type'].',';
				}


			$Smaquery="Select sma_id,sma_name from sr_sma,sma where sma.id=sr_sma.sma_id AND sr_sma.sr_id=".$resource[$i]['id'];
			$resource2= operations($Smaquery);

			for($k=0; $k<count($resource2); $k++)
				{
					$sma_name .=$resource2[$k]['sma_name'].',';
					$sma_id .=$resource2[$k]['sma_id'].',';
				}
			$Valquery="Select value from sr_value where sr_id=".$resource[$i]['id'];
			$resource3= operations($Valquery);
			for($l=0; $l<count($resource3); $l++)
				{
					$value .=$resource3[$l]['value'].',';
				}

			
			$contents[$i]['id']=$resource[$i]['id'];
			$contents[$i]['package_code']=$resource[$i]['package_code'];
			$contents[$i]['sma_id'] = $sma_id;
			$contents[$i]['sma_name'] = $sma_name;
			$contents[$i]['vehicle_code']=$vehicle_code;
			$contents[$i]['service_type']=$service_typeString;

			$contents[$i]['value']=$value;
			$contents[$i]['amount'] = $resource[$i]['amount'];
			$contents[$i]['packageDescription'] = $resource[$i]['package_description'];


			$getArraySort[$i]=strtoupper($resource[$i]['package_code'].'@'.$resource[$i]['id']);


		}

		sort($getArraySort);
		
		$getSmaId=[];
		$gearraysplit=[];
		for($i=0; $i<count($contents); $i++ )
		{
			$gearraysplit=explode("@",$getArraySort[$i]);
			
			$getSmaId[$i]=$gearraysplit[1];
		}
		

		$getArrayFullResult=[];
		$m=0;
		
		for($k=0; $k<count($getSmaId); $k++)
		{
			for($l=0; $l<count($contents); $l++)
			{
				if($contents[$l]['id']==$getSmaId[$k])
				{
					$getArrayFullResult[$m]=$contents[$l];
					$m++;

				}
			}

		}

       }





		if(count($contents)>0 && gettype($contents)!="boolean")
		   {
	

			   $result=global_message(200,1007,$getArrayFullResult);
			   
		   }
		   else
		   {
			   $result=global_message(200,1006);
		   }		  
	  
	  
  }
 else
  {
	  $result=global_message(201,1003);
  }
  return  $result;
}



function deleteSpecialRequest()
{
		
 if((isset($_REQUEST['sr_id']) && !empty($_REQUEST['sr_id'])))
	   {
		  $rowId=$_REQUEST['sr_id'];
		 
		$query="delete from special_request where id='".$rowId."'";
	    $resource = operations($query);
			$queryDelete="delete  from sr_value where sr_id='".$rowId."'";
			$resource1 = operations($queryDelete);
				$queryDelete1="delete  from sr_sma where sr_id='".$rowId."'";
				$resource2 = operations($queryDelete1);
					$queryDelete2="delete  from sr_vehicle where sr_id='".$rowId."'";
					$resource3 = operations($queryDelete2);
					$queryDelete2="delete  from sr_service where sr_id='".$rowId."'";
					$resource3 = operations($queryDelete2);
					
		$result=global_message(200,1010);   
		   
	   }
  else
  {
	   $result=global_message(201,1003);
  }
return $result;
}

