<?php

include_once 'service_server.php';
$action = $_REQUEST['action'];
$response = array();

switch ($action) {



    case "forgetPassword":
        $response = forgetPassword();
        echo json_encode($response);
        break;

    case "checkCutoffTime":
        $response = checkCutoffTime();
        echo json_encode($response);
        break;
    case "checkSurchargeCutoffTime":
        $response = checkSurchargeCutoffTime();
        echo json_encode($response);
        break;    
    case "checkHourlyCutoffTime":
        $response = checkHourlyCutoffTime();
        echo json_encode($response);
        break;
                
    case "getSpecialPackageItem":
        $response = getSpecialPackageItem();
        echo json_encode($response);
        break;
    case "getToTrainRate":
        $response = getToTrainRate();
        echo json_encode($response);
        break;


    case "getFromTrainRate":
        $response = getFromTrainRate();
        echo json_encode($response);
        break;



    case "getDiscountPromoCode":
        $response = getDiscountPromoCode();
        echo json_encode($response);
        break;



    case "getAutoDispuntPackagePromoCode":
        $response = getAutoDispuntPackagePromoCode();
        echo json_encode($response);
        break;

    case "getDiscountPackagePromoCode":
        $response = getDiscountPackagePromoCode();
        echo json_encode($response);
        break;



    case "passengerReservation":
        $response = passengerReservation();
        echo json_encode($response);
        break;

    case "getChildSeatRate":
        $response = getChildSeatRate();
        echo json_encode($response);
        break;


    case "getQuoteToBackOffice":
        $response = getQuoteToBackOffice();
        echo json_encode($response);
        break;



    case "getPerPassengerRate":
        $response = getPerPassengerRate();
        echo json_encode($response);
        break;



    case "isHrlyBlackOutDate":
        $response = isHrlyBlackOutDate();
        echo json_encode($response);
        break;



    case "specialRequestInfoAllPackage":
        $response = specialRequestInfoAllPackage();
        echo json_encode($response);
        break;

    case "isBlackOutDate":
        $response = isBlackOutDate();
        echo json_encode($response);
        break;


    case "specialRequestInfo":
        $response = specialRequestInfo();
        echo json_encode($response);
        break;




    case "getCityAndStateNameGoogleAPI":
        $response = getCityAndStateNameGoogleAPI();
        echo json_encode($response);
        break;




    case "getHourlyRate":
        $response = getHourlyRate();
        echo json_encode($response);
        break;

    case "getToSeaportRate":
        $response = getToSeaportRate();
        echo json_encode($response);
        break;


    case "getToAirportRate":
        $response = getToAirportRate();
        echo json_encode($response);
        break;

    case "getFromSeaportRate":
        $response = getFromSeaportRate();
        echo json_encode($response);
        break;
    case "getFromAirportRate":
        $response = getFromAirportRate();
        echo json_encode($response);
        break;
    case "getPointToPointRate":
        $response = getPointToPointRate();
        echo json_encode($response);
        break;
    
    case "getDistance":
        $response = getDistance();
        echo json_encode($response);
        break;

    case "getPaymentSetup":
        $response = getPaymentSetup();
        echo json_encode($response);
        break;  

        case "getCreditCardDetails":
        $response = getCreditCardDetails();
        echo json_encode($response);
        break;    
    case "getHolidaySurcharge":
        $response = getHolidaySurcharge();
        echo json_encode($response);
        break;

     case "getCreditSurchargerate":
        $response = getCreditSurchargerate();
        echo json_encode($response);
        break;    

}
?>	