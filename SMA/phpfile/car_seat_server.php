<?php 

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');

	/*****************************************************************
	Method:             setCarSeatSurcharge()
	InputParameter:     vehicle_code
	Return:             set Car Seat Surcharge
	*****************************************************************/
	function setCarSeatSurcharge()
	{	
	 	if(isset($_REQUEST['sma_id'])&&(isset($_REQUEST['vehicle_code'])  )&&(isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
	   	{
	      	$userId=$_REQUEST['user_id'];
	  	   	$VehicleCode=explode(',',$_REQUEST['vehicle_code']);
		   	$service_typeObject=explode(',',$_REQUEST['service_typeObject']);
			$addSma=explode(',',$_REQUEST['sma_id']);
			$query ="insert into carseat_surcharge(seat_1,seat_2,seat_3,seat_4,user_id) value('".$_REQUEST['seat_1']."','".$_REQUEST['seat_2']."','".$_REQUEST['seat_3']."','".$_REQUEST['seat_4']."','".$userId."')";
            $carseat_id = operations($query);

		  	for($i=0;$i<count($VehicleCode);$i++)
		  	{
		  		$Vehquery="insert into carseat_vehicle(carseat_id,vehicle_code,user_id) value('".$carseat_id."','".$VehicleCode[$i]."','".$userId."')";	
		  		$resource1 = operations($Vehquery);
		  	}

		    for($i=0;$i<count($service_typeObject);$i++)
		  	{
			  	$Vehquery="insert into carseat_service(carseat_id,service_type,user_id) value('".$carseat_id."','".$service_typeObject[$i]."','".$userId."')";	
			  	$resource1 = operations($Vehquery);
		  	}

		  	for($j=0;$j<count($addSma);$j++)
		  	{
				$Smaquery="insert into carseat_sma(carseat_id,sma_id,user_id) value('".$carseat_id."','".$addSma[$j]."','".$userId."')";	
			  	$resource2 = operations($Smaquery);
			}
			 $result=global_message(200,1008,$carseat_id);		   
		}
  	 	else
   		{
	    	$result=global_message(201,1003);
		}	
		return $result;	
	}

	/*****************************************************************
	Method:             getRateMatrixList()
	InputParameter:     user_id
	Return:             get Rate Matrix List
	*****************************************************************/
	function getRateMatrixList()
	{
		if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
	  	{
			$query="Select * from carseat_surcharge where user_id='".$_REQUEST['user_id']."' order by seat_1";
			$resource= operations($query);
			$contents = array();
		    if(count($resource)>0 && gettype($resource)!="boolean"){
				for($i=0; $i<count($resource); $i++)
				{
					$vehicle_code=''; 
					$sma_name='';
					$sma_id='';
					$value='';
					$service_type='';
					$Vehquery="Select vehicle_code from carseat_vehicle where carseat_id=".$resource[$i]['id'];
					$resource1= operations($Vehquery);
					for($j=0; $j<count($resource1); $j++)
						{
							$vehicle_code .=$resource1[$j]['vehicle_code'].',';
						}

						$ServiceType="Select * from carseat_service  where carseat_id=".$resource[$i]['id'];
					$ServiceTypeResult= operations($ServiceType);
					for($j=0; $j<count($ServiceTypeResult); $j++)
						{
							$service_type .=$ServiceTypeResult[$j]['service_type'].',';
						}

					$Smaquery="Select sma_id,sma_name from carseat_sma,sma where sma.id=carseat_sma.sma_id AND carseat_sma.carseat_id=".$resource[$i]['id'];
					$resource2= operations($Smaquery);
					for($k=0; $k<count($resource2); $k++)
						{
							$sma_name .=$resource2[$k]['sma_name'].',';
							$sma_id .=$resource2[$k]['sma_id'].',';
						}
					$contents[$i]['id']=$resource[$i]['id'];
					$contents[$i]['seat_1']=$resource[$i]['seat_1'];
					$contents[$i]['seat_2']=$resource[$i]['seat_2'];
					$contents[$i]['seat_3']=$resource[$i]['seat_3'];
					$contents[$i]['seat_4']=$resource[$i]['seat_4'];
					$contents[$i]['sma_id'] = $sma_id;
					$contents[$i]['sma_name'] = $sma_name;
					$contents[$i]['vehicle_code']=$vehicle_code;
					$contents[$i]['service_type']=$service_type;
					
				}
			}
			if(count($contents)>0 && gettype($contents)!="boolean")
		   	{
			   $result=global_message(200,1007,$contents);
			}
		  	else
		   	{
		   		$result=global_message(200,1006);
		   	}		  		  
	  	}
	 	else
	  	{
	  		$result=global_message(201,1003);
	  	}
	  	return  $result;
	}


	/*****************************************************************
	Method:             getRateMatrixList()
	InputParameter:     user_id
	Return:             get Rate Matrix List
	*****************************************************************/
	function deleteCarSeatSurcharge()
	{		
		if((isset($_REQUEST['carseat_id']) && !empty($_REQUEST['carseat_id'])))
   		{
		  	$rowId=$_REQUEST['carseat_id'];		 
			$query="delete from carseat_surcharge where id='".$rowId."'";
	    	$resource = operations($query);
			$queryDelete1="delete  from carseat_sma where carseat_id='".$rowId."'";
			$resource2 = operations($queryDelete1);
			$queryDelete2="delete  from carseat_vehicle where carseat_id='".$rowId."'";
			$resource3 = operations($queryDelete2);
			$result=global_message(200,1010);   
   	   	}
  		else
  		{
	   		$result=global_message(201,1003);
  		}
		return $result;
	}

