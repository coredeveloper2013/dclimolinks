<?php 

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');
/*****************************************************************
Method:             checkUniqueMat  rix()
InputParameter:    	matrix_name
Return:             check Unique Matrix
*****************************************************************/
	function updateHourlyRate($fullresult)
	{
		$query="delete from hourly_setup_sma where parent_id='".$fullresult->getRowId."'";
	 	$resource1 = operations($query);
 		$query="delete from hourly_event where  parent_id='".$fullresult->getRowId."'";
	 	$resource1 = operations($query);
 		$query="delete from hourly_vehicle where  parent_id='".$fullresult->getRowId."'";
	 	$resource1 = operations($query);
		$setRadius=($fullresult->setRadius[0]=='notSet')?1:0;
		$setBlackoutValue=($fullresult->setBlackoutValue[0]=='notSet')?1:0;
		$hrlySmaValueArray=($fullresult->hrlySmaValueArray[0]=='checkedTrue')?1:0;
        if($hrlySmaValueArray == 1){
            $defaultUpdate = "UPDATE `master_hour_setup` SET `is_sma_default` = '0'";
            operations($defaultUpdate);
        }
		if($setBlackoutValue==0)
		{
			$query="update master_hour_setup set hourly_name='".$fullresult->hrly_rate_name."',radius_miles='".$fullresult->setRadius[0]."',radius_msg='".$fullresult->setRadius[1]."',is_miles_set='".$setRadius."',is_black_out='".$setBlackoutValue."',is_sma_default='".$hrlySmaValueArray."',cut_off_time='".$fullresult->setBlackoutValue[0]."',increase_hrs='".$fullresult->setBlackoutValue[1]."',increase_rate='".$fullresult->setBlackoutValue[2]."',rate_type='".$fullresult->setBlackoutValue[3]."' where id='".$fullresult->getRowId."'";
		}
		else
		{
            $query="update master_hour_setup set hourly_name='".$fullresult->hrly_rate_name."',radius_miles='".$fullresult->setRadius[0]."',radius_msg='".$fullresult->setRadius[1]."',is_miles_set='".$setRadius."',is_black_out='".$setBlackoutValue."',is_sma_default='".$hrlySmaValueArray."',cut_off_time='',increase_hrs='',increase_rate='',rate_type='' where id='".$fullresult->getRowId."'";
		}
		$resource1 = operations($query);	
		$lastinsertedId=$fullresult->getRowId;
		for($i=0; $i<count($fullresult->getVehicleValue); $i++)
		{
			$query_vehicle="insert into hourly_vehicle(parent_id,vehicle_code,std_hrs,std_price,dsc_hrs,dsc_price,sdc_hrs,sdc_price) values('".$lastinsertedId."','".$fullresult->getVehicleValue[$i]->vehicleCode."','".$fullresult->getVehicleValue[$i]->getstdHour."','".$fullresult->getVehicleValue[$i]->getstdPrice."','".$fullresult->getVehicleValue[$i]->dschrs."','".$fullresult->getVehicleValue[$i]->dscprice."','".$fullresult->getVehicleValue[$i]->sdchrs."','".$fullresult->getVehicleValue[$i]->sdcprice."')";
			operations($query_vehicle);
		}
		
		for($i=0; $i<count($fullresult->eventArray); $i++)
		{
			$queryBlackOut="insert into hourly_event(parent_id,event_id) values('".$lastinsertedId."','".$fullresult->eventArray[$i]."')";
			operations($queryBlackOut);
		}

		if($hrlySmaValueArray==0)
		{
			for($i=0; $i<count($fullresult->hrlySmaValueArray); $i++)
			{
				$queryBlackOut="insert into hourly_setup_sma(parent_id,sma_id) values('".$lastinsertedId."','".$fullresult->hrlySmaValueArray[$i]."')";
				operations($queryBlackOut);
			}
		}
	}

/*****************************************************************
Method:             viewSpecialHourly()
InputParameter:    	fullresult
Return:             view Special Hourly
*****************************************************************/
	function viewSpecialHourly($fullresult)
	{
        $query="select * from master_hour_setup where id='".$fullresult->rowId."' ";
        $finalArray=[];
	 	$resource1 = operations($query);
		if(count($resource1)>=1 && gettype($resource1)!="boolean")
	 	{
			for($i=0; $i<count($resource1); $i++)
		 	{
				$finalArray[$i]=$resource1[$i];
		 		$query="select * from hourly_vehicle where  parent_id='".$fullresult->rowId."'";
	 		 	$resource2 = operations($query);
				for($j=0; $j<count($resource2); $j++)
	 		 	{
					$finalArray[$i]['vehicle_info'][$j]=$resource2[$j];
				}
	 		 	$query="select * from hourly_event where  parent_id='".$fullresult->rowId."'";
	 		 	$resource2 = operations($query);
				for($j=0; $j<count($resource2); $j++)
			 	{
					$finalArray[$i]['event_info'][$j]=$resource2[$j];
				}
				$query="select * from hourly_setup_sma where  parent_id='".$fullresult->rowId."'";
                $resource2 = operations($query);
				for($j=0; $j<count($resource2); $j++)
	 		 	{
					$finalArray[$i]['sma_info'][$j]=$resource2[$j];
	 		 	}
			}
			$result=global_message(200,1003,$finalArray);
		}
	 	else
	 	{
			$result=global_message(200,1006);
		}
		 return $result;
	}


/*****************************************************************
Method:             deleteHourlyRate()
InputParameter:    	fullresult
Return:             delete Hourly Rate
*****************************************************************/
	function deleteHourlyRate($fullresult)
	{
		$query="delete from hourly_setup_sma where parent_id='".$fullresult->rowId."'";
		$resource1 = operations($query);
		$query="delete from hourly_event where  parent_id='".$fullresult->rowId."'";
		$resource1 = operations($query);
		$query="delete from hourly_vehicle where  parent_id='".$fullresult->rowId."'";
		$resource1 = operations($query);
		$query="delete from master_hour_setup where  id='".$fullresult->rowId."'";
		$resource1 = operations($query);
	}
/*****************************************************************
Method:             getHourlyRate()
InputParameter:    	fullresult
Return:             get Hourly Rate
*****************************************************************/
	function getHourlyRate($fullresult)
	{
		$query="select * from master_hour_setup where user_id='".$fullresult->userId."' order by hourly_name asc";
		$resource1 = operations($query);
		if(count($resource1)>=1 && gettype($resource1)!="boolean")
	 	{
			$result=global_message(200,1003,$resource1);
		}
	 	else
	 	{
			$result=global_message(201,1003);
	 	}
		return $result;
	}

/*****************************************************************
Method:             setHourlyRate()
InputParameter:    	fullresult
Return:             set Hourly Rate
*****************************************************************/

	function setHourlyRate($fullresult)
	{	
		$setRadius=($fullresult->setRadius[0]=='notSet')?1:0;
		$setBlackoutValue=($fullresult->setBlackoutValue[0]=='notSet')?1:0;
		$hrlySmaValueArray=($fullresult->hrlySmaValueArray[0]=='checkedTrue')?1:0;
		$query2="select count(*) as totalResult from master_hour_setup where hourly_name='".$fullresult->hrly_rate_name."'";	
		$resource12 = operations($query2);	
		if($resource12[0]['totalResult']>0)
		{
			return false;
		}
		if($setBlackoutValue==0)
		{
			$query="insert into master_hour_setup(hourly_name,radius_miles,radius_msg,is_miles_set,is_black_out,is_sma_default,cut_off_time,increase_hrs,increase_rate,rate_type,user_id) values('".$fullresult->hrly_rate_name."','".$fullresult->setRadius[0]."','".$fullresult->setRadius[1]."','".$setRadius."','".$setBlackoutValue."','".$hrlySmaValueArray."','".$fullresult->setBlackoutValue[0]."','".$fullresult->setBlackoutValue[1]."','".$fullresult->setBlackoutValue[2]."','".$fullresult->setBlackoutValue[3]."','".$fullresult->userId."')";
		}
		else
		{
			$query="insert into master_hour_setup(hourly_name,radius_miles,radius_msg,is_miles_set,is_black_out,is_sma_default,cut_off_time,increase_hrs,increase_rate,rate_type,user_id) values('".$fullresult->hrly_rate_name."','".$fullresult->setRadius[0]."','".$fullresult->setRadius[1]."','".$setRadius."','".$setBlackoutValue."','".$hrlySmaValueArray."','','','','','".$fullresult->userId."')";	
		}
        $lastinsertedId = operations($query);
		for($i=0; $i<count($fullresult->getVehicleValue); $i++)
		{
			$query_vehicle="insert into hourly_vehicle(parent_id,vehicle_code,std_hrs,std_price,dsc_hrs,dsc_price,sdc_hrs,sdc_price) values('".$lastinsertedId."','".$fullresult->getVehicleValue[$i]->vehicleCode."','".$fullresult->getVehicleValue[$i]->getstdHour."','".$fullresult->getVehicleValue[$i]->getstdPrice."','".$fullresult->getVehicleValue[$i]->dschrs."','".$fullresult->getVehicleValue[$i]->dscprice."','".$fullresult->getVehicleValue[$i]->sdchrs."','".$fullresult->getVehicleValue[$i]->sdcprice."')";
			operations($query_vehicle);
		}

		for($i=0; $i<count($fullresult->eventArray); $i++)
		{
			$queryBlackOut="insert into hourly_event(parent_id,event_id) values('".$lastinsertedId."','".$fullresult->eventArray[$i]."')";
			operations($queryBlackOut);
		}
		
		if($hrlySmaValueArray==0)
		{
			for($i=0; $i<count($fullresult->hrlySmaValueArray); $i++)
			{
				$queryBlackOut="insert into hourly_setup_sma(parent_id,sma_id) values('".$lastinsertedId."','".$fullresult->hrlySmaValueArray[$i]."')";
				operations($queryBlackOut);
			}

		}
		return  true;
	}



?>