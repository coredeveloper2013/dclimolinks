<?php

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');
/*****************************************************************
Method:             specialPackage()
InputParameter:     userId
Return:             Checked Limo AnyWhere
 *****************************************************************/
function specialPackage()
{
    if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
    {
        $query='Select * from special_request where user_id="'.$_REQUEST['user_id'].'"';
        $contents= operations($query);
        if(count($contents)>0 && gettype($contents)!="boolean")
        {
            $result=global_message(200,1007,$contents);
        }
        else
        {
            $result=global_message(200,1006);
        }
    }
    else
    {
        $result=global_message(201,1003);
    }
    return  $result;
}


/*****************************************************************
Method:             setPromoDiscount()
InputParameter:     user_id
Return:             set Promo Discount
 *****************************************************************/
function setPromoDiscount()
{
    if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
    {
        $start_date = $_REQUEST['promo_newstart_date'];
        $start_date=explode('/',$start_date);
        $start_date=$start_date[2]."-".$start_date[0]."-".$start_date[1];
        $end_date = $_REQUEST['promo_newend_date'];
        $end_date=explode('/',$end_date);
        $end_date=$end_date[2]."-".$end_date[0]."-".$end_date[1];
        $promo_service_start_date = $_REQUEST['promo_service_start_date'];
        $promo_service_start_date=explode('/',$promo_service_start_date);
        $promo_service_start_date=$promo_service_start_date[2]."-".$promo_service_start_date[0]."-".$promo_service_start_date[1];
        $promo_service_end_date = $_REQUEST['promo_service_end_date'];
        $promo_service_end_date=explode('/',$promo_service_end_date);
        $promo_service_end_date=$promo_service_end_date[2]."-".$promo_service_end_date[0]."-".$promo_service_end_date[1];
        $special_package=explode(',',$_REQUEST['promo_special_package']);
        $query12="select count(*) totalnumber from vehicle_special_discount_table where (code='".$_REQUEST['promo_couppon_code']."' or name='".$_REQUEST['promo_couppon_name']."' ) and  user_id='".$_REQUEST['user_id']."'";
        $query12Result = operations($query12);
        if($query12Result[0]['totalnumber']==0)
        {
            $promo_autoapply_code = (int)$_REQUEST['promo_autoapply_code'];
            $combine_discount = (int)$_REQUEST['combine_discount'];
            $query ="insert into vehicle_special_discount_table(name,code,discount_value,discount_type,start_date,end_date,service_start_date,service_end_date,promo_pref,is_combine_discount,user_id,apply_service) value('".$_REQUEST['promo_couppon_name']."','".$_REQUEST['promo_couppon_code']."','".$_REQUEST['promo_couppon_value']."','".$_REQUEST['promcop_discount_type']."','".$start_date."','".$end_date."','". $promo_service_start_date."','".$promo_service_end_date."','".$promo_autoapply_code."','".$combine_discount."','".$_REQUEST['user_id']."','')";
                $discount_id = operations($query);
				for($k=0;$k<count($special_package);$k++)
                {
                    $special_package[$k] = (int)$special_package[$k];
                    $Servicequery="insert into special_discount_package(parent_id,apply_package_id) value('".$discount_id."','".$special_package[$k]."')";
                    $resource3 = operations($Servicequery);
                }
	   			$result=global_message(200,1007,[]);
		   	}
        else
        {
            $result=global_message(200,1006);
        }
        return  $result;
    }
}


/*****************************************************************
Method:             getDiscountCuponList()
InputParameter:     user_id
Return:             get Discount Cupon List
 *****************************************************************/
function getDiscountCuponList()
{
    if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
    {
        $query="Select * from vehicle_special_discount_table where user_id='".$_REQUEST['user_id']."' order by name asc";
        $resource= operations($query);
        $contents = array();
        for($i=0; $i<count($resource); $i++)
        {
            $package_name='';
            $special_pkg="Select b.package_code package_name from special_discount_package a inner join special_request b on a.apply_package_id=b.id where a.parent_id='".$resource[$i]['id']."'";
            $resource2= operations($special_pkg);
            for($k=0; $k<count($resource2); $k++)
            {
                $package_name .=$resource2[$k]['package_name'].',';
            }
            $resource[$i]['start_date']=explode("-",$resource[$i]['start_date']);
            $resource[$i]['start_date']=$resource[$i]['start_date'][1]."-".$resource[$i]['start_date'][2]."-".$resource[$i]['start_date'][0];
            $resource[$i]['end_date']=explode("-",$resource[$i]['end_date']);
            $resource[$i]['end_date']=$resource[$i]['end_date'][1]."-".$resource[$i]['end_date'][2]."-".$resource[$i]['end_date'][0];
            $resource[$i]['service_start_date']=explode("-",$resource[$i]['service_start_date']);
            $resource[$i]['service_start_date']=$resource[$i]['service_start_date'][1]."-".$resource[$i]['service_start_date'][2]."-".$resource[$i]['service_start_date'][0];
            $resource[$i]['service_end_date']=explode("-",$resource[$i]['service_end_date']);
            $resource[$i]['service_end_date']=$resource[$i]['service_end_date'][1]."-".$resource[$i]['service_end_date'][2]."-".$resource[$i]['service_end_date'][0];

            $contents[$i]['id']=$resource[$i]['id'];
            $contents[$i]['promo_couppon_name']=$resource[$i]['name'];
            $contents[$i]['promo_couppon_code']=$resource[$i]['code'];
            $contents[$i]['promo_couppon_value']=$resource[$i]['discount_value'];
            $contents[$i]['promcop_discount_type']=$resource[$i]['discount_type'];
            $contents[$i]['promo_newstart_date']=$resource[$i]['start_date'];
            $contents[$i]['promo_newend_date']=$resource[$i]['end_date'];
            $contents[$i]['promo_start_date']=$resource[$i]['service_start_date'];
            $contents[$i]['promo_end_date']=$resource[$i]['service_end_date'];
            $contents[$i]['promo_autoapply_code']=$resource[$i]['promo_pref'];
            $contents[$i]['combine_discount']=$resource[$i]['is_combine_discount'];
            $contents[$i]['package_name'] = $package_name;
        }
        if(count($contents)>0 && gettype($contents)!="boolean")
        {
            $result=global_message(200,1007,$contents);
        }
        else
        {
            $result=global_message(200,1006);
        }
    }
    else
    {
        $result=global_message(201,1003);
    }
    return  $result;
}

/*****************************************************************
Method:             viewSpecialDiscountCuppon()
InputParameter:     view_Id
Return:             view Special Discount Cuppon
 *****************************************************************/
function viewSpecialDiscountCuppon()
{
    $query="Select * from vehicle_special_discount_table where id='".$_REQUEST['view_Id']."'";
    $resource= operations($query);
    $contents = array();

    for($i=0; $i<count($resource); $i++)
    {
        $spl_package_id='';
        $packagequery="Select apply_package_id from special_discount_package where parent_id='".$_REQUEST['view_Id']."'";
        $resource2= operations($packagequery);
        for($k=0; $k<count($resource2); $k++)
        {
            $spl_package_id .=$resource2[$k]['apply_package_id'].',';
        }
        $contents[$i]['id']=$resource[$i]['id'];
        $contents[$i]['promo_couppon_name']=$resource[$i]['name'];
        $contents[$i]['promo_couppon_code']=$resource[$i]['code'];
        $contents[$i]['promo_couppon_value']=$resource[$i]['discount_value'];
        $contents[$i]['promcop_discount_type']=$resource[$i]['discount_type'];
        $contents[$i]['promo_newstart_date']=$resource[$i]['start_date'];
        $contents[$i]['promo_newend_date']=$resource[$i]['end_date'];
        $contents[$i]['promo_start_date']=$resource[$i]['service_start_date'];
        $contents[$i]['promo_end_date']=$resource[$i]['service_end_date'];
        $contents[$i]['promo_service_type']=isset($service_type) ? $service_type : '';
        $contents[$i]['promo_autoapply_code']=$resource[$i]['promo_pref'];
        $contents[$i]['is_combine_discount']=$resource[$i]['is_combine_discount'];
        $contents[$i]['special_package_id'] = $spl_package_id;
    }
    if(count($contents)>0 && gettype($contents)!="boolean")
    {
        $result=global_message(200,1007,$contents);
    }
    else
    {
        $result=global_message(200,1006);
    }
    return  $result;
}


/*****************************************************************
Method:             deleteSpecialCuponList()
InputParameter:     row_Id
Return:             delete Special CuponList
 *****************************************************************/
function deleteSpecialCuponList()
{
    if((isset($_REQUEST['row_Id']) && !empty($_REQUEST['row_Id'])))
    {
        $rowId=$_REQUEST['row_Id'];
        $query="delete from vehicle_special_discount_table where id='".$rowId."'";
        $resource = operations($query);
        $queryDelete1="delete  from vehicle_special_dicount_extra_info where parent_id='".$rowId."'";
        $resource2 = operations($queryDelete1);
        $queryDelete2="delete  from vehicle_special_dicount_sma_info where parent_id='".$rowId."'";
        $resource3 = operations($queryDelete2);
        $queryDelete3="delete  from vehicle_special_discount_service_info where parent_id='".$rowId."'";
        $resource4 = operations($queryDelete3);
        $result=global_message(200,1010);
    }
    else
    {
        $result=global_message(201,1003);
    }
    return $result;
}

/*****************************************************************
Method:             specialPromocupponUpdate()
InputParameter:
Return:             special Promo cuppon Update
 *****************************************************************/
function specialPromocupponUpdate()
{
    $start_date = $_REQUEST['promo_newstart_date'];
    $start_date=explode('/',$start_date);
    $start_date=$start_date[2]."-".$start_date[0]."-".$start_date[1];
    $end_date = $_REQUEST['promo_newend_date'];
    $end_date=explode('/',$end_date);
    $end_date=$end_date[2]."-".$end_date[0]."-".$end_date[1];
    $promo_service_start_date = $_REQUEST['promo_service_start_date'];
    $promo_service_start_date=explode('/',$promo_service_start_date);
    $promo_service_start_date=$promo_service_start_date[2]."-".$promo_service_start_date[0]."-".$promo_service_start_date[1];
    $promo_service_end_date = $_REQUEST['promo_service_end_date'];
    $promo_service_end_date=explode('/',$promo_service_end_date);
    $promo_service_end_date=$promo_service_end_date[2]."-".$promo_service_end_date[0]."-".$promo_service_end_date[1];
    $getRowId=$_REQUEST['edit_row_id'];
    $query12="select count(*) totalnumber from vehicle_special_discount_table where (code='".$_REQUEST['promo_couppon_code']."' or name='".$_REQUEST['promo_couppon_name']."' )   and id<>'".$getRowId."'";

    $query12Result = operations($query12);
    if($query12Result[0]['totalnumber']==0)
    {
        $queryforDelete2="delete from special_discount_package where parent_id=".$getRowId;
        operations($queryforDelete2);
        $query="update vehicle_special_discount_table set `name`='".$_REQUEST['promo_couppon_name']."', `code`='".$_REQUEST['promo_couppon_code']."', `discount_value`='".$_REQUEST['promo_couppon_value']."', `discount_type`='".$_REQUEST['promcop_discount_type']."', `start_date`='".$start_date."', `end_date`='".$end_date."', `service_start_date`='".$promo_service_start_date."', `service_end_date`='".$promo_service_end_date."', `promo_pref`='".$_REQUEST['promo_autoapply_code']."', `is_combine_discount`='".$_REQUEST['combine_discount']."' where id=".$getRowId;
        operations($query);
        $special_package=explode(',',$_REQUEST['promo_special_package']);
        for($k=0;$k<count($special_package);$k++)
        {
            $special_package[$k] = (int)$special_package[$k];
            $Servicequery="insert into special_discount_package(parent_id,apply_package_id) value('".$getRowId."','".$special_package[$k]."')";
            $resource3 = operations($Servicequery);
        }
        $result=global_message(200,1006,"successInserted");
    }
    else
    {
        $result=global_message(200,1005);
    }
    return $result;
}

?>