<?php

include_once 'config.php';
include_once 'comman.php';
//define('WP_MEMORY_LIMIT', '564M');
/*****************************************************************
 * Method:             getCommanpeakHourRate()
 * InputParameter:     userId
 * Return:             get Comman peakHourRate
 *****************************************************************/
function getCommanpeakHourRate()
{
    $query = "select * from peak_hour_master where user_id='" . $_REQUEST['user_id'] . "'";
    $resource = operations($query);
    if (count($resource) >= 1 && gettype($resource) != 'boolean') {
        $result = global_message(200, 1007, $resource);
    } else {
        $result = global_message(201, 1005);
    }
    return $result;
}

/*****************************************************************
 * Method:             showPointToPointVehicleRate()
 * InputParameter:     userId
 * Return:             show Point To Point VehicleRate
 *****************************************************************/
function showPointToPointVehicleRate()
{
    $resultFollow = array();
    $query = "select * from point_to_point_rate where id='" . $_REQUEST['getSeq'] . "' ";
    $resource = operations($query);
    $query2 = "select * from point_to_point_vehicle_rate where  point_parent_id='" . $_REQUEST['getSeq'] . "' ";
    $resource2 = operations($query2);
    $resultFollow['singleData'] = $resource;
    $resultFollow['secondData'] = $resource2;
    $result = global_message(200, 1007, $resultFollow);
    return $result;
}

/*****************************************************************
 * Method:             deletePointToPointVehicleRate()
 * InputParameter:
 * Return:             delete Point To Point VehicleRate
 *****************************************************************/
function deletePointToPointVehicleRate()
{
    $query = "delete from point_to_point_vehicle_rate  where point_parent_id='" . $_REQUEST['getSeq'] . "'";
    $query2 = "delete from point_to_point_rate  where id='" . $_REQUEST['getSeq'] . "'";
    $resource = operations($query);
    $resource = operations($query2);
    $result = global_message(200, 1007, $resource);
    return $result;
}

/*****************************************************************
 * Method:             deletePointToPointVehicleRate()
 * InputParameter:
 * Return:             delete Point To Point VehicleRate
 *****************************************************************/
function showZoneToZoneTollRateDelete()
{
    $query = "delete from zone_to_zone_toll  where id='" . $_REQUEST['getSeq'] . "'";
    $query2 = "delete from zone_to_zone_toll_city  where zone_to_zone_toll_id='" . $_REQUEST['getSeq'] . "'";

    $query3 = "delete from zone_to_zone_toll_rate  where zone_to_zone_toll_id='" . $_REQUEST['getSeq'] . "'";

    $query4 = "delete from zone_to_zone_toll_service  where zone_to_zone_toll_id='" . $_REQUEST['getSeq'] . "'";
    $query5 = "delete from zone_to_zone_toll_zip  where zone_to_zone_toll_id='" . $_REQUEST['getSeq'] . "'";
    $resource = operations($query);
    $resource = operations($query2);
    $resource = operations($query3);
    $resource = operations($query4);
    $resource = operations($query5);
    $result = global_message(200, 1007, $resource);
    return $result;
}

/*****************************************************************
 * Method:             updatePointToPointRate()
 * InputParameter:
 * Return:             update Point To PointRate
 *****************************************************************/
function updatePointToPointRate()
{

    $query = "UPDATE `point_to_point_rate` SET  `currency_type`='" . $_REQUEST['currencyTypePoint'] . "', `pickup_zone_id` = '" . $_REQUEST['pick_name'] . "', `drop_off_zone` = '" . $_REQUEST['drop_name'] . "', `peak_increase_rate` = '" . $_REQUEST['getIncrementValue'] . "', `peak_hour_db` = '" . $_REQUEST['pickHrsDatabase'] . "',  `point_to_name` = '" . $_REQUEST['getpointName'] . "' WHERE `id` = '" . $_REQUEST['getSeq'] . "'";
    $queryInsert = operations($query);
    $deleteQuery = "delete from point_to_point_vehicle_rate where point_parent_id='" . $_REQUEST['getSeq'] . "' ";
    $queryInsert = operations($deleteQuery);
    for ($i = 0; $i < count($_REQUEST['vehicle_rate']); $i++) {
        $queryInsert = "INSERT INTO `point_to_point_vehicle_rate` (`point_parent_id`, `vehicle_id`, `amount`, `toll_amt`) VALUES ('" . $_REQUEST['getSeq'] . "', '" . $_REQUEST['vehicle_rate'][$i]['vehicle_code'] . "','" . $_REQUEST['vehicle_rate'][$i]['vehicle_rate'] . "', '" . $_REQUEST['vehicle_rate'][$i]['tollRate'] . "')";
        operations($queryInsert);

    }
    $result = global_message(200, 1007, $queryInsert);
    return $result;
}

/*****************************************************************
 * Method:             savePointToPointVehicleRate()
 * InputParameter:
 * Return:             save Point To Point VehicleRate
 *****************************************************************/
function savePointToPointVehicleRate()
{
    $query = "select * from point_to_point_vehicle_rate where point_parent_id='" . $_REQUEST['getSeq'] . "'";
    $resource = operations($query);
    $result = global_message(200, 1007, $resource);
    return $result;
}

/*****************************************************************
 * Method:             getPointToPointRetSetUp()
 * InputParameter:     user_id
 * Return:             get Point To Point RetSetUp
 *****************************************************************/
function getPointToPointRetSetUp()
{
    $query = "select * from point_to_point_rate where user_id='" . $_REQUEST['user_id'] . "' order by point_to_name asc, pickup_zone_id asc";
    $resource = operations($query);
    if (count($resource) >= 1 and gettype($resource) != 'boolean') {
        $result = global_message(200, 1007, $resource);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}


/*****************************************************************
 * Method:             getPointToPointRetSetUp()
 * InputParameter:     user_id
 * Return:             get Point To Point RetSetUp
 *****************************************************************/
function getZonetoZoneToll()
{
    $fullResult = [];
    $query = "select a.* ,(select b.sma_name from sma b where b.id=a.sma_from_id) smafrom ,(select c.sma_name from sma c where c.id=a.`sma_to_id`) smato  from zone_to_zone_toll a where a.user_id='" . $_REQUEST['user_id'] . "' order by a.name asc";


    $resource = operations($query);
    if (count($resource) >= 1 and gettype($resource) != 'boolean') {


        for ($i = 0; $i < count($resource); $i++) {
            $fullResult[$i] = $resource[$i];
            $zoneRate = "select * from zone_to_zone_toll_rate where zone_to_zone_toll_id='" . $resource[$i]['id'] . "'";
            $zoneRateResult = operations($zoneRate);
            $fullResult[$i]['zone_rate'] = $zoneRateResult;


            $zoneService = "select * from zone_to_zone_toll_service where user_id='" . $_REQUEST['user_id'] . "' and zone_to_zone_toll_id='" . $resource[$i]['id'] . "'";
            $zoneServiceResult = operations($zoneService);
            $fullResult[$i]['zone_service'] = $zoneServiceResult;


            $zoneZip = "select distinct postal_code,type from zone_to_zone_toll_zip where  zone_to_zone_toll_id='" . $resource[$i]['id'] . "'";
            $zoneZipResult = operations($zoneZip);
            $fullResult[$i]['zone_zip'] = $zoneZipResult;


            // $query="select city_name,city_id from sma_location  where sma_id='".$_REQUEST['countryId']."' and county_name='".$getZoneZipCodes[$i]."' GROUP by city_name";

            $zoneCity = "select distinct b.city_name , a.type from zone_to_zone_toll_city a,sma_location b where a.city_id=b.city_id and a.user_id='" . $_REQUEST['user_id'] . "' and a.zone_to_zone_toll_id='" . $resource[$i]['id'] . "'";
            $zoneCityResult = operations($zoneCity);
            $fullResult[$i]['zone_city'] = $zoneCityResult;
        }

        $result = global_message(200, 1007, $fullResult);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}


/*****************************************************************
 *
 * Method:             getPointToPointRetSetUp()
 * InputParameter:     user_id
 * Return:             get Point To Point RetSetUp
 *****************************************************************/
function showZoneToZoneTollRate()
{
    $fullResult = [];
    $query = "select a.* from zone_to_zone_toll a where id ='" . $_REQUEST['getSeq'] . "' order by a.name asc";
    $resource = operations($query);
    if (count($resource) >= 1 and gettype($resource) != 'boolean') {


        for ($i = 0; $i < count($resource); $i++) {
            $fullResult[$i] = $resource[$i];
            $zoneRate = "select * from zone_to_zone_toll_rate where zone_to_zone_toll_id='" . $resource[$i]['id'] . "'";
            $zoneRateResult = operations($zoneRate);
            $fullResult[$i]['zone_rate'] = $zoneRateResult;


            $zoneService = "select service_type from zone_to_zone_toll_service where  zone_to_zone_toll_id='" . $resource[$i]['id'] . "'";
            $zoneServiceResult = operations($zoneService);
            $fullResult[$i]['zone_service'] = $zoneServiceResult;

            $zoneZip = "select postal_code,type from zone_to_zone_toll_zip where  zone_to_zone_toll_id='" . $resource[$i]['id'] . "'";
            $zoneZipResult = operations($zoneZip);
            $fullResult[$i]['zoneZipResult'] = $zoneZipResult;


            $zoneCity = "select * from zone_to_zone_toll_city where  zone_to_zone_toll_id='" . $resource[$i]['id'] . "'";
            $zoneCityResult = operations($zoneCity);
            $fullResult[$i]['zone_city'] = $zoneCityResult;

            $zoneCounty = "select * from zone_to_zone_toll_county where  zone_to_zone_toll_id='" . $resource[$i]['id'] . "'";
            $zoneCountyResult = operations($zoneCounty);
            $fullResult[$i]['zone_county'] = $zoneCountyResult;
        }

        $result = global_message(200, 1007, $fullResult);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}

/*****************************************************************
 * Method:             getPointToPointRetSetUp()
 * InputParameter:     user_id
 * Return:             get Point To Point RetSetUp
 *****************************************************************/
function getzoneTozoneSma()
{
    $query = "select * from sma where user_id='" . $_REQUEST['user_id'] . "' order by sma_name asc";
    $resource = operations($query);
    if (count($resource) >= 1 and gettype($resource) != 'boolean') {
        $result = global_message(200, 1007, $resource);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}


/*****************************************************************
 * Method:             getzoneTozonePoatalCodeSma()
 * InputParameter:     user_id
 * Return:             get Point To Point RetSetUp
 *****************************************************************/
function getzoneTozonePostalCodeSma()
{
    print_r($_REQUEST['city_id']);
    $query = "select * from sma where user_id='" . $_REQUEST['user_id'] . "' order by sma_name asc";
    $resource = operations($query);
    if (count($resource) >= 1 and gettype($resource) != 'boolean') {
        $result = global_message(200, 1007, $resource);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}

/*****************************************************************
 * Method:             getPointToPointRetSetUp()
 * InputParameter:     user_id
 * Return:             get Point To Point RetSetUp
 *****************************************************************/
function getzoneTozoneCitySma()
{
    $getZoneZipCodes = $_REQUEST['tollZone_A_smaSetup'];
    $getAllCode = [];
    for ($i = 0; $i < count($getZoneZipCodes); $i++) {
        // sma_zone_data
        // .$_REQUEST['countryId']
        $query = "select city_name,city_id from sma_location  where sma_id='" . $_REQUEST['countryId'] . "' and county_name='" . $getZoneZipCodes[$i] . "' GROUP by city_name";


        $resource = operations($query);


        for ($j = 0; $j < count($resource); $j++) {
            array_push($getAllCode, $resource[$j]);

        }


        // print_r($resource);


    }


    if (count($getAllCode) >= 1 and gettype($getAllCode) != 'boolean') {
        $result = global_message(200, 1007, $getAllCode);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}


/*****************************************************************
 * Method:             getPointToPointRetSetUp()
 * InputParameter:     user_id
 * Return:             get Point To Point RetSetUp
 *****************************************************************/
function getzoneTozoneCountySma()
{
    $query = "select county_name,county_id from sma_location  where sma_id=" . $_REQUEST['sma_id'] . " and user_id='" . $_REQUEST['user_id'] . "' GROUP by county_name order by county_name asc";

    // echo $query;
    $resource = operations($query);
    if (count($resource) >= 1 and gettype($resource) != 'boolean') {
        $result = global_message(200, 1007, $resource);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}


/*****************************************************************
 * Method:             getPointToPointRetSetUp()
 * InputParameter:     user_id
 * Return:             get Point To Point RetSetUp
 *****************************************************************/
function getZoneZipCodes()
{
    $getZoneZipCodes = $_REQUEST['tollZone_A_smaSetup'];
    $getAllCode = [];
    for ($i = 0; $i < count($getZoneZipCodes); $i++) {
        // sma_zone_data
        // .$_REQUEST['countryId']
        $query = "select distinct  postal_code from sma_location  where sma_id='" . $_REQUEST['countryId'] . "' and city_name='" . $getZoneZipCodes[$i] . "'";


        $resource = operations($query);

        for ($j = 0; $j < count($resource); $j++) {
            array_push($getAllCode, $resource[$j]['postal_code']);

        }


        // print_r($resource);


    }
    if (count($getAllCode) >= 1 and gettype($getAllCode) != 'boolean') {
        $result = global_message(200, 1007, $getAllCode);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}

/*****************************************************************
 * Method:             getPointToPointRetSetUp()
 * InputParameter:     user_id
 * Return:             get Point To Point RetSetUp
 *****************************************************************/
function getzoneTozoneZipSma()
{
    $query = "select postal_code from sma_location  where sma_id=" . $_REQUEST['sma_id'] . " and  user_id='" . $_REQUEST['user_id'] . "' order by postal_code asc";


    $resource = operations($query);
    if (count($resource) >= 1 and gettype($resource) != 'boolean') {
        $result = global_message(200, 1007, $resource);
    } else {
        $result = global_message(200, 1006);
    }
    return $result;
}

/*****************************************************************
 * Method:             savePointToPointRate()
 * InputParameter:
 * Return:             save Point To PointRate
 *****************************************************************/
function savePointToPointRate()
{
    $query = "INSERT INTO `point_to_point_rate` (`pickup_zone_id`, `drop_off_zone`, `peak_increase_rate`, `peak_hour_db`, `point_to_name`,`currency_type`,`user_id`)VALUES ('" . $_REQUEST['pick_name'] . "', '" . $_REQUEST['drop_name'] . "', '" . $_REQUEST['getIncrementValue'] . "', '" . $_REQUEST['pickHrsDatabase'] . "', '" . $_REQUEST['getpointName'] . "', '" . $_REQUEST['currencyTypePoint'] . "', '" . $_REQUEST['user_id'] . "');";
    $lastInsertedId = operations($query);
    $getVehiclRate = $_REQUEST['vehicle_rate'];
    for ($i = 0; $i < count($getVehiclRate); $i++) {
        $query2 = "INSERT INTO `point_to_point_vehicle_rate` (`point_parent_id`, `vehicle_id`, `amount`, `toll_amt`) VALUES ('" . $lastInsertedId . "', '" . $getVehiclRate[$i]['vehicle_code'] . "', '" . $getVehiclRate[$i]['vehicle_rate'] . "', '" . $getVehiclRate[$i]['tollRate'] . "')";
        $resource = operations($query2);
    }
    $result = global_message(200, 1007, $resource);
    return $result;
}

/*****************************************************************
 * Method:             savePointToPointRate()
 * InputParameter:
 * Return:             save Point To PointRate
 *****************************************************************/
function saveZoneToZoneRateMatrix()
{


    $query = "INSERT INTO `zone_to_zone_toll` (`name`, `peak_hour_db`, `sma_from_id`, `sma_to_id`, `user_id`,`is_reverse`,`disclaimer_box`,`disclaimer_message`)VALUES ('" . $_REQUEST['zoneMatrixName'] . "', '" . $_REQUEST['peakHrsDatabase'] . "', '" . $_REQUEST['tollZone_A_smaSetup'] . "', '" . $_REQUEST['tollZone_B_smaSetup'] . "', '" . $_REQUEST['user_id'] . "', '" . $_REQUEST['isRevese'] . "', '" . $_REQUEST['disclaimerbox'] . "', '" . $_REQUEST['disclaimerMessage'] . "');";

    // echo $query;
    $lastInsertedId = operations($query);
    $getVehiclRate = $_REQUEST['tollZoneACity'];
    for ($i = 0; $i < count($getVehiclRate); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_city` (`city_id`, `user_id`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $getVehiclRate[$i] . "', '" . $_REQUEST['user_id'] . "', 'from', '" . $lastInsertedId . "')";
        $resource = operations($query2);
    }

    $tollZoneACountyObject = $_REQUEST['tollZoneACountyObject'];
    for ($i = 0; $i < count($tollZoneACountyObject); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_county` (`county_id`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $tollZoneACountyObject[$i] . "', 'from', '" . $lastInsertedId . "')";
        $resource = operations($query2);
    }
    $tollZoneBCountyObject = $_REQUEST['tollZoneBCountyObject'];
    for ($i = 0; $i < count($tollZoneBCountyObject); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_county` (`county_id`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $tollZoneBCountyObject[$i] . "', 'to', '" . $lastInsertedId . "')";
        $resource = operations($query2);
    }


    $tollZoneBCity = $_REQUEST['tollZoneBCity'];
    for ($i = 0; $i < count($tollZoneBCity); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_city` (`city_id`, `user_id`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $tollZoneBCity[$i] . "', '" . $_REQUEST['user_id'] . "', 'to', '" . $lastInsertedId . "')";
        $resource = operations($query2);
    }


    $postelCodeObject = $_REQUEST['postelCodeObject'];
    for ($i = 0; $i < count($postelCodeObject); $i++) {
        $postalCodeQuery = "INSERT INTO `zone_to_zone_toll_zip` (`postal_code`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $postelCodeObject[$i] . "', 'from', '" . $lastInsertedId . "')";
        $postalCodeQueryResult = operations($postalCodeQuery);
    }


    $postelbCodeObject = $_REQUEST['postelbCodeObject'];
    for ($i = 0; $i < count($postelbCodeObject); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_zip` (`postal_code`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $postelbCodeObject[$i] . "', 'to', '" . $lastInsertedId . "')";
        $resource = operations($query2);
    }

    $serviceType = $_REQUEST['serviceType'];
    for ($i = 0; $i < count($serviceType); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_service` (`service_type`, `user_id`, `zone_to_zone_toll_id`) VALUES ('" . $serviceType[$i] . "', '" . $_REQUEST['user_id'] . "','" . $lastInsertedId . "')";
        $resource = operations($query2);
    }

    $vehicle_rate = $_REQUEST['vehicle_rate'];
    for ($i = 0; $i < count($vehicle_rate); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_rate` (`vehicle_name`, `zone_to_zone_toll_id`, `rate`) VALUES ('" . $vehicle_rate[$i]['vehicle_code'] . "','" . $lastInsertedId . "', '" . $vehicle_rate[$i]['vehicle_rate'] . "')";
        $resource = operations($query2);
    }

    $result = global_message(200, 1007, $resource);
    return $result;
}


/*****************************************************************
 * Method:             updateZoneToZoneRateMatrix()
 * InputParameter:
 * Return:             update Zone toll rate
 *****************************************************************/
function updateZoneToZoneRateMatrix()
{

    $query2 = "delete from zone_to_zone_toll_city  where zone_to_zone_toll_id='" . $_REQUEST['getSeq'] . "'";
    $query3 = "delete from zone_to_zone_toll_rate  where zone_to_zone_toll_id='" . $_REQUEST['getSeq'] . "'";
    $query4 = "delete from zone_to_zone_toll_service  where zone_to_zone_toll_id='" . $_REQUEST['getSeq'] . "'";

    $query5 = "delete from zone_to_zone_toll_zip  where zone_to_zone_toll_id='" . $_REQUEST['getSeq'] . "'";
    $resource = operations($query2);
    $query6 = "delete from zone_to_zone_toll_county  where zone_to_zone_toll_id='" . $_REQUEST['getSeq'] . "'";
    $resource = operations($query6);
    $resource = operations($query3);
    $resource = operations($query4);
    $resource = operations($query5);


    $lastInsertedId = $_REQUEST['getSeq'];
// '".$_REQUEST['tollZone_A_smaSetup']."'
    $query = "update zone_to_zone_toll set name='" . $_REQUEST['zoneMatrixName'] . "',peak_hour_db='" . $_REQUEST['peakHrsDatabase'] . "',sma_from_id='" . $_REQUEST['tollZone_A_smaSetup'] . "',disclaimer_box='" . $_REQUEST['disclaimerbox'] . "',disclaimer_message='" . $_REQUEST['disclaimerMessage'] . "',is_reverse='" . $_REQUEST['isRevese'] . "',sma_to_id='" . $_REQUEST['tollZone_B_smaSetup'] . "'  where id='" . $lastInsertedId . "'";
    $resource = operations($query);
    $getVehiclRate = $_REQUEST['tollZoneACity'];
    for ($i = 0; $i < count($getVehiclRate); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_city` (`city_id`, `user_id`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $getVehiclRate[$i] . "', '" . $_REQUEST['user_id'] . "', 'from', '" . $lastInsertedId . "')";
        $resource = operations($query2);
    }


    $tollZoneACountyObject = $_REQUEST['tollZoneACountyObject'];
    for ($i = 0; $i < count($tollZoneACountyObject); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_county` (`county_id`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $tollZoneACountyObject[$i] . "', 'from', '" . $lastInsertedId . "')";
        $resource = operations($query2);
    }
    $tollZoneBCountyObject = $_REQUEST['tollZoneBCountyObject'];
    for ($i = 0; $i < count($tollZoneBCountyObject); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_county` (`county_id`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $tollZoneBCountyObject[$i] . "', 'to', '" . $lastInsertedId . "')";
        $resource = operations($query2);
    }


    $tollZoneBCity = $_REQUEST['tollZoneBCity'];
    for ($i = 0; $i < count($tollZoneBCity); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_city` (`city_id`, `user_id`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $tollZoneBCity[$i] . "', '" . $_REQUEST['user_id'] . "', 'to', '" . $lastInsertedId . "')";
        $resource = operations($query2);
    }


    $postelCodeObject = $_REQUEST['postelCodeObject'];
    for ($i = 0; $i < count($postelCodeObject); $i++) {
        $postalCodeQuery = "INSERT INTO `zone_to_zone_toll_zip` (`postal_code`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $postelCodeObject[$i] . "', 'from', '" . $lastInsertedId . "')";
        $postalCodeQueryResult = operations($postalCodeQuery);
    }


    $postelbCodeObject = $_REQUEST['postelbCodeObject'];
    for ($i = 0; $i < count($postelbCodeObject); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_zip` (`postal_code`, `type`, `zone_to_zone_toll_id`) VALUES ('" . $postelbCodeObject[$i] . "', 'to', '" . $lastInsertedId . "')";
        $resource = operations($query2);
    }


    $serviceType = $_REQUEST['serviceType'];
    for ($i = 0; $i < count($serviceType); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_service` (`service_type`, `user_id`, `zone_to_zone_toll_id`) VALUES ('" . $serviceType[$i] . "', '" . $_REQUEST['user_id'] . "','" . $lastInsertedId . "')";
        $resource = operations($query2);
    }

    $vehicle_rate = $_REQUEST['vehicle_rate'];
    for ($i = 0; $i < count($vehicle_rate); $i++) {
        $query2 = "INSERT INTO `zone_to_zone_toll_rate` (`vehicle_name`, `zone_to_zone_toll_id`, `rate`) VALUES ('" . $vehicle_rate[$i]['vehicle_code'] . "','" . $lastInsertedId . "', '" . $vehicle_rate[$i]['vehicle_rate'] . "')";
        $resource = operations($query2);
    }

    $result = global_message(200, 1007, $resource);
    return $result;
}


/*****************************************************************
 * Method:             getPointToPointZone()
 * InputParameter:
 * Return:             get Point To PointZone
 *****************************************************************/
function getPointToPointZone()
{
    if (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) {
        $query = "select * from sma_zone_data where user_id='" . $_REQUEST['user_id'] . "' GROUP BY type_name Order by type_name asc";
        $resource = operations($query);
        if (count($resource) > 0 && gettype($resource) != "boolean") {
            $result = global_message(200, 1007, $resource);
        } else {
            $result = global_message(200, 1006);
        }
    } else {
        $result = global_message(201, 1003);
    }
    return $result;
}

?>