<?php

include_once 'manfees_server.php';

$action = $_REQUEST['action'];
$response=array();
switch ($action) {
	case "getRateMatrix":	
	$response=getRateMatrix();
	echo json_encode($response);
	break;

	case "setManFees":	
	$response=setManFees();
	echo json_encode($response);
	break;	
		
	case "getRateMatrixList":		
	$response=getRateMatrixList();
	echo json_encode($response);
	break;
		
	case "editRateMatrix":	
	$response=editRateMatrix();
	echo json_encode($response);
	break;
		
	case "deleteMandatoryFees":	
	$response=deleteMandatoryFees();
	echo json_encode($response);
	break;
	
	case "checkUniqueMatrix":		
	$response=checkUniqueMatrix();
	echo json_encode($response);
	break;
}//switch


