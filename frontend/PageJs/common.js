var _SERVICEPATHServer = _SERVICEPATHSERVICECLIENT;
//code for google map start===========


function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1;
    var queryEnd = url.indexOf("#") + 1 || url.length + 1;
    var query = url.slice(queryStart, queryEnd - 1);

    if (query === url || query === "")
        return;

    var params = {};
    var nvPairs = query.replace(/\+/g, " ").split("&");

    for (var i = 0; i < nvPairs.length; i++) {
        var nv = nvPairs[i].split("=");
        var n = decodeURIComponent(nv[0]);
        var v = decodeURIComponent(nv[1]);
        if (!(n in params)) {
            params[n] = [];
        }
        params[n].push(nv.length === 2 ? v : null);
    }
    return params;
}

//google.maps.event.addDomListener(window, 'load', Demo.init);


//code for google map end============



//global variable declare for map ====

var option;
var startlocation;
var endlocation;

//global variable declare for map end ====


/*Enabel the form input value function start=================*/
enableFormField();
function enableFormField() {
    $("#searchRateForm :input").prop("disabled", true);
    $("#services").prop("disabled", false);

}
/*Enabel the form input value function end=================*/

//stoplocation value on yes and no radi button ====

$('input[type=radio][name=add_stop]').change(function () {


    if (this.value == 'yes') {

        //$('.form-control2').show();

        $('#stop_points').show();
        $('.aadMore').html('<div class="stopLocationfield"><input type="text" id="stopLocation" class="form-control2 form-control1" name="mytext[]"/><a href="#" class="btn btn-primary3 remove_field">X</a></div>');

        var places = new google.maps.places.Autocomplete(document.getElementById('stopLocation'), options);
       

    } else if (this.value == 'no') {
        $('#stop_points').hide();
        $('.aadMore').html(' ');
    }
});

/*google api for usa location setting=================*/
var options = {

};
/*service onchange function start=================*/
$('#services').change(function ()
{


    option = $(this).find('option:selected').val();
    if (option == 'AIRA') {

        $('#intFlt').show();
    } else {
        $('#intFlt').hide();
    }



    //reset the all form value=======

    $('.stopLocationfield').remove();

    $('#searchRateForm')[0].reset();
    $('#services').val(option);
    $('#stop_points').hide();
    $("#location_map").hide();
    /*again reset the passenger and luggage quantity to zero start===*/

    $('#luggage_quantity').val("0");
    $('#total_passenger').val("0");
    $('#luggage_quantity_small').val("0");
    $('#luggage_quantity_medium').val("0");
    $('#luggage_quantity_large').val("0");


    /*again reset the passenger and luggage quantity to zero end===*/

    /*start to select the service an all work perform for selected service===*/


    option = $(this).find('option:selected').val();

    if (option == 'PTP') {

        $("#searchRateForm :input").prop("disabled", false);
        $('#map_loaction').show()
        $('#stop_point').show();
        $('#triphr').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_hourly_loc').hide();
        $('#pick_charter_loc').hide();
        $('#drop_charte_loc').hide();


        $('#drop_point_loc').show();
        $('#from_pont_to_point').show();


        /* validation applied start here */
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#pick_point').prop("required", true);
        $('#drop_point').prop("required", true);
        /* validation applied End here */



        $('#pick_hourly').hide();
        $('#from_seapick').hide();
        $('#from_seadrop').hide();
        $('#to_seapick').hide();
        $('#drop_hourly_loc').hide();
        $('#pick_from_seaport').hide();
        $('#to_seadrop').hide();
    } else if (option == 'CH') {
        $("#searchRateForm :input").prop("disabled", false);
        $('#map_loaction').show();
        $('#stop_point').show();
        $('#triphr').show();
        $('#drop_hourly_loc').hide();
        $('#drop_charte_loc').show();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_hourly_loc').hide();
        $('#pick_charter_loc').show();
        $('#from_seapick').hide();
        $('#from_seadrop').hide();
        $('#to_seapick').hide();
        $('#to_seadrop').hide();
        $('#pick_from_seaport').hide();

        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);

        $('#pick_charter').prop("required", true);
        $('#drop_charter').prop("required", true);


        /* validation End here */


    } else if (option == 'HRLY') {

        $("#searchRateForm :input").prop("disabled", false);
        $('#map_loaction').show();
        $('#drop_charte_loc').hide();
        $('#stop_point').show();
        $('#pick_charter_loc').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_from_seaport').hide();
        $('#triphr').show();
        $('#drop_hourly_loc').show();
        $('#pick_hourly_loc').show();
        $('#pick_hourly').show();
        $('#from_seapick').hide();
        $('#from_seadrop').hide();
        $('#to_seapick').hide();

        $('#to_seadrop').hide();


        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);

        $('#pick_hourly').prop("required", true);
        $('#drop_hourly').prop("required", true);




        /* validation End here */

    } else if (option == 'SEAA') {  
        $("#searchRateForm :input").prop("disabled", false);
        $('#map_loaction').show();
        //$('#location_map').show();
        $('#stop_point').show();
        $('#drop_charte_loc').hide();
        $('#pick_hourly_loc').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_from_seaport').show();
        $('#from_seapick').show();
        $('#from_seadrop').show();
        $('#to_seapick').hide();
        $('#pick_charter_loc').hide();
        $('#drop_hourly_loc').hide();
        $('#to_seadrop').hide();
        $('#triphr').hide();


        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#pick_from_seaport').prop("required", true);
        $('#drop_from_seaport').prop("required", true);




        /* validation End here */

    } else if (option == 'SEAD') {

        $("#searchRateForm :input").prop("disabled", false);
        $('#map_loaction').show();
        //$('#location_map').show();
        $('#stop_point').show();
        $('#drop_charte_loc').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#pick_charter_loc').hide();
        $('#drop_hourly_loc').hide();
        $('#from_seapick').hide();
        $('#to_seapick').show();
        $('#pick_hourly_loc').hide();
        $('#to_seadrop').show();
        $('#from_seadrop').hide();
        $('#pick_from_seaport').hide();
        $('#triphr').hide();



        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#pick_to_seaport').prop("required", true);
        $('#drop_to_seaport').prop("required", true);



        /* validation End here */

    } else if (option == 'AIRD') {
        $("#searchRateForm :input").prop("disabled", false);
        $('#map_loaction').show();
        //$('#location_map').show();
        $('#stop_point').show();
        $('#drop_charte_loc').hide();
        $('#pick_hourly_loc').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#pick_charter_loc').hide();
        $('#drop_hourly_loc').hide();
        $('#triphr').hide();
        $('#from_airport_drop').hide();
        $('#from_airport_pickup').hide();
        $('#from_seapick').hide();

        $('#pick_from_seaport').hide();
        $('from_seapick').hide();
        $('#from_seadrop').hide();
        $('#to_airport_pickup').show();
        $('#to_airport_drop').show();
        $('#to_seapick').hide();
        $('#to_seadrop').hide();


        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#from_airport_pickloc').prop("required", false);
        $('#from_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#to_airport_pickloc').prop("required", true);
        $('#to_airport_droploc').prop("required", true);




        /* validation End here */

    } else if (option == 'AIRA') {
        $("#searchRateForm :input").prop("disabled", false);




        $('#drop_charte_loc').hide();
        $('#map_loaction').show();
        //$('#location_map').show();
        $('#stop_point').show();
        $('#drop_hourly_loc').hide();
        $('#pick_hourly_loc').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#to_airport_pickup').hide();
        $('#to_airport_drop').hide();
        $('#to_seapick').hide();
        $('#to_seadrop').hide();
        $('#triphr').hide();
        $('#from_airport_drop').show();
        $('#from_seadrop').hide();
        $('#from_airport_pickup').show();
        $('#from_seadrop').hide();
        $('#pick_charter_loc').hide();
        $('#pick_from_seaport').hide();


        /* validation start here */
        $('#pick_point').prop("required", false);
        $('#drop_point').prop("required", false);
        $('#to_airport_pickloc').prop("required", false);
        $('#to_airport_droploc').prop("required", false);
        $('#pick_from_seaport').prop("required", false);
        $('#drop_from_seaport').prop("required", false);
        $('#pick_to_seaport').prop("required", false);
        $('#drop_to_seaport').prop("required", false);
        $('#pick_hourly').prop("required", false);
        $('#drop_hourly').prop("required", false);
        $('#pick_charter').prop("required", false);
        $('#drop_charter').prop("required", false);
        $('#from_airport_pickloc').prop("required", true);
        $('#from_airport_droploc').prop("required", true);


        /* validation End here */

    } else {
        $("#map_icon").css('background-image', 'url(images/map-img.jpg)');
        $("#searchRateForm :input").prop("disabled", true);
        $("#services").prop("disabled", false);
        $('#map_loaction').show();
        $('#drop_hourly_loc').hide();
        $('#stop_point').show();
        $('#drop_charte_loc').hide();
        $('#pick_hourly_loc').hide();
        $('#from_pont_to_point').hide();
        $('#drop_point_loc').hide();
        $('#to_airport_pickup').show();
        $('#to_airport_drop').hide();
        $('#to_seapick').hide();
        $('#to_seadrop').hide();
        $('#triphr').hide();
        $('#from_airport_drop').show();
        $('#from_seadrop').hide();
        $('#from_airport_pickup').hide();
        $('#from_seadrop').hide();
        $('#pick_charter_loc').hide();
        $('#pick_from_seaport').hide();

    }
});

/*work ended for the selected service */



/*function start for map start and end point of map location  */

$('input[type=radio][name=map]').change(function () {
    
    if (this.value == 'yes') {

        if (option == 'AIRA') {

            startlocation = $('#from_airport_pickloc').val();
            endlocation = $('#from_airport_droploc').val();


        } else if (option == 'AIRD') {

            endlocation = $('#to_airport_droploc').val();
            startlocation = $('#to_airport_pickloc').val();


        } else if (option == 'PTP') {

            startlocation = $('#pick_point').val();
            endlocation = $('#drop_point').val();

        } else if (option == 'SEAA') {

            startlocation = $('#pick_from_seaport').val();
            endlocation = $('#drop_from_seaport').val();


        } else if (option == 'CH') {

            startlocation = $('#pick_charter').val();
            endlocation = $('#drop_charter').val();


        } else if (option == 'HRLY') {
                
            startlocation = $('#pick_hourly').val();
            endlocation = $('#drop_hourly').val();
               

        } else if (option == 'SEAD') {

            endlocation = $('#drop_to_seaport').val();
            startlocation = $('#pick_to_seaport').val();


        }

        // Demo.init(startlocation,endlocation);
        if (option == undefined) {
            alert("Please Select service");
            $('#no_map').prop("checked", true);
        } else {

            if (startlocation == 'Select' || startlocation == '') {
                alert('Plese Provide Start Point');
                $('#no_map').prop("checked", true);

            } else if (endlocation == 'Select' || endlocation == '') {
                alert('Plese Provide End Point');
                $('#no_map').prop("checked", true);
            } else {
                Demo.init(startlocation, endlocation)
                //$("#map_icon").hide();
                $("#location_map").show();

                $("#map_icon").css('background-image', 'none');
            }
        }
    }
    if (this.value == 'no') {

        //$("#map_icon").show();
        $("#location_map").hide();
        $("#map_icon").css('background-image', 'url(images/map-img.jpg)');


    }

});
/*function start for map start and end point of map location  */

/*function start for google api for start and end point for map location */
google.maps.event.addDomListener(window, 'load', function () {

    var places = new google.maps.places.Autocomplete(document.getElementById('stopLocation'), options);
    var places = new google.maps.places.Autocomplete(document.getElementById('from_airport_droploc'), options);

    var toAirportPickupLocation = new google.maps.places.Autocomplete(document.getElementById('to_airport_pickloc'), options);
    google.maps.event.addListener(toAirportPickupLocation, 'place_changed', function () {
        var getDate = $('#selected_date').val();
        var getTime = $('#selected_time').val();
        var getLocation = $('#to_airport_pickloc').val();
        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '')
        {
            checkBlackOutDate(getDate, getTime, getLocation);
        }


    });


    var places = new google.maps.places.Autocomplete(document.getElementById('drop_from_seaport'), options);
    var to_seaport_pick_up_location = new google.maps.places.Autocomplete(document.getElementById('pick_to_seaport'), options);
    google.maps.event.addListener(to_seaport_pick_up_location, 'place_changed', function () {
        var getDate = $('#selected_date').val();
        var getTime = $('#selected_time').val();
        var getLocation = $('#pick_to_seaport').val();
        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '')
        {
            checkBlackOutDate(getDate, getTime, getLocation);
        }


    });



    var pick_charter_pickup_location = new google.maps.places.Autocomplete(document.getElementById('pick_charter'), options);



    google.maps.event.addListener(pick_charter_pickup_location, 'place_changed', function () {
        var getDate = $('#selected_date').val();
        var getTime = $('#selected_time').val();
        var getLocation = $('#pick_charter').val();
        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '')
        {
            checkBlackOutDate(getDate, getTime, getLocation);
        }


    });





    /*  point to point pickup location google api*/

    var places2 = new google.maps.places.Autocomplete(document.getElementById('pick_point'), options);
    google.maps.event.addListener(places2, 'place_changed', function () {
        var place = places2.getPlace();


        if (place.name.toLowerCase().indexOf("airport") >= 0)
        {
            $('#airportDisclaimerMsg').html("please select the Airport Arrival service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();

        } else if (place.name.toLowerCase().indexOf("port") >= 0)
        {
            $('#airportDisclaimerMsg').html("please select the From Seaport service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();
        } else
        {
            $('#commanDisclaimerMsg').hide();
            var getDate = $('#selected_date').val();
            var getTime = $('#selected_time').val();
            var getLocation = $('#pick_point').val();

            if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '')
            {

                checkBlackOutDate(getDate, getTime, getLocation);
            }




        }


    });






    var point_to_point_drop_location = new google.maps.places.Autocomplete(document.getElementById('drop_point'), options);


    google.maps.event.addListener(point_to_point_drop_location, 'place_changed', function () {
        var place = point_to_point_drop_location.getPlace();
        if (place.name.toLowerCase().indexOf("airport") >= 0)
        {
            $('#airportDisclaimerMsg').html("please select the Airport Departure service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();
        } else if (place.name.toLowerCase().indexOf("port") >= 0)
        {
            $('#airportDisclaimerMsg').html("please select the To Seaport service type as not doing so will produce an inaccurate quotation");
            $('#commanDisclaimerMsg').show();
        } else
        {
            $('#commanDisclaimerMsg').hide();
        }


    });






    var places = new google.maps.places.Autocomplete(document.getElementById('stoplocation'), options);
    var hourly_pickUp_location = new google.maps.places.Autocomplete(document.getElementById('pick_hourly'), options);



    google.maps.event.addListener(hourly_pickUp_location, 'place_changed', function () {
        var getDate = $('#selected_date').val();
        var getTime = $('#selected_time').val();
        var getLocation = $('#pick_hourly').val();
        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '')
        {
            checkHourlyBlackOutDate(getDate, getTime, getLocation);
        }


    });



    var places = new google.maps.places.Autocomplete(document.getElementById('drop_hourly'), options);
    var places = new google.maps.places.Autocomplete(document.getElementById('drop_charter'), options);
});

/*function end for google api for start and end point for map location */

/*function start for getting the service list from limoany where */
getServiceTypes();




function checkHourlyBlackOutDate(getDate, getTime, getLocation)
{

    $('#refresh_overlay').show()
    var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
    if (typeof (getLocalStorageValue) == "string")
    {
        getLocalStorageValue = JSON.parse(getLocalStorageValue);
        var getJson = {"action": "isHrlyBlackOutDate", "pick_date": getDate, "pick_time": getTime, "pick_up_location": getLocation, "user_id": getLocalStorageValue[0].user_id, "limoanywhereKey": getLocalStorageValue[0].limo_any_where_api_key, "limoanywhereID": getLocalStorageValue[0].limo_any_where_api_id};
        $.ajax({

            url: _SERVICEPATHServer,

            type: 'POST',

            data: getJson,

            success: function (response) {

                var responseObj = response;
                if (typeof (response) == "string")
                {

                    responseObj = JSON.parse(response);

                    $('#refresh_overlay').hide()

                    if (responseObj.code == 1007)
                    {
                        var isCheckAllBlackOut = 'Value Found';
                        var isAllVehicleExist = 0;
                        var totalVehicleLoop = 0;
                        var totalVehicleNotExit = 0;

                        var allVehicleArray = [];
                        var disclamer_message = 'no';
                        // console.log(responseObj.data);
                        // return 0;
                        $.each(responseObj.data, function (index, result) {


                            totalVehicleLoop = parseInt(index) + 1;
                            // alert(disclamer_message=result.blackOutDateInformation);
                            if (typeof (disclamer_message = result.blackOutDateInformation) != "undefined")
                            {
                                disclamer_message = result.blackOutDateInformation[0].msg
                                // alert(result.blackOutDateInformation[0].msg);  
                            }


                            if (result.vehicle == "No Value Found")
                            {
                                // isCheckAllBlackOut='No Value Found';
                                totalVehicleNotExit++;
                                // console.log(result.vehicleNotExist);

                                allVehicleArray.push({"vehicle_info": result.vehicleNotExist, "black_out_info": disclamer_message});



                            } else
                            {

                                isAllVehicleExist++;
                                disclamer_message = result.blackOutDateInformation[0].msg;

                                console.log(result.blackOutDateInformation);
                                // alert(disclamer_message);

                                // allVehicleArray.push({"vehicle_info":result.vehicle,"black_out_info":result.blackOutDateInformation});

                            }


                        });
                        if (totalVehicleLoop == isAllVehicleExist)
                        {

                            alert("Black out date :- " + disclamer_message);
                            localStorage.removeItem("blackOutDateInformation");

                            $('#submit_button').prop("disabled", true);

                        } else if (isAllVehicleExist > 0)
                        {
                            var getJsonFullBlackOut = {"allVehicle": allVehicleArray, "checkBlackOutDate": "blackout"};

                            getJsonFullBlackOut = JSON.stringify(getJsonFullBlackOut)
                            // alert("some vehicle exist and this is black out date");
                            // console.log(getJsonFullBlackOut);
                            localStorage.setItem("blackOutDateInformation", getJsonFullBlackOut);
                            $('#submit_button').prop("disabled", false);


                        }
                        if (totalVehicleLoop == totalVehicleNotExit)
                        {

                            // alert("No Black Out date");
                            localStorage.removeItem("blackOutDateInformation");
                            $('#submit_button').prop("disabled", false);

                        }




                    } else
                    {

                        localStorage.removeItem("blackOutDateInformation");
                        $('#submit_button').prop("disabled", false);
                    }


                }



            }
        });


    }

}






function checkBlackOutDate(getDate, getTime, getLocation)
{
    $('#refresh_overlay').show()
    var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
    if (typeof (getLocalStorageValue) == "string")
    {
        getLocalStorageValue = JSON.parse(getLocalStorageValue);
        var getJson = {"action": "isBlackOutDate", "pick_date": getDate, "pick_time": getTime, "pick_up_location": getLocation, "user_id": getLocalStorageValue[0].user_id, "limoanywhereKey": getLocalStorageValue[0].limo_any_where_api_key, "limoanywhereID": getLocalStorageValue[0].limo_any_where_api_id};
        $.ajax({

            url: _SERVICEPATHServer,

            type: 'POST',

            data: getJson,

            success: function (response) {


                var responseObj = response;
                if (typeof (response) == "string")
                {

                    responseObj = JSON.parse(response);

                    $('#refresh_overlay').hide()

                    if (responseObj.code == 1007)
                    {
                        var isCheckAllBlackOut = 'Value Found';
                        var isAllVehicleExist = 0;
                        var totalVehicleLoop = 0;
                        var totalVehicleNotExit = 0;

                        var allVehicleArray = [];
                        var disclamer_message = 'no';
                        // console.log(responseObj.data);
                        // return 0;
                        $.each(responseObj.data, function (index, result) {


                            totalVehicleLoop = parseInt(index) + 1;
                            // alert(disclamer_message=result.blackOutDateInformation);
                            if (typeof (disclamer_message = result.blackOutDateInformation) != "undefined")
                            {
                                disclamer_message = result.blackOutDateInformation[0].msg
                                // alert(result.blackOutDateInformation[0].msg);  
                            }


                            if (result.vehicle == "No Value Found")
                            {
                                // isCheckAllBlackOut='No Value Found';
                                totalVehicleNotExit++;
                                // console.log(result.vehicleNotExist);

                                allVehicleArray.push({"vehicle_info": result.vehicleNotExist, "black_out_info": disclamer_message});



                            } else
                            {

                                isAllVehicleExist++;
                                disclamer_message = result.blackOutDateInformation[0].msg;
                                // alert(disclamer_message);

                                // allVehicleArray.push({"vehicle_info":result.vehicle,"black_out_info":result.blackOutDateInformation});

                            }


                        });
                        if (totalVehicleLoop == isAllVehicleExist)
                        {

                            alert("Black out date :- " + disclamer_message);
                            localStorage.removeItem("blackOutDateInformation");

                            $('#submit_button').prop("disabled", true);

                        } else if (isAllVehicleExist > 0)
                        {
                            var getJsonFullBlackOut = {"allVehicle": allVehicleArray, "checkBlackOutDate": "blackout"};

                            getJsonFullBlackOut = JSON.stringify(getJsonFullBlackOut)
                            // alert("some vehicle exist and this is black out date");
                            // console.log(getJsonFullBlackOut);
                            localStorage.setItem("blackOutDateInformation", getJsonFullBlackOut);
                            $('#submit_button').prop("disabled", false);


                        }
                        if (totalVehicleLoop == totalVehicleNotExit)
                        {

                            // alert("No Black Out date");
                            localStorage.removeItem("blackOutDateInformation");
                            $('#submit_button').prop("disabled", false);

                        }




                    } else
                    {

                        localStorage.removeItem("blackOutDateInformation");
                        $('#submit_button').prop("disabled", false);
                    }


                }



            }
        });


    }

}



function getServiceTypes()
{
    
    var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
    if (typeof (getLocalStorageValue) == "string")
    {
        getLocalStorageValue = JSON.parse(getLocalStorageValue);
    }
    var user_id = getLocalStorageValue[0].user_id;
    var serviceTypeData = [];
    $.ajax({
        url: SERVICEPATHSERVICETYPE,
        type: 'post',
        data: 'action=GetServiceTypes&user_id=' + userInfoObj[0].id,
        dataType: 'json',
        success: function (data) {
            //console.log(data);
            if (data.ResponseText == 'OK') {

                var ResponseHtml = '<option value="">Select</option>';
                $.each(data.ServiceTypes.ServiceType, function (index, result) {
                    ResponseHtml += "<option value='" + result.SvcTypeCode + "'>" + result.SvcTypeDescription + "</option>";
                    //console.log(result);
                });



                $('#services').html(ResponseHtml);

            }
        }});
}


var getinfo = {
    _Serverpath: "phpfile/client.php",

    getAirportName: function (inputBoxFieldValue) {

        // console.log("om agrawal "+inputBoxFieldValue);

        var inputBoxFieldValue = inputBoxFieldValue;
        if (typeof (getUserId) == "string")
        {
            getUserId = JSON.parse(getUserId);

        }
        var fd = new FormData();
        fd.append("action", "getAirportName");
        fd.append("inputValue", inputBoxFieldValue);
        //fd.append("user_id",getUserId.user_id);

        $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function (result) {

            var locationdata1 = [];
            var response = JSON.parse(result);


            var ResponseHtml = '<option value="">SeWlect</option>';

            for (i = 0; i < response.data.length; i++) {

                locationdata1.push({"value": response.data[i].is_int_flight + "@@" + response.data[i].insidemeet_msg + "@@" + response.data[i].curbside_msg + "@@" + response.data[i].int_flight_msg + "@@" + response.data[i].is_insidemeet_greet + "@@" + response.data[i].is_curbside, "label": response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')'});
                ResponseHtml += "<option value='" + response.data[i].airport_name + '(' + response.data[i].airport_code + ')' + '(' + response.data[i].city_name + ')' + "' seq='" + response.data[i].is_int_flight + "' insidemeet_msg='" + response.data[i].insidemeet_msg + "' curbside_msg='" + response.data[i].curbside_msg + "' int_flight_msg='" + response.data[i].int_flight_msg + "' meatandGreet='" + response.data[i].is_insidemeet_greet + "' curbsideSeq='" + response.data[i].is_curbside + "'>" + response.data[i].airport_name + "</option>";

            }
            var airportLocation = locationdata1;
            setTimeout(function () {


                /* from airport service autocomplete start here */


                $("#from_airport_pickloc").autocomplete({
                    source: airportLocation,
                    delay: 0,
                    select: function (event, ui) {
                        event.preventDefault()
                        $(this).val(ui.item.label);


                        var getDate = $('#selected_date').val();
                        var getTime = $('#selected_time').val();
                        var getLocation = ui.item.label;

                        if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '')
                        {

                            checkBlackOutDate(getDate, getTime, getLocation);
                        }



                        var selectedValue = ui.item.value;
                        var selectedAirportValue = selectedValue.split('@@');
                        var option = selectedAirportValue[0];
                        var meatandGreet = selectedAirportValue[4];
                        var curbsideSeq = selectedAirportValue[5];
                        var insidemeet_msg = selectedAirportValue[1];
                        var curbside_msg = selectedAirportValue[2];
                        var int_flight_msg = selectedAirportValue[3];

                        if (meatandGreet != '0') {
                            //var msg ="this is the content";
                            $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                            //var popover = $('#meetAndGreet').data('bs.popover');
                            // popover.a.content = "YOUR NEW TEXT";
                            //$(".popover-content")[0].innerHTML = 'something else';
                            $('#meetAndGreet').show();
                        } else
                        {

                            $('#meetAndGreet').hide();
                        }
                        if (curbsideSeq != '0') {
                            $('#curbside_disclaimer').attr("data-content", curbside_msg);
                            $('#curbSide').show();
                        } else
                        {
                            $('#curbSide').hide();


                        }



                        if (option != '0')
                        {

                            $('#international_disclaimer').attr("data-content", int_flight_msg);
                            $('.interNationFlight').show();
                        } else
                        {
                            $('.interNationFlight').hide();


                        }

                    },
                    focus: function ()
                    {



                        //$(this).val('asdfasdfasd');
                    }



                });



                /* from airport service autocomplete end here */


                $("#to_airport_droploc").autocomplete({
                    source: airportLocation,
                    delay: 0,
                    select: function (event, ui) {
                        event.preventDefault()
                        $(this).val(ui.item.label);
                        var selectedValue = ui.item.value;
                        var selectedAirportValue = selectedValue.split('@@');
                        var option = selectedAirportValue[0];
                        var meatandGreet = selectedAirportValue[4];
                        var curbsideSeq = selectedAirportValue[5];
                        var insidemeet_msg = selectedAirportValue[1];
                        var curbside_msg = selectedAirportValue[2];
                        var int_flight_msg = selectedAirportValue[3];

                        if (meatandGreet != '0') {
                            //var msg ="this is the content";
                            $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                            //var popover = $('#meetAndGreet').data('bs.popover');
                            // popover.a.content = "YOUR NEW TEXT";
                            //$(".popover-content")[0].innerHTML = 'something else';
                            $('#meetAndGreet').show();
                        } else
                        {

                            $('#meetAndGreet').hide();
                        }
                        if (curbsideSeq != '0') {
                            $('#curbside_disclaimer').attr("data-content", curbside_msg);
                            $('#curbSide').show();
                        } else
                        {
                            $('#curbSide').hide();


                        }



                        if (option != '0')
                        {

                            $('#international_disclaimer').attr("data-content", int_flight_msg);
                            $('.interNationFlight').show();
                        } else
                        {
                            $('.interNationFlight').hide();


                        }

                    },
                    focus: function ()
                    {



                        //$(this).val('asdfasdfasd');
                    }



                });


            }, 1000);

        });

    },

    /*function end for getting the airport list */

    /*function start for getting the seaportname list */
    getSeaPortName: function (inputBoxFieldValue) {
        alert(inputBoxFieldValue);
        //var getUserId=window.localStorage.getItem('companyInfo');
//var getinputTextValue = $('#pick_from_seaport').val();
        var locationdata1 = [];
        var inputBoxFieldValue = inputBoxFieldValue;

        /*if(typeof(getUserId)=="string")
         {   
         
         getUserId=JSON.parse(getUserId);
         
         }
         */
        var fd = new FormData();
        fd.append("action", "getSeaPortName");

        fd.append("inputValue", inputBoxFieldValue);
        //fd.append("user_id",getUserId[0].id)

        $.ajax({
            url: getinfo._Serverpath,

            type: 'POST',
            processData: false,
            contentType: false,
            data: fd
        }).done(function (result) {


            var response = JSON.parse(result);

            var ResponseHtml = '<option value="">Select</option>';
            for (i = 0; i < response.data.length; i++) {

                // locationdata1.push(response.data[i].seaport_name +'('+response.data[i].seaport_code+')' +'('+response.data[i].city_name+')');

                //ResponseHtml+="<option value='"+response.data[i].seaport_code+"'>"+response.data[i].seaport_name+"</option>";


                locationdata1.push({"value": +response.data[i].inside_meet_text + "@@" + response.data[i].curbside_text + "@@" + response.data[i].is_inside_meet_check + "@@" + response.data[i].is_curbside_check, "label": response.data[i].seaport_name + '(' + response.data[i].seaport_code + ')' + '(' + response.data[i].city_name + ')'});
                ResponseHtml += "<option value='" + response.data[i].seaport_name + '(' + response.data[i].seaport_code + ')' + '(' + response.data[i].city_name + ')' + "'  insidemeet_msg='" + response.data[i].insidemeet_msg + "' curbside_msg='" + response.data[i].curbside_msg + "'  meatandGreet='" + response.data[i].is_insidemeet_greet + "' curbsideSeq='" + response.data[i].is_curbside + "'>" + response.data[i].airport_name + "</option>";



            }
            // console.log()

            var airportLocation = locationdata1;
            //console.log(airportLocation);




            /*   code start here */



            $("#pick_from_seaport").autocomplete({
                source: airportLocation,
                select: function (event, ui) {
                    event.preventDefault()
                    $(this).val(ui.item.label);
                    var getDate = $('#selected_date').val();
                    var getTime = $('#selected_time').val();
                    var getLocation = ui.item.label;

                    if ($.trim(getDate) != '' && $.trim(getTime) != '' && $.trim(getLocation) != '')
                    {
                        checkBlackOutDate(getDate, getTime, getLocation);
                    }



                    var selectedValue = ui.item.value;
                    var selectedAirportValue = selectedValue.split('@@');

                    // var option = selectedAirportValue[0];
                    var meatandGreet = selectedAirportValue[2];

                    var curbsideSeq = selectedAirportValue[3];
                    var insidemeet_msg = selectedAirportValue[0];
                    var curbside_msg = selectedAirportValue[1];
                    if (meatandGreet != 0) {

                        alert(insidemeet_msg);
                        $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                        $('#intFlt').show();
                        $('#meetAndGreet').show();


                    } else
                    {
                        $('#meetAndGreet').hide();
                    }
                    if (curbsideSeq != '0') {
                        $('#curbside_disclaimer').attr("data-content", curbside_msg);
                        $('#intFlt').show();
                        $('#curbSide').show();
                    } else
                    {
                        $('#curbSide').hide();
                    }



                    // if(option!='0')
                    // {

                    //     $('#international_disclaimer').attr("data-content",int_flight_msg);
                    //     $('.interNationFlight').show();
                    // }
                    // else
                    // {
                    //       $('.interNationFlight').hide();


                    // }

                }
            });





            /* code end here*/





            $("#drop_to_seaport").autocomplete({
                source: airportLocation,
                select: function (event, ui) {
                    event.preventDefault()
                    $(this).val(ui.item.label);
                    var selectedValue = ui.item.value;
                    var selectedAirportValue = selectedValue.split('@@');

                    // var option = selectedAirportValue[0];
                    var meatandGreet = selectedAirportValue[2];

                    var curbsideSeq = selectedAirportValue[3];
                    var insidemeet_msg = selectedAirportValue[0];
                    var curbside_msg = selectedAirportValue[1];
                    if (meatandGreet != 0) {

                        $('#meetAndGreet_disclaimer').attr("data-content", insidemeet_msg);
                        $('#intFlt').show();
                        $('#meetAndGreet').show();


                    } else
                    {
                        $('#meetAndGreet').hide();
                    }
                    if (curbsideSeq != '0') {
                        $('#curbside_disclaimer').attr("data-content", curbside_msg);
                        $('#intFlt').show();
                        $('#curbSide').show();
                    } else
                    {
                        $('#curbSide').hide();
                    }



                    // if(option!='0')
                    // {

                    //     $('#international_disclaimer').attr("data-content",int_flight_msg);
                    //     $('.interNationFlight').show();
                    // }
                    // else
                    // {
                    //       $('.interNationFlight').hide();


                    // }

                }
            });




        });

    }

    /*function end for getting the seaportname list */
};

// getinfo.getAirportName('r');
getinfo.getAirportName('f');
getinfo.getSeaPortName('c');
//getinfo.getSeaPortName();
