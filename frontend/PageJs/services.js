var getParams = function (url) {

    var params = {};

    var parser = document.createElement('a');

    parser.href = url;

    var query = parser.search.substring(1);

    var vars = query.split('&');

    for (var i = 0; i < vars.length; i++) {

        var pair = vars[i].split('=');

        params[pair[0]] = decodeURIComponent(pair[1]);

    }

    return params;

};

if(localStorage.localUserInfo === undefined){

    localStorage.localUserInfo = 'guestinfo';
    //window.location.href = './';


}

$(function(){

    if(localStorage.localUserInfo == 'guestinfo' && localStorage.limo_editMode != "1"){
        //$('#authModal').modal('show');
    }
});


var is_mini_page = document.getElementById('mini_service');

/* twitter login */
var get_params = getParams(window.location.href);
if(get_params['oauth_token'] != undefined) {



    //var twitter_data = [{ 'f_name': 'User' }];

    //var user_info = JSON.stringify(twitter_data);

    //window.localStorage.setItem("localUserInfo", user_info);

    window.localStorage.setItem('localUserInfo', 'guestinfo');



}
/* ./twitter login */

var localUserInfo = window.localStorage.getItem("localUserInfo");
if (localUserInfo != "guestinfo") {

    localUserInfo = JSON.parse(localUserInfo);

    var f_name = localUserInfo[0].f_name;

    $('.ufname').text(f_name);

    $('.glyphicon-log-in').addClass('glyphicon-log-out').removeClass('glyphicon-log-in');

} else {

    $('.ufname').text('User');

    $('.glyphicon-log-out').addClass('glyphicon-log-in').removeClass('glyphicon-log-out');

}

function notifyParentWindow(data) {

    var send_data = new Uint32Array(2);
    if(document.getElementById('mini_service')){
        if(data.stopAddtress == '' || data.stopAddtress == undefined){
            if(localStorage.getItem('stops_for_mini_services') !== ''){
                data.stopAddtress = localStorage.getItem('stops_for_mini_services');
                data.isLocationPut = 'yes';
            }
        }
    }

    window.parent.postMessage(JSON.stringify({detail : {
        userInfo : JSON.stringify(data),
        limoanyWhereVerification : localStorage.getItem("limoanyWhereVerification")
    }}) , '*' , [send_data.buffer]);
}


function checkHourlyBlackOutDate(getDate, getTime, getLocation) {

    $('#refresh_overlay').show()

    var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");

    if (typeof(getLocalStorageValue) == "string") {

        getLocalStorageValue = JSON.parse(getLocalStorageValue);

        var getJson = { "action": "isHrlyBlackOutDate", "pick_date": getDate, "pick_time": getTime, "pick_up_location": getLocation, "user_id": getLocalStorageValue[0].user_id, "limoanywhereKey": getLocalStorageValue[0].limo_any_where_api_key, "limoanywhereID": getLocalStorageValue[0].limo_any_where_api_id };

        $.ajax({

            url: _SERVICEPATHServer,

            type: 'POST',

            data: getJson,

            success: function(response) {

                var responseObj = response;

                if (typeof(response) == "string") {

                    responseObj = JSON.parse(response);

                    console.log(responseObj);

                    $('#refresh_overlay').hide()

                    if (responseObj.code == 1007) {

                        var isCheckAllBlackOut = 'Value Found';

                        var isAllVehicleExist = 0;

                        var totalVehicleLoop = 0;

                        var totalVehicleNotExit = 0;

                        var allVehicleArray = [];

                        var disclamer_message = 'no';

                        if (responseObj.data[0].hourlyInfo !== undefined && responseObj.data[0].hourlyInfo[0].cut_off_time > 0) {

                            var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");

                            if (typeof(getLocalStorageValue) == "string") {

                                getLocalStorageValue = JSON.parse(getLocalStorageValue);

                            }

                            var user_id = getLocalStorageValue[0].user_id;

                            var serviceTypeData = [];

                            var currentDateTime = new Date();

                            var getCurrentDtate = currentDateTime.getDate();

                            if (getCurrentDtate < 10) {

                                getCurrentDtate = '0' + getCurrentDtate;



                            } else {

                                getCurrentDtate = getCurrentDtate;

                            }

                            var getCurrentMonth = currentDateTime.getMonth() + 1;



                            if (getCurrentMonth < 10) {



                                getCurrentMonth = '0' + getCurrentMonth;



                            } else {

                                getCurrentMonth = getCurrentMonth;

                            }

                            var getCurrentyear = currentDateTime.getFullYear();



                            var currentFullDtae = getCurrentMonth + '/' + getCurrentDtate + '/' + getCurrentyear;





                            var getcurrentHour = currentDateTime.getHours();

                            var getCurrentMinute = currentDateTime.getMinutes();



                            var currentTime = getcurrentHour + ':' + getCurrentMinute;







                            $.ajax({

                                url: _SERVICEPATHServer,

                                type: 'post',

                                data: 'action=checkHourlyCutoffTime&user_id=' + user_id + "&getDate=" + getDate + "&current_date=" + currentFullDtae + "&current_time=" + currentTime + "&getTime=" + getTime + "&servicesType=HRLY",

                                dataType: 'json',

                                success: function(data) {



                                    var dataObj = data;



                                    if (typeof(data) == "string") {



                                        dataObj = JSON.parse(data);



                                    }



                                    if (dataObj.code == "1006") {

                                        var cut_off_hr = dataObj.data / 60;



                                        var a = parseInt(cut_off_hr);

                                        var b = parseInt(responseObj.data[0].hourlyInfo[0].cut_off_time);



                                        if (b > a) {

                                            $('#CutoffTimeMain').css("display", "block").html('Cut off hour : ' + responseObj.data[0].hourlyInfo[0].cut_off_time);

                                            $('#submit_button').css("display", "none");

                                        }





                                    } else {

                                        $('#CutoffTimeMain').css("display", "none").html('');

                                        // $('#submit_button').prop("disabled",false);

                                        $('#submit_button').css("display", "block");











                                    }









                                }

                            });



                        }







                    } else {



                        localStorage.removeItem("blackOutDateInformation");

                        $('#submit_button').prop("disabled", false);

                    }





                }







            }

        });





    }



}
var registrationPageClass = {
    _Serverpath: "phpfile/client.php",
    _SERVICEPATHServer: _SERVICEPATHSERVICECLIENT,

    checkCutoffTime: function(getDate, getTime, servicesType) {
        var getLocalStorageValue = localStorage.getItem("limoanyWhereVerification");
        if (typeof(getLocalStorageValue) == "string") {
            getLocalStorageValue = JSON.parse(getLocalStorageValue);
        }
        var user_id = getLocalStorageValue[0].user_id;
        var serviceTypeData = [];
        var currentDateTime = new Date();
        var getCurrentDtate = currentDateTime.getDate();
        if (getCurrentDtate < 10) {
            getCurrentDtate = '0' + getCurrentDtate;
        } else {
            getCurrentDtate = getCurrentDtate;
        }
        var getCurrentMonth = currentDateTime.getMonth() + 1;
        if (getCurrentMonth < 10) {
            getCurrentMonth = '0' + getCurrentMonth;
        } else {
            getCurrentMonth = getCurrentMonth;
        }
        var getCurrentyear = currentDateTime.getFullYear();
        var currentFullDtae = getCurrentMonth + '/' + getCurrentDtate + '/' + getCurrentyear;
        var getcurrentHour = currentDateTime.getHours();
        var getCurrentMinute = currentDateTime.getMinutes();
        var currentTime = getcurrentHour + ':' + getCurrentMinute;
        $.ajax({
            url: _SERVICEPATHServer,
            type: 'post',
            data: 'action=checkCutoffTime&user_id=' + user_id + "&getDate=" + getDate + "&current_date=" + currentFullDtae + "&current_time=" + currentTime + "&getTime=" + getTime + "&servicesType=" + servicesType,
            dataType: 'json',
            success: function(data) {
                var dataObj = data;
                if (typeof(data) == "string") {
                    dataObj = JSON.parse(data);
                }
                if (dataObj.code == "1006") {
                    $('#CutoffTimeMain').css("display", "block").html(dataObj.data.cutoff_message);
                    $('#submit_button').css("display", "none");
                } else {
                    $('#CutoffTimeMain').css("display", "none").html('');
                    // $('#submit_button').prop("disabled",false);
                    $('#submit_button').css("display", "block");
                }
            }
        });
    },
    passengerLogin: function() {
        /* black out date check functionality start here */
        $('#selected_time').focusout(function() {
            var getLocation = '';
            var selectService = $('#services').val();
            if ($.trim($('#pick_point').val()) != '') {



                getLocation = $('#pick_point').val();

            }

            if ($.trim($('#pick_hourly').val()) != '') {

                getLocation = $('#pick_hourly').val();

            }



            if (selectService == "HRLY") {

                var getDate = $('#selected_date').val();

                var getTime = $('#selected_time').val();

                if ($.trim(getLocation) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {

                    checkHourlyBlackOutDate(getDate, getTime, getLocation);

                    return 0;

                }

            }



            if ($.trim($('#from_airport_pickloc').val()) != '') {

                getLocation = $('#from_airport_pickloc').val();

            }

            if ($.trim($('#to_airport_pickloc').val()) != '') {

                getLocation = $('#to_airport_pickloc').val();

            }

            if ($.trim($('#pick_charter').val()) != '') {

                getLocation = $('#pick_charter').val();

            }

            if ($.trim($('#pick_from_seaport').val()) != '') {

                getLocation = $('#pick_from_seaport').val();

            }

            if ($.trim($('#pick_to_seaport').val()) != '') {

                getLocation = $('#pick_to_seaport').val();

            }

            if ($.trim($('#pick_hourly').val()) != '') {

                getLocation = $('#pick_hourly').val();

            }











            var getDate = $('#selected_date').val();

            var getTime = $('#selected_time').val();









            if ($.trim(getLocation) != '' && $.trim(getDate) != '' && $.trim(getTime) != '')



            {





                /* comman file function call here */

                // checkBlackOutDate();

                checkBlackOutDate(getDate, getTime, getLocation);







            }





        })









        $('#selected_date').change(function() {

            var selectService = $('#services').val();

            var getLocation = '';

            if ($.trim($('#pick_point').val()) != '') {



                getLocation = $('#pick_point').val();

            }

            if ($.trim($('#pick_hourly').val()) != '') {

                getLocation = $('#pick_hourly').val();

            }



            if (selectService == "HRLY") {





                var getDate = $('#selected_date').val();

                var getTime = $('#selected_time').val();

                if ($.trim(getLocation) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {



                    checkHourlyBlackOutDate(getDate, getTime, getLocation);

                    return 0;

                }







            }







            if ($.trim($('#from_airport_pickloc').val()) != '') {

                getLocation = $('#from_airport_pickloc').val();

            }

            if ($.trim($('#to_airport_pickloc').val()) != '') {

                getLocation = $('#to_airport_pickloc').val();

            }

            if ($.trim($('#pick_charter').val()) != '') {

                getLocation = $('#pick_charter').val();

            }

            if ($.trim($('#pick_from_seaport').val()) != '') {

                getLocation = $('#pick_from_seaport').val();

            }

            if ($.trim($('#pick_to_seaport').val()) != '') {

                getLocation = $('#pick_to_seaport').val();

            }



















            var getDate = $('#selected_date').val();

            var getTime = $('#selected_time').val();





            if ($.trim(getLocation) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {





                /* comman file function call here */



                checkBlackOutDate(getDate, getTime, getLocation);







            }

            var servicesType = $('#services').val();

            if ($.trim(servicesType) != '' && $.trim(getDate) != '' && $.trim(getTime) != '')



            {

                /* comman file function call here */

                registrationPageClass.checkCutoffTime(getDate, getTime, servicesType);

            }

        });

        /* black out edatye check functionality end here */

        function show_disclaimer(){

            $('#luggage_disclaimer_message').html(window.localStorage.getItem("LuggageDisclaimer"))

            var current_luggageJson = {
                small : $('#luggage_quantity_small').val(),
                medium : $('#luggage_quantity_medium').val(),
                large : $('#luggage_quantity_large').val()
            }
            var current_cost = getExtraLuggageCost_new(current_luggageJson , Number(document.getElementById('total_passenger').value)) , extra_now = 0;

            Object.values(current_cost).forEach(function (item) {
                extra_now += Number(item);
            })

            if(extra_now > 0)
                $('#luggage_disclaimer').show();
            else
                $('#luggage_disclaimer').hide();
        }


        $('#luggage_disclaimer').hide();
        $('.bootstrap-touchspin-up').click(function () {

            if(window.localStorage.getItem("LuggageActive") === "1")
            show_disclaimer();
        })

        $('.bootstrap-touchspin-down').click(function () {

            if(window.localStorage.getItem("LuggageActive") === "1")
            show_disclaimer();
        })


        $('#lugSelector').click(function () {
            if($(this).prop('checked'))
            {
                $('#luggage_disclaimer').hide();
                $('#luggage_pick_error').hide();
            }
            else{
                show_disclaimer();

            }

        })


        $('#searchRateForm').on("submit", function(event) {

            event.preventDefault();


            var lugSelector = is_mini_page !== null && document.getElementById('mini_service').value === 'frontend/services-mini.php'  ? true :  $('#lugSelector').prop('checked');

            var smallLuggage = $('#luggage_quantity_small').val();

            var mediumLuggage = $('#luggage_quantity_medium').val();

            var largeLuggage = $('#luggage_quantity_large').val();

            var totalLuggage = 0;





            if(lugSelector == true){

                var luggageJson = {

                    "small": 0,

                    "medium": 0,

                    "large": 0

                };



                window.localStorage.setItem("noLuggage", 'yes');
                window.localStorage.setItem("ExtraLuggageCost", JSON.stringify({ large : 0, medium : 0 , small : 0 }));

                window.localStorage.setItem("luggageJson", JSON.stringify(luggageJson));

                totalLuggage = 0;

            } else {

                if (smallLuggage == 0 && mediumLuggage == 0 && largeLuggage == 0) {

                    $('#luggage_pick_error').show();

                    return 0

                } else {

                    var luggageJson = {

                        "small": smallLuggage,

                        "medium": mediumLuggage,

                        "large": largeLuggage

                    }

                }




                window.localStorage.setItem("ExtraLuggageCost", JSON.stringify(getExtraLuggageCost_new(luggageJson , Number(document.getElementById('total_passenger').value))));

                //return;

                window.localStorage.setItem("luggageJson", JSON.stringify(luggageJson));
                window.localStorage.setItem("noLuggage", 'no');

                totalLuggage = parseInt(smallLuggage) + parseInt(mediumLuggage) + parseInt(largeLuggage);


            }

            if(document.getElementById("selected_date").value === ''){
                alert('please select a pickup date');
                return 0;
            }





            var getPessangerQuntity = parseInt($('#total_passenger').val());

            if (getPessangerQuntity == '0') {

                alert("Number Of Passenger Should Be Atleast One");

                $('#refresh_overlay').css('display', 'none');

                return 0;

            }

            $('#refresh_overlay').css('display', 'block');

            var interNationFlightChecked = "no";

            var iscurbsideUpdateChecked = "no";

            var ismeetGreetUpdateChecked = "no";



            if ($('#interNationFlightUpdateService').is(":checked")) {

                interNationFlightChecked = "yes";



            }



            if ($('#curbsideUpdateService').prop("checked")) {

                iscurbsideUpdateChecked = "yes";



            }



            if ($('#meetGreetUpdateService').prop("checked")) {

                ismeetGreetUpdateChecked = "yes";



            }

            // alert(ismeetGreetUpdateChecked);







            var isCheckRadioBtn = $("#add_stop").is(":checked");

            var isLocationPut = 'no';

            var isSubmitServiceVehicleRate = "yes";

            // alert(isCheckRadioBtn);

            if (isCheckRadioBtn) {

                var i = 1;

                var isEmptyTextBox = 'no' , initial_sign = '';

                var stopAddtress = '' , one_empty_box = false;




                $('.stopLocationfield').each(function(index, value) {

                        if ($('#stopLocation' + i).val() == '') {

                            one_empty_box = true

                        } else {
                            initial_sign = stopAddtress == '' ? '' : '@';
                            stopAddtress += initial_sign + $('#stopLocation' + i).val();
                            isLocationPut = 'yes';
                        }


                    i++;

                });






                if (one_empty_box) {



                    alert("Please enter Stop location");

                    window.localStorage.setItem("isStop", "no");

                    return;



                } else {



                    window.localStorage.setItem("isStop", "yes");

                    window.localStorage.setItem("StopsAll", stopAddtress);

                }






            }









            if (!isCheckRadioBtn || isEmptyTextBox == "no") {





                var getVehicleInfo = window.localStorage.getItem("limoanyWhereVerification");



                if (typeof(getVehicleInfo) == "string") {



                    getVehicleInfo = JSON.parse(getVehicleInfo);



                }

                var getServiceName = $('#services').val();



                var fullDate = new Date()



                var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);



                var currentDate = twoDigitMonth + "/" + fullDate.getDate() + "/" + fullDate.getFullYear();

                var localUserInfo = window.localStorage.getItem("localUserInfo");

                var passenger_id = '';

                // alert(localUserInfo);

                if (localUserInfo != "guestinfo") {



                    localUserInfo = JSON.parse(localUserInfo);

                    // console.log();

                    passenger_id = localUserInfo[0].ac_number;

                }











                if ($('#curbsideUpdateService').is(':visible') && $('#meetAndGreet').is(':visible')) {



                    if ($('#curbsideUpdateService').prop("checked") || $('#meetGreetUpdateService').prop("checked")) {



                    } else {

                        alert('Please select Curbside or Meet & Greet');

                        $('#refresh_overlay').css('display', 'none');

                        return 0;

                    }



                }



                if (getServiceName == "PTP") {



                    var getServiceName = $('#services').val();

                    var getPickLocation = $('#pick_point').val();

                    var drop_point = $('#drop_point').val();

                    var selected_date = $('#selected_date').val();

                    var selected_time = $('#selected_time').val();

                    var total_passenger = $('#total_passenger').val();

                    var luggage_quantity = $('#luggage_quantity').val();



                    if(stopAddtress == undefined){
                        if(localStorage.getItem('stops_for_mini_services') !== undefined){
                            stopAddtress = localStorage.getItem('stops_for_mini_services')
                        }
                    }




                    var getJson = {

                        "action": "getPointToPointRate",

                        "pickuplocation": getPickLocation,

                        "dropoff_location": drop_point,

                        "pickup_date": selected_date,

                        "pickup_time": selected_time,

                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,

                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,

                        "user_id": getVehicleInfo[0].user_id,

                        "luggage_quantity": luggage_quantity,

                        "total_passenger": total_passenger,

                        "serviceType": getServiceName,

                        "stopAddtress": stopAddtress,

                        "isLocationPut": isLocationPut,

                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,

                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,

                        "interNationFlightChecked": interNationFlightChecked,

                        "current_date": currentDate,

                        "passenger_id": passenger_id,

                        "totalLuggage": totalLuggage

                    };

                    localStorage.setItem('limo_editMode' , '0');
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));



                    if(is_mini_page !== null){
                        notifyParentWindow(getJson);
                    } else {
                        window.location.href = "select-vehicle.php";
                    }




                } else if (getServiceName == "PPS") {

                    var getServiceName = $('#services').val();

                    var getPickLocation = $('#perpassenger_pickup_location').val();

                    var drop_point = $('#perpassenger_dropof_location').val();

                    var selected_date = $('#selected_date').val();

                    var selected_time = $('#selected_time').val();

                    var total_passenger = $('#total_passenger').val();

                    var luggage_quantity = $('#luggage_quantity').val();

                    if(stopAddtress == undefined){
                        if(localStorage.getItem('stops_for_mini_services') !== undefined){
                            stopAddtress = localStorage.getItem('stops_for_mini_services')
                        }
                    }



                    var getJson = {

                        "action": "getPerPassengerRate",

                        "pickuplocation": getPickLocation,

                        "dropoff_location": drop_point,

                        "pickup_date": selected_date,

                        "pickup_time": selected_time,

                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,

                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,

                        "user_id": getVehicleInfo[0].user_id,

                        "luggage_quantity": luggage_quantity,

                        "total_passenger": total_passenger,

                        "serviceType": getServiceName,

                        "stopAddtress": stopAddtress,

                        "isLocationPut": isLocationPut,

                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,

                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,

                        "interNationFlightChecked": interNationFlightChecked,

                        "current_date": currentDate,

                        "passenger_id": passenger_id,

                        "totalLuggage": totalLuggage

                    };

                    localStorage.setItem('limo_editMode' , '0');
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));

                    if(is_mini_page !== null){
                        notifyParentWindow(getJson);
                    } else {
                        window.location.href = "select-vehicle.php";
                    }



                } else if (getServiceName == "CH") {

                    var getServiceName = $('#services').val();

                    var getPickLocation = $('#pick_charter').val();

                    var drop_point = $('#drop_charter').val();

                    var selected_date = $('#selected_date').val();

                    var selected_time = $('#selected_time').val();

                    var total_passenger = $('#total_passenger').val();

                    var luggage_quantity = $('#luggage_quantity').val();

                    var slect_hour = $('#slect_hour').val();

                    if(stopAddtress == undefined){
                        if(localStorage.getItem('stops_for_mini_services') !== undefined){
                            stopAddtress = localStorage.getItem('stops_for_mini_services')
                        }
                    }


                    var getJson = {

                        "action": "getHourlyRate",

                        "pickuplocation": getPickLocation,

                        "dropoff_location": drop_point,

                        "pickup_date": selected_date,

                        "pickup_time": selected_time,

                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,

                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,

                        "user_id": getVehicleInfo[0].user_id,

                        "luggage_quantity": luggage_quantity,

                        "total_passenger": total_passenger,

                        "serviceType": getServiceName,

                        "jurney_hour": slect_hour,

                        "stopAddtress": stopAddtress,

                        "isLocationPut": isLocationPut,

                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,

                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,

                        "interNationFlightChecked": interNationFlightChecked,

                        "current_date": currentDate,

                        "passenger_id": passenger_id,

                        "totalLuggage": totalLuggage

                    }






                    localStorage.setItem('limo_editMode' , '0');
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));



                    if(is_mini_page !== null){
                        notifyParentWindow(getJson);
                    } else {
                        window.location.href = "select-vehicle.php";
                    }







                } else if (getServiceName == "HRLY") {

                    var getServiceName = $('#services').val();

                    var getPickLocation = $('#pick_hourly').val();

                    var drop_point = $('#drop_hourly').val();

                    var selected_date = $('#selected_date').val();

                    var selected_time = $('#selected_time').val();

                    var total_passenger = $('#total_passenger').val();

                    var luggage_quantity = $('#luggage_quantity').val();

                    var slect_hour = $('#slect_hour').val();

                    if(stopAddtress == undefined){
                        if(localStorage.getItem('stops_for_mini_services') !== undefined){
                            stopAddtress = localStorage.getItem('stops_for_mini_services')
                        }
                    }


                    var getJson = {

                        "action": "getHourlyRate",

                        "pickuplocation": getPickLocation,

                        "dropoff_location": drop_point,

                        "pickup_date": selected_date,

                        "pickup_time": selected_time,

                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,

                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,

                        "user_id": getVehicleInfo[0].user_id,

                        "luggage_quantity": luggage_quantity,

                        "total_passenger": total_passenger,

                        "serviceType": getServiceName,

                        "jurney_hour": slect_hour,

                        "stopAddtress": stopAddtress,

                        "isLocationPut": isLocationPut,

                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,

                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,

                        "interNationFlightChecked": interNationFlightChecked,

                        "current_date": currentDate,

                        "passenger_id": passenger_id,

                        "totalLuggage": totalLuggage

                    }






                    localStorage.setItem('limo_editMode' , '0');
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));



                    if(is_mini_page !== null){
                        notifyParentWindow(getJson);
                    } else {
                        window.location.href = "select-vehicle.php";
                    }



                } else if (getServiceName == "AIRA") {








                    var getServiceName = $('#services').val();

                    var getPickLocation = $('#from_airport_pickloc').val();

                    var drop_point = $('#from_airport_droploc').val();

                    var selected_date = $('#selected_date').val();

                    var selected_time = $('#selected_time').val();

                    var total_passenger = $('#total_passenger').val();

                    var luggage_quantity = $('#luggage_quantity').val();

                    var slect_hour = $('#slect_hour').val();

                    if(stopAddtress == undefined){
                        if(localStorage.getItem('stops_for_mini_services') !== undefined){
                            stopAddtress = localStorage.getItem('stops_for_mini_services')
                        }
                    }

                    var getJson = {

                        "action": "getFromAirportRate",

                        "pickuplocation": getPickLocation,

                        "dropoff_location": drop_point,

                        "pickup_date": selected_date,

                        "pickup_time": selected_time,

                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,

                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,

                        "user_id": getVehicleInfo[0].user_id,

                        "luggage_quantity": luggage_quantity,

                        "total_passenger": total_passenger,

                        "serviceType": getServiceName,

                        "jurney_hour": slect_hour,

                        "stopAddtress": stopAddtress,

                        "isLocationPut": isLocationPut,

                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,

                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,

                        "interNationFlightChecked": interNationFlightChecked,

                        "current_date": currentDate,

                        "passenger_id": passenger_id,

                        "totalLuggage": totalLuggage

                    }




                    localStorage.setItem('limo_editMode' , '0');
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));



                    if(is_mini_page !== null){
                        notifyParentWindow(getJson);
                    } else {
                        window.location.href = "select-vehicle.php";
                    }







                } else if (getServiceName == "SEAA") {







                    var getServiceName = $('#services').val();

                    var getPickLocation = $('#pick_from_seaport').val();

                    var drop_point = $('#drop_from_seaport').val();

                    var selected_date = $('#selected_date').val();

                    var selected_time = $('#selected_time').val();

                    var total_passenger = $('#total_passenger').val();

                    var luggage_quantity = $('#luggage_quantity').val();

                    var slect_hour = $('#slect_hour').val();

                    if(stopAddtress == undefined){
                        if(localStorage.getItem('stops_for_mini_services') !== undefined){
                            stopAddtress = localStorage.getItem('stops_for_mini_services')
                        }
                    }


                    var getJson = {

                        "action": "getFromSeaportRate",

                        "pickuplocation": getPickLocation,

                        "dropoff_location": drop_point,

                        "pickup_date": selected_date,

                        "pickup_time": selected_time,

                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,

                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,

                        "user_id": getVehicleInfo[0].user_id,

                        "luggage_quantity": luggage_quantity,

                        "total_passenger": total_passenger,

                        "serviceType": getServiceName,

                        "jurney_hour": slect_hour,

                        "stopAddtress": stopAddtress,

                        "isLocationPut": isLocationPut,

                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,

                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,

                        "interNationFlightChecked": interNationFlightChecked,

                        "current_date": currentDate,

                        "passenger_id": passenger_id,

                        "totalLuggage": totalLuggage

                    }


                    localStorage.setItem('limo_editMode' , '0');
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));

                    if(is_mini_page !== null){
                        notifyParentWindow(getJson);
                    } else {
                        window.location.href = "select-vehicle.php";
                    }



                } else if (getServiceName == "AIRD") {







                    var getServiceName = $('#services').val();

                    var getPickLocation = $('#to_airport_pickloc').val();

                    var drop_point = $('#to_airport_droploc').val();

                    var selected_date = $('#selected_date').val();

                    var selected_time = $('#selected_time').val();

                    var total_passenger = $('#total_passenger').val();

                    var luggage_quantity = $('#luggage_quantity').val();

                    var slect_hour = $('#slect_hour').val();

                    if(stopAddtress == undefined){
                        if(localStorage.getItem('stops_for_mini_services') !== undefined){
                            stopAddtress = localStorage.getItem('stops_for_mini_services')
                        }
                    }


                    var getJson = {

                        "action": "getToAirportRate",

                        "pickuplocation": getPickLocation,

                        "dropoff_location": drop_point,

                        "pickup_date": selected_date,

                        "pickup_time": selected_time,

                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,

                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,

                        "user_id": getVehicleInfo[0].user_id,

                        "luggage_quantity": luggage_quantity,

                        "total_passenger": total_passenger,

                        "serviceType": getServiceName,

                        "jurney_hour": slect_hour,

                        "stopAddtress": stopAddtress,

                        "isLocationPut": isLocationPut,

                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,

                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,

                        "interNationFlightChecked": interNationFlightChecked,

                        "current_date": currentDate,

                        "passenger_id": passenger_id,

                        "totalLuggage": totalLuggage

                    }








                    localStorage.setItem('limo_editMode' , '0');
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));



                    if(is_mini_page !== null){
                        notifyParentWindow(getJson);
                    } else {
                        window.location.href = "select-vehicle.php";
                    }




                } else if (getServiceName == "SEAD") {







                    var getServiceName = $('#services').val();

                    var getPickLocation = $('#pick_to_seaport').val();

                    var drop_point = $('#drop_to_seaport').val();

                    var selected_date = $('#selected_date').val();

                    var selected_time = $('#selected_time').val();

                    var total_passenger = $('#total_passenger').val();

                    var luggage_quantity = $('#luggage_quantity').val();

                    var slect_hour = $('#slect_hour').val();


                    if(stopAddtress == undefined){
                        if(localStorage.getItem('stops_for_mini_services') !== undefined){
                            stopAddtress = localStorage.getItem('stops_for_mini_services')
                        }
                    }


                    var getJson = {

                        "action": "getToSeaportRate",

                        "pickuplocation": getPickLocation,

                        "dropoff_location": drop_point,

                        "pickup_date": selected_date,

                        "pickup_time": selected_time,

                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,

                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,

                        "user_id": getVehicleInfo[0].user_id,

                        "luggage_quantity": luggage_quantity,

                        "total_passenger": total_passenger,

                        "serviceType": getServiceName,

                        "jurney_hour": slect_hour,

                        "stopAddtress": stopAddtress,

                        "isLocationPut": isLocationPut,

                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,

                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,

                        "interNationFlightChecked": interNationFlightChecked,

                        "current_date": currentDate,

                        "passenger_id": passenger_id,

                        "totalLuggage": totalLuggage

                    }




                    localStorage.setItem('limo_editMode' , '0');
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));



                    if(is_mini_page !== null){
                        notifyParentWindow(getJson);
                    } else {
                        window.location.href = "select-vehicle.php";
                    }





                } else if (getServiceName == "FTS") {



                    var getServiceName = $('#services').val();

                    var getPickLocation = $('#from_train_pickup_location_input').val();

                    var drop_point = $('#from_train_dropof_location_input').val();

                    var selected_date = $('#selected_date').val();

                    var selected_time = $('#selected_time').val();

                    var total_passenger = $('#total_passenger').val();

                    var luggage_quantity = $('#luggage_quantity').val();

                    var slect_hour = $('#slect_hour').val();

                    if(stopAddtress == undefined){
                        if(localStorage.getItem('stops_for_mini_services') !== undefined){
                            stopAddtress = localStorage.getItem('stops_for_mini_services')
                        }
                    }


                    var getJson = {

                        "action": "getFromTrainRate",

                        "pickuplocation": getPickLocation,

                        "dropoff_location": drop_point,

                        "pickup_date": selected_date,

                        "pickup_time": selected_time,

                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,

                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,

                        "user_id": getVehicleInfo[0].user_id,

                        "luggage_quantity": luggage_quantity,

                        "total_passenger": total_passenger,

                        "serviceType": getServiceName,

                        "jurney_hour": slect_hour,

                        "stopAddtress": stopAddtress,

                        "isLocationPut": isLocationPut,

                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,

                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,

                        "interNationFlightChecked": interNationFlightChecked,

                        "current_date": currentDate,

                        "passenger_id": passenger_id,

                        "totalLuggage": totalLuggage

                    }




                    localStorage.setItem('limo_editMode' , '0');
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));



                    if(is_mini_page !== null){
                        notifyParentWindow(getJson);
                    } else {
                        window.location.href = "select-vehicle.php";
                    }



                } else if (getServiceName == "TTS") {



                    var getServiceName = $('#services').val();

                    var getPickLocation = $('#to_train_pickup_location_input').val();

                    var drop_point = $('#to_train_dropof_location_input').val();

                    var selected_date = $('#selected_date').val();

                    var selected_time = $('#selected_time').val();

                    var total_passenger = $('#total_passenger').val();

                    var luggage_quantity = $('#luggage_quantity').val();

                    var slect_hour = $('#slect_hour').val();

                    if(stopAddtress == undefined){
                        if(localStorage.getItem('stops_for_mini_services') !== undefined){
                            stopAddtress = localStorage.getItem('stops_for_mini_services')
                        }
                    }

                    var getJson = {

                        "action": "getToTrainRate",

                        "pickuplocation": getPickLocation,

                        "dropoff_location": drop_point,

                        "pickup_date": selected_date,

                        "pickup_time": selected_time,

                        "limo_any_where_api_key": getVehicleInfo[0].limo_any_where_api_key,

                        "limo_any_where_api_id": getVehicleInfo[0].limo_any_where_api_id,

                        "user_id": getVehicleInfo[0].user_id,

                        "luggage_quantity": luggage_quantity,

                        "total_passenger": total_passenger,

                        "serviceType": getServiceName,

                        "jurney_hour": slect_hour,

                        "stopAddtress": stopAddtress,

                        "isLocationPut": isLocationPut,

                        "ismeetGreetUpdateChecked": ismeetGreetUpdateChecked,

                        "iscurbsideUpdateChecked": iscurbsideUpdateChecked,

                        "interNationFlightChecked": interNationFlightChecked,

                        "current_date": currentDate,

                        "passenger_id": passenger_id,

                        "totalLuggage": totalLuggage

                    }




                    localStorage.setItem('limo_editMode' , '0');
                    window.localStorage.setItem("rateSetInformation", JSON.stringify(getJson));



                    if(is_mini_page !== null){
                        notifyParentWindow(getJson);
                    } else {
                        window.location.href = "select-vehicle.php";
                    }


                }


            }

            //$('#refresh_overlay').css('display','none');



        });



    }

}


function GetHolidaySurcharge(){
    var pick_location = '';
    var servicesType = $('#services').val();
    if(servicesType == 'AIRA'){ pick_location = $('#from_airport_pickloc').val(); }
    if(servicesType == 'AIRD'){ pick_location = $('#to_airport_pickloc').val(); }
    if(servicesType == 'PTP'){  pick_location = $('#pick_point').val(); }
    if(servicesType == 'SEAA'){ pick_location = $('#pick_from_seaport').val(); }
    if(servicesType == 'SEAD'){ pick_location = $('#pick_to_seaport').val(); }
    if(servicesType == 'HRLY'){ pick_location = $('#pick_hourly').val(); }
    if(servicesType == 'PPS'){  pick_location = $('#perpassenger_pickup_location').val(); }
    if(servicesType == 'FTS'){  pick_location = $('#from_train_pickup_location_input').val(); }
    if(servicesType == 'TTS'){  pick_location = $('#to_train_pickup_location_input').val(); }
    var getDate = $('#selected_date').val();
    var getTime = $('#selected_time').val();



    if((new Date() > new Date(getDate+' '+getTime))){

        $('#selected_time').val('');

        alert("Please select a valid date. You can't choose past date & time.");

    }
    else {

        if ($.trim(servicesType) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {

            /* comman file function call here */

            registrationPageClass.checkCutoffTime(getDate, getTime, servicesType);

            //CutoffTimeMain

            $.ajax({

                url : _SERVICEPATHServer,

                type: 'post',

                data : {

                    action: 'getHolidaySurcharge',
                    pick_location: pick_location,
                    servicesType: servicesType,
                    pick_date: getDate,

                    pick_time: getTime

                },

                success : function (response) {

                    var res = JSON.parse(response);

                    if(res[0] !== undefined){

                        console.log(res);
                        setTimeout(function () {
                            $('#CutoffTimeMain').show();
                            $('#CutoffTimeMain').html(res[0].blackout_Msg);
                        }, 1000);
                        $('#selected_time').val('');
                        if(servicesType == 'AIRA'){ pick_location = $('#from_airport_pickloc').val(''); }
                        if(servicesType == 'AIRD'){ pick_location = $('#to_airport_pickloc').val(''); }
                        if(servicesType == 'PTP'){  pick_location = $('#pick_point').val(''); }
                        if(servicesType == 'SEAA'){ pick_location = $('#pick_from_seaport').val(''); }
                        if(servicesType == 'SEAD'){ pick_location = $('#pick_to_seaport').val(''); }
                        if(servicesType == 'HRLY'){ pick_location = $('#pick_hourly').val(''); }
                        if(servicesType == 'PPS'){  pick_location = $('#perpassenger_pickup_location').val(''); }
                        if(servicesType == 'FTS'){  pick_location = $('#from_train_pickup_location_input').val(''); }
                        if(servicesType == 'TTS'){  pick_location = $('#to_train_pickup_location_input').val(''); }

                    }

                }

            });

        }

    }
}
// $("#selected_time").blur(function() {
//     GetHolidaySurcharge();
// });
$("#from_airport_pickloc").change(function() { GetHolidaySurcharge(); });
$("#to_airport_pickloc").change(function() { GetHolidaySurcharge(); });
$("#pick_point").change(function() { GetHolidaySurcharge(); });
$("#pick_from_seaport").change(function() { GetHolidaySurcharge(); });
$("#pick_to_seaport").change(function() { GetHolidaySurcharge(); });
$("#pick_hourly").change(function() { GetHolidaySurcharge(); });
$("#perpassenger_pickup_location").change(function() { GetHolidaySurcharge(); });
$("#from_train_pickup_location_input").change(function() { GetHolidaySurcharge(); });
$("#to_train_pickup_location_input").change(function() { GetHolidaySurcharge(); });





$('#selected_date,#services').on("change", function() {
    var servicesType = $('#services').val();
    var getDate = $('#selected_date').val();
    var getTime = $('#selected_time').val();
    if((new Date() > new Date(getDate+' '+getTime))){

        if(getTime != ''){

            $('#selected_time').val('');

            alert("Please select a valid date. You can't choose past date & time.");

        }

    } else {

        if ($.trim(servicesType) != '' && $.trim(getDate) != '' && $.trim(getTime) != '') {

            /* comman file function call here */

            registrationPageClass.checkCutoffTime(getDate, getTime, servicesType);

            $.ajax({

                url : _SERVICEPATHServer,

                type: 'post',

                data : {

                    action: 'getHolidaySurcharge',

                    pick_date: getDate,

                    pick_time: getTime

                },

                success : function (response) {

                    var res = JSON.parse(response);

                    console.log(res);

                    if(res !== 0){

                        $('#CutoffTimeMain').css("display", "block").html(res[0].blackout_Msg);

                        $('#selected_time').val('');

                    }

                }

            });

        }

    }
});


function PPShuttle(){
    console.log('being called for per passenger settings');
    var servicesType = $('#services').val();
    var pickup_location = $('#perpassenger_pickup_location').val();
    var dropoff_location = $('#perpassenger_dropof_location').val();
    var getDate = $('#selected_date').val();
    var days = (new Date(getDate)).getDay();
    days = days === 0 ? 7 : days;
    var target = $('.timePickerR');
    if(servicesType === 'PPS'){
        if(pickup_location !== '' && getDate !== '' && dropoff_location !== ''){
            var _SERVICEPATHPass = location.href.replace(is_mini_page == null ? 'frontend/services.php' : document.getElementById('mini_service').value , 'SMA/phpfile/passenger_client.php');
            $.ajax({
                url: _SERVICEPATHPass,
                type: 'POST',
                data: {action: 'getPPTimeList',days:days,pickup_location:pickup_location,dropoff_location:dropoff_location},
                success: function (response) {
                    var res = JSON.parse(response);
                    if(res.code === 1006){
                        $('#PPS-time-picker').html('');
                        alert('No Pickup time available. Please try another day');


                            window.localStorage.setItem("Extra_Max_Luggage_allowed", "{}");
                            window.localStorage.setItem("ExtraLuggageSetup", "{}");
                            window.localStorage.setItem("LuggageDisclaimer", "");
                            window.localStorage.setItem("LuggageActive", "0");

                    } else {
                        var options = '';
                        var select = `<label class="lblfont">Pickup Time </label>
                                        <div class="form-group">
                                            <div class='input-group date' id="datetimepicker2">
                                                <select class="form-control" name="selected_time" id="selected_time">`;

                        $.each(res.data, function (i, vv) {
                            $.each(vv.tm, function (j, v) {
                                select += `<option luggage="`+vv.luggage_settings_id+`" value="`+v.tm+`">`+v.tm+`</option>`;
                                options += `<option luggage="`+vv.luggage_settings_id+`" value="`+v.tm+`">`+v.tm+`</option>`;
                            });
                        });

                        select += `</select>
                                                <span class="input-group-addon inputbg " style="display: none" id="icon_time"> <span class="glyphicon glyphicon-time datepickerbutton"></span> </span>
                                            </div>
                                        </div>`;
                        if(options === ''){
                            $('#PPS-time-picker').html('');
                            alert('No Pickup time available. Please try another day');
                        } else{
                            $('#PPS-time-picker').html(select);

                            setTimeout(function () {
                                $('#selected_time').trigger('change');
                                show_disclaimer_on_reload();
                            }, 200)


                        }
                    }
                }
            });
        } else {
            target.html('');
            if(pickup_location === '' && getDate === '' && dropoff_location === ''){
                alert('Please insert Pickup date and location correctly.')
            }
        }
    }
    else {
        var input = null;
        //if(!is_mini_page){
            if(true){
             input = `<label class="lblfont">Pickup Time </label>
                        <div class="form-group">
                            <div class='input-group date' id="datetimepicker2">
                                <input type='text' id="selected_time" required name="selected_time"
                                       class="form-control inputbg datepickerbutton" style="border-top-right-radius:0px;
                                   border-bottom-right-radius: 0px; width:100%;"/>
                                <span class="input-group-addon inputbg " id="icon_time"> <span
                                            class="glyphicon glyphicon-time datepickerbutton"></span> </span>
                            </div>
                        </div>`;
        }

        /*else {
             input = `<div class='input-group date' id="datetimepicker2">
                                <input type='text' id="selected_time" required name="selected_time"
                                       class="form-control inputbg datepickerbutton" style="border-top-right-radius:0px;
                                   border-bottom-right-radius: 0px; width:100%;"/>
                                <span class="input-group-addon inputbg " id="icon_time"> <span
                                            class="glyphicon glyphicon-time datepickerbutton"></span> </span>
                            </div>
                        `;
        }*/
        target.html(input);
        $('#selected_time').datetimepickerNew({
            pickDate : false,
            pickTime : true

        });
    }
}

window.removeReferrence = function(obj){

    return JSON.parse(JSON.stringify(obj));
}

function remove_luggage_settings(){
    window.localStorage.setItem("Extra_Max_Luggage_allowed", "{}");
    window.localStorage.setItem("ExtraLuggageSetup", "{}");
    window.localStorage.setItem("LuggageDisclaimer",  "");
    window.localStorage.setItem("LuggageActive", "0");
}



$(document).on('change' , '#selected_time' ,function(){
    var selected_luggage_settings_id = $('#selected_time option:selected').attr('luggage');
    if(selected_luggage_settings_id !== undefined || selected_luggage_settings_id !== null){
        //console.log(existing_luggage_settings);
        //console.log('luggae settings id : ' , selected_luggage_settings_id);


        if(window.existing_luggage_settings['matrix-' + selected_luggage_settings_id] !== undefined){
            //console.log('apply luggae filter :' , window.existing_luggage_settings['matrix-' + selected_luggage_settings_id]);
            window.localStorage.setItem("Extra_Max_Luggage_allowed", window.existing_luggage_settings['matrix-' + selected_luggage_settings_id].max_allowed);
            window.localStorage.setItem("ExtraLuggageSetup", window.existing_luggage_settings['matrix-' + selected_luggage_settings_id].extra_price);
            window.localStorage.setItem("LuggageDisclaimer", window.existing_luggage_settings['matrix-' + selected_luggage_settings_id].disclaimer);
            window.localStorage.setItem("LuggageActive", String(window.existing_luggage_settings['matrix-' + selected_luggage_settings_id].active));
        } else {
            remove_luggage_settings();
        }
    } else {
        remove_luggage_settings();
    }
})

var existing_luggage_settings = {};

function getLuggageSettings() {


    $.ajax({

        url: location.href.replace(is_mini_page == null ? 'frontend/services.php' : document.getElementById('mini_service').value , 'SMA/phpfile/passenger_client.php'),

        type: 'POST',

        data: "action=getLuggageSettings",

        success: function (response) {
            var parsed_data =  JSON.parse(response);
            if(parsed_data.data.length > 0)
                parsed_data.data.forEach(function (item) {
                    window.existing_luggage_settings['matrix-' + item.id] = removeReferrence(item);
                })


        },

        error: function () {

            alert("Some Error");

        }

    });


}

getLuggageSettings();


$('#services').on("change", function() {
    $('#PPS-time-picker').html('');
    var servicesType = $('#services').val();
    var target = $('.timePickerR');
    if(servicesType === 'PPS'){
        target.html('');
    } else {
        remove_luggage_settings();
        var input = null;
        //if(is_mini_page === null){
            if(true){
            input = `<label class="lblfont">Pickup Time </label>
                        <div class="form-group">
                            <div class='input-group date' id="datetimepicker2">
                                <input type='text' id="selected_time" required name="selected_time"
                                       class="form-control inputbg datepickerbutton" style="border-top-right-radius:0px;
                                   border-bottom-right-radius: 0px; width:100%;"/>
                                <span class="input-group-addon inputbg " id="icon_time"> <span
                                            class="glyphicon glyphicon-time datepickerbutton"></span> </span>
                            </div>
                        </div>`;
        }
        /*else {
            input = `<div class='input-group date' id="datetimepicker2">
                                <input type='text' id="selected_time" required name="selected_time"
                                       class="form-control inputbg datepickerbutton" style="border-top-right-radius:0px;
                                   border-bottom-right-radius: 0px; width:100%;"/>
                                <span class="input-group-addon inputbg " id="icon_time"> <span
                                            class="glyphicon glyphicon-time datepickerbutton"></span> </span>
                            </div>
                        `;
        }*/

        target.html(input);
        $('#selected_time').datetimepickerNew({
            pickDate : false,
            pickTime : true

        });
    }
});
$('#selected_date').on("change", function() { PPShuttle(); });
$('#perpassenger_pickup_location').on("change", function() {
    PPShuttle();
    var v = $('#perpassenger_pickup_location').val();
    $('#perpassenger_dropof_location').find('option').removeAttr("disabled");
    $('#perpassenger_dropof_location').find('option[value="'+v+'"]').attr("disabled", "disabled");
});
$('#perpassenger_dropof_location').on("change", function() {
    PPShuttle();
});

//selected_date


registrationPageClass.passengerLogin();
function changeLugSelector(trigger){

    var trigger = $(trigger);

    var targets = trigger.closest('.section-border').find('input[type="text"]');

    var btns = trigger.closest('.section-border').find('button');

    var v = trigger.prop('checked');



    if(v === true){
        remove_luggage_settings();
        document.getElementById('luggage_quantity_small').value = 0;
        document.getElementById('luggage_quantity_medium').value = 0;
        document.getElementById('luggage_quantity_large').value = 0;

        targets.prop("disabled", true);

        btns.prop("disabled", true);

    } else {

        if(document.getElementById('services').value === 'PPS'){
            $('#selected_time').trigger('change');
            show_disclaimer_on_reload();
        }


        targets.prop("disabled", false);

        btns.prop("disabled", false);

    }

}