function back_to_first_step() {

    $('#final_step').hide(500);
    $('#first_step').show(500);
}
function go_to_final_step() {
    $('#first_step').hide(500);
    $('#final_step').show(500);
}

function add_error_highlghter(selector) {
    $(selector).addClass('input-error')
}
function remove_error_highlghter(selector) {
    $(selector).removeClass('input-error')
}

$(document).ready(function () {
    $('input').focus(function () {
        $(this).removeClass('input-error');
    })

    $('#final_step').hide();


    $('#complete_step_1').on("click", function(event) {

        event.preventDefault();


        if(document.getElementById("selected_date").value === ''){
            add_error_highlghter('#selected_date')
            return 0;
        }


        if(document.getElementById("selected_time").value === ''){
            add_error_highlghter('#selected_time')
            return 0;
        } else {
            remove_error_highlghter('#selected_time')
        }

        $('#refresh_overlay').css('display', 'block');




            var getServiceName = $('#services').val();




            if (getServiceName == "PTP") {

                if(document.getElementById("pick_point").value === ''){
                    add_error_highlghter('#pick_point')
                    return 0;
                }
                if(document.getElementById("drop_point").value === ''){
                    add_error_highlghter('#drop_point')
                    return 0;
                }

                go_to_final_step();



            } else if (getServiceName == "PPS") {

                if(document.getElementById("perpassenger_pickup_location").value === ''){
                    add_error_highlghter('#perpassenger_pickup_location')
                    return 0;
                }
                if(document.getElementById("perpassenger_dropof_location").value === ''){
                    add_error_highlghter('#perpassenger_dropof_location')
                    return 0;
                }

                go_to_final_step();



            } else if (getServiceName == "CH") {

                go_to_final_step();

            } else if (getServiceName == "HRLY") {

                if(document.getElementById("pick_hourly").value === ''){
                    add_error_highlghter('#pick_hourly')
                    return 0;
                }
                if(document.getElementById("drop_hourly").value === ''){
                    add_error_highlghter('#drop_hourly')
                    return 0;
                }

                go_to_final_step();

            } else if (getServiceName == "AIRA") {

                if(document.getElementById("from_airport_pickloc").value === ''){
                    add_error_highlghter('#from_airport_pickloc')
                    return 0;
                }
                if(document.getElementById("from_airport_droploc").value === ''){
                    add_error_highlghter('#from_airport_droploc')
                    return 0;
                }

                go_to_final_step();

            } else if (getServiceName == "SEAA") {

                if(document.getElementById("pick_from_seaport").value === ''){
                    add_error_highlghter('#pick_from_seaport')
                    return 0;
                }
                if(document.getElementById("drop_from_seaport").value === ''){
                    add_error_highlghter('#drop_from_seaport')
                    return 0;
                }

                go_to_final_step();


            } else if (getServiceName == "AIRD") {

                if(document.getElementById("to_airport_pickloc").value === ''){
                    add_error_highlghter('#to_airport_pickloc')
                    return 0;
                }
                if(document.getElementById("to_airport_droploc").value === ''){
                    add_error_highlghter('#to_airport_droploc')
                    return 0;
                }

                go_to_final_step();
            } else if (getServiceName == "SEAD") {

                if(document.getElementById("pick_to_seaport").value === ''){
                    add_error_highlghter('#pick_to_seaport')
                    return 0;
                }
                if(document.getElementById("drop_to_seaport").value === ''){
                    add_error_highlghter('#drop_to_seaport')
                    return 0;
                }

                go_to_final_step();


            } else if (getServiceName == "FTS") {

                if(document.getElementById("from_train_pickup_location_input").value === ''){
                    add_error_highlghter('#from_train_pickup_location_input')
                    return 0;
                }
                if(document.getElementById("from_train_dropof_location_input").value === ''){
                    add_error_highlghter('#from_train_dropof_location_input')
                    return 0;
                }

                go_to_final_step();
            } else if (getServiceName == "TTS") {

                if(document.getElementById("to_train_pickup_location_input").value === ''){
                    add_error_highlghter('#to_train_pickup_location_input')
                    return 0;
                }
                if(document.getElementById("to_train_dropof_location_input").value === ''){
                    add_error_highlghter('#to_train_dropof_location_input')
                    return 0;
                }
                go_to_final_step();
            }





    });
})