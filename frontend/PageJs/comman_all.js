var getLoadPage = {

    _Serverpath: _SERVICEPATHCLIENT,

    passengerInformation: function () {
        var getLocalStorageValue = window.localStorage.getItem("localUserInfo");

        // alert(12);
        if (typeof (getLocalStorageValue) == "string")
        {
            if (getLocalStorageValue != "guestinfo")
            {
                getLocalStorageValue = JSON.parse(getLocalStorageValue);
            }



        } else
        {
            //window.location.href = "./";

        }
        // console.log(getLocalStorageValue);

        $('#logoutUserVehicle').on("click", function () {

            // window.localStorage.remove("localUserInfo");
            localStorage.removeItem('localUserInfo');
            localStorage.removeItem('limo_editMode');
            localStorage.removeItem('rateSetInformation');
            localStorage.removeItem('name_combination');
            window.location.href = "./";

        });
        if (getLocalStorageValue == "guestinfo")
        {

            $('#userNameCommand').html(" Guest");

        } else
        {

            // alert(getLocalStorageValue[0].f_name);
            $('#userNameCommand').html(" " + getLocalStorageValue[0].f_name);
        }



    }




}

function getExtraLuggageCost_new(luggageJson , total_passenger){

    var total_small = Number(luggageJson.small);
    var total_medium = Number(luggageJson.medium);
    var total_large = Number(luggageJson.large);
   var used = 0 , can_be_used = 0;

    //console.log('active : ' , window.localStorage.getItem("LuggageActive"));
    if(window.localStorage.getItem("LuggageActive") !== "1")
        return { large : 0, medium : 0 , small : 0 };

    var max_allowed_list = JSON.parse(window.localStorage.getItem("Extra_Max_Luggage_allowed")) ,  per_people = [];

    for(var k = 0; k<= total_passenger-1 ; k++){

        per_people.push({
            large : 0, medium : 0 , small : 0
        });

        used = 0;

        if(total_large > 0 && total_large >= Number(max_allowed_list.large)){
            if(total_large >= Number(max_allowed_list.large)){
                total_large = total_large - Number(max_allowed_list.large);
                used += Number(max_allowed_list.large);
                per_people[k].large = max_allowed_list.large;
            } else {
                used += total_large;
                per_people[k].large = total_large;
                total_large = 0;
            }
        }



        if(used < 3){
            if(total_medium > 0 && total_medium >= Number(max_allowed_list.medium)){
                if(total_medium >= Number(max_allowed_list.medium)){
                    total_medium = total_medium - Number(max_allowed_list.medium);
                    used += Number(max_allowed_list.medium);
                    per_people[k].medium = max_allowed_list.medium;
                } else {
                    used += total_medium;
                    per_people[k].medium = total_medium;
                    total_medium = 0;
                }
            } else if(total_medium > 0 && total_medium < Number(max_allowed_list.medium)){
                used += total_medium;
                per_people[k].medium = total_medium;
                total_medium = 0;
            }
        }



        if(used < 3){
            if(total_small > 0 && total_small >= Number(max_allowed_list.small)){
                if(total_small >= Number(max_allowed_list.small)){

                    can_be_used = Number(max_allowed_list.small) - used <= 0 ? 1 : Number(max_allowed_list.small) - used;
                    //console.log('from first ' , can_be_used);
                    total_small = total_small - can_be_used;
                    used += Number(can_be_used);
                    per_people[k].small = can_be_used;
                } else {
                    //console.log('from here 2');
                    used += total_small;
                    per_people[k].small = total_small;
                    total_small = 0;
                }
            } else if(total_small > 0 && total_small < Number(max_allowed_list.small)){
                can_be_used = can_be_used = Number(max_allowed_list.small) - used < 0 ? 1 : Number(max_allowed_list.small) - used;;
                used += can_be_used;
                per_people[k].small = can_be_used;
                total_small = total_small - can_be_used;
            }
        }


    }


console.log({ large : total_large, medium : total_medium , small : total_small })
    return { large : total_large, medium : total_medium , small : total_small };
}

function getExtraLuggageCost(luggageJson , total_passenger){
    //console.log(luggageJson);

    var small = Number(luggageJson.small);
    var medium = Number(luggageJson.medium);
    var large = Number(luggageJson.large);
    var cost = { large : 0, medium : 0 , small : 0 }, large_saved = 0, medium_saved = 0;
    // large max allowed 1

    //console.log('s m l : ' , small , medium , large);

    var max_allowed_large =  1 * total_passenger;
    var max_allowed_medium = 1 * total_passenger;
    var max_allowed_small = 1 * total_passenger;

    if(large > max_allowed_large){
        cost['large'] = large - max_allowed_large
    }
    if(max_allowed_large > large){
        large_saved = max_allowed_large - large;
    }

    //console.log('large saved' ,  large_saved);
    if(large_saved > 0){
        max_allowed_medium = max_allowed_medium - large_saved +  (2 * large_saved);
    }

    //console.log('medium slot ' ,  max_allowed_medium);

    if(medium > max_allowed_medium){
        cost['medium'] = medium - max_allowed_medium
    }

    if(max_allowed_medium > medium){
        medium_saved = max_allowed_medium - medium;
    }

    //console.log('medium saved :' , medium_saved)

    if(medium_saved > 0){
        max_allowed_small = max_allowed_medium - medium_saved +  (2 * medium_saved);
    }

    //console.log('small slot : ' , max_allowed_small);

    if(small > max_allowed_small){
        cost['small'] = small - max_allowed_small
    }



    //console.log(cost);


    return cost;
}

getLoadPage.passengerInformation();