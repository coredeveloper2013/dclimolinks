
/*create sdit train vehicle page*/
var EditTrainVehicle={
  /*web service path using php file */  
	_Serverpath:"phpfile/addVehicle_client.php",
  /*---------------------------------------------
  Function Name: editTrainInformationShow()
  Input Parameter:rowId
  return:json data
---------------------------------------------*/
	editTrainInformationShow:function(rowId){
    $('#refresh_overlay').css("display","block");
		 var getForm={"action":"editTrainInformationShow","rowId":rowId};
     $.ajax({
           url:EditTrainVehicle._Serverpath,
           type:'POST',
           dataType:'json',
           data:getForm
       }).done(function(response){
    
       	$('#train_country_name').val(response.data[0].train_country);
       	$('#train_state_name').val(response.data[0].state_name);
       	$('#train_city_name').val(response.data[0].city_name);
       	$('#train_city_zipCode').val(response.data[0].city_code);
       	$('#train_name').val(response.data[0].train_master_id[0].train_name);
       	$('#train_code').val(response.data[0].train_master_id[0].train_code);
		    $('#train_selected_vehicle_rat_btn').attr("seq",response.data[0].id);
       	$('#showTraintData').show();
        $('#refresh_overlay').css("display","none");
    });
    $('#refresh_overlay').css("display","none");
	},
/*---------------------------------------------
  Function Name: deleteTrainInformation()
  Input Parameter:rowId
  return:json data
---------------------------------------------*/
  deleteTrainInformation:function(rowId){
    $('#refresh_overlay').css("display","block");
     var getForm={"action":"deleteTrainInformation","rowId":rowId};
       $.ajax({
           url:EditTrainVehicle._Serverpath,
           type:'POST',
           dataType:'json',
           data:getForm
       }).done(function(response){
          console.log(response);
          EditTrainVehicle.getTrainInformation();
       });
       $('#refresh_overlay').css("display","none");
  }, 
  
/*---------------------------------------------
  Function Name: getTrainInformation()
  Input Parameter:rowId
  return:json data
---------------------------------------------*/
  getTrainInformation:function(rowId)
    {
      $('#refresh_overlay').css("display","block");
        var getUserId=window.localStorage.getItem('companyInfo');
            getUserId=JSON.parse(getUserId);
        var getForm={"action":"getTrainVechileInformation","rowId":rowId,"user_id":getUserId[0].id};
         $.ajax({
             url:EditTrainVehicle._Serverpath,
             type:'POST',
             dataType:'json',
             data:getForm
         }).done(function(response){
    
        if(response.code==1003)
        {
          var getHTMl='';
          for(var i=0; i<response.data.length; i++)
          {
             getHTMl+='<tr><td>'+(i+1)+'</td><td>'+response.data[i].train_country+'</td><td>'+response.data[i].state_name+'</td> <td>'+response.data[i].city_name+'</td><td>'+response.data[i].city_code+'</td> <td>'+response.data[i].train_code[0].train_name+'</td> <td>'+response.data[i].train_code[0].train_code+'</td><td><button class="btn btn-primary editClass" style="margin-right: 5%;" seq="'+response.data[i].id+'">Edit</button><button class="btn btn-primary deleteClass" seq="'+response.data[i].id+'">Delete</button></td></tr>';
          
          }
   
        }else{

           getHTMl+='<tr><td colspan="4">Data not Found.</td></tr>'; 
        }
        $('#view_train_information_db_table').html(getHTMl);
        $('#refresh_overlay').css("display","none");
        $('.editClass').on("click",function(){
          var getSeq=$(this).attr("seq");
          EditTrainVehicle.editTrainInformationShow(getSeq);
         
        });
        $('.deleteClass').on("click",function(){
          var getAns=confirm("Are you sure.you want to delete item");
          if(getAns)
          {
            var getSeq=$(this).attr("seq");
            EditTrainVehicle.deleteTrainInformation(getSeq);
          }
        });

      });

    }
}
EditTrainVehicle.getTrainInformation();

$('#trainInformation').on("submit",function(event){
    event.preventDefault();
    $('#refresh_overlay').css("display","block");
    var getNewForm=new FormData($("#trainInformation")[0]);
    var getSeq=$('#train_selected_vehicle_rat_btn').attr("seq");
      getNewForm.append("rowId",getSeq);
      getNewForm.append("action","editClassTrain")
          $.ajax({
              url:EditTrainVehicle._Serverpath,
              type:'POST',
              dataType:'json',
              data:getNewForm,
              contentType: false,
              processData: false,
          }).done(function(response) {
                  alert('Successfully Updated');
                  EditTrainVehicle.getTrainInformation();
                  $('#trainInformation')[0].reset(); 
                  $('#showTraintData').hide();
             $('#refresh_overlay').css("display","none");
        }).fail(function(jqXHR, exception)
                {
 
                  $('#refresh_overlay').css("display","none");

          });
});
	