<?php
//The url you wish to send the POST request to
$url = 'https://api.linkedin.com/v2/me';

//The data you want to send via POST
$fields = $_REQUEST;

//url-ify the data for the POST
//$fields_string = http_build_query($fields);
$access_token = $fields['access_token'];

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Authorization: Bearer ".$access_token
));

//So that curl_exec returns the contents of the cURL; rather than echoing it
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

//execute post
$result['basic'] = curl_exec($ch);

$url = 'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))';

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Authorization: Bearer ".$access_token
));

//So that curl_exec returns the contents of the cURL; rather than echoing it
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

$result['email'] = curl_exec($ch);

echo json_encode($result);
